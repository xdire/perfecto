var app = require('electron').app;
var BrowserWindow = require('electron').BrowserWindow;

app.on('ready', ()=>{
    var mainWindow = new BrowserWindow({
        width: 1280,
        height: 720
        //frame: false
    })
    mainWindow.loadURL('file://' + __dirname + '/index.html');
});

app.on('window-all-closed', function() {
  if (process.platform != 'darwin')
    app.quit();
});