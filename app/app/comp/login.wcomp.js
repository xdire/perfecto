"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var window_component_1 = require("../system/window/window.component");
var app_1 = require("../system/app");
var apiconnector_component_1 = require("../system/connection/apiconnector.component");
var LoginWindowComponent = (function (_super) {
    __extends(LoginWindowComponent, _super);
    /** -----------------------------------------------------------------------------------------
   *
   *
   *    Construct login entity with
   *          Window paramaters
   *
   *
   *  -----------------------------------------------------------------------------------------
   *
   *
   */
    function LoginWindowComponent() {
        this.isLogin = true;
        this.isSignUp = false;
        this.isProcess = false;
        this.authCheckProcessStatus = "Welcome";
        this.email = "";
        this.userName = "";
        this.password = "";
        this.passwordConfirm = "";
        this.firstName = "";
        this.lastName = "";
        _super.prototype.constructor.call(this);
        this.isRoot = true;
        this.isWithMenu = false;
        this.isWithBar = false;
        this.isCacheable = false;
        this.publicName = "login";
    }
    LoginWindowComponent.prototype.onFocus = function () {
        var _this = this;
        this._avtAsProcess();
        setTimeout(function () {
            _this.authCheckProcessStatus = "Check authorization";
            _this.authorizeByKey();
        }, 500);
    };
    LoginWindowComponent.prototype.onExit = function () {
    };
    LoginWindowComponent.prototype.onCreate = function () {
    };
    /** -----------------------------------------------------------------------------------------
     *
     *
     *    Authorize User by Stored Key
     *
     *
     *  -----------------------------------------------------------------------------------------
     *
     */
    LoginWindowComponent.prototype.authorizeByKey = function () {
        var _this = this;
        var aapiconn = apiconnector_component_1.ApiConnectorComponent.getAuthApiConnector();
        var user = app_1.App.state.user;
        if (user.key != null) {
            aapiconn.checkUserAuthorization().subscribe(function (authInfo) {
                if (authInfo.user == user.id) {
                    _this.authCheckProcessStatus = "Logged";
                    _this.componentZone.run(function () { });
                    // Proceed to load windows
                    setTimeout(function () {
                        var last;
                        console.log("NOW WE WILL LOAD SOMETHING");
                        console.log(app_1.App.state.getLastLoadedComponent());
                        // Load last user opened component if it's exists in cache
                        if (last = app_1.App.state.getLastLoadedComponent())
                            app_1.App.window.loadWindowComponent(last);
                        else
                            app_1.App.window.loadWindowComponent("dashboardComponent");
                    }, 200);
                }
            }, function (error) {
                // Show some information about failed procedure
                _this.authCheckProcessStatus = "Login failed...";
                _this.refreshView();
                setTimeout(function () {
                    // Return to the form view
                    _this._actAsLogin();
                    _this.refreshView();
                }, 500);
            });
        }
        else {
            // Return to the form view
            this._actAsLogin();
            this.refreshView();
        }
    };
    /** -----------------------------------------------------------------------------------------
   *
   *    Submit User Information
   *    to API and Login user
   *    after success
   *
   *  -----------------------------------------------------------------------------------------
   *
   * @param $event
   *
   */
    LoginWindowComponent.prototype.onSubmitLogin = function ($event) {
        var _this = this;
        $event.preventDefault();
        var aapiconn = apiconnector_component_1.ApiConnectorComponent.getAuthApiConnector();
        this.authCheckProcessStatus = "Authorizing";
        this._avtAsProcess();
        aapiconn.authorizeUser({ email: this.userName, login: "", pass: this.password, agent: "dtp" })
            .subscribe(function (userInfo) {
            _this.authCheckProcessStatus = "Authorization done";
            var user = app_1.App.state.user;
            user.__clearCache();
            user.__keyInfoToCache(userInfo);
            app_1.App.state.__setAuthorized();
            _this.userInformationUpdate(userInfo.user, userInfo.key);
            /*if(user.login == null || user.email == null) {
    
              // If No Information about User then poll for it and continue
              this.userInformationUpdate(userInfo.user, userInfo.key);
    
            } else {
    
              // Load Dashboard Component After Standard Login
              App.window.loadWindowComponent("dashboardComponent");
    
            }*/
        }, function (error) {
            // Change notification
            _this.authCheckProcessStatus = "Login failed...";
            _this.refreshView();
            // Change back to login form
            setTimeout(function () {
                _this._actAsLogin();
                _this.refreshView();
            }, 1000);
        });
    };
    /** -----------------------------------------------------------------------------------------
   *
   *    Update User Information and
   *      Continue authorization
   *              process
   *
   *  -----------------------------------------------------------------------------------------
   *
   * @param user
   * @param key
   *
   */
    LoginWindowComponent.prototype.userInformationUpdate = function (user, key) {
        var aapiconn = apiconnector_component_1.ApiConnectorComponent.getAuthApiConnector();
        aapiconn.getUserInfo({ user: user, key: key }).subscribe(function (creds) {
            app_1.App.state.user.__fromUserEntity(creds);
            app_1.App.state.user.timestamp = Date.now();
            app_1.App.state.user.__toCache();
            app_1.App.window.loadWindowComponent("dashboardComponent");
        }, function (error) {
            var _this = this;
            // Change notification
            this.authCheckProcessStatus = "Credentials failed...";
            this.refreshView();
            // Change back to login form
            setTimeout(function () {
                _this._actAsLogin();
                _this.refreshView();
            }, 3000);
        });
    };
    /** -----------------------------------------------------------------------------------------
   *
   *       User Signup Submission and User Creation
   *
   *  -----------------------------------------------------------------------------------------
   * @param $event
   */
    LoginWindowComponent.prototype.onSubmitSignup = function ($event) {
        $event.preventDefault();
        console.log(this);
    };
    LoginWindowComponent.prototype.swapLoginSignup = function () {
        if (this.isLogin)
            this._actAsSignup();
        else
            this._actAsLogin();
    };
    LoginWindowComponent.prototype._actAsSignup = function () {
        this.isSignUp = true;
        this.isLogin = false;
        this.isProcess = false;
    };
    LoginWindowComponent.prototype._actAsLogin = function () {
        this.isSignUp = false;
        this.isLogin = true;
        this.isProcess = false;
    };
    LoginWindowComponent.prototype._avtAsProcess = function () {
        this.isSignUp = false;
        this.isLogin = false;
        this.isProcess = true;
    };
    LoginWindowComponent = __decorate([
        core_1.Component({
            selector: 'login',
            template: "\n    <div class=\"loginBgr\">\n    \n      <div class=\"loginBox\" [class.hiddenFrame]=\"!isLogin\">\n      \n        <h3 class=\"text-white\"> Login </h3>\n        \n        <form class=\"loginForm\">\n\n          <div class=\"row offset-top-s\">\n            <div class=\"col-md-12\">\n              <input id=\"user\" \n              name=\"username\" \n              type=\"text\" \n              class=\"form-control\" \n              placeholder=\"Username\" \n              [value]=\"userName\" \n              (input)=\"userName=$event.target.value\">\n            </div>\n          </div>\n          \n          <div class=\"row offset-top-s\">\n            <div class=\"col-md-12\">\n              <input id=\"pass\" \n              name=\"password\" \n              type=\"password\" \n              class=\"form-control\" \n              placeholder=\"Password\"\n              [value]=\"password\" \n              (input)=\"password=$event.target.value\">\n            </div>\n          </div> \n          \n          <div class=\"row offset-top-s\">\n            <div class=\"col-md-12\">\n              <button class=\"btn btn-primary\" (click)=\"onSubmitLogin($event)\"> Login </button>\n              <button class=\"btn btn-primary offset-top-s\" (click)=\"swapLoginSignup()\"> Sign up </button>\n            </div>\n          </div>\n          \n        </form>\n        \n      </div>\n      \n      <div class=\"signupBox\" [class.hiddenFrame]=\"!isSignUp\">\n      \n        <h3 class=\"text-white\"> Create Account </h3>\n        \n        <form class=\"signupForm\">\n          \n          <div class=\"row offset-top-s\">\n            <div class=\"col-md-12\">\n              <input id=\"email\" \n              name=\"email\" \n              type=\"text\" \n              class=\"form-control\" \n              placeholder=\"E-mail\" \n              [value]=\"email\" \n              (input)=\"email = $event.target.value\">\n            </div>\n          </div>\n          \n          <div class=\"row offset-top-s\">\n            <div class=\"col-md-12\">\n              <input id=\"user\" \n              name=\"username\" \n              type=\"text\" \n              class=\"form-control\" \n              placeholder=\"Username\" \n              [value]=\"userName\" \n              (input)=\"userName = $event.target.value\">\n            </div>\n          </div>\n          \n          <div class=\"row offset-top-s\">\n            <div class=\"col-md-12\">\n              <input id=\"pass\" \n              name=\"password\" \n              type=\"password\" \n              class=\"form-control\" \n              placeholder=\"Password\"\n              [value]=\"password\" \n              (input)=\"password = $event.target.value\">\n            </div>\n          </div> \n          \n          <div class=\"row offset-top-s\">\n            <div class=\"col-md-12\">\n              <input id=\"passconfirm\" \n              name=\"password-confirm\" \n              type=\"password\" \n              class=\"form-control\" \n              placeholder=\"Password Confirmation\"\n              [value]=\"passwordConfirm\" \n              (input)=\"passwordConfirm = $event.target.value\">\n            </div>\n          </div> \n          \n          <div class=\"row offset-top-s\">\n            <div class=\"col-md-12\">\n              <input id=\"firstname\" \n              name=\"firstname\" \n              type=\"text\" \n              class=\"form-control\" \n              placeholder=\"First Name\"\n              [value]=\"firstName\" \n              (input)=\"firstName = $event.target.value\">\n            </div>\n          </div> \n          \n          <div class=\"row offset-top-s\">\n            <div class=\"col-md-12\">\n              <input id=\"lastname\" \n              name=\"lastname\" \n              type=\"text\" \n              class=\"form-control\" \n              placeholder=\"Last Name\"\n              [value]=\"lastName\" \n              (input)=\"lastName = $event.target.value\">\n            </div>\n          </div> \n          \n          <div class=\"row offset-top-s\">\n            <div class=\"col-md-12\">\n              <button class=\"btn btn-primary\" (click)=\"onSubmitSignup($event)\"> Continue Sign Up </button>\n              <button class=\"btn btn-primary offset-top-s\" (click)=\"swapLoginSignup()\"> Return to Login </button>\n            </div>\n          </div>\n          \n        </form>\n        \n      </div>\n      \n      <div class=\"loginBox\" [class.hiddenFrame]=\"!isProcess\">\n        <div class=\"authHeader\"><h3> {{authCheckProcessStatus}}</h3></div> \n      </div>\n      \n    </div>\n  ",
            styles: ["\n\n    .loginBgr {\n      width:100%;\n      height:100%;\n      background: #3D7EAA; /* fallback for old browsers */\n      background: -webkit-linear-gradient(to left, #3D7EAA , #FFE47A); /* Chrome 10-25, Safari 5.1-6 */\n      background: linear-gradient(to left, #3D7EAA , #FFE47A); /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */\n    }\n    \n    .loginBox {\n      border: 1px solid whitesmoke;\n      border-radius: 8px;\n      position: absolute;\n      width:300px;\n      height: 300px;\n      top: 50%;\n      left: 50%;\n      margin: -150px 0 0 -150px;\n      padding: 10px;\n      text-align: center;\n    }\n    \n    .signupBox {\n      border: 1px solid whitesmoke;\n      border-radius: 8px;\n      position: absolute;\n      width:300px;\n      height: 400px;\n      top: 50%;\n      left: 50%;\n      margin: -200px 0 0 -150px;\n      padding: 10px;\n      text-align: center;\n    }\n    \n    .loginForm {\n      margin-top: 20px;\n    }\n    \n    .loginForm input {\n      border: solid 1px #4985A7 !important;\n    }\n    \n    .loginForm button, .signupForm button {\n      width:100%;\n    }\n    \n    .authHeader {\n      position: absolute;\n      top:50%;\n      margin-top: -30px;\n      width: 280px;\n    }\n    "
            ]
        }), 
        __metadata('design:paramtypes', [])
    ], LoginWindowComponent);
    return LoginWindowComponent;
}(window_component_1.WindowComponent));
exports.LoginWindowComponent = LoginWindowComponent;
//# sourceMappingURL=login.wcomp.js.map