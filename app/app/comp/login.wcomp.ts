
import {Component} from "@angular/core";
import {WindowComponent, WindowComponentInterface} from "../system/window/window.component";
import {App} from "../system/app";
import {ApiConnectorComponent} from "../system/connection/apiconnector.component";
import {AuthApiConnector} from "../system/connection/authapiconnector";

@Component({
  selector: 'login',
  template: `
    <div class="loginBgr">
    
      <div class="loginBox" [class.hiddenFrame]="!isLogin">
      
        <h3 class="text-white"> Login </h3>
        
        <form class="loginForm">

          <div class="row offset-top-s">
            <div class="col-md-12">
              <input id="user" 
              name="username" 
              type="text" 
              class="form-control" 
              placeholder="Username" 
              [value]="userName" 
              (input)="userName=$event.target.value">
            </div>
          </div>
          
          <div class="row offset-top-s">
            <div class="col-md-12">
              <input id="pass" 
              name="password" 
              type="password" 
              class="form-control" 
              placeholder="Password"
              [value]="password" 
              (input)="password=$event.target.value">
            </div>
          </div> 
          
          <div class="row offset-top-s">
            <div class="col-md-12">
              <button class="btn btn-primary" (click)="onSubmitLogin($event)"> Login </button>
              <button class="btn btn-primary offset-top-s" (click)="swapLoginSignup()"> Sign up </button>
            </div>
          </div>
          
        </form>
        
      </div>
      
      <div class="signupBox" [class.hiddenFrame]="!isSignUp">
      
        <h3 class="text-white"> Create Account </h3>
        
        <form class="signupForm">
          
          <div class="row offset-top-s">
            <div class="col-md-12">
              <input id="email" 
              name="email" 
              type="text" 
              class="form-control" 
              placeholder="E-mail" 
              [value]="email" 
              (input)="email = $event.target.value">
            </div>
          </div>
          
          <div class="row offset-top-s">
            <div class="col-md-12">
              <input id="user" 
              name="username" 
              type="text" 
              class="form-control" 
              placeholder="Username" 
              [value]="userName" 
              (input)="userName = $event.target.value">
            </div>
          </div>
          
          <div class="row offset-top-s">
            <div class="col-md-12">
              <input id="pass" 
              name="password" 
              type="password" 
              class="form-control" 
              placeholder="Password"
              [value]="password" 
              (input)="password = $event.target.value">
            </div>
          </div> 
          
          <div class="row offset-top-s">
            <div class="col-md-12">
              <input id="passconfirm" 
              name="password-confirm" 
              type="password" 
              class="form-control" 
              placeholder="Password Confirmation"
              [value]="passwordConfirm" 
              (input)="passwordConfirm = $event.target.value">
            </div>
          </div> 
          
          <div class="row offset-top-s">
            <div class="col-md-12">
              <input id="firstname" 
              name="firstname" 
              type="text" 
              class="form-control" 
              placeholder="First Name"
              [value]="firstName" 
              (input)="firstName = $event.target.value">
            </div>
          </div> 
          
          <div class="row offset-top-s">
            <div class="col-md-12">
              <input id="lastname" 
              name="lastname" 
              type="text" 
              class="form-control" 
              placeholder="Last Name"
              [value]="lastName" 
              (input)="lastName = $event.target.value">
            </div>
          </div> 
          
          <div class="row offset-top-s">
            <div class="col-md-12">
              <button class="btn btn-primary" (click)="onSubmitSignup($event)"> Continue Sign Up </button>
              <button class="btn btn-primary offset-top-s" (click)="swapLoginSignup()"> Return to Login </button>
            </div>
          </div>
          
        </form>
        
      </div>
      
      <div class="loginBox" [class.hiddenFrame]="!isProcess">
        <div class="authHeader"><h3> {{authCheckProcessStatus}}</h3></div> 
      </div>
      
    </div>
  `,
  styles: [`

    .loginBgr {
      width:100%;
      height:100%;
      background: #3D7EAA; /* fallback for old browsers */
      background: -webkit-linear-gradient(to left, #3D7EAA , #FFE47A); /* Chrome 10-25, Safari 5.1-6 */
      background: linear-gradient(to left, #3D7EAA , #FFE47A); /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */
    }
    
    .loginBox {
      border: 1px solid whitesmoke;
      border-radius: 8px;
      position: absolute;
      width:300px;
      height: 300px;
      top: 50%;
      left: 50%;
      margin: -150px 0 0 -150px;
      padding: 10px;
      text-align: center;
    }
    
    .signupBox {
      border: 1px solid whitesmoke;
      border-radius: 8px;
      position: absolute;
      width:300px;
      height: 400px;
      top: 50%;
      left: 50%;
      margin: -200px 0 0 -150px;
      padding: 10px;
      text-align: center;
    }
    
    .loginForm {
      margin-top: 20px;
    }
    
    .loginForm input {
      border: solid 1px #4985A7 !important;
    }
    
    .loginForm button, .signupForm button {
      width:100%;
    }
    
    .authHeader {
      position: absolute;
      top:50%;
      margin-top: -30px;
      width: 280px;
    }
    `
  ]
})

export class LoginWindowComponent extends WindowComponent implements WindowComponentInterface{

  private isLogin: boolean = true;

  private isSignUp: boolean = false;

  private isProcess: boolean = false;

  private authCheckProcessStatus: string = "Welcome";

  private email: string = "";

  private userName: string = "";

  private password: string = "";

  private passwordConfirm: string = "";

  private firstName: string = "";

  private lastName: string = "";

	/** -----------------------------------------------------------------------------------------
   *
   *
   *    Construct login entity with
   *          Window paramaters
   *
   *
   *  -----------------------------------------------------------------------------------------
   *
   *
   */
  constructor() {

    super.constructor();
    this.isRoot = true;
    this.isWithMenu = false;
    this.isWithBar = false;
    this.isCacheable = false;
    this.publicName = "login";

  }

  onFocus() {

    this._avtAsProcess();

    setTimeout(() => {
      this.authCheckProcessStatus = "Check authorization";
      this.authorizeByKey();
    },500);

  }

  onExit() {

  }

  onCreate() {
  }

  /** -----------------------------------------------------------------------------------------
   *
   *
   *    Authorize User by Stored Key
   *
   *
   *  -----------------------------------------------------------------------------------------
   *
   */
  private authorizeByKey() {

    var aapiconn: AuthApiConnector = ApiConnectorComponent.getAuthApiConnector();
    var user = App.state.user;

    if(user.key != null) {

      aapiconn.checkUserAuthorization().subscribe((authInfo) => {

        if (authInfo.user == user.id) {

          this.authCheckProcessStatus = "Logged";
          this.componentZone.run(()=> {});

          // Proceed to load windows
          setTimeout(() => {

            var last: string;

            console.log("NOW WE WILL LOAD SOMETHING");
            console.log(App.state.getLastLoadedComponent());
            // Load last user opened component if it's exists in cache
            if (last = App.state.getLastLoadedComponent())
              App.window.loadWindowComponent(last);
            // Load component dashboardComponent
            else
              App.window.loadWindowComponent("dashboardComponent");

          }, 200);

        }

      }, (error) => {

        // Show some information about failed procedure
        this.authCheckProcessStatus = "Login failed...";
        this.refreshView();

        setTimeout(() => {

          // Return to the form view
          this._actAsLogin();
          this.refreshView();
        }, 500);

      });

    } else {

      // Return to the form view
      this._actAsLogin();
      this.refreshView();

    }

  }

	/** -----------------------------------------------------------------------------------------
   *
   *    Submit User Information
   *    to API and Login user
   *    after success
   *
   *  -----------------------------------------------------------------------------------------
   *
   * @param $event
   *
   */
  public onSubmitLogin($event) {

    $event.preventDefault();

    var aapiconn: AuthApiConnector = ApiConnectorComponent.getAuthApiConnector();

    this.authCheckProcessStatus = "Authorizing";
    this._avtAsProcess();

    aapiconn.authorizeUser({email:this.userName, login:"", pass: this.password, agent:"dtp"})
      .subscribe((userInfo) => {

        this.authCheckProcessStatus = "Authorization done";

        var user = App.state.user;
        user.__clearCache();
        user.__keyInfoToCache(userInfo);
        App.state.__setAuthorized();

        this.userInformationUpdate(userInfo.user, userInfo.key);

        /*if(user.login == null || user.email == null) {

          // If No Information about User then poll for it and continue
          this.userInformationUpdate(userInfo.user, userInfo.key);

        } else {

          // Load Dashboard Component After Standard Login
          App.window.loadWindowComponent("dashboardComponent");

        }*/

      }, (error) => {

        // Change notification
        this.authCheckProcessStatus = "Login failed...";
        this.refreshView();

        // Change back to login form
        setTimeout(() => {
          this._actAsLogin();
          this.refreshView();
        }, 1000);

      });

  }

	/** -----------------------------------------------------------------------------------------
   *
   *    Update User Information and
   *      Continue authorization
   *              process
   *
   *  -----------------------------------------------------------------------------------------
   *
   * @param user
   * @param key
   *
   */
  public userInformationUpdate(user: number, key: string) {

      var aapiconn: AuthApiConnector = ApiConnectorComponent.getAuthApiConnector();

      aapiconn.getUserInfo({user:user, key:key}).subscribe((creds) => {

        App.state.user.__fromUserEntity(creds);
        App.state.user.timestamp = Date.now();
        App.state.user.__toCache();
        App.window.loadWindowComponent("dashboardComponent");

      }, function (error) {

        // Change notification
        this.authCheckProcessStatus = "Credentials failed...";
        this.refreshView();

        // Change back to login form
        setTimeout(() => {
          this._actAsLogin();
          this.refreshView();
        }, 3000);

      });


  }

	/** -----------------------------------------------------------------------------------------
   *
   *       User Signup Submission and User Creation
   *
   *  -----------------------------------------------------------------------------------------
   * @param $event
   */
  public onSubmitSignup($event) {

    $event.preventDefault();
    console.log(this);

  }

  private swapLoginSignup() {
    if(this.isLogin)
      this._actAsSignup();
    else
      this._actAsLogin();
  }

  private _actAsSignup(){
    this.isSignUp = true;
    this.isLogin = false;
    this.isProcess = false;
  }

  private _actAsLogin(){
    this.isSignUp = false;
    this.isLogin = true;
    this.isProcess = false;
  }

  private _avtAsProcess(){
    this.isSignUp = false;
    this.isLogin = false;
    this.isProcess = true;
  }

}
