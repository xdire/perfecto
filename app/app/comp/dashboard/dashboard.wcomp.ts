import {Component} from "@angular/core";
import {WindowComponent} from "../../system/window/window.component";

@Component({
  selector:"dashboard",
  template:`

    <div>
      <h1> DASHBOARD </h1>
    </div>
    
  `
})

export class DashboardComponent extends WindowComponent {

  constructor() {
    super.constructor();
    this.isRoot = true;
    this.isWithMenu = true;
    this.publicName = "dashboard";
  }

  public onCreate() {

  }

}
