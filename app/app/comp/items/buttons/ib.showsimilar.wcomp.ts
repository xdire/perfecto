import {Component, Input} from "@angular/core";
import {App} from "../../../system/app";
import {WarehouseLocationListComponent} from "../../warehouse/location/whs.loc.list.wcomp";

@Component({
  selector: "ib-show-similar",
  host: {

  },
  template: `
    <button 
      class="btn btn-primary" 
      (click)="openSimilarSearchWindow()">
        <i class="fa fa-cubes"></i>
    </button>
  `,
  styles:[`

  `]
})

export class ItemButtonShowSimilar {

  @Input() public variationId: number = null;

  @Input() public visible: boolean = true;
  
  @Input() public listeners: Array<string> = [];

  constructor() {

  }

  public openSimilarSearchWindow() {

    if(this.variationId === null) return;

    App.window.loadComponentIntoModalFrame(WarehouseLocationListComponent, {
      title: "Boxes which contains items similar to selected",
      data: {
        variationId: this.variationId
      },
      onSuccess: () => {},
      onLoad: () => {},
      onClose: () => {App.inputScannerObserver.resumeListenerWithGIUD(this.listeners);},
      onFailure: () => {},
      getReference: () => {App.inputScannerObserver.pauseListenerWithGIUD(this.listeners);}
    });

  }

}
