"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var app_1 = require("../../../system/app");
var whs_loc_list_wcomp_1 = require("../../warehouse/location/whs.loc.list.wcomp");
var ItemButtonShowSimilar = (function () {
    function ItemButtonShowSimilar() {
        this.variationId = null;
        this.visible = true;
        this.listeners = [];
    }
    ItemButtonShowSimilar.prototype.openSimilarSearchWindow = function () {
        var _this = this;
        if (this.variationId === null)
            return;
        app_1.App.window.loadComponentIntoModalFrame(whs_loc_list_wcomp_1.WarehouseLocationListComponent, {
            title: "Boxes which contains items similar to selected",
            data: {
                variationId: this.variationId
            },
            onSuccess: function () { },
            onLoad: function () { },
            onClose: function () { app_1.App.inputScannerObserver.resumeListenerWithGIUD(_this.listeners); },
            onFailure: function () { },
            getReference: function () { app_1.App.inputScannerObserver.pauseListenerWithGIUD(_this.listeners); }
        });
    };
    __decorate([
        core_1.Input(), 
        __metadata('design:type', Number)
    ], ItemButtonShowSimilar.prototype, "variationId", void 0);
    __decorate([
        core_1.Input(), 
        __metadata('design:type', Boolean)
    ], ItemButtonShowSimilar.prototype, "visible", void 0);
    __decorate([
        core_1.Input(), 
        __metadata('design:type', Array)
    ], ItemButtonShowSimilar.prototype, "listeners", void 0);
    ItemButtonShowSimilar = __decorate([
        core_1.Component({
            selector: "ib-show-similar",
            host: {},
            template: "\n    <button \n      class=\"btn btn-primary\" \n      (click)=\"openSimilarSearchWindow()\">\n        <i class=\"fa fa-cubes\"></i>\n    </button>\n  ",
            styles: ["\n\n  "]
        }), 
        __metadata('design:paramtypes', [])
    ], ItemButtonShowSimilar);
    return ItemButtonShowSimilar;
}());
exports.ItemButtonShowSimilar = ItemButtonShowSimilar;
//# sourceMappingURL=ib.showsimilar.wcomp.js.map