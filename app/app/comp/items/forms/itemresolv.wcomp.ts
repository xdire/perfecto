import {Component} from "@angular/core";
import {WindowComponent} from "../../../system/window/window.component";
import {ComponentImplementingModalFramePass} from "../../../system/entities/system.entities";
import {ItemEntity, ItemVariationEntity} from "../../../system/connection/itemapiconnector";
import {WarehouseScannedItemEntity} from "../../warehouse/entities/whsent.comp";
import {App} from "../../../system/app";

@Component({
  selector: 'whsitmrslv',
  template: `
     <div>
     
        <form style="color:#333333;">
          
          <div class="left-side">
              
            <div>
              <i class="fa fa-pencil" aria-hidden="true"></i> <input class="sform-input-wide" [(ngModel)]="title">
            </div>
            
            <div>
              <i class="fa fa-tag" aria-hidden="true"></i> <input class="sform-input-norm" [(ngModel)]="variationId">
            </div>
            
            <div>
              <i class="fa fa-expand" aria-hidden="true"></i> <input class="sform-input-norm" [(ngModel)]="size">
            </div>
            
            <div>
              <i class="fa fa-paint-brush" aria-hidden="true"></i> <input class="sform-input-norm" [(ngModel)]="color">
            </div>
              
          </div>
          
          <div class="right-side">
              
              <div class="left-side">
              
                <h4>Style list</h4>
                <div>
                  <button 
                  *ngFor="let stl of styles; let s=index" 
                  class="btn btn-primary" 
                  [class.buttonSelected]="s==selectedStyleIndex" (click)="applyStyle(s)">{{stl}}
                  </button>
                </div>
                
                <h4>Image list</h4>
                <div 
                  class="imageblock"
                  *ngFor="let img of images; let i=index"
                  [class.imageSelected]="i==selectedImageIndex" (click)="applyImage(i)">
                  <img class="imagesrc" src="{{img}}">
                </div>
                
              </div>
              
              <div class="right-side">
              
                <h1> Quantity </h1>
                <button class="btn btn-primary btn-wide" (click)="increaseQty()"><i class="fa fa-chevron-up" aria-hidden="true"></i></button>
                <div class="header-block">{{quantity}}</div>
                <button class="btn btn-primary btn-wide" (click)="decreaseQty()"><i class="fa fa-chevron-down" aria-hidden="true"></i></button>
     
              </div>
              
          </div>
          
          <div style="margin-top: 20px; text-align: center;">
              <button class="btn btn-primary btn-normal" type="submit" (click)="clickSave($event)"> Save </button>
          </div>
          
        </form>
        
     </div>
  `,
  host: {
    'class':'addscanner',
  },
  styles: [
    `
      .sform-input-wide {width:90%; text-align: center; padding: 5px; border:1px solid #DEDEDE; margin: 2px;}
      .sform-input-norm {text-align: center; padding: 5px; border:1px solid #DEDEDE; margin: 2px;}
      
      .imageblock {display: inline-block; width: 64px; height: 64px;}
      .imagesrc {width: 64px; height: auto}
      
      .buttonSelected {background-color: #00AA00;}
      
      .imageSelected {    
          border: 2px solid #00AA00;
          border-style: inset;
          box-sizing: content-box;
      }
      
      .left-side {
        color: #333333;
        display: inline-block;
        width: 49%;
        vertical-align: top;
      }
      .right-side {
        color: #333333;
        text-align: center;
        display: inline-block;
        width: 49%;
        vertical-align: top;
      }
      
    `
  ]
})

export class ItemResolveFormComponent extends WindowComponent implements ComponentImplementingModalFramePass {

	dataObject:any;
  closingMethod:any;
  successCallback:any;
  failureCallback:any;

  quantity: number = 1;
  styles: Array<string> = [];
  images: Array<string> = [];
  title: string = "";
  variationId: number = null;
  size: string = null;
  color: string = null;
  brand: string = null;

  selectedStyle: string = null;
  selectedImage: string = null;

  selectedImageIndex: number = 0;
  selectedStyleIndex: number = 0;

  item: ItemEntity = null;
  variation: ItemVariationEntity = null;

  listeners: Array<string> = [];
  /** ------------------------------------------------------------------------
   *
   *  ------------------------------------------------------------------------
   */
  onLoad() {

    this.listeners.push(App.inputScannerObserver.addCommandListener("close",() => {
      this.clickSave(null);
    }));

    var data = <ItemEntity>this.dataObject;
    var i = 0;

    this.variationId = data.variations[0].id;
    this.title = data.title;
    this.size = data.variations[0].size;
    this.color = data.variations[0].color;
    this.brand = data.brand;

    this.item = data;

    if(this.item.variations.length > 0) {
      this.variation = data.variations[0];
    }

    // Set styles
    if(typeof data.style !== "undefined") {

      for(i = 0; i < data.style.length; i++) {
        this.styles.push(data.style[i].name);
      }
      if(data.style.length > 0){
        this.applyStyle(0);
      }

    }

    // Set image from variation if can be found
    if(typeof data.variations[0].info !== "undefined") {

      if(typeof data.variations[0].info.images !== "undefined") {

        // Select title image
        if(typeof data.variations[0].info.images.titleImage !== "undefined") {

          this.images.push(data.variations[0].info.images.titleImage.src+
            "s."+data.variations[0].info.images.titleImage.ext);
          this.applyImage(0);

        }
        // Add other
        if(typeof data.info.images.imageSet !== "undefined") {

          var set = data.info.images.imageSet;
          for(i = 0; i < set.length; i++) {
            this.images.push(set[i].src+
              "s."+set[i].ext);
          }

        }

      }

    }
    // Set image from item if can be found
    else {

      if(typeof data.info !== "undefined") {

        if(typeof data.info.images !== "undefined") {

          // Select title image
          if(typeof data.info.images.titleImage !== "undefined") {

            this.images.push(data.info.images.titleImage.src+
              "s."+data.info.images.titleImage.ext);
            this.applyImage(0);

          }
          // Add other
          if(typeof data.info.images.imageSet !== "undefined") {

            var set = data.info.images.imageSet;
            for(i = 0; i < set.length; i++) {
              this.images.push(set[i].src+
                "s."+set[i].ext);
            }

          }

        }

      }

    }

    this.refreshView();

  }

	/**
   * 
   */
  onClose() {
    App.inputScannerObserver.removeListenerWithGUID(this.listeners);
  }

  /** ------------------------------------------------------------------------
   *
   *  ------------------------------------------------------------------------
   */
  applyStyle(index:number) {
    this.selectedStyle = this.styles[index];
    this.selectedStyleIndex = index;
    this.refreshView();
  }

  /** ------------------------------------------------------------------------------
   *
   *  ------------------------------------------------------------------------------ */
  public increaseQty() {
    this.quantity++;
  }

  /** ------------------------------------------------------------------------------
   *
   *  ------------------------------------------------------------------------------ */
  public decreaseQty() {
    if(this.quantity > 0) {
      this.quantity--;
    }
  }

  /** ------------------------------------------------------------------------
   *
   *  ------------------------------------------------------------------------
   */
  applyImage(index:number) {
    this.selectedImage = this.images[index];
    this.selectedImageIndex = index;
    this.refreshView();
  }

  /** ------------------------------------------------------------------------
   *
   *  ------------------------------------------------------------------------
   */
  isStyleIndexSelected(ci: number){
    console.log("CHECK EQUALITY STL "+ci+ " "+ this.selectedStyleIndex);
    return this.selectedStyleIndex == ci;
  }

  /** ------------------------------------------------------------------------
   *
   *  ------------------------------------------------------------------------
   */
  isImageIndexSelected(ci: number) {
    console.log("CHECK EQUALITY IMG "+ci+ " "+ this.selectedImageIndex);
    return this.selectedImageIndex == ci;
  }

	/** ------------------------------------------------------------------------
   *
   *  ------------------------------------------------------------------------
   */
  clickSave($e: any = null) {

    if($e != null)
      $e.preventDefault();

    var object = new WarehouseScannedItemEntity();
    object.title = this.title;
    object.variationId = this.variationId;
    object.image = this.selectedImage;

    object.setExtraOption("style",this.selectedStyle);
    object.setExtraOption("size",this.size);
    object.setExtraOption("color",this.color);
    object.setExtraOption("brand",this.brand);
    object.quantity = this.quantity;

    if(this.variationId !== null)
      object.resolved = true;

    this.item.definedStyle = this.selectedStyle;
    object.itemObject = this.item;
    object.variationObject = this.variation;

    this.successCallback(object);

    this.closingMethod(/*{reset:{listeners:this.listeners}}*/);

  }

}
