"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var window_component_1 = require("../../../system/window/window.component");
var whsent_comp_1 = require("../../warehouse/entities/whsent.comp");
var app_1 = require("../../../system/app");
var ItemResolveFormComponent = (function (_super) {
    __extends(ItemResolveFormComponent, _super);
    function ItemResolveFormComponent() {
        _super.apply(this, arguments);
        this.quantity = 1;
        this.styles = [];
        this.images = [];
        this.title = "";
        this.variationId = null;
        this.size = null;
        this.color = null;
        this.brand = null;
        this.selectedStyle = null;
        this.selectedImage = null;
        this.selectedImageIndex = 0;
        this.selectedStyleIndex = 0;
        this.item = null;
        this.variation = null;
        this.listeners = [];
    }
    /** ------------------------------------------------------------------------
     *
     *  ------------------------------------------------------------------------
     */
    ItemResolveFormComponent.prototype.onLoad = function () {
        var _this = this;
        this.listeners.push(app_1.App.inputScannerObserver.addCommandListener("close", function () {
            _this.clickSave(null);
        }));
        var data = this.dataObject;
        var i = 0;
        this.variationId = data.variations[0].id;
        this.title = data.title;
        this.size = data.variations[0].size;
        this.color = data.variations[0].color;
        this.brand = data.brand;
        this.item = data;
        if (this.item.variations.length > 0) {
            this.variation = data.variations[0];
        }
        // Set styles
        if (typeof data.style !== "undefined") {
            for (i = 0; i < data.style.length; i++) {
                this.styles.push(data.style[i].name);
            }
            if (data.style.length > 0) {
                this.applyStyle(0);
            }
        }
        // Set image from variation if can be found
        if (typeof data.variations[0].info !== "undefined") {
            if (typeof data.variations[0].info.images !== "undefined") {
                // Select title image
                if (typeof data.variations[0].info.images.titleImage !== "undefined") {
                    this.images.push(data.variations[0].info.images.titleImage.src +
                        "s." + data.variations[0].info.images.titleImage.ext);
                    this.applyImage(0);
                }
                // Add other
                if (typeof data.info.images.imageSet !== "undefined") {
                    var set = data.info.images.imageSet;
                    for (i = 0; i < set.length; i++) {
                        this.images.push(set[i].src +
                            "s." + set[i].ext);
                    }
                }
            }
        }
        else {
            if (typeof data.info !== "undefined") {
                if (typeof data.info.images !== "undefined") {
                    // Select title image
                    if (typeof data.info.images.titleImage !== "undefined") {
                        this.images.push(data.info.images.titleImage.src +
                            "s." + data.info.images.titleImage.ext);
                        this.applyImage(0);
                    }
                    // Add other
                    if (typeof data.info.images.imageSet !== "undefined") {
                        var set = data.info.images.imageSet;
                        for (i = 0; i < set.length; i++) {
                            this.images.push(set[i].src +
                                "s." + set[i].ext);
                        }
                    }
                }
            }
        }
        this.refreshView();
    };
    /**
   *
   */
    ItemResolveFormComponent.prototype.onClose = function () {
        app_1.App.inputScannerObserver.removeListenerWithGUID(this.listeners);
    };
    /** ------------------------------------------------------------------------
     *
     *  ------------------------------------------------------------------------
     */
    ItemResolveFormComponent.prototype.applyStyle = function (index) {
        this.selectedStyle = this.styles[index];
        this.selectedStyleIndex = index;
        this.refreshView();
    };
    /** ------------------------------------------------------------------------------
     *
     *  ------------------------------------------------------------------------------ */
    ItemResolveFormComponent.prototype.increaseQty = function () {
        this.quantity++;
    };
    /** ------------------------------------------------------------------------------
     *
     *  ------------------------------------------------------------------------------ */
    ItemResolveFormComponent.prototype.decreaseQty = function () {
        if (this.quantity > 0) {
            this.quantity--;
        }
    };
    /** ------------------------------------------------------------------------
     *
     *  ------------------------------------------------------------------------
     */
    ItemResolveFormComponent.prototype.applyImage = function (index) {
        this.selectedImage = this.images[index];
        this.selectedImageIndex = index;
        this.refreshView();
    };
    /** ------------------------------------------------------------------------
     *
     *  ------------------------------------------------------------------------
     */
    ItemResolveFormComponent.prototype.isStyleIndexSelected = function (ci) {
        console.log("CHECK EQUALITY STL " + ci + " " + this.selectedStyleIndex);
        return this.selectedStyleIndex == ci;
    };
    /** ------------------------------------------------------------------------
     *
     *  ------------------------------------------------------------------------
     */
    ItemResolveFormComponent.prototype.isImageIndexSelected = function (ci) {
        console.log("CHECK EQUALITY IMG " + ci + " " + this.selectedImageIndex);
        return this.selectedImageIndex == ci;
    };
    /** ------------------------------------------------------------------------
   *
   *  ------------------------------------------------------------------------
   */
    ItemResolveFormComponent.prototype.clickSave = function ($e) {
        if ($e === void 0) { $e = null; }
        if ($e != null)
            $e.preventDefault();
        var object = new whsent_comp_1.WarehouseScannedItemEntity();
        object.title = this.title;
        object.variationId = this.variationId;
        object.image = this.selectedImage;
        object.setExtraOption("style", this.selectedStyle);
        object.setExtraOption("size", this.size);
        object.setExtraOption("color", this.color);
        object.setExtraOption("brand", this.brand);
        object.quantity = this.quantity;
        if (this.variationId !== null)
            object.resolved = true;
        this.item.definedStyle = this.selectedStyle;
        object.itemObject = this.item;
        object.variationObject = this.variation;
        this.successCallback(object);
        this.closingMethod();
    };
    ItemResolveFormComponent = __decorate([
        core_1.Component({
            selector: 'whsitmrslv',
            template: "\n     <div>\n     \n        <form style=\"color:#333333;\">\n          \n          <div class=\"left-side\">\n              \n            <div>\n              <i class=\"fa fa-pencil\" aria-hidden=\"true\"></i> <input class=\"sform-input-wide\" [(ngModel)]=\"title\">\n            </div>\n            \n            <div>\n              <i class=\"fa fa-tag\" aria-hidden=\"true\"></i> <input class=\"sform-input-norm\" [(ngModel)]=\"variationId\">\n            </div>\n            \n            <div>\n              <i class=\"fa fa-expand\" aria-hidden=\"true\"></i> <input class=\"sform-input-norm\" [(ngModel)]=\"size\">\n            </div>\n            \n            <div>\n              <i class=\"fa fa-paint-brush\" aria-hidden=\"true\"></i> <input class=\"sform-input-norm\" [(ngModel)]=\"color\">\n            </div>\n              \n          </div>\n          \n          <div class=\"right-side\">\n              \n              <div class=\"left-side\">\n              \n                <h4>Style list</h4>\n                <div>\n                  <button \n                  *ngFor=\"let stl of styles; let s=index\" \n                  class=\"btn btn-primary\" \n                  [class.buttonSelected]=\"s==selectedStyleIndex\" (click)=\"applyStyle(s)\">{{stl}}\n                  </button>\n                </div>\n                \n                <h4>Image list</h4>\n                <div \n                  class=\"imageblock\"\n                  *ngFor=\"let img of images; let i=index\"\n                  [class.imageSelected]=\"i==selectedImageIndex\" (click)=\"applyImage(i)\">\n                  <img class=\"imagesrc\" src=\"{{img}}\">\n                </div>\n                \n              </div>\n              \n              <div class=\"right-side\">\n              \n                <h1> Quantity </h1>\n                <button class=\"btn btn-primary btn-wide\" (click)=\"increaseQty()\"><i class=\"fa fa-chevron-up\" aria-hidden=\"true\"></i></button>\n                <div class=\"header-block\">{{quantity}}</div>\n                <button class=\"btn btn-primary btn-wide\" (click)=\"decreaseQty()\"><i class=\"fa fa-chevron-down\" aria-hidden=\"true\"></i></button>\n     \n              </div>\n              \n          </div>\n          \n          <div style=\"margin-top: 20px; text-align: center;\">\n              <button class=\"btn btn-primary btn-normal\" type=\"submit\" (click)=\"clickSave($event)\"> Save </button>\n          </div>\n          \n        </form>\n        \n     </div>\n  ",
            host: {
                'class': 'addscanner',
            },
            styles: [
                "\n      .sform-input-wide {width:90%; text-align: center; padding: 5px; border:1px solid #DEDEDE; margin: 2px;}\n      .sform-input-norm {text-align: center; padding: 5px; border:1px solid #DEDEDE; margin: 2px;}\n      \n      .imageblock {display: inline-block; width: 64px; height: 64px;}\n      .imagesrc {width: 64px; height: auto}\n      \n      .buttonSelected {background-color: #00AA00;}\n      \n      .imageSelected {    \n          border: 2px solid #00AA00;\n          border-style: inset;\n          box-sizing: content-box;\n      }\n      \n      .left-side {\n        color: #333333;\n        display: inline-block;\n        width: 49%;\n        vertical-align: top;\n      }\n      .right-side {\n        color: #333333;\n        text-align: center;\n        display: inline-block;\n        width: 49%;\n        vertical-align: top;\n      }\n      \n    "
            ]
        }), 
        __metadata('design:paramtypes', [])
    ], ItemResolveFormComponent);
    return ItemResolveFormComponent;
}(window_component_1.WindowComponent));
exports.ItemResolveFormComponent = ItemResolveFormComponent;
//# sourceMappingURL=itemresolv.wcomp.js.map