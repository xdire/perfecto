"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var window_component_1 = require("../../../system/window/window.component");
var apiconnector_component_1 = require("../../../system/connection/apiconnector.component");
var itemvar_wcomp_1 = require("./itemvar.wcomp");
var itemgrid_wcomp_1 = require("./itemgrid.wcomp");
var ItemVariationsGridViewComponent = (function (_super) {
    __extends(ItemVariationsGridViewComponent, _super);
    function ItemVariationsGridViewComponent() {
        this.itemList = [];
        this.renderList = [];
        this.sizeFilter = [];
        this.colorFilter = [];
        this.filterAmount = 0;
        this.selectedSize = null;
        this.selectedColor = null;
        this.selectable = false;
        this.connector = apiconnector_component_1.ApiConnectorComponent.getItemApiConnector();
    }
    /** -----------------------------------------------------------------
     *           Execute onload method for Window Component
     *  -----------------------------------------------------------------
     */
    ItemVariationsGridViewComponent.prototype.onLoad = function () {
        this.itemList = this.dataObject.variations;
        if (typeof this.dataObject.selectable === "boolean") {
            this.selectable = this.dataObject.selectable;
        }
        this.renderList = this.itemList;
        this.refillFilters(null);
    };
    /** -----------------------------------------------------------------
   *
   *  -----------------------------------------------------------------
   *
   * @param variation
   */
    ItemVariationsGridViewComponent.prototype.onSelect = function (variation) {
        var entity = {
            type: itemgrid_wcomp_1.ItemGridAnswerType.OnlyVariation,
            variation: variation
        };
        this.successCallback(entity);
        this.closingMethod();
    };
    /** -----------------------------------------------------------------
   *
   *
   *  -----------------------------------------------------------------
   *
   *  @param filter
   *  @param type
   */
    ItemVariationsGridViewComponent.prototype.applyFilter = function (filter, type) {
        var _this = this;
        if (type == 0) {
            if (filter !== null)
                this.selectedSize = filter;
        }
        else if (type == 1) {
            if (filter !== null)
                this.selectedColor = filter;
        }
        var filters = { colors: [], sizes: [] };
        var addedColors = {};
        var addedSizes = {};
        this.renderList = this.itemList.filter(function (item) {
            var passBySize = false;
            var passByColor = false;
            if (item.size == _this.selectedSize || _this.selectedSize == null)
                passBySize = true;
            if (item.color == _this.selectedColor || _this.selectedColor == null)
                passByColor = true;
            if (passBySize && passByColor) {
                if (item.color !== null && !addedColors.hasOwnProperty(item.color)) {
                    filters.colors.push(item.color);
                    addedColors[item.color] = true;
                }
                if (item.size !== null && !addedSizes.hasOwnProperty(item.size)) {
                    filters.sizes.push(item.size);
                    addedSizes[item.size] = true;
                }
                return true;
            }
        });
        this.refillFilters(filters);
    };
    /** -----------------------------------------------------------------
   *
   *
   *  -----------------------------------------------------------------
   *
   *  @param type
   */
    ItemVariationsGridViewComponent.prototype.clearFilter = function (type) {
        if (type == 0) {
            this.selectedSize = null;
        }
        else if (type == 1) {
            this.selectedColor = null;
        }
        this.applyFilter(null, type);
    };
    /** -----------------------------------------------------------------
   *
   *
   *  -----------------------------------------------------------------
   *
   *  @param filters
   */
    ItemVariationsGridViewComponent.prototype.refillFilters = function (filters) {
        var _this = this;
        if (filters === void 0) { filters = null; }
        if (filters !== null) {
            this.colorFilter = filters.colors;
            this.sizeFilter = filters.sizes;
        }
        else {
            var addedColors = {};
            var addedSizes = {};
            this.renderList.map(function (item) {
                if (item.color !== null && !addedColors.hasOwnProperty(item.color)) {
                    _this.colorFilter.push(item.color);
                    addedColors[item.color] = true;
                    _this.filterAmount++;
                }
                if (item.size !== null && !addedSizes.hasOwnProperty(item.size)) {
                    _this.sizeFilter.push(item.size);
                    addedSizes[item.size] = true;
                    _this.filterAmount++;
                }
            });
            addedColors = null;
            addedSizes = null;
        }
    };
    ItemVariationsGridViewComponent = __decorate([
        core_1.Component({
            selector: 'itemv-grid-view',
            template: "\n   <div>\n      \n     <div class=\"filter-block\">\n     \n      <table class=\"filters-table\" [class.hiddenFrame]=\"filterAmount==0\">\n      \n        <tr>\n          \n          <!--Colors-->\n          <td> \n            <button \n              class=\"btn btn-primary filter-button\" \n              *ngFor=\"let size of sizeFilter\"\n              [class.filter-selected]=\"size==selectedSize\"\n              (click)=\"applyFilter(size,0);\">{{size}}</button>\n            <button \n              class=\"btn btn-danger filter-button\" \n              [class.hiddenFrame]=\"selectedSize==null\" \n              (click)=\"clearFilter(0)\">Clear</button>\n          </td>\n          <!--Sizes-->\n          <td> \n            <button \n            class=\"btn btn-primary filter-button\" \n            *ngFor=\"let color of colorFilter\" \n            [class.filter-selected]=\"color==selectedColor\"\n            (click)=\"applyFilter(color,1);\">{{color}}</button>\n            <button \n              class=\"btn btn-danger filter-button\" \n              [class.hiddenFrame]=\"selectedColor==null\" \n              (click)=\"clearFilter(1)\">Clear</button>\n          </td>\n          \n        </tr>\n        \n      </table>\n      \n     </div>\n     \n     <div> \n        <ivent *ngFor=\"let item of renderList\" [itemVariationData]=\"item\" [selectable]=\"selectable\" (selectedVariation)=\"onSelect($event)\"> </ivent>\n     </div>\n     \n   </div>\n  ",
            host: {
                'class': 'itemv-grid-view',
            },
            styles: [
                "\n      .filters-table {box-shadow: 0 10px 10px rgba(0,0,0,0.1); width: 100%;}\n      .filters-table tr td {padding: 10px; max-width: 60%; vertical-align: top; min-width: 192px;}\n      .filter-block {padding: 8px; margin-bottom: 8px;}\n      .filter-button {margin: 2px; vertical-align: top;}\n      .filter-selected {background-color: lightseagreen}\n    "
            ],
            directives: [
                itemvar_wcomp_1.ItemVariationEntityComponent
            ]
        }), 
        __metadata('design:paramtypes', [])
    ], ItemVariationsGridViewComponent);
    return ItemVariationsGridViewComponent;
}(window_component_1.WindowComponent));
exports.ItemVariationsGridViewComponent = ItemVariationsGridViewComponent;
//# sourceMappingURL=itemvargrid.wcomp.js.map