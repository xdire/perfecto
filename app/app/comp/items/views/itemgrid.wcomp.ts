import {Component} from "@angular/core";
import {WindowComponent} from "../../../system/window/window.component";
import {ComponentImplementingModalFramePass} from "../../../system/entities/system.entities";
import {ItemApiConnector, ItemEntity, ItemVariationEntity} from "../../../system/connection/itemapiconnector";
import {ApiConnectorComponent} from "../../../system/connection/apiconnector.component";
import {ItemEntityComponent} from "./itementity.wcomp";

@Component({
  selector: 'item-grid-view',
  template: `
   <div>
   
     <div *ngIf="itemList.length == 0"> {{message}} </div>
     
     <div> 
        <ient *ngFor="let item of itemList" 
        [definedStyle]="definedStyle"
        [itemData]="item" 
        [withVariations]="variations"
        [passThroughSelection]="passThrough"
        [selectable]="selectable" 
        [selectableVariation]="selectableVariation"
        (selectedVariation)="onSelectedVariation($event)"
        (selectedItem)="onSelectedItem($event)">
        </ient>
     </div>
     
   </div>
  `,
  host: {
    'class':'addscanner',
  },
  styles: [
    `
    
    `
  ],
  directives: [
    ItemEntityComponent
  ]
})

export class ItemGridViewComponent extends WindowComponent implements ComponentImplementingModalFramePass {

	dataObject:any;
  closingMethod:any;
  successCallback:any;
  failureCallback:any;

  public message: string = "Loading...";

  public itemList: ItemEntity[] = [];

  private variations: boolean = false;

  private selectable: boolean = false;

  private selectableVariation: boolean = false;

  private passThrough: boolean = false;

  private connector: ItemApiConnector;

  private selectedVariation: ItemVariationEntity = null;

  private selectedItem: ItemEntity = null;

  private definedStyle: string = null;

  constructor() {
    this.connector = ApiConnectorComponent.getItemApiConnector();
  }

	/** -----------------------------------------------------------------
   *
   *  -----------------------------------------------------------------
   * @param data
   */
  onSelectedVariation(data: ItemGridAnswerObject) {

    data.item.variations = null;

    if(this.passThrough) {

      this.successCallback(data);
      this.closingMethod();

    } else {
      this.selectedVariation = data.variation;
    }

  }

	/** -----------------------------------------------------------------
   *
   *  -----------------------------------------------------------------
   * @param data
   */
  onSelectedItem(data: ItemGridAnswerObject) {

    if(this.passThrough) {

      data.item.variations = null;
      if(this.selectableVariation !== null) {
        data.type = ItemGridAnswerType.BothEntities;
        data.variation = this.selectableVariation;
      }
      this.successCallback(data);
      this.closingMethod();

    } else {

      this.selectedItem = data.item;

    }

  }

	/** -----------------------------------------------------------------
   *           Execute onload method for Window Component
   *  -----------------------------------------------------------------
   */
  public onLoad() {

    var d = <ItemGridViewComponentProperties>this.dataObject;

    if(d.hasOwnProperty("variations")) {
      this.variations = d.variations;
    }
    if(d.hasOwnProperty("selectableItem")) {
      this.selectable = d.selectableItem;
    }
    if(d.hasOwnProperty("selectableVariation")) {
      this.selectableVariation = d.selectableVariation;
    }
    if(d.hasOwnProperty("onVariationSelectPassThrough")) {
      this.passThrough = d.onVariationSelectPassThrough;
    }
    if(d.hasOwnProperty("requestType")) {

      // Select by style code
      if(d.requestType == ItemGridViewRequestType.ByStyleCode) {

        var p = d.requestParameters;
        if(p.hasOwnProperty("style")) {
          this.definedStyle = p.style;
          this.getItemsForItemStyle(p.style);
        }

      }
      // Select by Variation Id
      else if(d.requestType == ItemGridViewRequestType.ByVariationId) {

      }
      // Select by Item Id
      else if(d.requestType == ItemGridViewRequestType.ByItemId) {

      }
      // Select by Combination of parameters
      else if(d.requestType == ItemGridViewRequestType.ByCombination) {

      }

    }

  }

	/** -----------------------------------------------------------------
   *            Select Item by Item Style Id
   *  -----------------------------------------------------------------
   */
  public getItemsForItemStyle(style: string) {

    this.connector.getItemsByStyleId(style).subscribe((items) => {
      this.itemList = items;
    }, (err) => {
      this.message = "No items found for that style";
    });

  }

	/** -----------------------------------------------------------------
   *
   *  -----------------------------------------------------------------
   */
  public getItemsByVariationId(variationId: number) {

  }

	/** -----------------------------------------------------------------
   *
   *  -----------------------------------------------------------------
   */
  public getItemByItemId(itemId: number) {

  }



}

export interface ItemGridViewComponentProperties {
  requestType: ItemGridViewRequestType;
  requestParameters: ItemGridViewRequestParameters;
  variations?: boolean;
  selectableItem?: boolean;
  selectableVariation?: boolean;
  onVariationSelectPassThrough?: boolean;
}

export interface ItemGridViewRequestParameters {
  style?: string;
  color?: string;
  size?: string;
  variationId?: number;
  itemId?: number;
}

export interface ItemGridAnswerObject {
  type: ItemGridAnswerType;
  item?: ItemEntity;
  variation?: ItemVariationEntity;
}

export enum ItemGridAnswerType {
  BothEntities,
  OnlyItem,
  OnlyVariation
}

export enum ItemGridViewRequestType {
  ByStyleCode,
  ByVariationId,
  ByItemId,
  ByCombination
}
