import {Component, Input, Output, EventEmitter} from "@angular/core";
import {ItemVariationEntity} from "../../../system/connection/itemapiconnector";

@Component({
  selector: 'ivent',
  template: `
   <div class="item-entity-card">
   
     <div class="item-entity-image">
      <img src="{{itemVariationData.info?.images?.titleImage?.src? itemVariationData.info?.images?.titleImage?.src+'s.'+itemVariationData.info?.images?.titleImage?.ext : 'https://s3.amazonaws.com/afimg/nopicture.png'}}">
     </div>
     
     <div class="item-entity-info">
      <div class="item-entity-info-title">Code: {{itemVariationData.code}}</div>
      <div class="item-entity-info-desc">Color: {{itemVariationData.color}}<br>Size: {{itemVariationData.size}}</div>
     </div>
     
     <div class="right-text item-entity-buttons">
      <button [style.display] = "!selectable?'none':'inline-block'" class="btn btn-primary btn-third" (click)="onSelect()">Select</button>
      <button class="btn btn-primary btn-third">Edit</button>
     </div>
     
   </div>
  `,
  host: {
    'class':'item-var-entity',
  },
  styles: [
    `
    
    `
  ]
})

export class ItemVariationEntityComponent {

  @Input() public itemVariationData: ItemVariationEntity;

  @Input() public selectable: boolean = false;

  @Output() public selectedVariation = new EventEmitter();

  public onSelect() {
    this.selectedVariation.emit(this.itemVariationData);
  }

}
