import {Component, Input, Output, EventEmitter} from "@angular/core";
import {ItemEntity} from "../../../system/connection/itemapiconnector";
import {ItemVariationEntityComponent} from "./itemvar.wcomp";
import {App} from "../../../system/app";
import {ItemVariationsGridViewComponent} from "./itemvargrid.wcomp";
import {ItemGridAnswerObject, ItemGridAnswerType} from "./itemgrid.wcomp";

@Component({
  selector: 'ient',
  template: `
   <div class="item-entity-card">
      
     <div class="item-entity-image">
      <img src="{{itemData.info?.images?.titleImage?.src? itemData.info?.images?.titleImage?.src+'s.'+itemData.info?.images?.titleImage?.ext : 'https://s3.amazonaws.com/afimg/nopicture.png'}}">
     </div>
     
     <div class="item-entity-info">
      <div class="item-entity-info-brand">{{itemData.brand}}</div>
      <div class="item-entity-info-title">{{itemData.title}}</div>
      <div class="item-entity-info-desc">{{itemData.description?.short}}</div>
     </div>
     
     <div class="item-entity-buttons">
      <button class="btn btn-primary btn-third" [style.display] = "withVariations?'inline-block':'none'" (click)="showItemVariations()"> Variations </button>
      <button class="btn btn-primary btn-third" [style.display] = "selectable?'inline-block':'none'" (click)="onSelect()"> Select </button>
      <button class="btn btn-primary btn-third"> Edit </button>
     </div>
     
   </div>
  `,
  host: {
    'class':'item-entity',
  },
  styles: [
    `
      
    `
  ],
  directives: [
    ItemVariationEntityComponent
  ]
})

export class ItemEntityComponent {

  @Input() public itemData: ItemEntity;

  @Input() public passThroughSelection: boolean = false;

  @Input() public withVariations: boolean = false;

  @Input() public selectable: boolean = false;

  @Input() public selectableVariation: boolean = false;

  @Input() public definedStyle: string = null;

  public selectedVariationData: ItemGridAnswerObject = null;

  @Output() public selectedVariation = new EventEmitter();

  @Output() public selectedItem = new EventEmitter();

	/** -----------------------------------------------------------------------
   *
   *  ----------------------------------------------------------------------- */
  public onSelect() {
    var entity: ItemGridAnswerObject = {
      type: ItemGridAnswerType.OnlyItem,
      item: this.itemData
    };
    this.selectedItem.emit(entity);
  }

  /** -----------------------------------------------------------------------
   *
   *  ----------------------------------------------------------------------- */
  public showItemVariations() {

    App.window.loadComponentIntoModalFrame(ItemVariationsGridViewComponent,{
      title:"Item variation view",
      data: {
        selectable: this.selectableVariation,
        variations: this.itemData.variations
      },
      onClose: () => {

      },
      onSuccess: (data) => {

        this.selectedVariationData = <ItemGridAnswerObject>data;
        this.itemData.definedStyle = this.definedStyle;
        data.item = this.itemData;
        data.type = ItemGridAnswerType.BothEntities;

        if(this.passThroughSelection) {
          this.selectedVariation.emit(data);
        }

      },
      onFailure: (data) => {

      },
      getReference: (ref)=>{

      }

    });

  }

}
