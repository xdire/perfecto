"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var ItemVariationEntityComponent = (function () {
    function ItemVariationEntityComponent() {
        this.selectable = false;
        this.selectedVariation = new core_1.EventEmitter();
    }
    ItemVariationEntityComponent.prototype.onSelect = function () {
        this.selectedVariation.emit(this.itemVariationData);
    };
    __decorate([
        core_1.Input(), 
        __metadata('design:type', Object)
    ], ItemVariationEntityComponent.prototype, "itemVariationData", void 0);
    __decorate([
        core_1.Input(), 
        __metadata('design:type', Boolean)
    ], ItemVariationEntityComponent.prototype, "selectable", void 0);
    __decorate([
        core_1.Output(), 
        __metadata('design:type', Object)
    ], ItemVariationEntityComponent.prototype, "selectedVariation", void 0);
    ItemVariationEntityComponent = __decorate([
        core_1.Component({
            selector: 'ivent',
            template: "\n   <div class=\"item-entity-card\">\n   \n     <div class=\"item-entity-image\">\n      <img src=\"{{itemVariationData.info?.images?.titleImage?.src? itemVariationData.info?.images?.titleImage?.src+'s.'+itemVariationData.info?.images?.titleImage?.ext : 'https://s3.amazonaws.com/afimg/nopicture.png'}}\">\n     </div>\n     \n     <div class=\"item-entity-info\">\n      <div class=\"item-entity-info-title\">Code: {{itemVariationData.code}}</div>\n      <div class=\"item-entity-info-desc\">Color: {{itemVariationData.color}}<br>Size: {{itemVariationData.size}}</div>\n     </div>\n     \n     <div class=\"right-text item-entity-buttons\">\n      <button [style.display] = \"!selectable?'none':'inline-block'\" class=\"btn btn-primary btn-third\" (click)=\"onSelect()\">Select</button>\n      <button class=\"btn btn-primary btn-third\">Edit</button>\n     </div>\n     \n   </div>\n  ",
            host: {
                'class': 'item-var-entity',
            },
            styles: [
                "\n    \n    "
            ]
        }), 
        __metadata('design:paramtypes', [])
    ], ItemVariationEntityComponent);
    return ItemVariationEntityComponent;
}());
exports.ItemVariationEntityComponent = ItemVariationEntityComponent;
//# sourceMappingURL=itemvar.wcomp.js.map