import {Component} from "@angular/core";
import {WindowComponent} from "../../../system/window/window.component";
import {ComponentImplementingModalFramePass} from "../../../system/entities/system.entities";
import {ItemApiConnector, ItemVariationEntity} from "../../../system/connection/itemapiconnector";
import {ApiConnectorComponent} from "../../../system/connection/apiconnector.component";
import {ItemVariationEntityComponent} from "./itemvar.wcomp";
import {ItemGridAnswerObject, ItemGridAnswerType} from "./itemgrid.wcomp";

@Component({
  selector: 'itemv-grid-view',
  template: `
   <div>
      
     <div class="filter-block">
     
      <table class="filters-table" [class.hiddenFrame]="filterAmount==0">
      
        <tr>
          
          <!--Colors-->
          <td> 
            <button 
              class="btn btn-primary filter-button" 
              *ngFor="let size of sizeFilter"
              [class.filter-selected]="size==selectedSize"
              (click)="applyFilter(size,0);">{{size}}</button>
            <button 
              class="btn btn-danger filter-button" 
              [class.hiddenFrame]="selectedSize==null" 
              (click)="clearFilter(0)">Clear</button>
          </td>
          <!--Sizes-->
          <td> 
            <button 
            class="btn btn-primary filter-button" 
            *ngFor="let color of colorFilter" 
            [class.filter-selected]="color==selectedColor"
            (click)="applyFilter(color,1);">{{color}}</button>
            <button 
              class="btn btn-danger filter-button" 
              [class.hiddenFrame]="selectedColor==null" 
              (click)="clearFilter(1)">Clear</button>
          </td>
          
        </tr>
        
      </table>
      
     </div>
     
     <div> 
        <ivent *ngFor="let item of renderList" [itemVariationData]="item" [selectable]="selectable" (selectedVariation)="onSelect($event)"> </ivent>
     </div>
     
   </div>
  `,
  host: {
    'class':'itemv-grid-view',
  },
  styles: [
    `
      .filters-table {box-shadow: 0 10px 10px rgba(0,0,0,0.1); width: 100%;}
      .filters-table tr td {padding: 10px; max-width: 60%; vertical-align: top; min-width: 192px;}
      .filter-block {padding: 8px; margin-bottom: 8px;}
      .filter-button {margin: 2px; vertical-align: top;}
      .filter-selected {background-color: lightseagreen}
    `
  ],
  directives: [
    ItemVariationEntityComponent
  ]
})

export class ItemVariationsGridViewComponent extends WindowComponent implements ComponentImplementingModalFramePass {

  dataObject:any;
  closingMethod:any;
  successCallback:any;
  failureCallback:any;

  public itemList: ItemVariationEntity[] = [];

  public renderList: ItemVariationEntity[] = [];

  public sizeFilter: String[] = [];

  public colorFilter: String[] = [];

  public filterAmount: number = 0;

  public selectedSize: any = null;

  public selectedColor: any = null;

  private selectable: boolean = false;

  private connector: ItemApiConnector;

  constructor() {
    this.connector = ApiConnectorComponent.getItemApiConnector();
  }

  /** -----------------------------------------------------------------
   *           Execute onload method for Window Component
   *  -----------------------------------------------------------------
   */
  public onLoad() {

    this.itemList = <ItemVariationEntity[]>this.dataObject.variations;

    if(typeof this.dataObject.selectable === "boolean"){
      this.selectable = this.dataObject.selectable;
    }

    this.renderList = this.itemList;

    this.refillFilters(null);

  }

	/** -----------------------------------------------------------------
   *
   *  -----------------------------------------------------------------
   *
   * @param variation
   */
  public onSelect(variation: ItemVariationEntity) {

    var entity: ItemGridAnswerObject = {
      type: ItemGridAnswerType.OnlyVariation,
      variation: variation
    };

    this.successCallback(entity);
    this.closingMethod();

  }

	/** -----------------------------------------------------------------
   *
   *
   *  -----------------------------------------------------------------
   *
   *  @param filter
   *  @param type
   */
  public applyFilter(filter, type) {

    if(type == 0) {
      if (filter !== null)
        this.selectedSize = filter;
    } else if (type == 1) {
      if (filter !== null)
        this.selectedColor = filter;
    }

    var filters = {colors:[],sizes:[]};
    var addedColors = {};
    var addedSizes = {};

    this.renderList = this.itemList.filter((item) => {

        var passBySize = false;
        var passByColor = false;

        if(item.size == this.selectedSize || this.selectedSize == null)
          passBySize = true;

        if(item.color == this.selectedColor || this.selectedColor == null)
          passByColor = true;

        if(passBySize && passByColor) {

          if(item.color !== null && !addedColors.hasOwnProperty(item.color)) {
            filters.colors.push(item.color);
            addedColors[item.color] = true;
          }

          if(item.size !== null && !addedSizes.hasOwnProperty(item.size)) {
            filters.sizes.push(item.size);
            addedSizes[item.size] = true;
          }

          return true;

        }

    });

    this.refillFilters(filters);

  }

	/** -----------------------------------------------------------------
   *
   *
   *  -----------------------------------------------------------------
   *
   *  @param type
   */
  public clearFilter(type) {

    if(type == 0) {
      this.selectedSize = null;
    } else if (type == 1) {
      this.selectedColor = null;
    }

    this.applyFilter(null,type);

  }

	/** -----------------------------------------------------------------
   *
   *
   *  -----------------------------------------------------------------
   *
   *  @param filters
   */
  public refillFilters(filters: {colors: Array<string>, sizes: Array<string>} = null) {

    if(filters !== null) {

      this.colorFilter = filters.colors;
      this.sizeFilter = filters.sizes;

    } else {

      var addedColors = {};
      var addedSizes = {};

      this.renderList.map((item) => {

        if(item.color !== null && !addedColors.hasOwnProperty(item.color)){
          this.colorFilter.push(item.color);
          addedColors[item.color] = true;
          this.filterAmount++;
        }
        if(item.size !== null && !addedSizes.hasOwnProperty(item.size)){
          this.sizeFilter.push(item.size);
          addedSizes[item.size] = true;
          this.filterAmount++;
        }

      });

      addedColors = null;
      addedSizes = null;

    }

  }

}
