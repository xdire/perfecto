"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var window_component_1 = require("../../../system/window/window.component");
var apiconnector_component_1 = require("../../../system/connection/apiconnector.component");
var itementity_wcomp_1 = require("./itementity.wcomp");
var ItemGridViewComponent = (function (_super) {
    __extends(ItemGridViewComponent, _super);
    function ItemGridViewComponent() {
        this.message = "Loading...";
        this.itemList = [];
        this.variations = false;
        this.selectable = false;
        this.selectableVariation = false;
        this.passThrough = false;
        this.selectedVariation = null;
        this.selectedItem = null;
        this.definedStyle = null;
        this.connector = apiconnector_component_1.ApiConnectorComponent.getItemApiConnector();
    }
    /** -----------------------------------------------------------------
   *
   *  -----------------------------------------------------------------
   * @param data
   */
    ItemGridViewComponent.prototype.onSelectedVariation = function (data) {
        data.item.variations = null;
        if (this.passThrough) {
            this.successCallback(data);
            this.closingMethod();
        }
        else {
            this.selectedVariation = data.variation;
        }
    };
    /** -----------------------------------------------------------------
   *
   *  -----------------------------------------------------------------
   * @param data
   */
    ItemGridViewComponent.prototype.onSelectedItem = function (data) {
        if (this.passThrough) {
            data.item.variations = null;
            if (this.selectableVariation !== null) {
                data.type = ItemGridAnswerType.BothEntities;
                data.variation = this.selectableVariation;
            }
            this.successCallback(data);
            this.closingMethod();
        }
        else {
            this.selectedItem = data.item;
        }
    };
    /** -----------------------------------------------------------------
   *           Execute onload method for Window Component
   *  -----------------------------------------------------------------
   */
    ItemGridViewComponent.prototype.onLoad = function () {
        var d = this.dataObject;
        if (d.hasOwnProperty("variations")) {
            this.variations = d.variations;
        }
        if (d.hasOwnProperty("selectableItem")) {
            this.selectable = d.selectableItem;
        }
        if (d.hasOwnProperty("selectableVariation")) {
            this.selectableVariation = d.selectableVariation;
        }
        if (d.hasOwnProperty("onVariationSelectPassThrough")) {
            this.passThrough = d.onVariationSelectPassThrough;
        }
        if (d.hasOwnProperty("requestType")) {
            // Select by style code
            if (d.requestType == ItemGridViewRequestType.ByStyleCode) {
                var p = d.requestParameters;
                if (p.hasOwnProperty("style")) {
                    this.definedStyle = p.style;
                    this.getItemsForItemStyle(p.style);
                }
            }
            else if (d.requestType == ItemGridViewRequestType.ByVariationId) {
            }
            else if (d.requestType == ItemGridViewRequestType.ByItemId) {
            }
            else if (d.requestType == ItemGridViewRequestType.ByCombination) {
            }
        }
    };
    /** -----------------------------------------------------------------
   *            Select Item by Item Style Id
   *  -----------------------------------------------------------------
   */
    ItemGridViewComponent.prototype.getItemsForItemStyle = function (style) {
        var _this = this;
        this.connector.getItemsByStyleId(style).subscribe(function (items) {
            _this.itemList = items;
        }, function (err) {
            _this.message = "No items found for that style";
        });
    };
    /** -----------------------------------------------------------------
   *
   *  -----------------------------------------------------------------
   */
    ItemGridViewComponent.prototype.getItemsByVariationId = function (variationId) {
    };
    /** -----------------------------------------------------------------
   *
   *  -----------------------------------------------------------------
   */
    ItemGridViewComponent.prototype.getItemByItemId = function (itemId) {
    };
    ItemGridViewComponent = __decorate([
        core_1.Component({
            selector: 'item-grid-view',
            template: "\n   <div>\n   \n     <div *ngIf=\"itemList.length == 0\"> {{message}} </div>\n     \n     <div> \n        <ient *ngFor=\"let item of itemList\" \n        [definedStyle]=\"definedStyle\"\n        [itemData]=\"item\" \n        [withVariations]=\"variations\"\n        [passThroughSelection]=\"passThrough\"\n        [selectable]=\"selectable\" \n        [selectableVariation]=\"selectableVariation\"\n        (selectedVariation)=\"onSelectedVariation($event)\"\n        (selectedItem)=\"onSelectedItem($event)\">\n        </ient>\n     </div>\n     \n   </div>\n  ",
            host: {
                'class': 'addscanner',
            },
            styles: [
                "\n    \n    "
            ],
            directives: [
                itementity_wcomp_1.ItemEntityComponent
            ]
        }), 
        __metadata('design:paramtypes', [])
    ], ItemGridViewComponent);
    return ItemGridViewComponent;
}(window_component_1.WindowComponent));
exports.ItemGridViewComponent = ItemGridViewComponent;
(function (ItemGridAnswerType) {
    ItemGridAnswerType[ItemGridAnswerType["BothEntities"] = 0] = "BothEntities";
    ItemGridAnswerType[ItemGridAnswerType["OnlyItem"] = 1] = "OnlyItem";
    ItemGridAnswerType[ItemGridAnswerType["OnlyVariation"] = 2] = "OnlyVariation";
})(exports.ItemGridAnswerType || (exports.ItemGridAnswerType = {}));
var ItemGridAnswerType = exports.ItemGridAnswerType;
(function (ItemGridViewRequestType) {
    ItemGridViewRequestType[ItemGridViewRequestType["ByStyleCode"] = 0] = "ByStyleCode";
    ItemGridViewRequestType[ItemGridViewRequestType["ByVariationId"] = 1] = "ByVariationId";
    ItemGridViewRequestType[ItemGridViewRequestType["ByItemId"] = 2] = "ByItemId";
    ItemGridViewRequestType[ItemGridViewRequestType["ByCombination"] = 3] = "ByCombination";
})(exports.ItemGridViewRequestType || (exports.ItemGridViewRequestType = {}));
var ItemGridViewRequestType = exports.ItemGridViewRequestType;
//# sourceMappingURL=itemgrid.wcomp.js.map