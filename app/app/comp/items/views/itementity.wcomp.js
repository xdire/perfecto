"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var itemvar_wcomp_1 = require("./itemvar.wcomp");
var app_1 = require("../../../system/app");
var itemvargrid_wcomp_1 = require("./itemvargrid.wcomp");
var itemgrid_wcomp_1 = require("./itemgrid.wcomp");
var ItemEntityComponent = (function () {
    function ItemEntityComponent() {
        this.passThroughSelection = false;
        this.withVariations = false;
        this.selectable = false;
        this.selectableVariation = false;
        this.definedStyle = null;
        this.selectedVariationData = null;
        this.selectedVariation = new core_1.EventEmitter();
        this.selectedItem = new core_1.EventEmitter();
    }
    /** -----------------------------------------------------------------------
   *
   *  ----------------------------------------------------------------------- */
    ItemEntityComponent.prototype.onSelect = function () {
        var entity = {
            type: itemgrid_wcomp_1.ItemGridAnswerType.OnlyItem,
            item: this.itemData
        };
        this.selectedItem.emit(entity);
    };
    /** -----------------------------------------------------------------------
     *
     *  ----------------------------------------------------------------------- */
    ItemEntityComponent.prototype.showItemVariations = function () {
        var _this = this;
        app_1.App.window.loadComponentIntoModalFrame(itemvargrid_wcomp_1.ItemVariationsGridViewComponent, {
            title: "Item variation view",
            data: {
                selectable: this.selectableVariation,
                variations: this.itemData.variations
            },
            onClose: function () {
            },
            onSuccess: function (data) {
                _this.selectedVariationData = data;
                _this.itemData.definedStyle = _this.definedStyle;
                data.item = _this.itemData;
                data.type = itemgrid_wcomp_1.ItemGridAnswerType.BothEntities;
                if (_this.passThroughSelection) {
                    _this.selectedVariation.emit(data);
                }
            },
            onFailure: function (data) {
            },
            getReference: function (ref) {
            }
        });
    };
    __decorate([
        core_1.Input(), 
        __metadata('design:type', Object)
    ], ItemEntityComponent.prototype, "itemData", void 0);
    __decorate([
        core_1.Input(), 
        __metadata('design:type', Boolean)
    ], ItemEntityComponent.prototype, "passThroughSelection", void 0);
    __decorate([
        core_1.Input(), 
        __metadata('design:type', Boolean)
    ], ItemEntityComponent.prototype, "withVariations", void 0);
    __decorate([
        core_1.Input(), 
        __metadata('design:type', Boolean)
    ], ItemEntityComponent.prototype, "selectable", void 0);
    __decorate([
        core_1.Input(), 
        __metadata('design:type', Boolean)
    ], ItemEntityComponent.prototype, "selectableVariation", void 0);
    __decorate([
        core_1.Input(), 
        __metadata('design:type', String)
    ], ItemEntityComponent.prototype, "definedStyle", void 0);
    __decorate([
        core_1.Output(), 
        __metadata('design:type', Object)
    ], ItemEntityComponent.prototype, "selectedVariation", void 0);
    __decorate([
        core_1.Output(), 
        __metadata('design:type', Object)
    ], ItemEntityComponent.prototype, "selectedItem", void 0);
    ItemEntityComponent = __decorate([
        core_1.Component({
            selector: 'ient',
            template: "\n   <div class=\"item-entity-card\">\n      \n     <div class=\"item-entity-image\">\n      <img src=\"{{itemData.info?.images?.titleImage?.src? itemData.info?.images?.titleImage?.src+'s.'+itemData.info?.images?.titleImage?.ext : 'https://s3.amazonaws.com/afimg/nopicture.png'}}\">\n     </div>\n     \n     <div class=\"item-entity-info\">\n      <div class=\"item-entity-info-brand\">{{itemData.brand}}</div>\n      <div class=\"item-entity-info-title\">{{itemData.title}}</div>\n      <div class=\"item-entity-info-desc\">{{itemData.description?.short}}</div>\n     </div>\n     \n     <div class=\"item-entity-buttons\">\n      <button class=\"btn btn-primary btn-third\" [style.display] = \"withVariations?'inline-block':'none'\" (click)=\"showItemVariations()\"> Variations </button>\n      <button class=\"btn btn-primary btn-third\" [style.display] = \"selectable?'inline-block':'none'\" (click)=\"onSelect()\"> Select </button>\n      <button class=\"btn btn-primary btn-third\"> Edit </button>\n     </div>\n     \n   </div>\n  ",
            host: {
                'class': 'item-entity',
            },
            styles: [
                "\n      \n    "
            ],
            directives: [
                itemvar_wcomp_1.ItemVariationEntityComponent
            ]
        }), 
        __metadata('design:paramtypes', [])
    ], ItemEntityComponent);
    return ItemEntityComponent;
}());
exports.ItemEntityComponent = ItemEntityComponent;
//# sourceMappingURL=itementity.wcomp.js.map