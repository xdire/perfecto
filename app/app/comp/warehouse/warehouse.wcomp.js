"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var window_component_1 = require("../../system/window/window.component");
var menu_att_component_1 = require("../../system/menu/menu.att.component");
var warehouse_additem_1 = require("./warehouse.additem");
var warehouse_listitems_1 = require("./warehouse.listitems");
var warehouse_listloc_1 = require("./warehouse.listloc");
var app_1 = require("../../system/app");
var WarehouseComponent = (function (_super) {
    __extends(WarehouseComponent, _super);
    /** ------------------------------------------------------------------------------
     *
     *  ------------------------------------------------------------------------------ */
    function WarehouseComponent() {
        this.currentVisibleComponent = 0;
        this.menuItems = [
            {
                className: "",
                title: "W:",
                callable: this.executeMenuSelectWarehouse.bind(this)
            },
            {
                className: "fa fa-plus",
                title: "",
                callable: this.executeMenuSwitchToAddComponent.bind(this)
            },
            {
                className: "fa fa-list",
                title: "",
                callable: this.executeMenuSwitchToListComponent.bind(this)
            },
            {
                className: "fa fa-object-group",
                title: "",
                callable: this.executeMenuSwitchToLocationComponent.bind(this)
            }
        ];
        _super.prototype.constructor.call(this);
        this.isRoot = true;
        this.isWithBar = true;
        this.isWithMenu = true;
        this.publicName = "warehouse";
    }
    /** ------------------------------------------------------------------------------
     *
     *  ------------------------------------------------------------------------------ */
    WarehouseComponent.prototype.getMenuOptions = function (something) {
        console.log(something);
    };
    /** ------------------------------------------------------------------------------
     *
     *  ------------------------------------------------------------------------------ */
    WarehouseComponent.prototype.onCreate = function () {
        console.log("CREATES A WAREHOUSE WINDOW");
        this.executeMenuSwitchToAddComponent(null, null);
    };
    /** ------------------------------------------------------------------------------
     *
     *  ------------------------------------------------------------------------------ */
    WarehouseComponent.prototype.executeMenuSelectWarehouse = function (event, menuId) {
        this.menuItems[menuId].title = "W:1";
    };
    /**
     *
     * @param event
     * @param menuId
     */
    WarehouseComponent.prototype.executeMenuSwitchToAddComponent = function (event, menuId) {
        var _this = this;
        this.currentVisibleComponent = 0;
        this.closeCurrentTab();
        app_1.App.window.loadComponentIntoLocation(warehouse_additem_1.WarehouseAddItemComponent, this.contentContainer, function (ref) {
            _this.openNewTab(ref);
        });
    };
    /** ------------------------------------------------------------------------------
     *
     *  ------------------------------------------------------------------------------ */
    WarehouseComponent.prototype.executeMenuSwitchToListComponent = function (event, menuId) {
        var _this = this;
        this.currentVisibleComponent = 1;
        this.closeCurrentTab();
        app_1.App.window.loadComponentIntoLocation(warehouse_listitems_1.WarehouseListItemsComponent, this.contentContainer, function (ref) {
            _this.openNewTab(ref);
        });
    };
    /** ------------------------------------------------------------------------------
     *
     *  ------------------------------------------------------------------------------ */
    WarehouseComponent.prototype.executeMenuSwitchToLocationComponent = function (event, menuId) {
        var _this = this;
        this.currentVisibleComponent = 2;
        this.closeCurrentTab();
        app_1.App.window.loadComponentIntoLocation(warehouse_listloc_1.WarehouseListLocationsComponent, this.contentContainer, function (ref) {
            _this.openNewTab(ref);
        });
    };
    /** ------------------------------------------------------------------------------
     *
     *  ------------------------------------------------------------------------------ */
    WarehouseComponent.prototype.openNewTab = function (ref) {
        this.currentComponentRef = ref;
        this.currentComponent = ref.instance;
        if (this.currentComponent != null) {
            this.currentComponent._isVisible = true;
            this.currentComponent.onFocus();
        }
    };
    /** ------------------------------------------------------------------------------
     *
     *  ------------------------------------------------------------------------------ */
    WarehouseComponent.prototype.closeCurrentTab = function () {
        if (this.currentComponentRef != null) {
            if (this.currentComponent != null)
                this.currentComponent.onLostFocus();
            this.currentComponentRef.destroy();
        }
    };
    __decorate([
        core_1.ViewChild("wcompframe", { read: core_1.ViewContainerRef }), 
        __metadata('design:type', core_1.ViewContainerRef)
    ], WarehouseComponent.prototype, "contentContainer", void 0);
    WarehouseComponent = __decorate([
        core_1.Component({
            selector: "warehouse",
            template: "\n    \n    <menubar [menuItemsList]=\"menuItems\" [menuBindedComponent]=\"this\">\n    </menubar>\n\n    <div class=\"wcontainer\">\n      <div class=\"container-fluid\">\n        <div #wcompframe></div>\n        \n      </div>\n    </div>\n    \n  ",
            styles: [
                "\n      .wcontainer {\n        width: calc(100% - 70px);\n        margin-left: 60px;\n        display: inline-block;\n        vertical-align: top;\n      }\n    "
            ],
            directives: [menu_att_component_1.AttachableMenuComponent, warehouse_additem_1.WarehouseAddItemComponent, warehouse_listitems_1.WarehouseListItemsComponent, warehouse_listloc_1.WarehouseListLocationsComponent]
        }), 
        __metadata('design:paramtypes', [])
    ], WarehouseComponent);
    return WarehouseComponent;
}(window_component_1.WindowComponent));
exports.WarehouseComponent = WarehouseComponent;
//# sourceMappingURL=warehouse.wcomp.js.map