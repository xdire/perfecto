"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var apiconnector_component_1 = require("../../system/connection/apiconnector.component");
var whslocation_wcomp_1 = require("./location/whslocation.wcomp");
var whsloc_sys_1 = require("./location/whsloc.sys");
var app_1 = require("../../system/app");
var whsaddloc_wcomp_1 = require("./location/whsaddloc.wcomp");
var WarehouseListLocationsComponent = (function () {
    function WarehouseListLocationsComponent() {
        this._isVisible = false;
        this.currentWarehouse = 1;
    }
    /** -------------------------------------------------------------------------------------------------
   *
   *
   *
   *  -------------------------------------------------------------------------------------------------
   *
   * @param warehouseId
   */
    WarehouseListLocationsComponent.prototype.addNewLocation = function (warehouseId) {
        var _this = this;
        app_1.App.window.loadComponentIntoModalFrame(whsaddloc_wcomp_1.WarehouseAddLocationComponent, {
            title: "Some sex with locations?",
            data: { id: null, parentId: null, warehouseId: warehouseId },
            onLoad: function () {
            },
            onSuccess: function (data) {
                console.log("It's success fucking fuck");
                console.log(data);
                _this.allLocationList.appendToElementWithId(null, data);
            },
            onClose: function () {
            },
            onFailure: function (error) {
                console.log("It's bad sucking shit fucking fuck");
                alert("Some error happened, operation can't be done");
            },
            getReference: function (ref) {
            }
        });
    };
    /** -------------------------------------------------------------------------------------------------
   *
   *              CREATE LIST OF WAREHOUSE LOCATIONS FROM RAW API DATA
   *
   *  -------------------------------------------------------------------------------------------------
   *
   * @param entity
   */
    WarehouseListLocationsComponent.prototype.createWarehouseLocationList = function (entity) {
        this.allLocationList.createFromArray(entity);
    };
    /** -------------------------------------------------------------------------------------------------
   *
   *
   *
   * -------------------------------------------------------------------------------------------------
   *
   *
   */
    WarehouseListLocationsComponent.prototype.onFocus = function () {
        var _this = this;
        this.allLocationList = new whsloc_sys_1.WarehouseLocationsListSystemTable();
        var aapiconn = apiconnector_component_1.ApiConnectorComponent.getWarehouseApiConnector();
        aapiconn.getAllLocationsForWarehouseId(1).subscribe(function (entity) {
            _this.createWarehouseLocationList(entity);
        });
    };
    WarehouseListLocationsComponent.prototype.onLostFocus = function () {
    };
    WarehouseListLocationsComponent.prototype.onParentUnload = function () {
    };
    __decorate([
        core_1.Input(), 
        __metadata('design:type', Boolean)
    ], WarehouseListLocationsComponent.prototype, "_isVisible", void 0);
    __decorate([
        core_1.Input(), 
        __metadata('design:type', Number)
    ], WarehouseListLocationsComponent.prototype, "currentWarehouse", void 0);
    WarehouseListLocationsComponent = __decorate([
        core_1.Component({
            selector: "whslistloc",
            host: {
                '[class.hiddenFrame]': '!_isVisible'
            },
            template: "\n    <div> <h1> List of locations </h1> </div>\n    <div>\n      <div>\n        <div class=\"location-root container-fluid scrollable-visible\">\n          \n         <div style=\"display: inline-block;\">\n           <button class=\"btn btn-primary\" (click)=\"addNewLocation(currentWarehouse)\"><i class=\"fa fa-plus\"></i></button>\n         </div>\n         \n         <div class=\"list-item\" style=\"display: inline-block; padding-top: 4px; width: auto;\"> Warehouse root </div>\n         \n        </div>\n      </div>\n      \n      <ul class=\"wtable\">\n      \n        <li *ngFor=\"let loc of allLocationList.list; let i = index;\">\n          \n          <whs-loc [location]=\"loc\" [name]=\"loc.name\" [id]=\"loc.id\" [warehouseId]=\"loc.warehouseId\" [parentId]=\"loc.parentId\" [childLocations]=\"loc.childs\" [_isVisible]=\"!loc.isDeleted\" [isExpanded]=\"depth!=1\" [listObject]=\"allLocationList\"> </whs-loc>\n          \n        </li>\n        \n      </ul>\n      \n    </div>\n  ",
            styles: ["\n    .location-root {\n        background-color: #dedede;\n        border-radius: 8px;\n        display: inline-block;\n        padding: 4px;\n        padding-left: 16px;\n        padding-right: 16px;\n        min-width: 192px;\n      }\n      \n  "],
            directives: [
                whslocation_wcomp_1.WarehouseLocationComponent
            ]
        }), 
        __metadata('design:paramtypes', [])
    ], WarehouseListLocationsComponent);
    return WarehouseListLocationsComponent;
}());
exports.WarehouseListLocationsComponent = WarehouseListLocationsComponent;
//# sourceMappingURL=warehouse.listloc.js.map