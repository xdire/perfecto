"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var window_component_1 = require("../../../system/window/window.component");
var apiconnector_component_1 = require("../../../system/connection/apiconnector.component");
var WarehouseItemShipConfirmationComponent = (function (_super) {
    __extends(WarehouseItemShipConfirmationComponent, _super);
    function WarehouseItemShipConfirmationComponent() {
        this.message = "";
        this.quantity = 1;
        this.item = null;
        this.whsApiConn = null;
        this.whsApiConn = apiconnector_component_1.ApiConnectorComponent.getWarehouseApiConnector();
    }
    WarehouseItemShipConfirmationComponent.prototype.onLoad = function () {
        this.item = this.dataObject;
    };
    WarehouseItemShipConfirmationComponent.prototype.increaseQty = function () {
        this.quantity++;
    };
    WarehouseItemShipConfirmationComponent.prototype.decreaseQty = function () {
        if (this.quantity > 1) {
            this.quantity--;
        }
    };
    WarehouseItemShipConfirmationComponent.prototype.saveForm = function ($ev) {
        $ev.preventDefault();
        this.whsApiConn.shipItemFromWarehouse(this.item.warehouseId, this.quantity).subscribe(function () {
        }, function (err) {
        });
    };
    WarehouseItemShipConfirmationComponent.prototype.closeForm = function ($ev) {
        $ev.preventDefault();
    };
    WarehouseItemShipConfirmationComponent = __decorate([
        core_1.Component({
            selector: "whswipeoffitem",
            host: {},
            template: "\n    <form>\n          \n        <h2> Fill properties attached to your reservation</h2>\n        \n        <div class=\"message\">{{message}}</div>\n        \n        <div class=\"left-side\">\n          \n          <h3> Select quantity which you want to ship</h3>\n               \n        </div>\n        \n        <div class=\"right-side\">\n          <button class=\"btn btn-primary btn-wide\"\n            (click)=\"increaseQty()\">\n            <i class=\"fa fa-chevron-up\" aria-hidden=\"true\"></i>\n          </button>\n          <div class=\"header-block\">{{quantity}}</div>\n          <button class=\"btn btn-primary btn-wide\"\n            (click)=\"decreaseQty()\">\n            <i class=\"fa fa-chevron-down\" aria-hidden=\"true\"></i>\n          </button>\n        </div>\n        \n        <div class=\"center-text offset-top-m\">\n          <button class=\"btn btn-primary btn-wide\" (click)=\"saveForm($event);\"> Save </button>\n          <button class=\"btn btn-primary btn-wide\" (click)=\"closeForm($event);\"> Cancel </button>\n        </div>\n          \n    </form>\n  ",
            styles: ["\n    \n    .message {\n      width:100%;\n      min-height:1px; \n      text-align:center\n    }\n    \n    .left-side {\n        color: #333333;\n        display: inline-block;\n        width: 49%;\n        vertical-align: top;\n     }\n      \n    .form-icons {\n      min-width:18px;\n    }\n      \n    .form-label {\n      min-width: 20%;\n    }\n      \n    .left-side input {\n      min-width: 192px;\n      width: 70%;\n      display: inline-block;\n      padding: 4px;\n      border-radius: 2px;\n      border:1px solid #CCCCCC;\n      margin: 2px;\n    }\n      \n    .right-side {\n      color: #333333;\n      text-align: center;\n      display: inline-block;\n      width: 49%;\n      vertical-align: top;\n    }\n      \n  "],
            directives: [],
            pipes: []
        }), 
        __metadata('design:paramtypes', [])
    ], WarehouseItemShipConfirmationComponent);
    return WarehouseItemShipConfirmationComponent;
}(window_component_1.WindowComponent));
exports.WarehouseItemShipConfirmationComponent = WarehouseItemShipConfirmationComponent;
//# sourceMappingURL=shipconf.wcomp.js.map