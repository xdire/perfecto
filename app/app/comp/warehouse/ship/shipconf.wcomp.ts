import {Component} from "@angular/core";
import {WindowComponent} from "../../../system/window/window.component";
import {ComponentImplementingModalFramePass} from "../../../system/entities/system.entities";
import {WarehouseApiConnector} from "../../../system/connection/whsapiconnector";
import {ApiConnectorComponent} from "../../../system/connection/apiconnector.component";
import {WarehouseScannedItemEntity} from "../entities/whsent.comp";

@Component({
  selector: "whswipeoffitem",
  host: {

  },
  template: `
    <form>
          
        <h2> Fill properties attached to your reservation</h2>
        
        <div class="message">{{message}}</div>
        
        <div class="left-side">
          
          <h3> Select quantity which you want to ship</h3>
               
        </div>
        
        <div class="right-side">
          <button class="btn btn-primary btn-wide"
            (click)="increaseQty()">
            <i class="fa fa-chevron-up" aria-hidden="true"></i>
          </button>
          <div class="header-block">{{quantity}}</div>
          <button class="btn btn-primary btn-wide"
            (click)="decreaseQty()">
            <i class="fa fa-chevron-down" aria-hidden="true"></i>
          </button>
        </div>
        
        <div class="center-text offset-top-m">
          <button class="btn btn-primary btn-wide" (click)="saveForm($event);"> Save </button>
          <button class="btn btn-primary btn-wide" (click)="closeForm($event);"> Cancel </button>
        </div>
          
    </form>
  `,
  styles:[`
    
    .message {
      width:100%;
      min-height:1px; 
      text-align:center
    }
    
    .left-side {
        color: #333333;
        display: inline-block;
        width: 49%;
        vertical-align: top;
     }
      
    .form-icons {
      min-width:18px;
    }
      
    .form-label {
      min-width: 20%;
    }
      
    .left-side input {
      min-width: 192px;
      width: 70%;
      display: inline-block;
      padding: 4px;
      border-radius: 2px;
      border:1px solid #CCCCCC;
      margin: 2px;
    }
      
    .right-side {
      color: #333333;
      text-align: center;
      display: inline-block;
      width: 49%;
      vertical-align: top;
    }
      
  `],
  directives:[

  ],
  pipes:[

  ]
})

export class WarehouseItemShipConfirmationComponent extends WindowComponent implements ComponentImplementingModalFramePass {

  public message: string = "";

  public quantity: number = 1;

  public item: WarehouseScannedItemEntity = null;

  dataObject:any;
  closingMethod:any;
  successCallback:any;
  failureCallback:any;

  whsApiConn: WarehouseApiConnector = null;

  constructor() {
    this.whsApiConn = ApiConnectorComponent.getWarehouseApiConnector();
  }

  onLoad() {
    this.item = <WarehouseScannedItemEntity>this.dataObject;
  }

  public increaseQty() : void {
    this.quantity++;
  }

  public decreaseQty() : void {
    if(this.quantity > 1) {
      this.quantity--;
    }
  }

  public saveForm($ev) {

    $ev.preventDefault();
    this.whsApiConn.shipItemFromWarehouse(this.item.warehouseId,this.quantity).subscribe(() => {

    }, (err) => {

    });

  }

  public closeForm($ev) {
    $ev.preventDefault();
  }

}
