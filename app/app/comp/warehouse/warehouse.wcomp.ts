import {Component, ComponentRef, ViewContainerRef, ViewChild} from "@angular/core";
import {WindowComponent} from "../../system/window/window.component";
import {AttachableMenuComponent, AttachableMenuItem} from "../../system/menu/menu.att.component";
import {WarehouseAddItemComponent} from "./warehouse.additem";
import {WarehouseListItemsComponent} from "./warehouse.listitems";
import {WarehouseListLocationsComponent} from "./warehouse.listloc";
import {App} from "../../system/app";
import {SwitchableComponent} from "../../system/entities/system.entities";

@Component({
  selector:"warehouse",
  template:`
    
    <menubar [menuItemsList]="menuItems" [menuBindedComponent]="this">
    </menubar>

    <div class="wcontainer">
      <div class="container-fluid">
        <div #wcompframe></div>
        
      </div>
    </div>
    
  `,
  styles:[
    `
      .wcontainer {
        width: calc(100% - 70px);
        margin-left: 60px;
        display: inline-block;
        vertical-align: top;
      }
    `
  ],
  directives:[AttachableMenuComponent, WarehouseAddItemComponent, WarehouseListItemsComponent, WarehouseListLocationsComponent]
})

export class WarehouseComponent extends WindowComponent {

  @ViewChild("wcompframe",{ read: ViewContainerRef }) contentContainer: ViewContainerRef;

  private currentVisibleComponent: number = 0;

  private currentComponentRef: ComponentRef;

  private currentComponent: SwitchableComponent;

  private menuItems: Array<AttachableMenuItem> = [
    {
      className:"",
      title:"W:",
      callable:this.executeMenuSelectWarehouse.bind(this)
    },
    {
      className:"fa fa-plus",
      title:"",
      callable:this.executeMenuSwitchToAddComponent.bind(this)
    },
    {
      className:"fa fa-list",
      title:"",
      callable:this.executeMenuSwitchToListComponent.bind(this)
    },
    {
      className:"fa fa-object-group",
      title:"",
      callable:this.executeMenuSwitchToLocationComponent.bind(this)
    }
  ];
  /** ------------------------------------------------------------------------------
   *
   *  ------------------------------------------------------------------------------ */
  constructor() {
    super.constructor();
    this.isRoot = true;
    this.isWithBar = true;
    this.isWithMenu = true;
    this.publicName = "warehouse";
  }
  /** ------------------------------------------------------------------------------
   *
   *  ------------------------------------------------------------------------------ */
  public getMenuOptions(something){
    console.log(something);
  }
  /** ------------------------------------------------------------------------------
   *
   *  ------------------------------------------------------------------------------ */
  public onCreate() {
    console.log("CREATES A WAREHOUSE WINDOW");
    this.executeMenuSwitchToAddComponent(null,null);
  }
  /** ------------------------------------------------------------------------------
   *
   *  ------------------------------------------------------------------------------ */
  public executeMenuSelectWarehouse(event, menuId) {
    this.menuItems[menuId].title = "W:1";
  }

  /**
   *
   * @param event
   * @param menuId
   */
  public executeMenuSwitchToAddComponent(event, menuId) {

    this.currentVisibleComponent = 0;
    this.closeCurrentTab();
    App.window.loadComponentIntoLocation(WarehouseAddItemComponent, this.contentContainer, (ref)=>{
      this.openNewTab(ref);
    });

  }
  /** ------------------------------------------------------------------------------
   *
   *  ------------------------------------------------------------------------------ */
  public executeMenuSwitchToListComponent(event, menuId) {

    this.currentVisibleComponent = 1;
    this.closeCurrentTab();

    App.window.loadComponentIntoLocation(WarehouseListItemsComponent, this.contentContainer, (ref)=>{
      this.openNewTab(ref);
    });

  }
  /** ------------------------------------------------------------------------------
   *
   *  ------------------------------------------------------------------------------ */
  public executeMenuSwitchToLocationComponent(event, menuId) {

    this.currentVisibleComponent = 2;
    this.closeCurrentTab();

    App.window.loadComponentIntoLocation(WarehouseListLocationsComponent, this.contentContainer, (ref)=>{
      this.openNewTab(ref);
    });

  }
  /** ------------------------------------------------------------------------------
   *
   *  ------------------------------------------------------------------------------ */
  public openNewTab(ref: ComponentRef) {

    this.currentComponentRef = ref;
    this.currentComponent = ref.instance;
    if(this.currentComponent != null) {
      this.currentComponent._isVisible = true;
      this.currentComponent.onFocus();
    }

  }
  /** ------------------------------------------------------------------------------
   *
   *  ------------------------------------------------------------------------------ */
  public closeCurrentTab() {

    if(this.currentComponentRef != null) {
      if(this.currentComponent != null)
        this.currentComponent.onLostFocus();
      this.currentComponentRef.destroy();
    }

  }

}
