"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var window_component_1 = require("../../../system/window/window.component");
var dataaccu_sys_1 = require("../../../system/data/dataaccu.sys");
var apiconnector_component_1 = require("../../../system/connection/apiconnector.component");
var app_1 = require("../../../system/app");
var itemgrid_wcomp_1 = require("../../items/views/itemgrid.wcomp");
/** -----------------------------------------------------------------------------------------  \
 *                                Component description
 *  ----------------------------------------------------------------------------------------- */
var WarehouseItemResolveComponent = (function (_super) {
    __extends(WarehouseItemResolveComponent, _super);
    /** ------------------------------------------------------------------------------
     *
     *  ------------------------------------------------------------------------------ */
    function WarehouseItemResolveComponent() {
        this.behaviorOnProceed = 0;
        this.styleCode = "";
        this.styleSupportList = [];
        this.itemApiConn = null;
        this.listeners = [];
        _super.prototype.constructor.call(this);
        this.isRoot = false;
        this.styleSearchAccu = new dataaccu_sys_1.DataAccumulator(2048);
        this.itemApiConn = apiconnector_component_1.ApiConnectorComponent.getItemApiConnector();
    }
    WarehouseItemResolveComponent.prototype.onLoad = function () {
        var _this = this;
        this.listeners.push(app_1.App.inputScannerObserver.addCommandListener("close", function () {
            _this.closeForm(null);
        }));
    };
    WarehouseItemResolveComponent.prototype.onClose = function () {
        app_1.App.inputScannerObserver.removeListenerWithGUID(this.listeners);
    };
    WarehouseItemResolveComponent.prototype.onFocus = function () {
        this.styleInput.nativeElement.focus();
    };
    /** ------------------------------------------------------------------------------
     *
     *  ------------------------------------------------------------------------------ */
    WarehouseItemResolveComponent.prototype.ngAfterViewInit = function () {
        this.styleInput.nativeElement.focus();
    };
    /** ---------------------------------------------------------------------------------
   *
   *  ---------------------------------------------------------------------------------
   * @param $event
   */
    WarehouseItemResolveComponent.prototype.clickSave = function ($event) {
        $event.preventDefault();
        this.successCallback();
        if (this.behaviorOnProceed == 0)
            this.closingMethod();
    };
    /** ---------------------------------------------------------------------------------
   *
   *  ---------------------------------------------------------------------------------
   * @param $event
   */
    WarehouseItemResolveComponent.prototype.clickCancel = function ($event) {
        $event.preventDefault();
        this.closingMethod();
    };
    /** ---------------------------------------------------------------------------------
   *
   *
   *  ---------------------------------------------------------------------------------
   *
   */
    WarehouseItemResolveComponent.prototype.clickSearchByStyle = function () {
        var _this = this;
        var props = {
            requestType: itemgrid_wcomp_1.ItemGridViewRequestType.ByStyleCode,
            requestParameters: { style: this.styleCode },
            variations: true,
            selectableVariation: true,
            onVariationSelectPassThrough: true
        };
        app_1.App.window.loadComponentIntoModalFrame(itemgrid_wcomp_1.ItemGridViewComponent, {
            title: "Item view",
            data: props,
            onSuccess: function (data) {
                if (props.onVariationSelectPassThrough) {
                    _this.successCallback(data);
                    if (_this.behaviorOnProceed == 0)
                        _this.closingMethod();
                }
            },
            onFailure: function (data) { },
            onClose: function () { },
            getReference: function (ref) { }
        });
    };
    /** ---------------------------------------------------------------------------------
     *
     *
     *  ---------------------------------------------------------------------------------
     *
     */
    WarehouseItemResolveComponent.prototype.clickChooseDropDownStyle = function (item) {
        this.styleInput.nativeElement.focus();
        this.styleCode = item;
        this.emptyStyleSupportList();
    };
    /** ---------------------------------------------------------------------------------
     *
     *
     *  ---------------------------------------------------------------------------------
     *
     */
    WarehouseItemResolveComponent.prototype.emptyStyleSupportList = function () {
        this.styleSupportList = [];
    };
    /** ---------------------------------------------------------------------------------
     *
     *
     *  ---------------------------------------------------------------------------------
     *
     */
    WarehouseItemResolveComponent.prototype.catchTypingOfStyleName = function () {
        var _this = this;
        var to = setTimeout(function () {
            var res = _this.styleSearchAccu.getValue(_this.styleCode, 10);
            if (res.length == 0) {
                _this.itemApiConn.getStylesByStyleSubstring(_this.styleCode).subscribe(function (data) {
                    _this.styleSearchAccu.fromArray(data);
                    var a = _this.styleSearchAccu.getValue(_this.styleCode, 5);
                    if (a.length == 0)
                        _this.styleSupportList = ["Nothing found"];
                    else
                        _this.styleSupportList = a;
                }, function (err) {
                    console.log("Request failed with status " + err.status);
                });
            }
            else {
                _this.styleSupportList = res;
            }
        }, 50);
    };
    /** ------------------------------------------------------------------------------
     *
     *  ------------------------------------------------------------------------------ */
    WarehouseItemResolveComponent.prototype.closeForm = function ($e) {
        if ($e === void 0) { $e = null; }
        if ($e !== null)
            $e.preventDefault();
        this.closingMethod();
    };
    __decorate([
        core_1.ViewChild('fstyleinput'), 
        __metadata('design:type', core_1.ElementRef)
    ], WarehouseItemResolveComponent.prototype, "styleInput", void 0);
    WarehouseItemResolveComponent = __decorate([
        core_1.Component({
            selector: 'whsaddreslv',
            template: "\n     <div>\n     \n        <form class=\"center-text\">\n          \n          <div class=\"panel panel-default\">\n      \n            <div class=\"panel-heading\"> Please use fields below to search for your item</div>\n            \n            <div class=\"panel-body\">\n            \n              <form>\n                  \n                  <div class=\"row\">\n                    <div class=\"col-lg-12 input-holder\">\n                    \n                      Ok so you know the style of your item?\n                      \n                      <div>\n                        <input #fstyleinput type=\"text\" \n                        style=\"width: 50%;display: inline-block;border: 1px solid #DEDEDE;border-radius: 4px;padding: 6px;\"\n                        placeholder=\"Search this field if you know style of the item\"\n                        (input)=\"catchTypingOfStyleName()\"\n                        (click)=\"emptyStyleSupportList()\"\n                        [(ngModel)]=\"styleCode\">\n                        <div style=\"vertical-align: top; display: inline-block;\">\n                          <button class=\"btn btn-default\" type=\"submit\" (click)=\"clickSearchByStyle()\">Search Style</button>\n                        </div>\n                      </div><!-- /input-group -->\n                      \n                      <div class=\"floatingList\">\n                        <ul>\n                          <li *ngFor=\"let item of styleSupportList\" (click)=\"clickChooseDropDownStyle(item)\">\n                            {{item}}\n                          </li>\n                        </ul>\n                      </div>\n                      \n                    </div><!-- /.col-lg-6 -->\n                  </div><!-- /.row -->\n           \n              </form>\n              \n            </div>\n            \n          </div>\n          \n          <div>\n            <button class=\"btn btn-primary btn-third\" type=\"submit\" (click)=\"clickSave($event)\"> Proceed </button>\n            <button class=\"btn btn-primary btn-third\" (click)=\"clickCancel($event)\"> Cancel </button>\n          </div>\n          \n        </form>\n        \n     </div>\n  ",
            host: {
                'class': 'addscanner',
            },
            styles: [
                "\n    .input-holder {position: relative;}\n    .floatingList {\n      position: absolute;\n      width:100%;\n      padding-right: 15px;\n      padding-left: 15px;\n      margin-left: -15px;\n    }\n    .floatingList ul {\n      margin: 0  auto !important;\n      list-style-type: none;\n      width:60%;\n      padding: 0;\n      font-size: 1.5em;\n    }\n    .floatingList ul li {\n          background-color: #f8f8f8;\n          width: 100%;\n          text-align: left;\n          cursor: pointer;\n          padding: 6px 10px 6px 5px;\n    }\n    .floatingList ul li:hover {\n      background-color: #d0e9c6; \n    }\n    "
            ]
        }), 
        __metadata('design:paramtypes', [])
    ], WarehouseItemResolveComponent);
    return WarehouseItemResolveComponent;
}(window_component_1.WindowComponent));
exports.WarehouseItemResolveComponent = WarehouseItemResolveComponent;
(function (WarehouseItemResolveBehavior) {
    WarehouseItemResolveBehavior[WarehouseItemResolveBehavior["CLOSE_ON_PROCEED"] = 0] = "CLOSE_ON_PROCEED";
    WarehouseItemResolveBehavior[WarehouseItemResolveBehavior["PASS_TO_NEXT_WINDOW"] = 1] = "PASS_TO_NEXT_WINDOW";
})(exports.WarehouseItemResolveBehavior || (exports.WarehouseItemResolveBehavior = {}));
var WarehouseItemResolveBehavior = exports.WarehouseItemResolveBehavior;
//# sourceMappingURL=whsaddreslv.wcomp.js.map