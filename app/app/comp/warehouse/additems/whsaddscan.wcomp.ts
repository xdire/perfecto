import {Component} from "@angular/core";
import {ComponentImplementingModalFramePass} from "../../../system/entities/system.entities";
import {WindowComponent} from "../../../system/window/window.component";
import {App} from "../../../system/app";
import {WarehouseItemResolveComponent} from "./whsaddreslv.wcomp";
import {ItemResolveFormComponent} from "../../items/forms/itemresolv.wcomp";
import {WarehouseApiConnector} from "../../../system/connection/whsapiconnector";
import {ApiConnectorComponent} from "../../../system/connection/apiconnector.component";
import {WarehouseScannedItemEntity, WarehouseScannedItem, WarehouseScannedItemType} from "../entities/whsent.comp";
import {WarehouseItemComponent} from "../items/whsitem.wcomp";
import {ItemApiConnector, ItemVariationEntity} from "../../../system/connection/itemapiconnector";
/** -----------------------------------------------------------------------------------------  \
 *                                SCANNING FORM COMPONENT
 *
 *   -- parent
 *      @link WarehouseAddItemComponent : Main parent component of this component
 *
 *   -- common descendants
 *      @link WarehouseItemResolveComponent : Resolve item component
 *
 *   Component is a modal form component which can be opened only through App.window -> modal
 *   call.
 *
 *   Component allowing to collect scanned items into form, resolve, edit and add/move them
 *   into locations.
 *
 *   Component can raise other modal forms for some supportive functions.
 *
 *
 *  ----------------------------------------------------------------------------------------- */
@Component({
  selector: 'whsaddscan',
  template: `
     <div>
     
        <form class="center-text">
          
          <!--Buttons for manual form opening-->
          <div class="row" style="border-bottom: 1px solid #CCCCCC; padding: 4px; margin-bottom: 4px;">
            
            <div class="col-md-8" style="text-align: left;">
              <button class="btn btn-primary btn-normal" (click)="appendInputItem(null,true)"> Add Items </button>
              <button class="btn btn-primary btn-normal" (click)="selectLocationManually()"> Add To Location </button>
            </div>
            
          </div>
          
          <div class="block-message" *ngIf="blocked"> Scan location label [cmd: {{blockReason}}] </div>
          <div class="message-block" *ngIf="message"> Notify: {{message}} </div>
          
          <div>
            
            <whslocitem *ngFor="let item of listOfInput"
              [warehouseEntity]="item" 
              [expanded]="false"
              (onDeleteCommand)="deleteListedElement($event)"
              (onEntityObjectRefresh)="refreshView()"
              >
            </whslocitem>
            
          </div>
          
          <div>
            <button class="btn btn-primary btn-normal" type="submit" (click)="clickSave($event)"> Close </button>
            <!--<button class="btn btn-primary" (click)="clickCancel($event)"> Cancel </button>-->
          </div>
          
        </form>
        
     </div>
  `,
  host: {
    'class':'addscanner',
  },
  styles: [
    `
      .block-message {padding: 8px; background-color: #dca7a7;}
      .message-block {padding: 8px; background-color: #93cede;}
    `
  ],
  directives: [
    WarehouseItemComponent
  ]
})
/** -----------------------------------------------------------------------------------------  \
 *                                    Component class
 *  ----------------------------------------------------------------------------------------- */
export class WarehouseAddScannerItemsFormComponent extends WindowComponent implements ComponentImplementingModalFramePass {

  dataObject:any;

	loadCallback:any;
  closingMethod:any;
  successCallback:any;
  failureCallback:any;

  private blocked: boolean = false;
  private blockReason: string = "Not recognized";
  private currentCommand: string = null;
  private currentLocation: number = null;

  private message: string = null;

  private whsAPIConn: WarehouseApiConnector = null;
  private itemApiConn: ItemApiConnector = null;

  private listeners: Array<string> = [];

  constructor() {
    super.constructor();
    this.isRoot = false;
    this.whsAPIConn = ApiConnectorComponent.getWarehouseApiConnector();
    this.itemApiConn = ApiConnectorComponent.getItemApiConnector();
  }

  onLoad() {
    this.listeners.push(App.inputScannerObserver.addCommandListener("close",() => {
      this.clickSave(null);
    }));
  }

  onClose() {
    App.inputScannerObserver.removeListenerWithGUID(this.listeners);
  }

  public listOfInput: WarehouseScannedItemEntity[] = [];

  /** -----------------------------------------------------------------------------------------
   *
   *                  Accept scanned object and process it into list of input
   *
   *  -----------------------------------------------------------------------------------------
   *
   * @param item
   * @param isDefault
   */
  public appendInputItem(item: WarehouseScannedItem, isDefault: boolean = false) {

    if(this.blocked) {
      this.message = "You can't add items at this stage";
      return;
    }

    if(isDefault) {
      item = {
        scannedId:null,
        variationId:null,
        title:"Manual Added Element",
        resolved:false,
        type: WarehouseScannedItemType.New,
        warehouseId: null,
        locationId: null
      }
    }

    if(item !== null) {

      var obj = new WarehouseScannedItemEntity();
      obj.fromObject(item);

      if(obj.type == WarehouseScannedItemType.Existing) {

        this.appendWarehouseItem(obj);

      } else {

        var len = this.listOfInput.push(obj);
        this.resolveInputItem(this.listOfInput[len -1]);

      }

    }

    this.refreshView();

  }

	/** -----------------------------------------------------------------------------------------
   *
   *
   *
   *  -----------------------------------------------------------------------------------------
   *  @param scannedItem
   */
  public appendWarehouseItem(scannedItem: WarehouseScannedItemEntity) {

    if(this.blocked) {
      this.message = "You can't add items at this stage";
      return;
    }

    var itemId = App.codes.convertStringToTypedInt(<string>scannedItem.scannedId).id;
    var isNew = true;

    // Check if item existed in the list and append selectedQuantity
    this.listOfInput.map((i)=>{

      if(i.scannedId == scannedItem.scannedId) {

        // Compare and update
        if(i.selectedQuantity < i.quantity) {
          i.selectedQuantity++;
        } else {
          i.selectedQuantity = i.quantity;
        }

        isNew = false;

      }

    });

    // Add item if not in the list
    if(isNew) {

      this.whsAPIConn.getItemById(<number>itemId).subscribe((item) => {

        scannedItem.fromWarehouseItemEntity(item);

        this.listOfInput.push(scannedItem);

        this.refreshView();

      }, (err) => {

        this.message = "Scanned item with id: " + scannedItem.scannedId + " not found";
        this.refreshView();

      });

    }

  }

  /** -----------------------------------------------------------------------------------------
   *
   *
   *
   *  -----------------------------------------------------------------------------------------
   *
   *  @param command
   */
  public waitForLocationScan(command: string) {

    this.blocked = true;
    this.blockReason = command;
    this.currentCommand = command.toLowerCase();
    this.refreshView();

  }


  /** -----------------------------------------------------------------------------------------
   *
   *
   *
   *  -----------------------------------------------------------------------------------------
   */
  public checkItemsConsistencyForMovement() {

    var errors: number = 0;

    for(var i = 0; i < this.listOfInput.length; i++){

      if(!this.listOfInput[i].resolved) {
        errors++;
      }

    }

    if(errors > 0) {
      this.message = "Some of the items are not resolved yet";
      this.refreshView();
      return false;
    }
    else
    {
      this.message = null;
      this.refreshView();
      return true;
    }

  }

  /** -----------------------------------------------------------------------------------------
   *
   *
   *
   *  -----------------------------------------------------------------------------------------
   *
   * @param location
   */
  public sendItemsToWarehouse(location: string) {


    var loc = App.codes.convertStringToTypedInt(location);
    this.currentLocation = <number>loc.id;

    for(var i = 0; i < this.listOfInput.length; i++) {

      if(this.listOfInput[i].locationId == null) {

        this.listOfInput[i].locationId = <number>loc.id;
        this.createWarehouseIdOnEntity(i);

      } else {

        //this.listOfInput[i].locationId = <number>loc.id;
        this.updateWarehouseEntity(i, <number>loc.id);

      }

    }

    this.refreshView();

  }
  // ----- ^^^^^^^^^^^^^^ SUPPORTIVE ^^^^^^^^^^^^^^ -----
  /** ------------------------------------------------------------------------------
   *
   *  ------------------------------------------------------------------------------ */
  private createWarehouseIdOnEntity(index) {

    this.whsAPIConn.createNewItem(this.listOfInput[index].toWarehouseItemEntity()).subscribe((data) => {

      this.listOfInput[index].scannedId = App.codes.convertStringToTypedString(App.codes.convertIntToItemId(data.id)).id;
      this.listOfInput[index].warehouseId = data.id;
      this.listOfInput[index].updated = true;

      this.message = "Item created in "+this.listOfInput[index].locationId;

      this.refreshView();

      this.listOfInput[index].printItemLabel(() => {
        this.refreshView();
      });

    }, (err) => {

      console.log("Cannot create entity "+err.status);

    });

  }
  // ----- ^^^^^^^^^^^^^^ SUPPORTIVE ^^^^^^^^^^^^^^ -----
  /** ------------------------------------------------------------------------------
   *
   *  ------------------------------------------------------------------------------ */
  private updateWarehouseEntity(index, locationId: number = null) {

    var e = this.listOfInput[index];

    var currentLocation = this.listOfInput[index].locationId;

    if(e.selectedQuantity == e.quantity) {

      //console.log("MOVING WHOLE ITEM FROM LOCATION : "+this.listOfInput[index].locationId+ " --> "+locationId);

      this.listOfInput[index].locationId = locationId;

      this.whsAPIConn.updateItem(this.listOfInput[index].toWarehouseItemEntity()).subscribe((data) => {

        this.message = "Item moved "+currentLocation+" -> "+locationId;
        this.listOfInput[index].updated = true;
        this.refreshView();

      }, (err) => {

        this.listOfInput[index].locationId = currentLocation;
        this.message = "Item failed to move and still in "+currentLocation;
        console.log("Cannot update entity "+err.status);

      });

    } else {

      //console.log("MOVING PART ITEM FROM LOCATION : "+this.listOfInput[index].locationId+ " --> "+locationId);

      var entity = new WarehouseScannedItemEntity();
      entity.fromObject(this.listOfInput[index]);
      entity.locationId = locationId;
      //var entity = <WarehouseScannedItemEntity>App.cloneObjectWithAttributes(this.listOfInput[index]);
      entity.quantity = entity.selectedQuantity;
      entity.warehouseId = null;
      entity.labeled = false;

      this.whsAPIConn.createNewItem(entity.toWarehouseItemEntity()).subscribe((newdata) => {

        this.listOfInput[index].quantity = this.listOfInput[index].quantity - this.listOfInput[index].selectedQuantity;
        this.message = "New chunked item created in "+locationId;
        entity.printItemLabel();

        this.whsAPIConn.updateItem(this.listOfInput[index].toWarehouseItemEntity()).subscribe((data) => {

          entity.selectedQuantity = 1;

          this.listOfInput[index] = entity;
          var item = this.listOfInput[index];
          item.scannedId = App.codes.convertStringToTypedString(App.codes.convertIntToItemId(newdata.id)).id;
          item.warehouseId = newdata.id;
          item.updated = true;
          this.message = "New chunked item created in "+locationId+" Separated item sits in "+currentLocation;

          this.refreshView();

        }, (err) => {

          this.listOfInput[index].locationId = currentLocation;
          this.message = "Separated failed to move and still in "+currentLocation;

          this.message += "Some operations with "+this.listOfInput[index].scannedId+" was corrupted";
          this.refreshView();

        });

      }, (err) => {

        this.message += "Some operations with "+this.listOfInput[index].scannedId+" was corrupted";
        this.refreshView();

      });

    }

  }

	/** -----------------------------------------------------------------------------------------
   *
   *                          Make an automatic scanned item resolve
   *
   *  -----------------------------------------------------------------------------------------
   *
   *  2 step function:
   *
   *    1 - query for EAN-12
   *
   *    2 - query for EAN-14 if first failed
   *
   *
   * @param item WarehouseScannedItemEntity : reference to original scanned item
   */
  public resolveInputItem(item: WarehouseScannedItemEntity) {

    if(item.scannedId === null) return;

    var upc = <string>item.scannedId;
    var upc14:string = "00"+upc;

    this.itemApiConn.getItemsByVariationCode(upc).subscribe((items) => {

      // If multiple variations returned
      if(items.length > 1) {



      }
      // If one variation returned
      else {

        // Return new form for select of confirm parameters
        App.window.loadComponentIntoModalFrame(ItemResolveFormComponent, {

          title: "Results of item resolving",
          data: items[0],
          onSuccess: (data) => {

            item.variationId = data.variationId;
            item.title = data.title;
            item.quantity = data.quantity;
            item.extras = data.extras;
            item.image = data.image;
            item.resolved = true;

            item.itemObject = data.itemObject;
            item.variationObject = data.variationObject;

            this.refreshView();

          },
          onFailure: () => {},
          onClose: () => {App.inputScannerObserver.resumeListenerWithGIUD(this.listeners);},
          getReference: (ref) => {App.inputScannerObserver.pauseListenerWithGIUD(this.listeners);}

        });

      }

    }, (err) => {

      // Get variation by UPC14 if simple UPC code is failed
      this.itemApiConn.getItemsByVariationCode(upc14).subscribe((items) => {

        // If multiple variations returned
        if(items.length > 1) {

        }
        // If one variation returned
        else {

          // Return new form for select of confirm parameters
          App.window.loadComponentIntoModalFrame(ItemResolveFormComponent, {

            title: "Results of item resolving",
            data: items[0],
            onSuccess: (data) => {

              item.variationId = data.variationId;
              item.title = data.title;
              item.quantity = data.quantity;
              item.extras = data.extras;
              item.image = data.image;
              item.resolved = true;

              item.itemObject = data.itemObject;
              item.variationObject = data.variationObject;

              this.refreshView();

            },
            onFailure: () => {},
            onClose: () => {App.inputScannerObserver.resumeListenerWithGIUD(this.listeners);},
            getReference: (ref) => {App.inputScannerObserver.pauseListenerWithGIUD(this.listeners);}

          });

        }

      },(err) => {



      });

    });

  }

	/** -----------------------------------------------------------------------------------------
   *
   *
   *
   *  -----------------------------------------------------------------------------------------
   */
  private resolveInputStyle(item: WarehouseScannedItemEntity) {

    this.itemApiConn.getStylesByItemId(item.variationObject.parentId).subscribe((data) => {

    }, (ref) => {

    });

  }

  /** ------------------------------------------------------------------------------
   *
   *
   *
   *  ------------------------------------------------------------------------------ */
  private applyAutoResolveData(item: WarehouseScannedItemEntity, data: ItemVariationEntity) {

  }

  /** -----------------------------------------------------------------------------------------
   *
   *
   *
   *  -----------------------------------------------------------------------------------------
   *
   */
  public selectLocationManually() {



  }

  /** -----------------------------------------------------------------------------------------
   *
   *
   *
   *  -----------------------------------------------------------------------------------------
   *
   * @param indexId
   */
  public removeElementWithIndexId(indexId: number) {

    this.listOfInput = this.listOfInput.filter((data,index)=>{
      return index != indexId;
    });
    this.refreshView();

  }

	/** -----------------------------------------------------------------------------------------
   *
   *
   *
   *  -----------------------------------------------------------------------------------------
   *
   * @param $e
   */
  public deleteListedElement($e: WarehouseScannedItemEntity) {

    this.listOfInput = this.listOfInput.filter((data,index)=>{
      return data.uid != $e.uid;
    });
    this.refreshView();

  }
	/** -----------------------------------------------------------------------------------------
   *
   *
   *
   *  -----------------------------------------------------------------------------------------
   *
   * @param $event
   */
  public clickSave($event) {

    if($event !== null)
      $event.preventDefault();

    if(this.blocked) {

      var blocked = 0;
      var updated = 0;

      this.listOfInput.filter((data)=>{
        if(!data.labeled){
          blocked++;
        }
        if(!data.updated){
          updated++;
        }
      });

      if(blocked > 0) {
        this.message = "Some item labels are not printed";
        this.refreshView();
        return;
      }

      if(updated > 0) {
        this.message = "Some items are not saved to location yet";
        this.refreshView();
        return;
      }

    }

    this.closingMethod({reset:{listeners:this.listeners}});

  }

  /** ------------------------------------------------------------------------------
   *
   *
   *
   *  ------------------------------------------------------------------------------ */
  public clickCancel($event) {
    $event.preventDefault();
    this.closingMethod(/*{reset:{listeners:this.listeners}}*/);
  }

}
