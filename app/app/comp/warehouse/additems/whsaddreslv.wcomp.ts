import {Component, ElementRef, ViewChild} from "@angular/core";
import {ComponentImplementingModalFramePass} from "../../../system/entities/system.entities";
import {WindowComponent} from "../../../system/window/window.component";
import {DataAccumulator} from "../../../system/data/dataaccu.sys";
import {ApiConnectorComponent} from "../../../system/connection/apiconnector.component";
import {App} from "../../../system/app";
import {
  ItemGridViewComponent, ItemGridViewComponentProperties,
  ItemGridViewRequestType
} from "../../items/views/itemgrid.wcomp";
import {ItemApiConnector} from "../../../system/connection/itemapiconnector";
/** -----------------------------------------------------------------------------------------  \
 *                                Component description
 *  ----------------------------------------------------------------------------------------- */
@Component({
  selector: 'whsaddreslv',
  template: `
     <div>
     
        <form class="center-text">
          
          <div class="panel panel-default">
      
            <div class="panel-heading"> Please use fields below to search for your item</div>
            
            <div class="panel-body">
            
              <form>
                  
                  <div class="row">
                    <div class="col-lg-12 input-holder">
                    
                      Ok so you know the style of your item?
                      
                      <div>
                        <input #fstyleinput type="text" 
                        style="width: 50%;display: inline-block;border: 1px solid #DEDEDE;border-radius: 4px;padding: 6px;"
                        placeholder="Search this field if you know style of the item"
                        (input)="catchTypingOfStyleName()"
                        (click)="emptyStyleSupportList()"
                        [(ngModel)]="styleCode">
                        <div style="vertical-align: top; display: inline-block;">
                          <button class="btn btn-default" type="submit" (click)="clickSearchByStyle()">Search Style</button>
                        </div>
                      </div><!-- /input-group -->
                      
                      <div class="floatingList">
                        <ul>
                          <li *ngFor="let item of styleSupportList" (click)="clickChooseDropDownStyle(item)">
                            {{item}}
                          </li>
                        </ul>
                      </div>
                      
                    </div><!-- /.col-lg-6 -->
                  </div><!-- /.row -->
           
              </form>
              
            </div>
            
          </div>
          
          <div>
            <button class="btn btn-primary btn-third" type="submit" (click)="clickSave($event)"> Proceed </button>
            <button class="btn btn-primary btn-third" (click)="clickCancel($event)"> Cancel </button>
          </div>
          
        </form>
        
     </div>
  `,
  host: {
    'class':'addscanner',
  },
  styles: [
    `
    .input-holder {position: relative;}
    .floatingList {
      position: absolute;
      width:100%;
      padding-right: 15px;
      padding-left: 15px;
      margin-left: -15px;
    }
    .floatingList ul {
      margin: 0  auto !important;
      list-style-type: none;
      width:60%;
      padding: 0;
      font-size: 1.5em;
    }
    .floatingList ul li {
          background-color: #f8f8f8;
          width: 100%;
          text-align: left;
          cursor: pointer;
          padding: 6px 10px 6px 5px;
    }
    .floatingList ul li:hover {
      background-color: #d0e9c6; 
    }
    `
  ]
})
/** ----------------------------------------------------------------------------------------- \
 *                                Component entity
 *  ----------------------------------------------------------------------------------------- */
export class WarehouseItemResolveComponent extends WindowComponent implements ComponentImplementingModalFramePass {

  @ViewChild('fstyleinput') styleInput: ElementRef;

  dataObject:any;

  loadCallback:any;
  closingMethod:any;
  successCallback:any;
  failureCallback:any;

  public behaviorOnProceed: WarehouseItemResolveBehavior = 0;

  private styleCode: string = "";

  private styleSearchAccu: DataAccumulator;

  private styleSupportList: Array = [];

  private itemApiConn: ItemApiConnector = null;

  public listeners: Array<string> = [];
  /** ------------------------------------------------------------------------------
   *
   *  ------------------------------------------------------------------------------ */
  constructor() {
    super.constructor();
    this.isRoot = false;
    this.styleSearchAccu = new DataAccumulator(2048);
    this.itemApiConn = ApiConnectorComponent.getItemApiConnector();
  }

  onLoad() {
    this.listeners.push(App.inputScannerObserver.addCommandListener("close", ()=>{
      this.closeForm(null);
    }));
  }

  onClose() {
    App.inputScannerObserver.removeListenerWithGUID(this.listeners);
  }

  onFocus() {
    this.styleInput.nativeElement.focus();
  }

  /** ------------------------------------------------------------------------------
   *
   *  ------------------------------------------------------------------------------ */
  ngAfterViewInit() {
    this.styleInput.nativeElement.focus();
  }
	/** ---------------------------------------------------------------------------------
   *
   *  ---------------------------------------------------------------------------------
   * @param $event
   */
  public clickSave($event) {

    $event.preventDefault();
    this.successCallback();

    if(this.behaviorOnProceed == 0)
      this.closingMethod();

  }

	/** ---------------------------------------------------------------------------------
   *
   *  ---------------------------------------------------------------------------------
   * @param $event
   */
  public clickCancel($event) {
    $event.preventDefault();
    this.closingMethod();
  }

	/** ---------------------------------------------------------------------------------
   *
   *
   *  ---------------------------------------------------------------------------------
   *
   */
  public clickSearchByStyle() {

    var props: ItemGridViewComponentProperties = {
      requestType: ItemGridViewRequestType.ByStyleCode,
      requestParameters: {style: this.styleCode},
      variations: true,
      selectableVariation: true,
      onVariationSelectPassThrough: true
    };

    App.window.loadComponentIntoModalFrame(ItemGridViewComponent,{

      title: "Item view",
      data: props,
      onSuccess: (data) => {

        if(props.onVariationSelectPassThrough) {
          this.successCallback(data);
          if(this.behaviorOnProceed == 0)
            this.closingMethod();
        }

      },
      onFailure: (data) => {},
      onClose: () => {},
      getReference: (ref) => {}

    });

  }

  /** ---------------------------------------------------------------------------------
   *
   *
   *  ---------------------------------------------------------------------------------
   *
   */
  public clickChooseDropDownStyle(item) {
    this.styleInput.nativeElement.focus();
    this.styleCode = item;
    this.emptyStyleSupportList();
  }

  /** ---------------------------------------------------------------------------------
   *
   *
   *  ---------------------------------------------------------------------------------
   *
   */
  public emptyStyleSupportList(){
    this.styleSupportList = [];
  }

  /** ---------------------------------------------------------------------------------
   *
   *
   *  ---------------------------------------------------------------------------------
   *
   */
  public catchTypingOfStyleName() {

    var to = setTimeout(() => {

      var res = this.styleSearchAccu.getValue(this.styleCode,10);

      if (res.length == 0) {

          this.itemApiConn.getStylesByStyleSubstring(this.styleCode).subscribe((data) => {

            this.styleSearchAccu.fromArray(data);
            var a = this.styleSearchAccu.getValue(this.styleCode, 5);
            if (a.length == 0)
              this.styleSupportList = ["Nothing found"];
            else
              this.styleSupportList = a;

          }, (err) => {

            console.log("Request failed with status "+err.status);

          });

      } else {

        this.styleSupportList = res;

      }

    }, 50);

  }

  /** ------------------------------------------------------------------------------
   *
   *  ------------------------------------------------------------------------------ */
  public closeForm($e:any=null) {

    if($e !== null)
      $e.preventDefault();

    this.closingMethod(/*{reset:{listeners:this.listeners}}*/);

  }

}

export enum WarehouseItemResolveBehavior {
  CLOSE_ON_PROCEED,
  PASS_TO_NEXT_WINDOW
}
