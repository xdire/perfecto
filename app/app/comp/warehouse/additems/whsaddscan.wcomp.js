"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var window_component_1 = require("../../../system/window/window.component");
var app_1 = require("../../../system/app");
var itemresolv_wcomp_1 = require("../../items/forms/itemresolv.wcomp");
var apiconnector_component_1 = require("../../../system/connection/apiconnector.component");
var whsent_comp_1 = require("../entities/whsent.comp");
var whsitem_wcomp_1 = require("../items/whsitem.wcomp");
/** -----------------------------------------------------------------------------------------  \
 *                                SCANNING FORM COMPONENT
 *
 *   -- parent
 *      @link WarehouseAddItemComponent : Main parent component of this component
 *
 *   -- common descendants
 *      @link WarehouseItemResolveComponent : Resolve item component
 *
 *   Component is a modal form component which can be opened only through App.window -> modal
 *   call.
 *
 *   Component allowing to collect scanned items into form, resolve, edit and add/move them
 *   into locations.
 *
 *   Component can raise other modal forms for some supportive functions.
 *
 *
 *  ----------------------------------------------------------------------------------------- */
var WarehouseAddScannerItemsFormComponent = (function (_super) {
    __extends(WarehouseAddScannerItemsFormComponent, _super);
    function WarehouseAddScannerItemsFormComponent() {
        this.blocked = false;
        this.blockReason = "Not recognized";
        this.currentCommand = null;
        this.currentLocation = null;
        this.message = null;
        this.whsAPIConn = null;
        this.itemApiConn = null;
        this.listeners = [];
        this.listOfInput = [];
        _super.prototype.constructor.call(this);
        this.isRoot = false;
        this.whsAPIConn = apiconnector_component_1.ApiConnectorComponent.getWarehouseApiConnector();
        this.itemApiConn = apiconnector_component_1.ApiConnectorComponent.getItemApiConnector();
    }
    WarehouseAddScannerItemsFormComponent.prototype.onLoad = function () {
        var _this = this;
        this.listeners.push(app_1.App.inputScannerObserver.addCommandListener("close", function () {
            _this.clickSave(null);
        }));
    };
    WarehouseAddScannerItemsFormComponent.prototype.onClose = function () {
        app_1.App.inputScannerObserver.removeListenerWithGUID(this.listeners);
    };
    /** -----------------------------------------------------------------------------------------
     *
     *                  Accept scanned object and process it into list of input
     *
     *  -----------------------------------------------------------------------------------------
     *
     * @param item
     * @param isDefault
     */
    WarehouseAddScannerItemsFormComponent.prototype.appendInputItem = function (item, isDefault) {
        if (isDefault === void 0) { isDefault = false; }
        if (this.blocked) {
            this.message = "You can't add items at this stage";
            return;
        }
        if (isDefault) {
            item = {
                scannedId: null,
                variationId: null,
                title: "Manual Added Element",
                resolved: false,
                type: whsent_comp_1.WarehouseScannedItemType.New,
                warehouseId: null,
                locationId: null
            };
        }
        if (item !== null) {
            var obj = new whsent_comp_1.WarehouseScannedItemEntity();
            obj.fromObject(item);
            if (obj.type == whsent_comp_1.WarehouseScannedItemType.Existing) {
                this.appendWarehouseItem(obj);
            }
            else {
                var len = this.listOfInput.push(obj);
                this.resolveInputItem(this.listOfInput[len - 1]);
            }
        }
        this.refreshView();
    };
    /** -----------------------------------------------------------------------------------------
   *
   *
   *
   *  -----------------------------------------------------------------------------------------
   *  @param scannedItem
   */
    WarehouseAddScannerItemsFormComponent.prototype.appendWarehouseItem = function (scannedItem) {
        var _this = this;
        if (this.blocked) {
            this.message = "You can't add items at this stage";
            return;
        }
        var itemId = app_1.App.codes.convertStringToTypedInt(scannedItem.scannedId).id;
        var isNew = true;
        // Check if item existed in the list and append selectedQuantity
        this.listOfInput.map(function (i) {
            if (i.scannedId == scannedItem.scannedId) {
                // Compare and update
                if (i.selectedQuantity < i.quantity) {
                    i.selectedQuantity++;
                }
                else {
                    i.selectedQuantity = i.quantity;
                }
                isNew = false;
            }
        });
        // Add item if not in the list
        if (isNew) {
            this.whsAPIConn.getItemById(itemId).subscribe(function (item) {
                scannedItem.fromWarehouseItemEntity(item);
                _this.listOfInput.push(scannedItem);
                _this.refreshView();
            }, function (err) {
                _this.message = "Scanned item with id: " + scannedItem.scannedId + " not found";
                _this.refreshView();
            });
        }
    };
    /** -----------------------------------------------------------------------------------------
     *
     *
     *
     *  -----------------------------------------------------------------------------------------
     *
     *  @param command
     */
    WarehouseAddScannerItemsFormComponent.prototype.waitForLocationScan = function (command) {
        this.blocked = true;
        this.blockReason = command;
        this.currentCommand = command.toLowerCase();
        this.refreshView();
    };
    /** -----------------------------------------------------------------------------------------
     *
     *
     *
     *  -----------------------------------------------------------------------------------------
     */
    WarehouseAddScannerItemsFormComponent.prototype.checkItemsConsistencyForMovement = function () {
        var errors = 0;
        for (var i = 0; i < this.listOfInput.length; i++) {
            if (!this.listOfInput[i].resolved) {
                errors++;
            }
        }
        if (errors > 0) {
            this.message = "Some of the items are not resolved yet";
            this.refreshView();
            return false;
        }
        else {
            this.message = null;
            this.refreshView();
            return true;
        }
    };
    /** -----------------------------------------------------------------------------------------
     *
     *
     *
     *  -----------------------------------------------------------------------------------------
     *
     * @param location
     */
    WarehouseAddScannerItemsFormComponent.prototype.sendItemsToWarehouse = function (location) {
        var loc = app_1.App.codes.convertStringToTypedInt(location);
        this.currentLocation = loc.id;
        for (var i = 0; i < this.listOfInput.length; i++) {
            if (this.listOfInput[i].locationId == null) {
                this.listOfInput[i].locationId = loc.id;
                this.createWarehouseIdOnEntity(i);
            }
            else {
                //this.listOfInput[i].locationId = <number>loc.id;
                this.updateWarehouseEntity(i, loc.id);
            }
        }
        this.refreshView();
    };
    // ----- ^^^^^^^^^^^^^^ SUPPORTIVE ^^^^^^^^^^^^^^ -----
    /** ------------------------------------------------------------------------------
     *
     *  ------------------------------------------------------------------------------ */
    WarehouseAddScannerItemsFormComponent.prototype.createWarehouseIdOnEntity = function (index) {
        var _this = this;
        this.whsAPIConn.createNewItem(this.listOfInput[index].toWarehouseItemEntity()).subscribe(function (data) {
            _this.listOfInput[index].scannedId = app_1.App.codes.convertStringToTypedString(app_1.App.codes.convertIntToItemId(data.id)).id;
            _this.listOfInput[index].warehouseId = data.id;
            _this.listOfInput[index].updated = true;
            _this.message = "Item created in " + _this.listOfInput[index].locationId;
            _this.refreshView();
            _this.listOfInput[index].printItemLabel(function () {
                _this.refreshView();
            });
        }, function (err) {
            console.log("Cannot create entity " + err.status);
        });
    };
    // ----- ^^^^^^^^^^^^^^ SUPPORTIVE ^^^^^^^^^^^^^^ -----
    /** ------------------------------------------------------------------------------
     *
     *  ------------------------------------------------------------------------------ */
    WarehouseAddScannerItemsFormComponent.prototype.updateWarehouseEntity = function (index, locationId) {
        var _this = this;
        if (locationId === void 0) { locationId = null; }
        var e = this.listOfInput[index];
        var currentLocation = this.listOfInput[index].locationId;
        if (e.selectedQuantity == e.quantity) {
            //console.log("MOVING WHOLE ITEM FROM LOCATION : "+this.listOfInput[index].locationId+ " --> "+locationId);
            this.listOfInput[index].locationId = locationId;
            this.whsAPIConn.updateItem(this.listOfInput[index].toWarehouseItemEntity()).subscribe(function (data) {
                _this.message = "Item moved " + currentLocation + " -> " + locationId;
                _this.listOfInput[index].updated = true;
                _this.refreshView();
            }, function (err) {
                _this.listOfInput[index].locationId = currentLocation;
                _this.message = "Item failed to move and still in " + currentLocation;
                console.log("Cannot update entity " + err.status);
            });
        }
        else {
            //console.log("MOVING PART ITEM FROM LOCATION : "+this.listOfInput[index].locationId+ " --> "+locationId);
            var entity = new whsent_comp_1.WarehouseScannedItemEntity();
            entity.fromObject(this.listOfInput[index]);
            entity.locationId = locationId;
            //var entity = <WarehouseScannedItemEntity>App.cloneObjectWithAttributes(this.listOfInput[index]);
            entity.quantity = entity.selectedQuantity;
            entity.warehouseId = null;
            entity.labeled = false;
            this.whsAPIConn.createNewItem(entity.toWarehouseItemEntity()).subscribe(function (newdata) {
                _this.listOfInput[index].quantity = _this.listOfInput[index].quantity - _this.listOfInput[index].selectedQuantity;
                _this.message = "New chunked item created in " + locationId;
                entity.printItemLabel();
                _this.whsAPIConn.updateItem(_this.listOfInput[index].toWarehouseItemEntity()).subscribe(function (data) {
                    entity.selectedQuantity = 1;
                    _this.listOfInput[index] = entity;
                    var item = _this.listOfInput[index];
                    item.scannedId = app_1.App.codes.convertStringToTypedString(app_1.App.codes.convertIntToItemId(newdata.id)).id;
                    item.warehouseId = newdata.id;
                    item.updated = true;
                    _this.message = "New chunked item created in " + locationId + " Separated item sits in " + currentLocation;
                    _this.refreshView();
                }, function (err) {
                    _this.listOfInput[index].locationId = currentLocation;
                    _this.message = "Separated failed to move and still in " + currentLocation;
                    _this.message += "Some operations with " + _this.listOfInput[index].scannedId + " was corrupted";
                    _this.refreshView();
                });
            }, function (err) {
                _this.message += "Some operations with " + _this.listOfInput[index].scannedId + " was corrupted";
                _this.refreshView();
            });
        }
    };
    /** -----------------------------------------------------------------------------------------
   *
   *                          Make an automatic scanned item resolve
   *
   *  -----------------------------------------------------------------------------------------
   *
   *  2 step function:
   *
   *    1 - query for EAN-12
   *
   *    2 - query for EAN-14 if first failed
   *
   *
   * @param item WarehouseScannedItemEntity : reference to original scanned item
   */
    WarehouseAddScannerItemsFormComponent.prototype.resolveInputItem = function (item) {
        var _this = this;
        if (item.scannedId === null)
            return;
        var upc = item.scannedId;
        var upc14 = "00" + upc;
        this.itemApiConn.getItemsByVariationCode(upc).subscribe(function (items) {
            // If multiple variations returned
            if (items.length > 1) {
            }
            else {
                // Return new form for select of confirm parameters
                app_1.App.window.loadComponentIntoModalFrame(itemresolv_wcomp_1.ItemResolveFormComponent, {
                    title: "Results of item resolving",
                    data: items[0],
                    onSuccess: function (data) {
                        item.variationId = data.variationId;
                        item.title = data.title;
                        item.quantity = data.quantity;
                        item.extras = data.extras;
                        item.image = data.image;
                        item.resolved = true;
                        item.itemObject = data.itemObject;
                        item.variationObject = data.variationObject;
                        _this.refreshView();
                    },
                    onFailure: function () { },
                    onClose: function () { app_1.App.inputScannerObserver.resumeListenerWithGIUD(_this.listeners); },
                    getReference: function (ref) { app_1.App.inputScannerObserver.pauseListenerWithGIUD(_this.listeners); }
                });
            }
        }, function (err) {
            // Get variation by UPC14 if simple UPC code is failed
            _this.itemApiConn.getItemsByVariationCode(upc14).subscribe(function (items) {
                // If multiple variations returned
                if (items.length > 1) {
                }
                else {
                    // Return new form for select of confirm parameters
                    app_1.App.window.loadComponentIntoModalFrame(itemresolv_wcomp_1.ItemResolveFormComponent, {
                        title: "Results of item resolving",
                        data: items[0],
                        onSuccess: function (data) {
                            item.variationId = data.variationId;
                            item.title = data.title;
                            item.quantity = data.quantity;
                            item.extras = data.extras;
                            item.image = data.image;
                            item.resolved = true;
                            item.itemObject = data.itemObject;
                            item.variationObject = data.variationObject;
                            _this.refreshView();
                        },
                        onFailure: function () { },
                        onClose: function () { app_1.App.inputScannerObserver.resumeListenerWithGIUD(_this.listeners); },
                        getReference: function (ref) { app_1.App.inputScannerObserver.pauseListenerWithGIUD(_this.listeners); }
                    });
                }
            }, function (err) {
            });
        });
    };
    /** -----------------------------------------------------------------------------------------
   *
   *
   *
   *  -----------------------------------------------------------------------------------------
   */
    WarehouseAddScannerItemsFormComponent.prototype.resolveInputStyle = function (item) {
        this.itemApiConn.getStylesByItemId(item.variationObject.parentId).subscribe(function (data) {
        }, function (ref) {
        });
    };
    /** ------------------------------------------------------------------------------
     *
     *
     *
     *  ------------------------------------------------------------------------------ */
    WarehouseAddScannerItemsFormComponent.prototype.applyAutoResolveData = function (item, data) {
    };
    /** -----------------------------------------------------------------------------------------
     *
     *
     *
     *  -----------------------------------------------------------------------------------------
     *
     */
    WarehouseAddScannerItemsFormComponent.prototype.selectLocationManually = function () {
    };
    /** -----------------------------------------------------------------------------------------
     *
     *
     *
     *  -----------------------------------------------------------------------------------------
     *
     * @param indexId
     */
    WarehouseAddScannerItemsFormComponent.prototype.removeElementWithIndexId = function (indexId) {
        this.listOfInput = this.listOfInput.filter(function (data, index) {
            return index != indexId;
        });
        this.refreshView();
    };
    /** -----------------------------------------------------------------------------------------
   *
   *
   *
   *  -----------------------------------------------------------------------------------------
   *
   * @param $e
   */
    WarehouseAddScannerItemsFormComponent.prototype.deleteListedElement = function ($e) {
        this.listOfInput = this.listOfInput.filter(function (data, index) {
            return data.uid != $e.uid;
        });
        this.refreshView();
    };
    /** -----------------------------------------------------------------------------------------
   *
   *
   *
   *  -----------------------------------------------------------------------------------------
   *
   * @param $event
   */
    WarehouseAddScannerItemsFormComponent.prototype.clickSave = function ($event) {
        if ($event !== null)
            $event.preventDefault();
        if (this.blocked) {
            var blocked = 0;
            var updated = 0;
            this.listOfInput.filter(function (data) {
                if (!data.labeled) {
                    blocked++;
                }
                if (!data.updated) {
                    updated++;
                }
            });
            if (blocked > 0) {
                this.message = "Some item labels are not printed";
                this.refreshView();
                return;
            }
            if (updated > 0) {
                this.message = "Some items are not saved to location yet";
                this.refreshView();
                return;
            }
        }
        this.closingMethod({ reset: { listeners: this.listeners } });
    };
    /** ------------------------------------------------------------------------------
     *
     *
     *
     *  ------------------------------------------------------------------------------ */
    WarehouseAddScannerItemsFormComponent.prototype.clickCancel = function ($event) {
        $event.preventDefault();
        this.closingMethod();
    };
    WarehouseAddScannerItemsFormComponent = __decorate([
        core_1.Component({
            selector: 'whsaddscan',
            template: "\n     <div>\n     \n        <form class=\"center-text\">\n          \n          <!--Buttons for manual form opening-->\n          <div class=\"row\" style=\"border-bottom: 1px solid #CCCCCC; padding: 4px; margin-bottom: 4px;\">\n            \n            <div class=\"col-md-8\" style=\"text-align: left;\">\n              <button class=\"btn btn-primary btn-normal\" (click)=\"appendInputItem(null,true)\"> Add Items </button>\n              <button class=\"btn btn-primary btn-normal\" (click)=\"selectLocationManually()\"> Add To Location </button>\n            </div>\n            \n          </div>\n          \n          <div class=\"block-message\" *ngIf=\"blocked\"> Scan location label [cmd: {{blockReason}}] </div>\n          <div class=\"message-block\" *ngIf=\"message\"> Notify: {{message}} </div>\n          \n          <div>\n            \n            <whslocitem *ngFor=\"let item of listOfInput\"\n              [warehouseEntity]=\"item\" \n              [expanded]=\"false\"\n              (onDeleteCommand)=\"deleteListedElement($event)\"\n              (onEntityObjectRefresh)=\"refreshView()\"\n              >\n            </whslocitem>\n            \n          </div>\n          \n          <div>\n            <button class=\"btn btn-primary btn-normal\" type=\"submit\" (click)=\"clickSave($event)\"> Close </button>\n            <!--<button class=\"btn btn-primary\" (click)=\"clickCancel($event)\"> Cancel </button>-->\n          </div>\n          \n        </form>\n        \n     </div>\n  ",
            host: {
                'class': 'addscanner',
            },
            styles: [
                "\n      .block-message {padding: 8px; background-color: #dca7a7;}\n      .message-block {padding: 8px; background-color: #93cede;}\n    "
            ],
            directives: [
                whsitem_wcomp_1.WarehouseItemComponent
            ]
        }), 
        __metadata('design:paramtypes', [])
    ], WarehouseAddScannerItemsFormComponent);
    return WarehouseAddScannerItemsFormComponent;
}(window_component_1.WindowComponent));
exports.WarehouseAddScannerItemsFormComponent = WarehouseAddScannerItemsFormComponent;
//# sourceMappingURL=whsaddscan.wcomp.js.map