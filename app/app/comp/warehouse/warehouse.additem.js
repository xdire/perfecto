"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var app_1 = require("../../system/app");
var window_component_1 = require("../../system/window/window.component");
var inpaccu_sys_1 = require("../../system/input/inpaccu.sys");
var codes_sys_1 = require("../../system/codes/codes.sys");
var whsaddscan_wcomp_1 = require("./additems/whsaddscan.wcomp");
var whslocscan_wcomp_1 = require("./locitems/whslocscan.wcomp");
var whsent_comp_1 = require("./entities/whsent.comp");
var whs_loc_list_wcomp_1 = require("./location/whs.loc.list.wcomp");
/** --------------------------------------------------------------------
 *
 *
 *                    Add Items tab for warehousing component
 *
 *
 *  -------------------------------------------------------------------- */
var WarehouseAddItemComponent = (function (_super) {
    __extends(WarehouseAddItemComponent, _super);
    function WarehouseAddItemComponent() {
        _super.apply(this, arguments);
        this.scannedInput = "";
        this.scannedCommand = "";
        this.upcLookupValue = "";
        this.boxLookupValue = "";
        this.varLookupValue = "";
        this.listeners = [];
        this.accumulator = new inpaccu_sys_1.InputAccumulator();
        this.currentModalComponent = null;
        this.currentModalComponentType = null;
        this._isVisible = false;
    }
    WarehouseAddItemComponent.prototype.onFocus = function () {
        app_1.App.inputScannerObserver.reset();
        app_1.App.inputScannerObserver.resetListeners();
        this.listeners.push(app_1.App.inputScannerObserver.addInputListener("*any", this.onScannedInput.bind(this)));
        this.listeners.push(app_1.App.inputScannerObserver.addCommandListener("add", this.onScannedCommand.bind(this)));
    };
    WarehouseAddItemComponent.prototype.onLostFocus = function () {
        app_1.App.inputScannerObserver.removeListenerWithGUID(this.listeners);
        app_1.App.inputScannerObserver.reset();
        app_1.App.inputScannerObserver.resetListeners();
    };
    WarehouseAddItemComponent.prototype.onParentUnload = function () {
    };
    WarehouseAddItemComponent.prototype.ngOnDestroy = function () {
        app_1.App.inputScannerObserver.removeListenerWithGUID(this.listeners);
    };
    /** --------------------------------------------------------------------
   *
   *                    Actions on some input scan
   *
   *  --------------------------------------------------------------------
   *
   * @param inputValue
   */
    WarehouseAddItemComponent.prototype.onScannedInput = function (inputValue) {
        var nInput = app_1.App.codes.convertStringToTypedString(inputValue);
        if (nInput.type == codes_sys_1.CodesTypes.ItemType || nInput.type == codes_sys_1.CodesTypes.NoType) {
            this.accumulator.push(nInput.id);
            var type = (nInput.type == codes_sys_1.CodesTypes.NoType) ? whsent_comp_1.WarehouseScannedItemType.New : whsent_comp_1.WarehouseScannedItemType.Existing;
            this.openScanningModal({
                scannedId: nInput.id,
                variationId: null,
                title: "Some title",
                resolved: false,
                type: type,
                warehouseId: null,
                locationId: null
            });
        }
        if (nInput.type == codes_sys_1.CodesTypes.LocationType) {
            if (this.currentModalComponent !== null) {
                if (this.currentModalComponent.checkItemsConsistencyForMovement()) {
                    this.currentModalComponent.sendItemsToWarehouse(nInput.id);
                }
            }
            else {
                this.openLocationModal(app_1.App.codes.convertStringToTypedInt(nInput.id).id);
            }
        }
        this.scannedInput = inputValue;
    };
    /** --------------------------------------------------------------------
   *
   *                      Action on type-in UPC
   *
   *  --------------------------------------------------------------------
   *
   */
    WarehouseAddItemComponent.prototype.onUPCInput = function () {
        this.openScanningModal({
            scannedId: this.upcLookupValue,
            variationId: null,
            title: "Some UPC Input",
            resolved: false,
            type: whsent_comp_1.WarehouseScannedItemType.New,
            warehouseId: null,
            locationId: null
        });
    };
    /** --------------------------------------------------------------------
     *
     *                      Action on type-in Location
     *
     *  --------------------------------------------------------------------
     *
     */
    WarehouseAddItemComponent.prototype.onLocationInput = function () {
        var value = 0;
        if ((value = parseInt(this.boxLookupValue)) > 0) {
            this.openLocationModal(value);
        }
        else {
            alert("Not well defined Id of location. Operation postponed.");
        }
    };
    /** --------------------------------------------------------------------
   *
   *                    Action on input of Variation Id
   *
   *  --------------------------------------------------------------------
   */
    WarehouseAddItemComponent.prototype.onVariationInput = function () {
        var _this = this;
        var varValue = parseInt(this.varLookupValue);
        if (varValue < 1)
            return;
        app_1.App.window.loadComponentIntoModalFrame(whs_loc_list_wcomp_1.WarehouseLocationListComponent, {
            title: "Boxes which contains items similar to selected",
            data: {
                variationId: this.varLookupValue
            },
            onSuccess: function () { },
            onLoad: function () { },
            onClose: function () { app_1.App.inputScannerObserver.resumeListenerWithGIUD(_this.listeners); },
            onFailure: function () { },
            getReference: function () { app_1.App.inputScannerObserver.pauseListenerWithGIUD(_this.listeners); }
        });
    };
    /** --------------------------------------------------------------------
   *
   *                      Action on command scan
   *
   *  --------------------------------------------------------------------
   *  @param inputCommand
   */
    WarehouseAddItemComponent.prototype.onScannedCommand = function (inputCommand) {
        console.log("CE EXEC " + inputCommand);
        if (this.currentModalComponent !== null) {
            this.currentModalComponent.waitForLocationScan(inputCommand);
        }
        this.scannedCommand = inputCommand;
    };
    /** --------------------------------------------------------------------
   *
   *                        Open new scanning modal
   *
   *  --------------------------------------------------------------------
   */
    WarehouseAddItemComponent.prototype.openScanningModal = function (withScannedData) {
        var _this = this;
        if (this.currentModalComponent === null) {
            app_1.App.window.loadComponentIntoModalFrame(whsaddscan_wcomp_1.WarehouseAddScannerItemsFormComponent, {
                title: "List of scanned items",
                data: withScannedData,
                onSuccess: function (data) {
                },
                onFailure: function (err) {
                },
                onClose: function (data) {
                    _this.currentModalComponent = null;
                    _this.currentModalComponentType = null;
                },
                getReference: function (ref) {
                    /** @type {WarehouseAddScannerItemsFormComponent} */
                    _this.currentModalComponent = ref;
                    _this.currentModalComponentType = 1;
                    _this.currentModalComponent.appendInputItem(withScannedData);
                }
            });
        }
        else {
            this.currentModalComponent.appendInputItem(withScannedData);
        }
    };
    /** --------------------------------------------------------------------
   *
   *              Open New Modal With Location Showing Items
   *
   *  --------------------------------------------------------------------
   *
   * @param locationId
   */
    WarehouseAddItemComponent.prototype.openLocationModal = function (locationId) {
        var _this = this;
        if (this.currentModalComponent === null) {
            app_1.App.window.loadComponentIntoModalFrame(whslocscan_wcomp_1.WarehouseLocationScannedComponent, {
                title: "List of items in the scanned location id: " + locationId,
                data: { id: locationId },
                onSuccess: function (data) {
                },
                onFailure: function (err) {
                },
                onClose: function (data) {
                    _this.currentModalComponent = null;
                    _this.currentModalComponentType = null;
                    app_1.App.inputScannerObserver.resumeListenerWithGIUD(_this.listeners);
                },
                getReference: function (ref) {
                    /** @type {WarehouseAddScannerItemsFormComponent} */
                    _this.currentModalComponent = ref;
                    _this.currentModalComponentType = 1;
                    app_1.App.inputScannerObserver.pauseListenerWithGIUD(_this.listeners);
                }
            });
        }
    };
    __decorate([
        core_1.Input(), 
        __metadata('design:type', Boolean)
    ], WarehouseAddItemComponent.prototype, "_isVisible", void 0);
    WarehouseAddItemComponent = __decorate([
        core_1.Component({
            selector: "whsadditem",
            host: {
                '[class.hiddenFrame]': '!_isVisible'
            },
            template: "\n    <div>\n    \n      <h1> Scanning and item addition </h1>\n\n      <!--Scan panel for see current scanning issues-->\n      <div class=\"panel panel-default\">\n      \n        <div class=\"panel-heading\"> Select action or scan barcode </div>\n        \n        <div class=\"panel-body\">\n        \n          <table class=\"table\">\n\n            <tr>\n              <td colspan=\"2\"> Add item manually </td>\n              <td style=\"padding-bottom: 20px;\"> <button class=\"btn btn-primary btn-wide\" (click)=\"openScanningModal(null)\">Add</button></td>\n            </tr>\n            \n            <tr>\n              <td> Type-in UPC code for lookup </td>\n              <td> <input class=\"form-control\" [(ngModel)]=\"upcLookupValue\" type=\"text\"> </td>\n              <td style=\"padding-bottom: 20px;\">\n                <button class=\"btn btn-primary btn-wide\" (click)=\"onUPCInput()\">Search </button>\n              </td>\n            </tr>\n            \n            <tr>\n              <td> Type-in location id </td>\n              <td> <input class=\"form-control\" [(ngModel)]=\"boxLookupValue\" type=\"text\"> </td>\n              <td style=\"padding-bottom: 20px;\">\n                <button class=\"btn btn-primary btn-wide\"\n                (click)=\"onLocationInput()\">Search </button>\n              </td>\n            </tr>\n            \n            <tr>\n              <td> Type-in variation id </td>\n              <td> <input class=\"form-control\" [(ngModel)]=\"varLookupValue\" type=\"text\"> </td>\n              <td style=\"padding-bottom: 20px;\">\n                <button class=\"btn btn-primary btn-wide\"\n                (click)=\"onVariationInput()\">Search </button>\n              </td>\n            </tr>\n            \n          </table>\n          \n        </div>\n        \n      </div>\n      \n    </div>\n  ",
            styles: ["\n  \n  "]
        }), 
        __metadata('design:paramtypes', [])
    ], WarehouseAddItemComponent);
    return WarehouseAddItemComponent;
}(window_component_1.WindowComponent));
exports.WarehouseAddItemComponent = WarehouseAddItemComponent;
//# sourceMappingURL=warehouse.additem.js.map