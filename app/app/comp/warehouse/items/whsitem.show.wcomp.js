"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var whsent_comp_1 = require("../entities/whsent.comp");
var colorize_pipe_1 = require("../../../system/pipes/colorize.pipe");
var whsitem_abstract_1 = require("./whsitem.abstract");
var WarehouseItemShowComponent = (function (_super) {
    __extends(WarehouseItemShowComponent, _super);
    function WarehouseItemShowComponent() {
        _super.apply(this, arguments);
        this.expanded = false;
        this.onDeleteCommand = new core_1.EventEmitter();
        this.onEntityObjectRefresh = new core_1.EventEmitter();
    }
    Object.defineProperty(WarehouseItemShowComponent.prototype, "listeners", {
        set: function (value) {
            this._listeners = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(WarehouseItemShowComponent.prototype, "warehouseEntity", {
        /** --------------------------------------------------------------------------------------------------------
         *
         *                                      ENTITY SETTER
         *
         *  --------------------------------------------------------------------------------------------------------
         *  @param value
         */
        set: function (value) {
            if (value.hasOwnProperty("id")) {
                var e = new whsent_comp_1.WarehouseScannedItemEntity();
                e.fromWarehouseItemEntity(value);
                this._warehouseEntity = e;
            }
            else if (value.hasOwnProperty("scannedId") && !value.hasOwnProperty("updated")) {
                var e = new whsent_comp_1.WarehouseScannedItemEntity();
                e.fromObject(value);
                this._warehouseEntity = e;
            }
            else {
                this._warehouseEntity = value;
            }
        },
        enumerable: true,
        configurable: true
    });
    /** -----------------------------------------------------------------------------------------
     *
     *                          Overriding Super.objectRefresh() method
     *
     *  -----------------------------------------------------------------------------------------
     */
    WarehouseItemShowComponent.prototype.objectRefresh = function (data) {
        console.log("Child object refresh in effect");
        this.onEntityObjectRefresh.emit(data);
    };
    /** -----------------------------------------------------------------------------------------
     *
     *
     *
     *  -----------------------------------------------------------------------------------------
     */
    WarehouseItemShowComponent.prototype.deleteCommand = function () {
        this.onDeleteCommand.emit(this._warehouseEntity);
    };
    __decorate([
        core_1.Input(), 
        __metadata('design:type', Boolean)
    ], WarehouseItemShowComponent.prototype, "expanded", void 0);
    __decorate([
        core_1.Output(), 
        __metadata('design:type', Object)
    ], WarehouseItemShowComponent.prototype, "onDeleteCommand", void 0);
    __decorate([
        core_1.Output(), 
        __metadata('design:type', Object)
    ], WarehouseItemShowComponent.prototype, "onEntityObjectRefresh", void 0);
    __decorate([
        core_1.Input(), 
        __metadata('design:type', Array), 
        __metadata('design:paramtypes', [Array])
    ], WarehouseItemShowComponent.prototype, "listeners", null);
    __decorate([
        core_1.Input(), 
        __metadata('design:type', Object), 
        __metadata('design:paramtypes', [Object])
    ], WarehouseItemShowComponent.prototype, "warehouseEntity", null);
    WarehouseItemShowComponent = __decorate([
        core_1.Component({
            selector: "whslocshowitem",
            host: {},
            template: "\n    <div class=\"whs-item-entity\">\n    \n      <div class=\"whs-iteme-left\">\n      \n        <div class=\"whs-iteme-imgqty\">\n          <img style=\"width: inherit; height: inherit\" src=\"{{_warehouseEntity?.image}}\">\n          \n          <div class=\"whs-iteme-imgqty-qty\">\n            <i class=\"fa fa-database\"></i>&nbsp;{{_warehouseEntity.quantity}}&nbsp;\n            <i class=\"fa fa-hand-lizard-o\"></i>&nbsp;{{_warehouseEntity.selectedQuantity}}\n          </div>\n        </div>\n        \n        <ul class=\"scanned-item-left nolist\" style=\"text-align: left;\">\n\n          <li><i class=\"fa fa-barcode\"></i>&nbsp;{{_warehouseEntity.scannedId}}</li>\n          <li><i class=\"fa fa-tag\"></i><span [innerHTML]=\"_warehouseEntity.extras | colorizeWHIObject\"></span></li>\n          <li><i class=\"fa fa-file-text\"></i>&nbsp;{{_warehouseEntity.title}}</li>\n          \n        </ul>\n        \n      </div>\n      \n      <div class=\"whs-iteme-right\">\n      \n        <button class=\"btn btn-primary scanned-button\"\n          [class.hiddenFrame]=\"_warehouseEntity.resolved\"\n          (click)=\"resolveManual()\">\n          <i class=\"fa fa-search\"></i>\n        </button>\n        \n        <button \n          class=\"btn btn-primary scanned-button\"\n          [style.background-color]=\"_warehouseEntity.labeled?'green':'#333333'\"\n          [class.hiddenFrame]=\"!_warehouseEntity.warehouseId\"\n          (click)=\"printItemLabel()\"> Print\n        </button>\n        \n        <button\n          class=\"btn btn-primary scanned-button\"\n          [class.hiddenFrame]=\"!_warehouseEntity.resolved\"\n          (click)=\"editItem()\">\n          <i class=\"fa fa-pencil-square-o\"></i>\n        </button>\n        \n        <button \n          class=\"btn btn-primary\"\n          [class.hiddenFrame]=\"!_warehouseEntity.warehouseId\" \n          (click)=\"reserveItem()\">\n            <i class=\"fa fa-exclamation-triangle\"></i>\n        </button>\n        \n        <button class=\"btn btn-primary\"\n          [class.hiddenFrame]=\"!_warehouseEntity.warehouseId\"\n          (click)=\"shipItem()\">\n            <i class=\"fa fa-arrow-up\"></i>\n        </button>\n        \n        <button class=\"btn btn-danger\" (click)=\"deleteCommand()\">\n          <i class=\"fa fa-trash\"></i>\n        </button>\n        \n      </div>\n      \n      <div style=\"clear: both; width: 100%; height:1px;\"></div>\n      \n    </div>\n  ",
            styles: ["\n\n  "],
            pipes: [
                colorize_pipe_1.WHIObjectToColorizedString
            ]
        }), 
        __metadata('design:paramtypes', [])
    ], WarehouseItemShowComponent);
    return WarehouseItemShowComponent;
}(whsitem_abstract_1.WarehouseItemComponentModel));
exports.WarehouseItemShowComponent = WarehouseItemShowComponent;
//# sourceMappingURL=whsitem.show.wcomp.js.map