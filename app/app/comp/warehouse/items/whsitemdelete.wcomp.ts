import {WindowComponent} from "../../../system/window/window.component";
import {ComponentImplementingModalFramePass} from "../../../system/entities/system.entities";
import {Component} from "@angular/core";
import {WarehouseScannedItemEntity} from "../entities/whsent.comp";
import {ApiConnectorComponent} from "../../../system/connection/apiconnector.component";
import {App} from "../../../system/app";
import {WarehouseApiConnector} from "../../../system/connection/whsapiconnector";

@Component({
  selector: 'whsaddscan',
  template: `
     <div>
        
        <form style="text-align: center; color: #0f0f0f;">
          
          <div>
            <h2> Are you sure you want to delete this item </h2>
            <h4>{{entity.scannedId}}</h4>
            <h4>{{entity.title}}</h4>
          </div>
          
          <div>
            {{message}}
          </div>
          
          <div style="margin-top: 20px">
            <button class="btn btn-primary btn-wide" (click)="saveForm($event)"> Yes </button>
            <button class="btn btn-primary btn-wide" (click)="closeForm($event)"> Cancel </button>
          </div>
          
        </form>
        
     </div>
  `,
  host: {
    'class':'addscanner',
  },
  styles: [
    `
      .left-side {
        color: #333333;
        display: inline-block;
        width: 49%;
        vertical-align: top;
      }
      
      .form-icons {
        min-width:18px;
      }
      
      .form-label {
        min-width: 20%;
      }
      
      .left-side input {
        min-width: 192px;
        width: 70%;
        display: inline-block;
        padding: 4px;
        border-radius: 2px;
        border:1px solid #CCCCCC;
        margin: 2px;
      }
      
      .right-side {
        color: #333333;
        text-align: center;
        display: inline-block;
        width: 49%;
        vertical-align: top;
      }
      
    `
  ],
  directives: [
  ]
})
/** -----------------------------------------------------------------------------------------  \
 *                                    Component class
 *  ----------------------------------------------------------------------------------------- */
export class WarehouseItemDeleteComponent extends WindowComponent implements ComponentImplementingModalFramePass {

  public dataObject:any;
  public closingMethod:any;
  public successCallback:any;
  public failureCallback:any;

  public message: string = "";

  public entity: WarehouseScannedItemEntity = null;

  public whsApiConn: WarehouseApiConnector = null;

  public listeners: Array<string> = [];

  /** -----------------------------------------------------------------------------------------
   *
   *  ----------------------------------------------------------------------------------------- */
  constructor() {

    this.entity = new WarehouseScannedItemEntity();
    this.whsApiConn = ApiConnectorComponent.getWarehouseApiConnector();

  }

  /** -----------------------------------------------------------------------------------------
   *
   *  ----------------------------------------------------------------------------------------- */
  public onLoad() {

    this.listeners.push(App.inputScannerObserver.addCommandListener("close", ()=>{
      this.closeForm(null);
    }));

    this.entity = this.dataObject;

  }

  /** -----------------------------------------------------------------------------------------
   *
   *  ----------------------------------------------------------------------------------------- */
  public onClose() {

    App.inputScannerObserver.removeListenerWithGUID(this.listeners);

  }


  /** -----------------------------------------------------------------------------------------
   *
   *  ----------------------------------------------------------------------------------------- */
  public saveForm($e) {

    $e.preventDefault();
    this.message = "Deletion in process";
    this.whsApiConn.archiveItem(this.entity.toWarehouseItemEntity()).subscribe((data) => {
      this.message = "Deleted";
      this.successCallback(this.entity);
      this.closingMethod();
    }, (err) => {
      this.message = "Failed to delete an Item";
    });

  }

  /** -----------------------------------------------------------------------------------------
   *
   *  ----------------------------------------------------------------------------------------- */
  public closeForm($e:any=null) {

    if($e !== null)
      $e.preventDefault();

    this.closingMethod();

  }

}
