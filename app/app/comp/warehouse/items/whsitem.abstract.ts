import {WarehouseScannedItemEntity} from "../entities/whsent.comp";
import {App} from "../../../system/app";
import {WarehouseEditItemComponent} from "./whsitemedit.wcomp";
import {WarehouseItemResolveComponent} from "../additems/whsaddreslv.wcomp";
import {WarehouseItemReservationConfirmComponent} from "../reserve/reserveconf.wcomp";
import {WarehouseItemShipConfirmationComponent} from "../ship/shipconf.wcomp";

export abstract class WarehouseItemComponentModel {

  protected _warehouseEntity: WarehouseScannedItemEntity = null;

  protected _listeners: Array<string> = [];

  /** -----------------------------------------------------------------------------------------
   *
   *                        Raise Edit Item Form
   *
   *  -----------------------------------------------------------------------------------------
   */
  public editItem() {

    App.window.loadComponentIntoModalFrame(WarehouseEditItemComponent, {
      title: "Edit item",
      data: this._warehouseEntity,
      onSuccess: (data) => {

        this._warehouseEntity.variationId = data.variation.id;
        this._warehouseEntity.title = data.item.title;
        this._warehouseEntity.quantity = data.quantity;

      },
      onFailure: () => {

      },
      onClose: () => {
        App.inputScannerObserver.resumeListenerWithGIUD(this._listeners);
      },
      getReference: (ref) => {
        App.inputScannerObserver.pauseListenerWithGIUD(this._listeners);
      }

    });

  }

  /** -----------------------------------------------------------------------------------------
   *
   *                          Raise a Manual Item Resolve Form
   *
   *  -----------------------------------------------------------------------------------------
   */
  public resolveManual() {

    App.window.loadComponentIntoModalFrame(WarehouseItemResolveComponent, {
      title: "Manual resolve scanned item",
      data: {},
      onSuccess: (data) => {

        if(this._warehouseEntity.scannedId == null)
          this._warehouseEntity.scannedId = data.variation.code;

        this._warehouseEntity.resolved = true;
        this._warehouseEntity.variationId = data.variation.id;
        this._warehouseEntity.title = data.item.title;
        this._warehouseEntity.variationObject = data.variation;
        this._warehouseEntity.itemObject = data.item;

        if(data.variation != null) {

          if (data.variation.color !== null) {
            this._warehouseEntity.setExtraOption("color", data.variation.color);
          }

          if (data.variation.size !== null) {
            this._warehouseEntity.setExtraOption("size", data.variation.size);
          }

          if (data.item.definedStyle !== null) {
            this._warehouseEntity.setExtraOption("style", data.item.definedStyle);
          }

          if (typeof data.variation.info !== "undefined"
            && typeof data.variation.info.images !== "undefined") {

            if (typeof data.variation.info.images.titleImage !== "undefined") {

              this._warehouseEntity.setExtraOption("image", data.variation.info.images.titleImage.src +
                "s." + data.variation.info.images.titleImage.ext);
              this._warehouseEntity.image = data.variation.info.images.titleImage.src +
                "s." + data.variation.info.images.titleImage.ext;
            }

          }

          this.objectRefresh(this._warehouseEntity);

        }

      },
      onFailure: () => {

      },
      onClose: () => {
        App.inputScannerObserver.resumeListenerWithGIUD(this._listeners);
      },
      getReference: (ref) => {
        App.inputScannerObserver.pauseListenerWithGIUD(this._listeners);
      }

    });

  }

  /** -----------------------------------------------------------------------------------------
   *
   *                  Refresh object public method (allows to be overridden)
   *
   *  -----------------------------------------------------------------------------------------
   */
  public objectRefresh(data){

  }

  /** -----------------------------------------------------------------------------------------
   *
   *                  Print Item Label with attached printer
   *
   *  -----------------------------------------------------------------------------------------
   */
  public printItemLabel() {

    this._warehouseEntity.printItemLabel(() => {
      this.objectRefresh(this._warehouseEntity);
    });

  }

  /** -----------------------------------------------------------------------------------------
   *
   *                  Open Form for Item Reservation Confirmation
   *
   *  -----------------------------------------------------------------------------------------
   */
  public reserveItem() {

    App.window.loadComponentIntoModalFrame(WarehouseItemReservationConfirmComponent, {
      title: "Create Item Reservation",
      data: {},
      onSuccess: (data) => {

        this.objectRefresh(this._warehouseEntity);

      },
      onFailure: () => {

      },
      onClose: () => {
        App.inputScannerObserver.resumeListenerWithGIUD(this._listeners);
      },
      getReference: (ref) => {
        App.inputScannerObserver.pauseListenerWithGIUD(this._listeners);
      }

    });

  }

  /** -----------------------------------------------------------------------------------------
   *
   *                  Open form for Item Shipment Confirmation
   *
   *  -----------------------------------------------------------------------------------------
   */
  public shipItem() {

    App.window.loadComponentIntoModalFrame(WarehouseItemShipConfirmationComponent, {
      title: "Confirm item shipment",
      data: {},
      onSuccess: (data) => {

        this.objectRefresh(this._warehouseEntity);

      },
      onFailure: () => {

      },
      onClose: () => {
        App.inputScannerObserver.resumeListenerWithGIUD(this._listeners);
      },
      getReference: (ref) => {
        App.inputScannerObserver.pauseListenerWithGIUD(this._listeners);
      }
    });

  }


}
