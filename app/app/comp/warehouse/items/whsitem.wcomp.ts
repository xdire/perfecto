import {Component, Input, Output, EventEmitter} from "@angular/core";
import {WarehouseScannedItemEntity, WarehouseScannedItem} from "../entities/whsent.comp";
import {WarehouseItemEntity} from "../../../system/connection/connection-entities/whsapi-entities";
import {ItemButtonShowSimilar} from "../../items/buttons/ib.showsimilar.wcomp";
import {WHIObjectToColorizedString} from "../../../system/pipes/colorize.pipe";
import {WarehouseItemComponentModel} from "./whsitem.abstract";

@Component({
  selector: "whslocitem",
  host: {

  },
  template: `
    <div class="whs-item-entity">
    
      <div class="whs-iteme-left">
      
        <div class="whs-iteme-imgqty">
          <img style="height: inherit" src="{{_warehouseEntity?.image? _warehouseEntity.image : 'https://s3.amazonaws.com/afimg/nopicture.png'}}">
          <div class="whs-iteme-imgqty-qty">
            <i class="fa fa-database"></i>&nbsp;{{_warehouseEntity.quantity}}&nbsp;
            <i class="fa fa-hand-lizard-o"></i>&nbsp;{{_warehouseEntity.selectedQuantity}}
          </div>
        </div>
        
        <ul class="scanned-item-left nolist" style="text-align: left;">

          <li><i class="fa fa-barcode"></i>&nbsp;{{_warehouseEntity.scannedId}}</li>
          <li><i class="fa fa-tag"></i><span [innerHTML]="_warehouseEntity.extras | colorizeWHIObject"></span></li>
          <li><i class="fa fa-file-text"></i>&nbsp;{{_warehouseEntity.title}}</li>
          
        </ul>
        
      </div>
      
      <div class="whs-iteme-right">
      
        <button class="btn btn-primary scanned-button"
          [class.hiddenFrame]="_warehouseEntity.resolved"
          (click)="resolveManual()">
          <i class="fa fa-search"></i>
        </button>
        <button 
          class="btn btn-primary scanned-button"
          [style.background-color]="_warehouseEntity.labeled?'green':'#333333'"
          [class.hiddenFrame]="!_warehouseEntity.warehouseId"
          (click)="printItemLabel()"> Print
        </button>
        <button
          class="btn btn-primary scanned-button"
          [class.hiddenFrame]="!_warehouseEntity.resolved"
          (click)="editItem()">
          <i class="fa fa-pencil-square-o"></i>
        </button>
        
        <ib-show-similar 
          [variationId]="_warehouseEntity.variationId"
          [listeners]="_listeners"
          [class.hiddenFrame]="!_warehouseEntity.resolved">
        </ib-show-similar>
        
        <button 
          class="btn btn-primary"
          [class.hiddenFrame]="!_warehouseEntity.warehouseId" 
          (click)="reserveItem()">
            <i class="fa fa-exclamation-triangle"></i>
        </button>
        
        <button class="btn btn-primary"
          [class.hiddenFrame]="!_warehouseEntity.warehouseId"
          (click)="shipItem()">
            <i class="fa fa-arrow-up"></i>
        </button>
        
        <button class="btn btn-danger" (click)="deleteCommand()">
          <i class="fa fa-trash"></i>
        </button>
        
      </div>
      
      <div style="clear: both; width: 100%; height:1px;"></div>
      
    </div>
  `,
  styles:[`

  `],
  directives:[
    ItemButtonShowSimilar
  ],
  pipes:[
    WHIObjectToColorizedString
  ]
})

export class WarehouseItemComponent extends WarehouseItemComponentModel {

  @Input() public expanded: boolean = false;

  @Output() public onDeleteCommand = new EventEmitter<WarehouseScannedItemEntity>();

  @Output() public onEntityObjectRefresh = new EventEmitter();

  @Input() set listeners(value: Array<string>){
    this._listeners = value;
  }

	/** --------------------------------------------------------------------------------------------------------
   *
   *                                          ENTITY SETTER
   *
   *  --------------------------------------------------------------------------------------------------------
   *  @param value
   */
  @Input() set warehouseEntity(value:WarehouseItemEntity | WarehouseScannedItem | WarehouseScannedItemEntity) {

    if(value.hasOwnProperty("id")) {

      var e = new WarehouseScannedItemEntity();
      e.fromWarehouseItemEntity(value);
      this._warehouseEntity = e;

    } else if(value.hasOwnProperty("scannedId") && !value.hasOwnProperty("updated")) {

      var e = new WarehouseScannedItemEntity();
      e.fromObject(<WarehouseScannedItem>value);
      this._warehouseEntity = e;

    } else {

      this._warehouseEntity = <WarehouseScannedItemEntity>value;

    }

  }

	/** -----------------------------------------------------------------------------------------
   *
   *
   *
   *  -----------------------------------------------------------------------------------------
   */
  ngAfterViewInit() {

    if(this._warehouseEntity.scannedId == null) {
      this.resolveManual();
    }

  }

  /** -----------------------------------------------------------------------------------------
   *
   *                      Overriding Super.objectRefresh() method
   *
   *  -----------------------------------------------------------------------------------------
   */
  public objectRefresh(data) {
    this.onEntityObjectRefresh.emit(data);
  }

  /** -----------------------------------------------------------------------------------------
   *
   *
   *
   *  -----------------------------------------------------------------------------------------
   */
  public deleteCommand() {
    this.onDeleteCommand.emit(this._warehouseEntity);
  }


}
