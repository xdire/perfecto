"use strict";
var app_1 = require("../../../system/app");
var whsitemedit_wcomp_1 = require("./whsitemedit.wcomp");
var whsaddreslv_wcomp_1 = require("../additems/whsaddreslv.wcomp");
var reserveconf_wcomp_1 = require("../reserve/reserveconf.wcomp");
var shipconf_wcomp_1 = require("../ship/shipconf.wcomp");
var WarehouseItemComponentModel = (function () {
    function WarehouseItemComponentModel() {
        this._warehouseEntity = null;
        this._listeners = [];
    }
    /** -----------------------------------------------------------------------------------------
     *
     *                        Raise Edit Item Form
     *
     *  -----------------------------------------------------------------------------------------
     */
    WarehouseItemComponentModel.prototype.editItem = function () {
        var _this = this;
        app_1.App.window.loadComponentIntoModalFrame(whsitemedit_wcomp_1.WarehouseEditItemComponent, {
            title: "Edit item",
            data: this._warehouseEntity,
            onSuccess: function (data) {
                _this._warehouseEntity.variationId = data.variation.id;
                _this._warehouseEntity.title = data.item.title;
                _this._warehouseEntity.quantity = data.quantity;
            },
            onFailure: function () {
            },
            onClose: function () {
                app_1.App.inputScannerObserver.resumeListenerWithGIUD(_this._listeners);
            },
            getReference: function (ref) {
                app_1.App.inputScannerObserver.pauseListenerWithGIUD(_this._listeners);
            }
        });
    };
    /** -----------------------------------------------------------------------------------------
     *
     *                          Raise a Manual Item Resolve Form
     *
     *  -----------------------------------------------------------------------------------------
     */
    WarehouseItemComponentModel.prototype.resolveManual = function () {
        var _this = this;
        app_1.App.window.loadComponentIntoModalFrame(whsaddreslv_wcomp_1.WarehouseItemResolveComponent, {
            title: "Manual resolve scanned item",
            data: {},
            onSuccess: function (data) {
                if (_this._warehouseEntity.scannedId == null)
                    _this._warehouseEntity.scannedId = data.variation.code;
                _this._warehouseEntity.resolved = true;
                _this._warehouseEntity.variationId = data.variation.id;
                _this._warehouseEntity.title = data.item.title;
                _this._warehouseEntity.variationObject = data.variation;
                _this._warehouseEntity.itemObject = data.item;
                if (data.variation != null) {
                    if (data.variation.color !== null) {
                        _this._warehouseEntity.setExtraOption("color", data.variation.color);
                    }
                    if (data.variation.size !== null) {
                        _this._warehouseEntity.setExtraOption("size", data.variation.size);
                    }
                    if (data.item.definedStyle !== null) {
                        _this._warehouseEntity.setExtraOption("style", data.item.definedStyle);
                    }
                    if (typeof data.variation.info !== "undefined"
                        && typeof data.variation.info.images !== "undefined") {
                        if (typeof data.variation.info.images.titleImage !== "undefined") {
                            _this._warehouseEntity.setExtraOption("image", data.variation.info.images.titleImage.src +
                                "s." + data.variation.info.images.titleImage.ext);
                            _this._warehouseEntity.image = data.variation.info.images.titleImage.src +
                                "s." + data.variation.info.images.titleImage.ext;
                        }
                    }
                    _this.objectRefresh(_this._warehouseEntity);
                }
            },
            onFailure: function () {
            },
            onClose: function () {
                app_1.App.inputScannerObserver.resumeListenerWithGIUD(_this._listeners);
            },
            getReference: function (ref) {
                app_1.App.inputScannerObserver.pauseListenerWithGIUD(_this._listeners);
            }
        });
    };
    /** -----------------------------------------------------------------------------------------
     *
     *                  Refresh object public method (allows to be overridden)
     *
     *  -----------------------------------------------------------------------------------------
     */
    WarehouseItemComponentModel.prototype.objectRefresh = function (data) {
    };
    /** -----------------------------------------------------------------------------------------
     *
     *                  Print Item Label with attached printer
     *
     *  -----------------------------------------------------------------------------------------
     */
    WarehouseItemComponentModel.prototype.printItemLabel = function () {
        var _this = this;
        this._warehouseEntity.printItemLabel(function () {
            _this.objectRefresh(_this._warehouseEntity);
        });
    };
    /** -----------------------------------------------------------------------------------------
     *
     *                  Open Form for Item Reservation Confirmation
     *
     *  -----------------------------------------------------------------------------------------
     */
    WarehouseItemComponentModel.prototype.reserveItem = function () {
        var _this = this;
        app_1.App.window.loadComponentIntoModalFrame(reserveconf_wcomp_1.WarehouseItemReservationConfirmComponent, {
            title: "Create Item Reservation",
            data: {},
            onSuccess: function (data) {
                _this.objectRefresh(_this._warehouseEntity);
            },
            onFailure: function () {
            },
            onClose: function () {
                app_1.App.inputScannerObserver.resumeListenerWithGIUD(_this._listeners);
            },
            getReference: function (ref) {
                app_1.App.inputScannerObserver.pauseListenerWithGIUD(_this._listeners);
            }
        });
    };
    /** -----------------------------------------------------------------------------------------
     *
     *                  Open form for Item Shipment Confirmation
     *
     *  -----------------------------------------------------------------------------------------
     */
    WarehouseItemComponentModel.prototype.shipItem = function () {
        var _this = this;
        app_1.App.window.loadComponentIntoModalFrame(shipconf_wcomp_1.WarehouseItemShipConfirmationComponent, {
            title: "Confirm item shipment",
            data: {},
            onSuccess: function (data) {
                _this.objectRefresh(_this._warehouseEntity);
            },
            onFailure: function () {
            },
            onClose: function () {
                app_1.App.inputScannerObserver.resumeListenerWithGIUD(_this._listeners);
            },
            getReference: function (ref) {
                app_1.App.inputScannerObserver.pauseListenerWithGIUD(_this._listeners);
            }
        });
    };
    return WarehouseItemComponentModel;
}());
exports.WarehouseItemComponentModel = WarehouseItemComponentModel;
//# sourceMappingURL=whsitem.abstract.js.map