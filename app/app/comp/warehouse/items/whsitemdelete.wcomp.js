"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var window_component_1 = require("../../../system/window/window.component");
var core_1 = require("@angular/core");
var whsent_comp_1 = require("../entities/whsent.comp");
var apiconnector_component_1 = require("../../../system/connection/apiconnector.component");
var app_1 = require("../../../system/app");
var WarehouseItemDeleteComponent = (function (_super) {
    __extends(WarehouseItemDeleteComponent, _super);
    /** -----------------------------------------------------------------------------------------
     *
     *  ----------------------------------------------------------------------------------------- */
    function WarehouseItemDeleteComponent() {
        this.message = "";
        this.entity = null;
        this.whsApiConn = null;
        this.listeners = [];
        this.entity = new whsent_comp_1.WarehouseScannedItemEntity();
        this.whsApiConn = apiconnector_component_1.ApiConnectorComponent.getWarehouseApiConnector();
    }
    /** -----------------------------------------------------------------------------------------
     *
     *  ----------------------------------------------------------------------------------------- */
    WarehouseItemDeleteComponent.prototype.onLoad = function () {
        var _this = this;
        this.listeners.push(app_1.App.inputScannerObserver.addCommandListener("close", function () {
            _this.closeForm(null);
        }));
        this.entity = this.dataObject;
    };
    /** -----------------------------------------------------------------------------------------
     *
     *  ----------------------------------------------------------------------------------------- */
    WarehouseItemDeleteComponent.prototype.onClose = function () {
        app_1.App.inputScannerObserver.removeListenerWithGUID(this.listeners);
    };
    /** -----------------------------------------------------------------------------------------
     *
     *  ----------------------------------------------------------------------------------------- */
    WarehouseItemDeleteComponent.prototype.saveForm = function ($e) {
        var _this = this;
        $e.preventDefault();
        this.message = "Deletion in process";
        this.whsApiConn.archiveItem(this.entity.toWarehouseItemEntity()).subscribe(function (data) {
            _this.message = "Deleted";
            _this.successCallback(_this.entity);
            _this.closingMethod();
        }, function (err) {
            _this.message = "Failed to delete an Item";
        });
    };
    /** -----------------------------------------------------------------------------------------
     *
     *  ----------------------------------------------------------------------------------------- */
    WarehouseItemDeleteComponent.prototype.closeForm = function ($e) {
        if ($e === void 0) { $e = null; }
        if ($e !== null)
            $e.preventDefault();
        this.closingMethod();
    };
    WarehouseItemDeleteComponent = __decorate([
        core_1.Component({
            selector: 'whsaddscan',
            template: "\n     <div>\n        \n        <form style=\"text-align: center; color: #0f0f0f;\">\n          \n          <div>\n            <h2> Are you sure you want to delete this item </h2>\n            <h4>{{entity.scannedId}}</h4>\n            <h4>{{entity.title}}</h4>\n          </div>\n          \n          <div>\n            {{message}}\n          </div>\n          \n          <div style=\"margin-top: 20px\">\n            <button class=\"btn btn-primary btn-wide\" (click)=\"saveForm($event)\"> Yes </button>\n            <button class=\"btn btn-primary btn-wide\" (click)=\"closeForm($event)\"> Cancel </button>\n          </div>\n          \n        </form>\n        \n     </div>\n  ",
            host: {
                'class': 'addscanner',
            },
            styles: [
                "\n      .left-side {\n        color: #333333;\n        display: inline-block;\n        width: 49%;\n        vertical-align: top;\n      }\n      \n      .form-icons {\n        min-width:18px;\n      }\n      \n      .form-label {\n        min-width: 20%;\n      }\n      \n      .left-side input {\n        min-width: 192px;\n        width: 70%;\n        display: inline-block;\n        padding: 4px;\n        border-radius: 2px;\n        border:1px solid #CCCCCC;\n        margin: 2px;\n      }\n      \n      .right-side {\n        color: #333333;\n        text-align: center;\n        display: inline-block;\n        width: 49%;\n        vertical-align: top;\n      }\n      \n    "
            ],
            directives: []
        }), 
        __metadata('design:paramtypes', [])
    ], WarehouseItemDeleteComponent);
    return WarehouseItemDeleteComponent;
}(window_component_1.WindowComponent));
exports.WarehouseItemDeleteComponent = WarehouseItemDeleteComponent;
//# sourceMappingURL=whsitemdelete.wcomp.js.map