import {WindowComponent} from "../../../system/window/window.component";
import {ComponentImplementingModalFramePass} from "../../../system/entities/system.entities";
import {Component} from "@angular/core";
import {WarehouseScannedItemEntity} from "../entities/whsent.comp";
import {ApiConnectorComponent} from "../../../system/connection/apiconnector.component";
import {App} from "../../../system/app";

@Component({
  selector: 'whsaddscan',
  template: `
     <div>
        
        <form>
          
          <div class="left-side">
            
            <div>
            <i class="fa fa-pencil form-icons" aria-hidden="true"></i> <input [(ngModel)]="entity.title">
            </div>
            <div>
            <i class="fa fa-tag form-icons" aria-hidden="true"></i> <input [(ngModel)]="entity.variationId">
            </div>
            <div>
            <i class="fa fa-picture-o form-icons" aria-hidden="true"></i> <input [(ngModel)]="entity.image">
            </div>
            <h4>Extra properties</h4>
            <ul style="list-style-type: none;">
              <li
                *ngFor="let item of extras">
                <label class="form-label">{{item}}</label> &nbsp;<input [(ngModel)]="entity.extras[item]"><br>
              </li>
            </ul>
            
          </div>
          
          <div class="right-side">
            <h1> Quantity </h1>
            <button class="btn btn-primary btn-wide" (click)="increaseQty()"><i class="fa fa-chevron-up" aria-hidden="true"></i></button>
            <div class="header-block">{{entity.quantity}}</div>
            <button class="btn btn-primary btn-wide" (click)="decreaseQty()"><i class="fa fa-chevron-down" aria-hidden="true"></i></button>
          </div>
          
          <div class="center-text offset-top-m">
            <button class="btn btn-primary btn-wide" (click)="saveForm($event);"> Save </button>
            <button class="btn btn-primary btn-wide" (click)="closeForm($event);"> Cancel </button>
          </div>
          
        </form>
        
     </div>
  `,
  host: {
    'class':'addscanner',
  },
  styles: [
    `
      .left-side {
        color: #333333;
        display: inline-block;
        width: 49%;
        vertical-align: top;
      }
      
      .form-icons {
        min-width:18px;
      }
      
      .form-label {
        min-width: 20%;
      }
      
      .left-side input {
        min-width: 192px;
        width: 70%;
        display: inline-block;
        padding: 4px;
        border-radius: 2px;
        border:1px solid #CCCCCC;
        margin: 2px;
      }
      
      .right-side {
        color: #333333;
        text-align: center;
        display: inline-block;
        width: 49%;
        vertical-align: top;
      }
      
    `
  ],
  directives: [
  ]
})
/** -----------------------------------------------------------------------------------------  \
 *                                    Component class
 *  ----------------------------------------------------------------------------------------- */
export class WarehouseEditItemComponent extends WindowComponent implements ComponentImplementingModalFramePass {

	public dataObject:any;
  public closingMethod:any;
  public successCallback:any;
  public failureCallback:any;

  public entity: WarehouseScannedItemEntity = null;
  public extras: Array<string> = [];

  public listeners: Array<string> = [];

  constructor() {
    this.entity = new WarehouseScannedItemEntity();
  }

  /** ------------------------------------------------------------------------------
   *
   *  ------------------------------------------------------------------------------ */
  public onLoad() {

    this.listeners.push(App.inputScannerObserver.addCommandListener("close", ()=>{
      this.closeForm(null);
    }));

    this.entity = this.dataObject;
    this.extras = Object.keys(this.dataObject.extras);

  }

	/**
   * 
   */
  public onClose() {
    App.inputScannerObserver.removeListenerWithGUID(this.listeners);
  }
  
  /** ------------------------------------------------------------------------------
   *
   *  ------------------------------------------------------------------------------ */
  public increaseQty() {
    this.entity.quantity++;
  }

  /** ------------------------------------------------------------------------------
   *
   *  ------------------------------------------------------------------------------ */
  public decreaseQty() {
    if(this.entity.quantity > 0) {
      this.entity.quantity--;
    }
  }

  /** ------------------------------------------------------------------------------
   *
   *  ------------------------------------------------------------------------------ */
  public saveForm($e) {

    $e.preventDefault();

    if(this.entity.warehouseId == null) {

      this.closingMethod();

    } else {

      var wconn = ApiConnectorComponent.getWarehouseApiConnector();
      wconn.updateItem(this.entity.toWarehouseItemEntity()).subscribe((ref) => {
        this.closingMethod();
      }, (err) => {
        alert("Didn't saved because of "+err.status);
      });

    }

  }

  /** ------------------------------------------------------------------------------
   *
   *  ------------------------------------------------------------------------------ */
  public closeForm($e:any=null) {

    if($e !== null)
      $e.preventDefault();

    this.closingMethod(/*{reset:{listeners:this.listeners}}*/);

  }

}
