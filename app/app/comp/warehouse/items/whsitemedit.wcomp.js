"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var window_component_1 = require("../../../system/window/window.component");
var core_1 = require("@angular/core");
var whsent_comp_1 = require("../entities/whsent.comp");
var apiconnector_component_1 = require("../../../system/connection/apiconnector.component");
var app_1 = require("../../../system/app");
var WarehouseEditItemComponent = (function (_super) {
    __extends(WarehouseEditItemComponent, _super);
    function WarehouseEditItemComponent() {
        this.entity = null;
        this.extras = [];
        this.listeners = [];
        this.entity = new whsent_comp_1.WarehouseScannedItemEntity();
    }
    /** ------------------------------------------------------------------------------
     *
     *  ------------------------------------------------------------------------------ */
    WarehouseEditItemComponent.prototype.onLoad = function () {
        var _this = this;
        this.listeners.push(app_1.App.inputScannerObserver.addCommandListener("close", function () {
            _this.closeForm(null);
        }));
        this.entity = this.dataObject;
        this.extras = Object.keys(this.dataObject.extras);
    };
    /**
   *
   */
    WarehouseEditItemComponent.prototype.onClose = function () {
        app_1.App.inputScannerObserver.removeListenerWithGUID(this.listeners);
    };
    /** ------------------------------------------------------------------------------
     *
     *  ------------------------------------------------------------------------------ */
    WarehouseEditItemComponent.prototype.increaseQty = function () {
        this.entity.quantity++;
    };
    /** ------------------------------------------------------------------------------
     *
     *  ------------------------------------------------------------------------------ */
    WarehouseEditItemComponent.prototype.decreaseQty = function () {
        if (this.entity.quantity > 0) {
            this.entity.quantity--;
        }
    };
    /** ------------------------------------------------------------------------------
     *
     *  ------------------------------------------------------------------------------ */
    WarehouseEditItemComponent.prototype.saveForm = function ($e) {
        var _this = this;
        $e.preventDefault();
        if (this.entity.warehouseId == null) {
            this.closingMethod();
        }
        else {
            var wconn = apiconnector_component_1.ApiConnectorComponent.getWarehouseApiConnector();
            wconn.updateItem(this.entity.toWarehouseItemEntity()).subscribe(function (ref) {
                _this.closingMethod();
            }, function (err) {
                alert("Didn't saved because of " + err.status);
            });
        }
    };
    /** ------------------------------------------------------------------------------
     *
     *  ------------------------------------------------------------------------------ */
    WarehouseEditItemComponent.prototype.closeForm = function ($e) {
        if ($e === void 0) { $e = null; }
        if ($e !== null)
            $e.preventDefault();
        this.closingMethod();
    };
    WarehouseEditItemComponent = __decorate([
        core_1.Component({
            selector: 'whsaddscan',
            template: "\n     <div>\n        \n        <form>\n          \n          <div class=\"left-side\">\n            \n            <div>\n            <i class=\"fa fa-pencil form-icons\" aria-hidden=\"true\"></i> <input [(ngModel)]=\"entity.title\">\n            </div>\n            <div>\n            <i class=\"fa fa-tag form-icons\" aria-hidden=\"true\"></i> <input [(ngModel)]=\"entity.variationId\">\n            </div>\n            <div>\n            <i class=\"fa fa-picture-o form-icons\" aria-hidden=\"true\"></i> <input [(ngModel)]=\"entity.image\">\n            </div>\n            <h4>Extra properties</h4>\n            <ul style=\"list-style-type: none;\">\n              <li\n                *ngFor=\"let item of extras\">\n                <label class=\"form-label\">{{item}}</label> &nbsp;<input [(ngModel)]=\"entity.extras[item]\"><br>\n              </li>\n            </ul>\n            \n          </div>\n          \n          <div class=\"right-side\">\n            <h1> Quantity </h1>\n            <button class=\"btn btn-primary btn-wide\" (click)=\"increaseQty()\"><i class=\"fa fa-chevron-up\" aria-hidden=\"true\"></i></button>\n            <div class=\"header-block\">{{entity.quantity}}</div>\n            <button class=\"btn btn-primary btn-wide\" (click)=\"decreaseQty()\"><i class=\"fa fa-chevron-down\" aria-hidden=\"true\"></i></button>\n          </div>\n          \n          <div class=\"center-text offset-top-m\">\n            <button class=\"btn btn-primary btn-wide\" (click)=\"saveForm($event);\"> Save </button>\n            <button class=\"btn btn-primary btn-wide\" (click)=\"closeForm($event);\"> Cancel </button>\n          </div>\n          \n        </form>\n        \n     </div>\n  ",
            host: {
                'class': 'addscanner',
            },
            styles: [
                "\n      .left-side {\n        color: #333333;\n        display: inline-block;\n        width: 49%;\n        vertical-align: top;\n      }\n      \n      .form-icons {\n        min-width:18px;\n      }\n      \n      .form-label {\n        min-width: 20%;\n      }\n      \n      .left-side input {\n        min-width: 192px;\n        width: 70%;\n        display: inline-block;\n        padding: 4px;\n        border-radius: 2px;\n        border:1px solid #CCCCCC;\n        margin: 2px;\n      }\n      \n      .right-side {\n        color: #333333;\n        text-align: center;\n        display: inline-block;\n        width: 49%;\n        vertical-align: top;\n      }\n      \n    "
            ],
            directives: []
        }), 
        __metadata('design:paramtypes', [])
    ], WarehouseEditItemComponent);
    return WarehouseEditItemComponent;
}(window_component_1.WindowComponent));
exports.WarehouseEditItemComponent = WarehouseEditItemComponent;
//# sourceMappingURL=whsitemedit.wcomp.js.map