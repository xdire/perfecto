import {Component, Input} from "@angular/core";
import {SwitchableComponent} from "../../system/entities/system.entities";
import {App} from "../../system/app";
import {WarehouseItemResolveComponent, WarehouseItemResolveBehavior} from "./additems/whsaddreslv.wcomp";
import {WarehouseLocationListComponent} from "./location/whs.loc.list.wcomp";
@Component({
  selector: "whslistitem",
  host: {
    '[class.hiddenFrame]':'!_isVisible'
  },
  template: `

    <div> <h1> List of items </h1> </div>
       
    <!--Scan panel for see current scanning issues-->
    <div class="panel panel-default">
    
      <div class="panel-heading"> Select action  </div>
      
      <div class="panel-body">
      
        <table class="table">

          <tr>
            <td colspan="2"> Select item by the style </td>
            <td style="padding-bottom: 20px;"> <button class="btn btn-primary" (click)="resolveManual()"> Search by style </button></td>
          </tr>
          
        </table>
        
      </div>
      
    </div>
  `,
  styles:[`
  
  `]
})
export class WarehouseListItemsComponent implements SwitchableComponent {

  @Input() public _isVisible: boolean = false;

	onFocus() {

  }

  onLostFocus() {

  }

  onParentUnload() {

  }

  /** -----------------------------------------------------------------------------------------
   *
   *                          Raise a Manual Item Resolve Form
   *
   *  -----------------------------------------------------------------------------------------
   *
   *
   */
  public resolveManual() {

    App.window.loadComponentIntoModalFrame(WarehouseItemResolveComponent, {

      title: "Item selection by parameters",
      data: {},
      onSuccess: (data) => {

        App.window.loadComponentIntoModalFrame(WarehouseLocationListComponent, {

          title: "Locations list for specific variation id",
          data: {variationId:data.variation.id},
          onSuccess: (list) => {

          },
          onFailure: () => {},
          onClose: () => {},
          getReference: (ref) => {}

        });

      },
      onFailure: () => {

      },
      onClose: () => {

      },
      getReference: (ref) => {
        ref.behaviorOnProceed = WarehouseItemResolveBehavior.PASS_TO_NEXT_WINDOW;
      }

    });

  }


}
