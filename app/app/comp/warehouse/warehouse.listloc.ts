import {Component, Input} from "@angular/core";
import {SwitchableComponent} from "../../system/entities/system.entities";
import {WarehouseApiConnector} from "../../system/connection/whsapiconnector";
import {ApiConnectorComponent} from "../../system/connection/apiconnector.component";
import {WarehouseLocationEntity} from "../../system/connection/connection-entities/whsapi-entities";
import {WarehouseLocationComponent} from "./location/whslocation.wcomp";
import {WarehouseLocationsListSystemTable} from "./location/whsloc.sys";
import {App} from "../../system/app";
import {WarehouseAddLocationComponent} from "./location/whsaddloc.wcomp";

@Component({
  selector:"whslistloc",
  host: {
    '[class.hiddenFrame]':'!_isVisible'
  },
  template: `
    <div> <h1> List of locations </h1> </div>
    <div>
      <div>
        <div class="location-root container-fluid scrollable-visible">
          
         <div style="display: inline-block;">
           <button class="btn btn-primary" (click)="addNewLocation(currentWarehouse)"><i class="fa fa-plus"></i></button>
         </div>
         
         <div class="list-item" style="display: inline-block; padding-top: 4px; width: auto;"> Warehouse root </div>
         
        </div>
      </div>
      
      <ul class="wtable">
      
        <li *ngFor="let loc of allLocationList.list; let i = index;">
          
          <whs-loc [location]="loc" [name]="loc.name" [id]="loc.id" [warehouseId]="loc.warehouseId" [parentId]="loc.parentId" [childLocations]="loc.childs" [_isVisible]="!loc.isDeleted" [isExpanded]="depth!=1" [listObject]="allLocationList"> </whs-loc>
          
        </li>
        
      </ul>
      
    </div>
  `,
  styles:[`
    .location-root {
        background-color: #dedede;
        border-radius: 8px;
        display: inline-block;
        padding: 4px;
        padding-left: 16px;
        padding-right: 16px;
        min-width: 192px;
      }
      
  `],
  directives:[
    WarehouseLocationComponent
  ]
})

export class WarehouseListLocationsComponent implements SwitchableComponent {

  @Input() public _isVisible: boolean = false;

  @Input() public currentWarehouse: number = 1;

  public allLocationList: WarehouseLocationsListSystemTable;

	/** -------------------------------------------------------------------------------------------------
   *
   *
   *
   *  -------------------------------------------------------------------------------------------------
   *
   * @param warehouseId
   */
  public addNewLocation(warehouseId: number) {

    App.window.loadComponentIntoModalFrame(WarehouseAddLocationComponent, {

      title:"Some sex with locations?",
      data: {id: null, parentId: null, warehouseId: warehouseId},
      onLoad: () => {

      },
      onSuccess: (data) => {
        console.log("It's success fucking fuck");
        console.log(data);
        this.allLocationList.appendToElementWithId(null,data);
      },
      onClose: () => {

      },
      onFailure: (error) => {
        console.log("It's bad sucking shit fucking fuck");
        alert("Some error happened, operation can't be done");
      },
      getReference: (ref) => {

      }

    });

  }

	/** -------------------------------------------------------------------------------------------------
   *
   *              CREATE LIST OF WAREHOUSE LOCATIONS FROM RAW API DATA
   *
   *  -------------------------------------------------------------------------------------------------
   *
   * @param entity
   */
  private createWarehouseLocationList(entity: WarehouseLocationEntity[]) {

    this.allLocationList.createFromArray(entity);

  }

	/** -------------------------------------------------------------------------------------------------
   *
   *
   *
   * -------------------------------------------------------------------------------------------------
   *
   *
   */
  onFocus() {

    this.allLocationList = new WarehouseLocationsListSystemTable();

    var aapiconn: WarehouseApiConnector = ApiConnectorComponent.getWarehouseApiConnector();

    aapiconn.getAllLocationsForWarehouseId(1).subscribe((entity) => {
      this.createWarehouseLocationList(entity);
    });

  }

  onLostFocus() {

  }

  onParentUnload() {

  }

}
