"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var app_1 = require("../../system/app");
var whsaddreslv_wcomp_1 = require("./additems/whsaddreslv.wcomp");
var whs_loc_list_wcomp_1 = require("./location/whs.loc.list.wcomp");
var WarehouseListItemsComponent = (function () {
    function WarehouseListItemsComponent() {
        this._isVisible = false;
    }
    WarehouseListItemsComponent.prototype.onFocus = function () {
    };
    WarehouseListItemsComponent.prototype.onLostFocus = function () {
    };
    WarehouseListItemsComponent.prototype.onParentUnload = function () {
    };
    /** -----------------------------------------------------------------------------------------
     *
     *                          Raise a Manual Item Resolve Form
     *
     *  -----------------------------------------------------------------------------------------
     *
     *
     */
    WarehouseListItemsComponent.prototype.resolveManual = function () {
        app_1.App.window.loadComponentIntoModalFrame(whsaddreslv_wcomp_1.WarehouseItemResolveComponent, {
            title: "Item selection by parameters",
            data: {},
            onSuccess: function (data) {
                app_1.App.window.loadComponentIntoModalFrame(whs_loc_list_wcomp_1.WarehouseLocationListComponent, {
                    title: "Locations list for specific variation id",
                    data: { variationId: data.variation.id },
                    onSuccess: function (list) {
                    },
                    onFailure: function () { },
                    onClose: function () { },
                    getReference: function (ref) { }
                });
            },
            onFailure: function () {
            },
            onClose: function () {
            },
            getReference: function (ref) {
                ref.behaviorOnProceed = whsaddreslv_wcomp_1.WarehouseItemResolveBehavior.PASS_TO_NEXT_WINDOW;
            }
        });
    };
    __decorate([
        core_1.Input(), 
        __metadata('design:type', Boolean)
    ], WarehouseListItemsComponent.prototype, "_isVisible", void 0);
    WarehouseListItemsComponent = __decorate([
        core_1.Component({
            selector: "whslistitem",
            host: {
                '[class.hiddenFrame]': '!_isVisible'
            },
            template: "\n\n    <div> <h1> List of items </h1> </div>\n       \n    <!--Scan panel for see current scanning issues-->\n    <div class=\"panel panel-default\">\n    \n      <div class=\"panel-heading\"> Select action  </div>\n      \n      <div class=\"panel-body\">\n      \n        <table class=\"table\">\n\n          <tr>\n            <td colspan=\"2\"> Select item by the style </td>\n            <td style=\"padding-bottom: 20px;\"> <button class=\"btn btn-primary\" (click)=\"resolveManual()\"> Search by style </button></td>\n          </tr>\n          \n        </table>\n        \n      </div>\n      \n    </div>\n  ",
            styles: ["\n  \n  "]
        }), 
        __metadata('design:paramtypes', [])
    ], WarehouseListItemsComponent);
    return WarehouseListItemsComponent;
}());
exports.WarehouseListItemsComponent = WarehouseListItemsComponent;
//# sourceMappingURL=warehouse.listitems.js.map