"use strict";
var app_1 = require("../../../system/app");
/** -----------------------------------------------------------------------------------------
 *
 *  -----------------------------------------------------------------------------------------
 */
var WarehouseScannedItemEntity = (function () {
    /** -----------------------------------------------------------------------------------------
   *
   *  -----------------------------------------------------------------------------------------
   */
    function WarehouseScannedItemEntity() {
        this.uid = null;
        this.scannedId = null;
        this.variationId = null;
        this.title = "";
        this.selectedQuantity = 1;
        this.quantity = 1;
        this.type = null;
        this.warehouseId = null;
        this.locationId = null;
        this.variationObject = null;
        this.itemObject = null;
        this.extras = {};
        this.resolved = false;
        this.labeled = false;
        this.updated = false;
        this.uid = app_1.App.newGUID();
        this.itemObject = null;
        this.variationObject = null;
    }
    /** -----------------------------------------------------------------------------------------
   *
   *  -----------------------------------------------------------------------------------------
   *  @param name
   *  @param value
   */
    WarehouseScannedItemEntity.prototype.setExtraOption = function (name, value) {
        this.extras[name] = value;
    };
    /** -----------------------------------------------------------------------------------------
   *  Get Defined style from object, if any defined
   *  -----------------------------------------------------------------------------------------
   *  @returns {string | null}
   */
    WarehouseScannedItemEntity.prototype.getDefinedStyle = function () {
        if (typeof this.extras["style"] !== "undefined") {
            return this.extras["style"];
        }
        if (this.itemObject !== null && typeof this.itemObject !== "undefined") {
            if (this.itemObject.definedStyle !== "undefined" && this.itemObject.definedStyle !== null)
                return this.itemObject.definedStyle;
        }
        return null;
    };
    /** -----------------------------------------------------------------------------------------
   *
   *
   *  -----------------------------------------------------------------------------------------
   *  @returns {WarehouseItemEntity}
   */
    WarehouseScannedItemEntity.prototype.toWarehouseItemEntity = function () {
        var keys = Object.keys(this.extras);
        // Set extras if not setted
        if (keys.length == 0 && typeof this.itemObject !== "undefined") {
            this.extras = {
                style: this.itemObject.definedStyle,
                size: this.variationObject["size"],
                color: this.variationObject["color"],
                brand: this.itemObject["brand"]
            };
        }
        // Set image to extras
        if (typeof this.image !== "undefined") {
            this.extras["image"] = this.image;
        }
        // Set condition
        var condition = "";
        switch (this.type) {
            case WarehouseScannedItemType.New:
                condition = "new";
                break;
            case WarehouseScannedItemType.Existing:
                condition = "exs";
                break;
            default: break;
        }
        // Create object of type
        var obj = {
            locationId: this.locationId,
            productVariationId: this.variationId,
            condition: condition,
            quantity: this.quantity,
            name: this.title,
            extra: this.extras
        };
        if (this.warehouseId !== null) {
            obj["id"] = this.warehouseId;
        }
        return obj;
    };
    /** -----------------------------------------------------------------------------------------
   *
   *
   *  -----------------------------------------------------------------------------------------
   *  @param obj
   */
    WarehouseScannedItemEntity.prototype.fromObject = function (obj) {
        this.scannedId = obj.scannedId;
        this.variationId = obj.variationId;
        this.title = obj.title;
        this.resolved = obj.resolved;
        this.type = obj.type;
        this.warehouseId = obj.warehouseId;
        this.locationId = obj.locationId;
        this.labeled = obj.labeled;
        this.variationObject = obj.variationObject;
        this.itemObject = obj.itemObject;
        if (obj.hasOwnProperty("extras")) {
            this.extras = obj.extras;
            if (this.extras.hasOwnProperty("image")) {
                this.image = this.extras["image"];
            }
        }
        if (obj.hasOwnProperty("selectedQuantity")) {
            this.selectedQuantity = obj.selectedQuantity;
        }
    };
    /** -----------------------------------------------------------------------------------------
   *
   *
   *  -----------------------------------------------------------------------------------------
   *  @param entity
   */
    WarehouseScannedItemEntity.prototype.fromWarehouseItemEntity = function (entity) {
        this.variationId = entity.productVariationId;
        this.title = entity.name;
        this.quantity = entity.quantity;
        this.locationId = entity.locationId;
        this.warehouseId = entity.id;
        this.labeled = true;
        this.resolved = true;
        this.extras = entity.extra;
        this.itemObject = null;
        this.variationObject = null;
        if (entity.extra.hasOwnProperty("image")) {
            this.image = entity.extra["image"];
        }
        if (this.scannedId == null) {
            this.scannedId = app_1.App.codes.convertStringToTypedString(app_1.App.codes.convertIntToItemId(this.warehouseId)).id;
        }
    };
    /** -----------------------------------------------------------------------------------------
     *
     *                  Print Item Label with attached printer (if any attached)
     *
     *  -----------------------------------------------------------------------------------------
     */
    WarehouseScannedItemEntity.prototype.printItemLabel = function (callback) {
        var _this = this;
        if (callback === void 0) { callback = null; }
        var id = this.warehouseId;
        var label = app_1.App.codes.convertIntToItemId(id);
        if (this.itemObject !== null) {
            var style = this.getDefinedStyle();
            var size = this.variationObject["size"];
            var color = this.variationObject["color"];
            var brand = this.itemObject["brand"];
        }
        else {
            var extras = this.extras;
            if (extras !== null) {
                if (extras.hasOwnProperty("style")) {
                    var style = extras["style"];
                }
                if (extras.hasOwnProperty("size")) {
                    var size = extras["size"];
                }
                if (extras.hasOwnProperty("color")) {
                    var color = extras["color"];
                }
                if (extras.hasOwnProperty("brand")) {
                    var brand = extras["brand"];
                }
            }
        }
        app_1.App.print.printBarCode128ZAddon(label, { A: style, B: size, C: color, D: brand }, id, function () {
            _this.labeled = true;
            if (typeof callback === "function") {
                callback();
            }
        });
    };
    return WarehouseScannedItemEntity;
}());
exports.WarehouseScannedItemEntity = WarehouseScannedItemEntity;
/** -----------------------------------------------------------------------------------------
 *
 *  -----------------------------------------------------------------------------------------
 */
(function (WarehouseScannedItemType) {
    WarehouseScannedItemType[WarehouseScannedItemType["New"] = 0] = "New";
    WarehouseScannedItemType[WarehouseScannedItemType["Existing"] = 1] = "Existing";
})(exports.WarehouseScannedItemType || (exports.WarehouseScannedItemType = {}));
var WarehouseScannedItemType = exports.WarehouseScannedItemType;
//# sourceMappingURL=whsent.comp.js.map