//noinspection TypeScriptCheckImport
import {ItemVariationEntity, ItemEntity} from "../../../system/connection/itemapiconnector";
import {WarehouseItemEntity} from "../../../system/connection/connection-entities/whsapi-entities";
import {App} from "../../../system/app";

/** -----------------------------------------------------------------------------------------
 *
 *  -----------------------------------------------------------------------------------------
 */
export interface WarehouseScannedItem {
  scannedId: number | string;
  variationId: number | string;
  title: string;
  resolved: boolean;
  type: WarehouseScannedItemType;
  warehouseId: number;
  locationId: number;
  labeled?: boolean;
  variationObject?: ItemVariationEntity;
  itemObject?: ItemEntity;
  extras?:Object;
  selectedQuantity?:number;
}

/** -----------------------------------------------------------------------------------------
 *
 *  -----------------------------------------------------------------------------------------
 */
export class WarehouseScannedItemEntity implements WarehouseScannedItem {

  public uid = null;

  public scannedId:number|string = null;
  public variationId:number = null;
  public title:string = "";

  public selectedQuantity: number = 1;
  public quantity: number = 1;
  public type:WarehouseScannedItemType = null;
  public warehouseId:number = null;
  public locationId:number = null;

  public variationObject: ItemVariationEntity = null;
  public itemObject: ItemEntity = null;

  public image: string;
  public extras: Object = {};

  public resolved:boolean = false;
  public labeled:boolean = false;
  public updated:boolean = false;

	/** -----------------------------------------------------------------------------------------
   *
   *  -----------------------------------------------------------------------------------------
   */
  constructor() {
    this.uid = App.newGUID();
    this.itemObject = null;
    this.variationObject = null;
  }

	/** -----------------------------------------------------------------------------------------
   *
   *  -----------------------------------------------------------------------------------------
   *  @param name
   *  @param value
   */
  public setExtraOption(name: string, value: string) {
    this.extras[name] = value;
  }

	/** -----------------------------------------------------------------------------------------
   *  Get Defined style from object, if any defined
   *  -----------------------------------------------------------------------------------------
   *  @returns {string | null}
   */
  public getDefinedStyle() {

    if(typeof this.extras["style"] !== "undefined") {
      return this.extras["style"];
    }

    if(this.itemObject !== null && typeof this.itemObject !== "undefined") {
      if(this.itemObject.definedStyle !== "undefined" && this.itemObject.definedStyle !== null)
        return this.itemObject.definedStyle;
    }
    return null;

  }

	/** -----------------------------------------------------------------------------------------
   *
   *
   *  -----------------------------------------------------------------------------------------
   *  @returns {WarehouseItemEntity}
   */
  public toWarehouseItemEntity() : WarehouseItemEntity {

    var keys = Object.keys(this.extras);

    // Set extras if not setted
    if(keys.length == 0 && typeof this.itemObject !== "undefined") {

      this.extras = {
        style: this.itemObject.definedStyle,
        size: this.variationObject["size"],
        color: this.variationObject["color"],
        brand: this.itemObject["brand"]
      };

    }

    // Set image to extras
    if(typeof this.image !== "undefined"){
      this.extras["image"] = this.image;
    }

    // Set condition
    var condition = "";
    switch (this.type) {
      case WarehouseScannedItemType.New:
        condition = "new";
        break;
      case WarehouseScannedItemType.Existing:
        condition = "exs";
        break;
      default:break;
    }

    // Create object of type
    var obj: WarehouseItemEntity  = {
      locationId: this.locationId,
      productVariationId: this.variationId,
      condition: condition,
      quantity: this.quantity,
      name: this.title,
      extra: this.extras
    };

    if(this.warehouseId !== null){
      obj["id"] = this.warehouseId;
    }

    return obj;

  }

	/** -----------------------------------------------------------------------------------------
   *
   *
   *  -----------------------------------------------------------------------------------------
   *  @param obj
   */
  public fromObject(obj: WarehouseScannedItem) : void {

    this.scannedId = obj.scannedId;
    this.variationId = <number>obj.variationId;
    this.title = obj.title;
    this.resolved = obj.resolved;
    this.type = obj.type;
    this.warehouseId = obj.warehouseId;
    this.locationId = obj.locationId;
    this.labeled = obj.labeled;
    this.variationObject = obj.variationObject;
    this.itemObject = obj.itemObject;

    if(obj.hasOwnProperty("extras")) {
      this.extras = obj.extras;
      if(this.extras.hasOwnProperty("image")) {
        this.image = this.extras["image"];
      }
    }
    if(obj.hasOwnProperty("selectedQuantity")){
      this.selectedQuantity = obj.selectedQuantity;
    }

  }

	/** -----------------------------------------------------------------------------------------
   *
   *
   *  -----------------------------------------------------------------------------------------
   *  @param entity
   */
  public fromWarehouseItemEntity(entity: WarehouseItemEntity) {

    this.variationId = entity.productVariationId;
    this.title = entity.name;
    this.quantity = entity.quantity;
    this.locationId = entity.locationId;
    this.warehouseId = entity.id;
    this.labeled = true;
    this.resolved = true;
    this.extras = entity.extra;

    this.itemObject = null;
    this.variationObject = null;

    if(entity.extra.hasOwnProperty("image")) {
      this.image = entity.extra["image"];
    }

    if(this.scannedId == null){
      this.scannedId = App.codes.convertStringToTypedString(App.codes.convertIntToItemId(this.warehouseId)).id;
    }

  }

  /** -----------------------------------------------------------------------------------------
   *
   *                  Print Item Label with attached printer (if any attached)
   *
   *  -----------------------------------------------------------------------------------------
   */
  public printItemLabel(callback: any = null) {

    var id = this.warehouseId;
    var label = App.codes.convertIntToItemId(id);

    if(this.itemObject !== null) {

      var style = this.getDefinedStyle();
      var size = this.variationObject["size"];
      var color = this.variationObject["color"];
      var brand = this.itemObject["brand"];

    } else {

      var extras: Object = this.extras;

      if(extras !== null){

        if(extras.hasOwnProperty("style")) {
          var style = extras["style"];
        }
        if(extras.hasOwnProperty("size")) {
          var size = extras["size"];
        }
        if(extras.hasOwnProperty("color")) {
          var color = extras["color"];
        }
        if(extras.hasOwnProperty("brand")) {
          var brand = extras["brand"];
        }

      }

    }

    App.print.printBarCode128ZAddon(label, {A:style, B:size, C:color, D:brand}, id, () => {

      this.labeled = true;
      if(typeof callback === "function"){
        callback();
      }

    });

  }

}

/** -----------------------------------------------------------------------------------------
 *
 *  -----------------------------------------------------------------------------------------
 */
export enum WarehouseScannedItemType {
  New,
  Existing
}
