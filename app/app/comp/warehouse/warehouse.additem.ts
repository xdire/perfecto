import {Component, Input} from "@angular/core";
import {SwitchableComponent} from "../../system/entities/system.entities";
import {App} from "../../system/app";
import {WindowComponent} from "../../system/window/window.component";
import {InputAccumulator} from "../../system/input/inpaccu.sys";
import {CodesTypes} from "../../system/codes/codes.sys";
import {
  WarehouseAddScannerItemsFormComponent,
} from "./additems/whsaddscan.wcomp";
import {WarehouseLocationScannedComponent} from "./locitems/whslocscan.wcomp";
import {WarehouseScannedItemType, WarehouseScannedItem} from "./entities/whsent.comp";
import {WarehouseLocationListComponent} from "./location/whs.loc.list.wcomp";
/** --------------------------------------------------------------------
 *
 *
 *                    Add Items tab for warehousing component
 *
 *
 *  -------------------------------------------------------------------- */
@Component({
  selector: "whsadditem",
  host: {
    '[class.hiddenFrame]':'!_isVisible'
  },
  template: `
    <div>
    
      <h1> Scanning and item addition </h1>

      <!--Scan panel for see current scanning issues-->
      <div class="panel panel-default">
      
        <div class="panel-heading"> Select action or scan barcode </div>
        
        <div class="panel-body">
        
          <table class="table">

            <tr>
              <td colspan="2"> Add item manually </td>
              <td style="padding-bottom: 20px;"> <button class="btn btn-primary btn-wide" (click)="openScanningModal(null)">Add</button></td>
            </tr>
            
            <tr>
              <td> Type-in UPC code for lookup </td>
              <td> <input class="form-control" [(ngModel)]="upcLookupValue" type="text"> </td>
              <td style="padding-bottom: 20px;">
                <button class="btn btn-primary btn-wide" (click)="onUPCInput()">Search </button>
              </td>
            </tr>
            
            <tr>
              <td> Type-in location id </td>
              <td> <input class="form-control" [(ngModel)]="boxLookupValue" type="text"> </td>
              <td style="padding-bottom: 20px;">
                <button class="btn btn-primary btn-wide"
                (click)="onLocationInput()">Search </button>
              </td>
            </tr>
            
            <tr>
              <td> Type-in variation id </td>
              <td> <input class="form-control" [(ngModel)]="varLookupValue" type="text"> </td>
              <td style="padding-bottom: 20px;">
                <button class="btn btn-primary btn-wide"
                (click)="onVariationInput()">Search </button>
              </td>
            </tr>
            
          </table>
          
        </div>
        
      </div>
      
    </div>
  `,
  styles:[`
  
  `]
})

export class WarehouseAddItemComponent extends WindowComponent implements SwitchableComponent {

  private scannedInput: String = "";

  private scannedCommand: String = "";

  private upcLookupValue: String = "";

  private boxLookupValue: String = "";

  private varLookupValue: String = "";

  private listeners: String[] = [];

  private accumulator: InputAccumulator = new InputAccumulator();

  private currentModalComponent: any = null;
  private currentModalComponentType: any = null;

  @Input() public _isVisible: boolean = false;

	onFocus() {
    App.inputScannerObserver.reset();
    App.inputScannerObserver.resetListeners();
    this.listeners.push(App.inputScannerObserver.addInputListener("*any",this.onScannedInput.bind(this)));
    this.listeners.push(App.inputScannerObserver.addCommandListener("add",this.onScannedCommand.bind(this)));
  }

  onLostFocus() {
    App.inputScannerObserver.removeListenerWithGUID(this.listeners);
    App.inputScannerObserver.reset();
    App.inputScannerObserver.resetListeners();
  }

  onParentUnload() {

  }

  ngOnDestroy() {
    App.inputScannerObserver.removeListenerWithGUID(this.listeners);
  }

	/** --------------------------------------------------------------------
   *
   *                    Actions on some input scan
   *
   *  --------------------------------------------------------------------
   *
   * @param inputValue
   */
  public onScannedInput(inputValue: String) {

    var nInput = App.codes.convertStringToTypedString(inputValue);

    if(nInput.type == CodesTypes.ItemType || nInput.type == CodesTypes.NoType) {

      this.accumulator.push(nInput.id);

      var type = (nInput.type == CodesTypes.NoType) ? WarehouseScannedItemType.New : WarehouseScannedItemType.Existing;

      this.openScanningModal(
        {
          scannedId:nInput.id,
          variationId:null,
          title:"Some title",
          resolved:false,
          type: type,
          warehouseId: null,
          locationId: null
        });

    }

    if(nInput.type == CodesTypes.LocationType) {

      if(this.currentModalComponent !== null) {

        if(this.currentModalComponent.checkItemsConsistencyForMovement()) {
          this.currentModalComponent.sendItemsToWarehouse(nInput.id);
        }

      } else {

        this.openLocationModal(<number>App.codes.convertStringToTypedInt(<string>nInput.id).id);

      }

    }

    this.scannedInput = inputValue;

  }

	/** --------------------------------------------------------------------
   *
   *                      Action on type-in UPC
   *
   *  --------------------------------------------------------------------
   *
   */
  public onUPCInput() {

    this.openScanningModal({
        scannedId:this.upcLookupValue,
        variationId:null,
        title:"Some UPC Input",
        resolved:false,
        type: WarehouseScannedItemType.New,
        warehouseId: null,
        locationId: null
    });

  }

  /** --------------------------------------------------------------------
   *
   *                      Action on type-in Location
   *
   *  --------------------------------------------------------------------
   *
   */
  public onLocationInput() {

    var value: number = 0;
    if((value = parseInt(this.boxLookupValue)) > 0) {

      this.openLocationModal(value);

    } else {

      alert("Not well defined Id of location. Operation postponed.");

    }

  }

	/** --------------------------------------------------------------------
   *
   *                    Action on input of Variation Id
   *
   *  --------------------------------------------------------------------
   */
  public onVariationInput() {

    var varValue = parseInt(this.varLookupValue);

      if(varValue < 1) return;

      App.window.loadComponentIntoModalFrame(WarehouseLocationListComponent, {
        title: "Boxes which contains items similar to selected",
        data: {
          variationId: this.varLookupValue
        },
        onSuccess: () => {},
        onLoad: () => {},
        onClose: () => {App.inputScannerObserver.resumeListenerWithGIUD(this.listeners);},
        onFailure: () => {},
        getReference: () => {App.inputScannerObserver.pauseListenerWithGIUD(this.listeners);}
      });

  }

	/** --------------------------------------------------------------------
   *
   *                      Action on command scan
   *
   *  --------------------------------------------------------------------
   *  @param inputCommand
   */
  public onScannedCommand(inputCommand: String) {

    console.log("CE EXEC "+inputCommand);

    if(this.currentModalComponent !== null) {
      this.currentModalComponent.waitForLocationScan(inputCommand);
    }

    this.scannedCommand = inputCommand;

  }

	/** --------------------------------------------------------------------
   *
   *                        Open new scanning modal
   *
   *  --------------------------------------------------------------------
   */
  private openScanningModal(withScannedData?: WarehouseScannedItem) {

    if(this.currentModalComponent === null) {

      App.window.loadComponentIntoModalFrame(WarehouseAddScannerItemsFormComponent, {
        title: "List of scanned items",
        data: withScannedData,
        onSuccess: (data) => {

        },
        onFailure: (err) => {

        },
        onClose: (data) => {
          this.currentModalComponent = null;
          this.currentModalComponentType = null;
        },
        getReference: (ref) => {
	        /** @type {WarehouseAddScannerItemsFormComponent} */
          this.currentModalComponent = ref;
          this.currentModalComponentType = 1;
          this.currentModalComponent.appendInputItem(withScannedData);
        }

      });

    } else {

      this.currentModalComponent.appendInputItem(withScannedData);

    }

  }

	/** --------------------------------------------------------------------
   *
   *              Open New Modal With Location Showing Items
   *
   *  --------------------------------------------------------------------
   *
   * @param locationId
   */
  private openLocationModal(locationId: number) {

    if(this.currentModalComponent === null) {

      App.window.loadComponentIntoModalFrame(WarehouseLocationScannedComponent, {

        title: "List of items in the scanned location id: "+locationId,
        data: {id:locationId},
        onSuccess: (data) => {

        },
        onFailure: (err) => {

        },
        onClose: (data) => {
          this.currentModalComponent = null;
          this.currentModalComponentType = null;
          App.inputScannerObserver.resumeListenerWithGIUD(this.listeners);
        },
        getReference: (ref) => {
          /** @type {WarehouseAddScannerItemsFormComponent} */
          this.currentModalComponent = ref;
          this.currentModalComponentType = 1;
          App.inputScannerObserver.pauseListenerWithGIUD(this.listeners);
        }

      });

    }

  }

}
