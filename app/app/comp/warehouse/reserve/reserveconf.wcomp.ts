import {Component} from "@angular/core";
import {WindowComponent} from "../../../system/window/window.component";
import {ComponentImplementingModalFramePass} from "../../../system/entities/system.entities";
import {App} from "../../../system/app";

@Component({
  selector: "whsreservitem",
  host: {

  },
  template: `
    
    <form>
          
          <h2> Fill properties attached to your reservation</h2>
          <div class="left-side">
            
            <div>
              <span style="width: 96px; display: inline-block;">Order Id </span> <input [(ngModel)]="resOrderId">
            </div>
            <div>
              <span style="width: 96px; display: inline-block;">Order MpId </span> <input [(ngModel)]="resOrderSpecId">
            </div>
            <div>
              <span style="width: 96px; display: inline-block;">Reason </span> <input [(ngModel)]="resReason">
            </div>
                 
          </div>
          
          <div class="right-side">
            <button class="btn btn-primary btn-wide" (click)="increaseQty()"><i class="fa fa-chevron-up" aria-hidden="true"></i></button>
            <div class="header-block">{{quantity}}</div>
            <button class="btn btn-primary btn-wide" (click)="decreaseQty()"><i class="fa fa-chevron-down" aria-hidden="true"></i></button>
          </div>
          
          <div class="center-text offset-top-m">
            <button class="btn btn-primary btn-wide" (click)="saveForm($event);"> Save </button>
            <button class="btn btn-primary btn-wide" (click)="closeForm($event);"> Cancel </button>
          </div>
          
    </form>
    
  `,
  styles:[`
    .left-side {
        color: #333333;
        display: inline-block;
        width: 49%;
        vertical-align: top;
      }
      
      .form-icons {
        min-width:18px;
      }
      
      .form-label {
        min-width: 20%;
      }
      
      .left-side input {
        min-width: 192px;
        width: 70%;
        display: inline-block;
        padding: 4px;
        border-radius: 2px;
        border:1px solid #CCCCCC;
        margin: 2px;
      }
      
      .right-side {
        color: #333333;
        text-align: center;
        display: inline-block;
        width: 49%;
        vertical-align: top;
      }
  `],
  directives:[

  ],
  pipes:[

  ]
})

export class WarehouseItemReservationConfirmComponent extends WindowComponent implements ComponentImplementingModalFramePass {

  dataObject:any;
  closingMethod:any;
  successCallback:any;
  failureCallback:any;

  public quantity: number = 1;

  public resOrderId: number = 0;

  public resOrderSpecId: String = "";

  public resReason: number = 1;

  public resUserId: number = null;

  constructor() {
    super.constructor();
    this.resUserId = App.state.user.id;
  }

  public increaseQty() : void {
    this.quantity++;
  }

  public decreaseQty() : void {
    if(this.quantity > 1){
      this.quantity--;
    }
  }

  public saveForm($ev) {
    $ev.preventDefault();
  }

  public closeForm($ev) {
    $ev.preventDefault();
  }

}
