"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var window_component_1 = require("../../../system/window/window.component");
var app_1 = require("../../../system/app");
var WarehouseItemReservationConfirmComponent = (function (_super) {
    __extends(WarehouseItemReservationConfirmComponent, _super);
    function WarehouseItemReservationConfirmComponent() {
        this.quantity = 1;
        this.resOrderId = 0;
        this.resOrderSpecId = "";
        this.resReason = 1;
        this.resUserId = null;
        _super.prototype.constructor.call(this);
        this.resUserId = app_1.App.state.user.id;
    }
    WarehouseItemReservationConfirmComponent.prototype.increaseQty = function () {
        this.quantity++;
    };
    WarehouseItemReservationConfirmComponent.prototype.decreaseQty = function () {
        if (this.quantity > 1) {
            this.quantity--;
        }
    };
    WarehouseItemReservationConfirmComponent.prototype.saveForm = function ($ev) {
        $ev.preventDefault();
    };
    WarehouseItemReservationConfirmComponent.prototype.closeForm = function ($ev) {
        $ev.preventDefault();
    };
    WarehouseItemReservationConfirmComponent = __decorate([
        core_1.Component({
            selector: "whsreservitem",
            host: {},
            template: "\n    \n    <form>\n          \n          <h2> Fill properties attached to your reservation</h2>\n          <div class=\"left-side\">\n            \n            <div>\n              <span style=\"width: 96px; display: inline-block;\">Order Id </span> <input [(ngModel)]=\"resOrderId\">\n            </div>\n            <div>\n              <span style=\"width: 96px; display: inline-block;\">Order MpId </span> <input [(ngModel)]=\"resOrderSpecId\">\n            </div>\n            <div>\n              <span style=\"width: 96px; display: inline-block;\">Reason </span> <input [(ngModel)]=\"resReason\">\n            </div>\n                 \n          </div>\n          \n          <div class=\"right-side\">\n            <button class=\"btn btn-primary btn-wide\" (click)=\"increaseQty()\"><i class=\"fa fa-chevron-up\" aria-hidden=\"true\"></i></button>\n            <div class=\"header-block\">{{quantity}}</div>\n            <button class=\"btn btn-primary btn-wide\" (click)=\"decreaseQty()\"><i class=\"fa fa-chevron-down\" aria-hidden=\"true\"></i></button>\n          </div>\n          \n          <div class=\"center-text offset-top-m\">\n            <button class=\"btn btn-primary btn-wide\" (click)=\"saveForm($event);\"> Save </button>\n            <button class=\"btn btn-primary btn-wide\" (click)=\"closeForm($event);\"> Cancel </button>\n          </div>\n          \n    </form>\n    \n  ",
            styles: ["\n    .left-side {\n        color: #333333;\n        display: inline-block;\n        width: 49%;\n        vertical-align: top;\n      }\n      \n      .form-icons {\n        min-width:18px;\n      }\n      \n      .form-label {\n        min-width: 20%;\n      }\n      \n      .left-side input {\n        min-width: 192px;\n        width: 70%;\n        display: inline-block;\n        padding: 4px;\n        border-radius: 2px;\n        border:1px solid #CCCCCC;\n        margin: 2px;\n      }\n      \n      .right-side {\n        color: #333333;\n        text-align: center;\n        display: inline-block;\n        width: 49%;\n        vertical-align: top;\n      }\n  "],
            directives: [],
            pipes: []
        }), 
        __metadata('design:paramtypes', [])
    ], WarehouseItemReservationConfirmComponent);
    return WarehouseItemReservationConfirmComponent;
}(window_component_1.WindowComponent));
exports.WarehouseItemReservationConfirmComponent = WarehouseItemReservationConfirmComponent;
//# sourceMappingURL=reserveconf.wcomp.js.map