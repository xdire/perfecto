import {WindowComponent} from "../../../system/window/window.component";
import {ApiConnectorComponent} from "../../../system/connection/apiconnector.component";
import {WarehouseApiConnector} from "../../../system/connection/whsapiconnector";
import {ItemApiConnector} from "../../../system/connection/itemapiconnector";
import {WarehouseScannedItemEntity, WarehouseScannedItem, WarehouseScannedItemType} from "../entities/whsent.comp";
import {App} from "../../../system/app";
import {ItemResolveFormComponent} from "../../items/forms/itemresolv.wcomp";
import {CodesTypes} from "../../../system/codes/codes.sys";
import {WarehouseItemEntity} from "../../../system/connection/connection-entities/whsapi-entities";
import {ComponentImplementingModalFramePass} from "../../../system/entities/system.entities";
import {WarehouseItemDeleteComponent} from "../items/whsitemdelete.wcomp";

export abstract class WarehouseLocationModel extends WindowComponent implements ComponentImplementingModalFramePass {

  dataObject:any;
  closingMethod:any;
  successCallback:any;
  failureCallback:any;

  protected message: string = "";

  protected whsAPIConn: WarehouseApiConnector = null;

  protected itemApiConn: ItemApiConnector = null;

  protected highlightedVariationId: number = null;

  protected locationId: number = null;

  protected _items: WarehouseScannedItemEntity[] = [];

  protected listeners: String[] = [];

  constructor() {
    super.constructor();
    this.isRoot = false;
    this.whsAPIConn = ApiConnectorComponent.getWarehouseApiConnector();
    this.itemApiConn = ApiConnectorComponent.getItemApiConnector();
  }


  public highlightVariationId(varationId: number) {
    this.highlightedVariationId = varationId;
  }

  /** -----------------------------------------------------------------------------------------
   *
   *  -----------------------------------------------------------------------------------------
   * @param value
   */
  set items(value: WarehouseItemEntity[]) {
    this._items = this.upscaleWarehouseEntities(value);
  }

  /** -----------------------------------------------------------------------------------------
   *
   *  -----------------------------------------------------------------------------------------
   *  @param w
   *  @returns {WarehouseScannedItemEntity[]}
   */
  private upscaleWarehouseEntities(w: WarehouseItemEntity[]) : WarehouseScannedItemEntity[] {

    var a: WarehouseScannedItemEntity[] = new Array(w.length);
    var i = 0;

    w.forEach((e) => {

      var se = new WarehouseScannedItemEntity();
      se.fromWarehouseItemEntity(e);
      a[i++] = se;

    });

    return a;

  }

  /** -----------------------------------------------------------------------------------------
   *
   *                              Actions on some input scan
   *
   *  -----------------------------------------------------------------------------------------
   *
   * @param inputValue
   */
  public onScannedInput(inputValue: String) {

    var nInput = App.codes.convertStringToTypedString(inputValue);

    if(nInput.type == CodesTypes.NoType) {

      var type = WarehouseScannedItemType.New;
      var ent = new WarehouseScannedItemEntity();

      ent.scannedId = nInput.id;
      ent.type = type;
      ent.locationId = this.locationId;

      this.resolveInputItem(ent, (item) => {

        this.whsAPIConn.createNewItem(item.toWarehouseItemEntity()).subscribe((data) => {

          item.warehouseId = data.id;
          this._items.push(item);
          this.refreshView();
          item.printItemLabel(()=>{
            this.refreshView();
          });

        });

      });

    }

    if(nInput.type == CodesTypes.ItemType) {

      var type = WarehouseScannedItemType.Existing;

      /*this.openScanningModal(
       {
       scannedId:nInput.id,
       variationId:null,
       title:"Some title",
       resolved:false,
       type: type,
       warehouseId: null,
       locationId: null
       });*/

    }

    //this.scannedInput = inputValue;
    //this.refreshView();

  }

  /** -----------------------------------------------------------------------------------------
   *
   *                        Append Empty Item Object (for manual resolve)
   *
   *  -----------------------------------------------------------------------------------------
   *
   *  @param item
   *  @param isDefault
   */
  public appendInputItem(item: WarehouseScannedItem, isDefault: boolean = false) {

    if(isDefault) {

      item = {
        scannedId:null,
        variationId:null,
        title:"Manual Added Element",
        resolved:false,
        type: WarehouseScannedItemType.New,
        warehouseId: null,
        locationId: null
      }

    }

    if(item !== null) {

      var obj = new WarehouseScannedItemEntity();
      obj.fromObject(item);
      this._items.push(obj);

    }

    this.refreshView();

  }

  /** -----------------------------------------------------------------------------------------
   *
   *
   *
   *  -----------------------------------------------------------------------------------------
   *  @param item
   */
  public itemRefreshed(item) {

    if(typeof item.variationId !== "undefined" && item.variationId != null) {

      if(item.locationId == null) {

        item.locationId = this.locationId;

        this.whsAPIConn.createNewItem(item.toWarehouseItemEntity()).subscribe((data) => {

          item.scannedId = App.codes.convertStringToTypedString(App.codes.convertIntToItemId(data.id)).id;
          item.warehouseId = data.id;

          this.refreshView();

          item.printItemLabel(()=>{
            this.refreshView();
          });

        });

      }

    }

  }

  /** -----------------------------------------------------------------------------------------
   *
   *                          Make an automatic scanned item resolve
   *
   *  -----------------------------------------------------------------------------------------
   *
   *  2 step function:
   *
   *    1 - query for EAN-12
   *
   *    2 - query for EAN-14 if first failed
   *
   *
   * @param item WarehouseScannedItemEntity : reference to original scanned item
   */
  public resolveInputItem(item: WarehouseScannedItemEntity, successResolve) {

    if(item.scannedId === null) return;

    var upc = <string>item.scannedId;
    var upc14:string = "00"+upc;

    this.itemApiConn.getItemsByVariationCode(upc).subscribe((items) => {

      // If multiple variations returned
      if(items.length > 1) {



      }
      // If one variation returned
      else {

        // Return new form for select of confirm parameters
        App.window.loadComponentIntoModalFrame(ItemResolveFormComponent, {

          title: "Results of item resolving",
          data: items[0],
          onSuccess: (data) => {

            item.variationId = data.variationId;
            item.title = data.title;
            item.quantity = data.quantity;
            item.extras = data.extras;
            item.image = data.image;
            item.resolved = true;

            item.itemObject = data.itemObject;
            item.variationObject = data.variationObject;

            successResolve(item);

          },
          onFailure: () => {},
          onClose: () => {App.inputScannerObserver.resumeListenerWithGIUD(this.listeners);},
          getReference: (ref) => {App.inputScannerObserver.pauseListenerWithGIUD(this.listeners);}

        });

      }

    }, (err) => {

      // Get variation by UPC14 if simple UPC code is failed
      this.itemApiConn.getItemsByVariationCode(upc14).subscribe((items) => {

        // If multiple variations returned
        if(items.length > 1) {

        }
        // If one variation returned
        else {

          // Return new form for select of confirm parameters
          App.window.loadComponentIntoModalFrame(ItemResolveFormComponent, {

            title: "Results of item resolving",
            data: items[0],
            onSuccess: (data) => {

              item.variationId = data.variationId;
              item.title = data.title;
              item.quantity = data.quantity;
              item.extras = data.extras;
              item.image = data.image;
              item.resolved = true;

              item.itemObject = data.itemObject;
              item.variationObject = data.variationObject;

              successResolve(item);

            },
            onFailure: () => {},
            onClose: () => {App.inputScannerObserver.resumeListenerWithGIUD(this.listeners);},
            getReference: (ref) => {App.inputScannerObserver.pauseListenerWithGIUD(this.listeners);}

          });

        }

      },(err) => {



      });

    });

  }

  /** -----------------------------------------------------------------------------------------
   *
   *  -----------------------------------------------------------------------------------------
   *  @param $item
   */
  public onDeleteItem($item) {

    if($item.warehouseId != null) {

      App.window.loadComponentIntoModalFrame(WarehouseItemDeleteComponent, {

        title: "Item delete confirmation",
        data: $item,
        onSuccess: (data) => {

          this._deleteItemFromItems(data);

        },
        onFailure: () => {},
        onClose: () => {App.inputScannerObserver.resumeListenerWithGIUD(this.listeners);},
        getReference: (ref) => {App.inputScannerObserver.pauseListenerWithGIUD(this.listeners);}

      });

    } else {

      this._deleteItemFromItems($item);

    }

  }

  /** -----------------------------------------------------------------------------------------
   *
   *  -----------------------------------------------------------------------------------------
   *  @param $item
   */
  protected _deleteItemFromItems($item) {
    this._items = this._items.filter((data) => {
      return data.uid != $item.uid;
    });
    this.refreshView();
  }

  /** -----------------------------------------------------------------------------------------
   *
   *  -----------------------------------------------------------------------------------------
   */
  public onCommandClose() {
    this.closingMethod(/*{reset:{listeners:this.listeners}}*/);
  }

	/** -----------------------------------------------------------------------------------------
   *
   *
   *
   *                      MODAL WINDOW COMPONENT LIFECYCLE
   *
   *
   *
   *  ----------------------------------------------------------------------------------------- */

  /** -----------------------------------------------------------------------------------------
   *                    Loading and listeners initialization stage
   *  ----------------------------------------------------------------------------------------- */
  onLoad() {

    this.listeners.push(App.inputScannerObserver.addInputListener("*any",this.onScannedInput.bind(this)));
    this.listeners.push(App.inputScannerObserver.addCommandListener("close",this.onCommandClose.bind(this)));

    this.message = "Loading information";
    this.refreshView();

    if(this.dataObject.hasOwnProperty("id")) {

      this.locationId = this.dataObject.id;
      this.whsAPIConn.getItemsFromLocation(this.dataObject.id).subscribe((entities) => {

        this.items = entities;
        this.message = "";
        this.refreshView();

      },(err) => {

        this.message = "Failed to load information";
        console.log(err);

      });

    }

  }

  /** -----------------------------------------------------------------------------------------
   *                            Window fires closing procedure
   *  ----------------------------------------------------------------------------------------- */
  onClose() {
    App.inputScannerObserver.removeListenerWithGUID(this.listeners);
  }

  /** -----------------------------------------------------------------------------------------
   *                            Angular fires destroy procedure
   *  ----------------------------------------------------------------------------------------- */
  ngOnDestroy() {
    App.inputScannerObserver.removeListenerWithGUID(this.listeners);
  }

}
