"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var window_component_1 = require("../../../system/window/window.component");
var apiconnector_component_1 = require("../../../system/connection/apiconnector.component");
var whsent_comp_1 = require("../entities/whsent.comp");
var app_1 = require("../../../system/app");
var itemresolv_wcomp_1 = require("../../items/forms/itemresolv.wcomp");
var codes_sys_1 = require("../../../system/codes/codes.sys");
var whsitemdelete_wcomp_1 = require("../items/whsitemdelete.wcomp");
var WarehouseLocationModel = (function (_super) {
    __extends(WarehouseLocationModel, _super);
    function WarehouseLocationModel() {
        this.message = "";
        this.whsAPIConn = null;
        this.itemApiConn = null;
        this.highlightedVariationId = null;
        this.locationId = null;
        this._items = [];
        this.listeners = [];
        _super.prototype.constructor.call(this);
        this.isRoot = false;
        this.whsAPIConn = apiconnector_component_1.ApiConnectorComponent.getWarehouseApiConnector();
        this.itemApiConn = apiconnector_component_1.ApiConnectorComponent.getItemApiConnector();
    }
    WarehouseLocationModel.prototype.highlightVariationId = function (varationId) {
        this.highlightedVariationId = varationId;
    };
    Object.defineProperty(WarehouseLocationModel.prototype, "items", {
        /** -----------------------------------------------------------------------------------------
         *
         *  -----------------------------------------------------------------------------------------
         * @param value
         */
        set: function (value) {
            this._items = this.upscaleWarehouseEntities(value);
        },
        enumerable: true,
        configurable: true
    });
    /** -----------------------------------------------------------------------------------------
     *
     *  -----------------------------------------------------------------------------------------
     *  @param w
     *  @returns {WarehouseScannedItemEntity[]}
     */
    WarehouseLocationModel.prototype.upscaleWarehouseEntities = function (w) {
        var a = new Array(w.length);
        var i = 0;
        w.forEach(function (e) {
            var se = new whsent_comp_1.WarehouseScannedItemEntity();
            se.fromWarehouseItemEntity(e);
            a[i++] = se;
        });
        return a;
    };
    /** -----------------------------------------------------------------------------------------
     *
     *                              Actions on some input scan
     *
     *  -----------------------------------------------------------------------------------------
     *
     * @param inputValue
     */
    WarehouseLocationModel.prototype.onScannedInput = function (inputValue) {
        var _this = this;
        var nInput = app_1.App.codes.convertStringToTypedString(inputValue);
        if (nInput.type == codes_sys_1.CodesTypes.NoType) {
            var type = whsent_comp_1.WarehouseScannedItemType.New;
            var ent = new whsent_comp_1.WarehouseScannedItemEntity();
            ent.scannedId = nInput.id;
            ent.type = type;
            ent.locationId = this.locationId;
            this.resolveInputItem(ent, function (item) {
                _this.whsAPIConn.createNewItem(item.toWarehouseItemEntity()).subscribe(function (data) {
                    item.warehouseId = data.id;
                    _this._items.push(item);
                    _this.refreshView();
                    item.printItemLabel(function () {
                        _this.refreshView();
                    });
                });
            });
        }
        if (nInput.type == codes_sys_1.CodesTypes.ItemType) {
            var type = whsent_comp_1.WarehouseScannedItemType.Existing;
        }
        //this.scannedInput = inputValue;
        //this.refreshView();
    };
    /** -----------------------------------------------------------------------------------------
     *
     *                        Append Empty Item Object (for manual resolve)
     *
     *  -----------------------------------------------------------------------------------------
     *
     *  @param item
     *  @param isDefault
     */
    WarehouseLocationModel.prototype.appendInputItem = function (item, isDefault) {
        if (isDefault === void 0) { isDefault = false; }
        if (isDefault) {
            item = {
                scannedId: null,
                variationId: null,
                title: "Manual Added Element",
                resolved: false,
                type: whsent_comp_1.WarehouseScannedItemType.New,
                warehouseId: null,
                locationId: null
            };
        }
        if (item !== null) {
            var obj = new whsent_comp_1.WarehouseScannedItemEntity();
            obj.fromObject(item);
            this._items.push(obj);
        }
        this.refreshView();
    };
    /** -----------------------------------------------------------------------------------------
     *
     *
     *
     *  -----------------------------------------------------------------------------------------
     *  @param item
     */
    WarehouseLocationModel.prototype.itemRefreshed = function (item) {
        var _this = this;
        if (typeof item.variationId !== "undefined" && item.variationId != null) {
            if (item.locationId == null) {
                item.locationId = this.locationId;
                this.whsAPIConn.createNewItem(item.toWarehouseItemEntity()).subscribe(function (data) {
                    item.scannedId = app_1.App.codes.convertStringToTypedString(app_1.App.codes.convertIntToItemId(data.id)).id;
                    item.warehouseId = data.id;
                    _this.refreshView();
                    item.printItemLabel(function () {
                        _this.refreshView();
                    });
                });
            }
        }
    };
    /** -----------------------------------------------------------------------------------------
     *
     *                          Make an automatic scanned item resolve
     *
     *  -----------------------------------------------------------------------------------------
     *
     *  2 step function:
     *
     *    1 - query for EAN-12
     *
     *    2 - query for EAN-14 if first failed
     *
     *
     * @param item WarehouseScannedItemEntity : reference to original scanned item
     */
    WarehouseLocationModel.prototype.resolveInputItem = function (item, successResolve) {
        var _this = this;
        if (item.scannedId === null)
            return;
        var upc = item.scannedId;
        var upc14 = "00" + upc;
        this.itemApiConn.getItemsByVariationCode(upc).subscribe(function (items) {
            // If multiple variations returned
            if (items.length > 1) {
            }
            else {
                // Return new form for select of confirm parameters
                app_1.App.window.loadComponentIntoModalFrame(itemresolv_wcomp_1.ItemResolveFormComponent, {
                    title: "Results of item resolving",
                    data: items[0],
                    onSuccess: function (data) {
                        item.variationId = data.variationId;
                        item.title = data.title;
                        item.quantity = data.quantity;
                        item.extras = data.extras;
                        item.image = data.image;
                        item.resolved = true;
                        item.itemObject = data.itemObject;
                        item.variationObject = data.variationObject;
                        successResolve(item);
                    },
                    onFailure: function () { },
                    onClose: function () { app_1.App.inputScannerObserver.resumeListenerWithGIUD(_this.listeners); },
                    getReference: function (ref) { app_1.App.inputScannerObserver.pauseListenerWithGIUD(_this.listeners); }
                });
            }
        }, function (err) {
            // Get variation by UPC14 if simple UPC code is failed
            _this.itemApiConn.getItemsByVariationCode(upc14).subscribe(function (items) {
                // If multiple variations returned
                if (items.length > 1) {
                }
                else {
                    // Return new form for select of confirm parameters
                    app_1.App.window.loadComponentIntoModalFrame(itemresolv_wcomp_1.ItemResolveFormComponent, {
                        title: "Results of item resolving",
                        data: items[0],
                        onSuccess: function (data) {
                            item.variationId = data.variationId;
                            item.title = data.title;
                            item.quantity = data.quantity;
                            item.extras = data.extras;
                            item.image = data.image;
                            item.resolved = true;
                            item.itemObject = data.itemObject;
                            item.variationObject = data.variationObject;
                            successResolve(item);
                        },
                        onFailure: function () { },
                        onClose: function () { app_1.App.inputScannerObserver.resumeListenerWithGIUD(_this.listeners); },
                        getReference: function (ref) { app_1.App.inputScannerObserver.pauseListenerWithGIUD(_this.listeners); }
                    });
                }
            }, function (err) {
            });
        });
    };
    /** -----------------------------------------------------------------------------------------
     *
     *  -----------------------------------------------------------------------------------------
     *  @param $item
     */
    WarehouseLocationModel.prototype.onDeleteItem = function ($item) {
        var _this = this;
        if ($item.warehouseId != null) {
            app_1.App.window.loadComponentIntoModalFrame(whsitemdelete_wcomp_1.WarehouseItemDeleteComponent, {
                title: "Item delete confirmation",
                data: $item,
                onSuccess: function (data) {
                    _this._deleteItemFromItems(data);
                },
                onFailure: function () { },
                onClose: function () { app_1.App.inputScannerObserver.resumeListenerWithGIUD(_this.listeners); },
                getReference: function (ref) { app_1.App.inputScannerObserver.pauseListenerWithGIUD(_this.listeners); }
            });
        }
        else {
            this._deleteItemFromItems($item);
        }
    };
    /** -----------------------------------------------------------------------------------------
     *
     *  -----------------------------------------------------------------------------------------
     *  @param $item
     */
    WarehouseLocationModel.prototype._deleteItemFromItems = function ($item) {
        this._items = this._items.filter(function (data) {
            return data.uid != $item.uid;
        });
        this.refreshView();
    };
    /** -----------------------------------------------------------------------------------------
     *
     *  -----------------------------------------------------------------------------------------
     */
    WarehouseLocationModel.prototype.onCommandClose = function () {
        this.closingMethod();
    };
    /** -----------------------------------------------------------------------------------------
   *
   *
   *
   *                      MODAL WINDOW COMPONENT LIFECYCLE
   *
   *
   *
   *  ----------------------------------------------------------------------------------------- */
    /** -----------------------------------------------------------------------------------------
     *                    Loading and listeners initialization stage
     *  ----------------------------------------------------------------------------------------- */
    WarehouseLocationModel.prototype.onLoad = function () {
        var _this = this;
        this.listeners.push(app_1.App.inputScannerObserver.addInputListener("*any", this.onScannedInput.bind(this)));
        this.listeners.push(app_1.App.inputScannerObserver.addCommandListener("close", this.onCommandClose.bind(this)));
        this.message = "Loading information";
        this.refreshView();
        if (this.dataObject.hasOwnProperty("id")) {
            this.locationId = this.dataObject.id;
            this.whsAPIConn.getItemsFromLocation(this.dataObject.id).subscribe(function (entities) {
                _this.items = entities;
                _this.message = "";
                _this.refreshView();
            }, function (err) {
                _this.message = "Failed to load information";
                console.log(err);
            });
        }
    };
    /** -----------------------------------------------------------------------------------------
     *                            Window fires closing procedure
     *  ----------------------------------------------------------------------------------------- */
    WarehouseLocationModel.prototype.onClose = function () {
        app_1.App.inputScannerObserver.removeListenerWithGUID(this.listeners);
    };
    /** -----------------------------------------------------------------------------------------
     *                            Angular fires destroy procedure
     *  ----------------------------------------------------------------------------------------- */
    WarehouseLocationModel.prototype.ngOnDestroy = function () {
        app_1.App.inputScannerObserver.removeListenerWithGUID(this.listeners);
    };
    return WarehouseLocationModel;
}(window_component_1.WindowComponent));
exports.WarehouseLocationModel = WarehouseLocationModel;
//# sourceMappingURL=whsloc.abstract.js.map