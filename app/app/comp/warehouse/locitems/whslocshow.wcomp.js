"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var whsitem_show_wcomp_1 = require("../items/whsitem.show.wcomp");
var system_pipe_1 = require("../../../system/pipes/system.pipe");
var whsloc_abstract_1 = require("./whsloc.abstract");
/** --------------------------------------------------------------------
 *
 *                      Secondary View Of Box Contents
 *
 *                      view lacking of buttons which can
 *                      circullary open additional windows
 *
 *  -------------------------------------------------------------------- */
var WarehouseLocationShowItemsComponent = (function (_super) {
    __extends(WarehouseLocationShowItemsComponent, _super);
    function WarehouseLocationShowItemsComponent() {
        _super.prototype.constructor.call(this);
    }
    /** --------------------------------------------------------------------
     *  Tracker DOM Support
     *  -------------------------------------------------------------------- */
    WarehouseLocationShowItemsComponent.prototype.ngTrackByItemSupport = function (index, item) {
        return item.uid;
    };
    WarehouseLocationShowItemsComponent = __decorate([
        core_1.Component({
            selector: "whslocitems",
            host: {},
            template: "\n    <div>\n      <div>{{message}}</div>\n      <whslocshowitem \n        *ngFor=\"let item of (_items | arrayReverse); let index of index\"\n        *ngForTrackBy=\"ngTrackByItemSupport\"\n        [class.highlighted]=\"item.variationId == highlightedVariationId\"\n        [listeners]=\"listeners\"\n        [warehouseEntity]=\"item\"\n        [expanded]=\"false\"\n        (onDeleteCommand)=\"onDeleteItem($event)\"\n        (onEntityObjectRefresh)=\"itemRefreshed($event)\">\n      </whslocshowitem>\n    </div>\n  ",
            styles: ["\n\n  "],
            directives: [whsitem_show_wcomp_1.WarehouseItemShowComponent],
            pipes: [system_pipe_1.ArrayReversePipe]
        }), 
        __metadata('design:paramtypes', [])
    ], WarehouseLocationShowItemsComponent);
    return WarehouseLocationShowItemsComponent;
}(whsloc_abstract_1.WarehouseLocationModel));
exports.WarehouseLocationShowItemsComponent = WarehouseLocationShowItemsComponent;
//# sourceMappingURL=whslocshow.wcomp.js.map