"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var whsitem_wcomp_1 = require("../items/whsitem.wcomp");
var system_pipe_1 = require("../../../system/pipes/system.pipe");
var whsloc_abstract_1 = require("./whsloc.abstract");
/** --------------------------------------------------------------------
 *
 *          Primary View Of Box Contents With Possibility to ADD
 *          items into a box
 *
 *  -------------------------------------------------------------------- */
var WarehouseLocationScannedComponent = (function (_super) {
    __extends(WarehouseLocationScannedComponent, _super);
    function WarehouseLocationScannedComponent() {
        _super.prototype.constructor.call(this);
    }
    /** --------------------------------------------------------------------
     *  Tracker DOM Support
     *  -------------------------------------------------------------------- */
    WarehouseLocationScannedComponent.prototype.ngTrackByItemSupport = function (index, item) {
        return item.uid;
    };
    WarehouseLocationScannedComponent = __decorate([
        core_1.Component({
            selector: "whslocitems",
            host: {},
            template: "\n    <div>\n      \n      <!--Buttons for manual form opening-->\n      <div class=\"row\" style=\"border-bottom: 1px solid #CCCCCC; padding: 4px; margin-bottom: 4px;\">\n        \n        <div class=\"col-md-8\" style=\"text-align: left;\">\n          <button class=\"btn btn-primary btn-normal\" (click)=\"appendInputItem(null,true)\"> Add Items </button>\n        </div>\n        \n      </div>\n          \n      <div>{{message}}</div>\n      \n      <whslocitem *ngFor=\"let item of (_items | arrayReverse); let i=index\"\n        *ngForTrackBy=\"ngTrackByItemSupport\"\n        [warehouseEntity]=\"item\"\n        [listeners]=\"listeners\"\n        [expanded]=\"false\"\n        (onEntityObjectRefresh)=\"itemRefreshed($event)\"\n        (onDeleteCommand)=\"onDeleteItem($event)\">\n      </whslocitem>\n      \n    </div>\n  ",
            styles: ["\n\n  "],
            directives: [whsitem_wcomp_1.WarehouseItemComponent],
            pipes: [system_pipe_1.ArrayReversePipe]
        }), 
        __metadata('design:paramtypes', [])
    ], WarehouseLocationScannedComponent);
    return WarehouseLocationScannedComponent;
}(whsloc_abstract_1.WarehouseLocationModel));
exports.WarehouseLocationScannedComponent = WarehouseLocationScannedComponent;
//# sourceMappingURL=whslocscan.wcomp.js.map