import {Component} from "@angular/core";
import {WarehouseScannedItemEntity} from "../entities/whsent.comp";
import {WarehouseItemShowComponent} from "../items/whsitem.show.wcomp";
import {ArrayReversePipe} from "../../../system/pipes/system.pipe";
import {WarehouseLocationModel} from "./whsloc.abstract";

/** --------------------------------------------------------------------
 *
 *                      Secondary View Of Box Contents
 *
 *                      view lacking of buttons which can
 *                      circullary open additional windows
 *
 *  -------------------------------------------------------------------- */
@Component({
  selector: "whslocitems",
  host: {

  },
  template: `
    <div>
      <div>{{message}}</div>
      <whslocshowitem 
        *ngFor="let item of (_items | arrayReverse); let index of index"
        *ngForTrackBy="ngTrackByItemSupport"
        [class.highlighted]="item.variationId == highlightedVariationId"
        [listeners]="listeners"
        [warehouseEntity]="item"
        [expanded]="false"
        (onDeleteCommand)="onDeleteItem($event)"
        (onEntityObjectRefresh)="itemRefreshed($event)">
      </whslocshowitem>
    </div>
  `,
  styles:[`

  `],
  directives:[WarehouseItemShowComponent],
  pipes:[ArrayReversePipe]
})

export class WarehouseLocationShowItemsComponent extends WarehouseLocationModel {

  constructor() {
    super.constructor();
  }

  /** --------------------------------------------------------------------
   *  Tracker DOM Support
   *  -------------------------------------------------------------------- */
  ngTrackByItemSupport(index: number, item: WarehouseScannedItemEntity) {
    return item.uid;
  }

}
