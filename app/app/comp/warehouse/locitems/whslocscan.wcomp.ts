import {Component} from "@angular/core";
import {WarehouseItemComponent} from "../items/whsitem.wcomp";
import {WarehouseScannedItemEntity} from "../entities/whsent.comp";
import {ArrayReversePipe} from "../../../system/pipes/system.pipe";
import {WarehouseLocationModel} from "./whsloc.abstract";

/** --------------------------------------------------------------------
 *
 *          Primary View Of Box Contents With Possibility to ADD
 *          items into a box
 *
 *  -------------------------------------------------------------------- */
@Component({
  selector: "whslocitems",
  host: {

  },
  template: `
    <div>
      
      <!--Buttons for manual form opening-->
      <div class="row" style="border-bottom: 1px solid #CCCCCC; padding: 4px; margin-bottom: 4px;">
        
        <div class="col-md-8" style="text-align: left;">
          <button class="btn btn-primary btn-normal" (click)="appendInputItem(null,true)"> Add Items </button>
        </div>
        
      </div>
          
      <div>{{message}}</div>
      
      <whslocitem *ngFor="let item of (_items | arrayReverse); let i=index"
        *ngForTrackBy="ngTrackByItemSupport"
        [warehouseEntity]="item"
        [listeners]="listeners"
        [expanded]="false"
        (onEntityObjectRefresh)="itemRefreshed($event)"
        (onDeleteCommand)="onDeleteItem($event)">
      </whslocitem>
      
    </div>
  `,
  styles:[`

  `],
  directives:[WarehouseItemComponent],
  pipes:[ArrayReversePipe]
})

export class WarehouseLocationScannedComponent extends WarehouseLocationModel {
  
  constructor() {
    super.constructor();
  }
  /** --------------------------------------------------------------------
   *  Tracker DOM Support
   *  -------------------------------------------------------------------- */
  ngTrackByItemSupport(index: number, item: WarehouseScannedItemEntity) {
    return item.uid;
  }

}
