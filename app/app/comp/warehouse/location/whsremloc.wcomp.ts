import {Component, Input} from "@angular/core";
import {ComponentImplementingModalFramePass, WindowCallbackError} from "../../../system/entities/system.entities";
import {WarehouseLocationEntity} from "../../../system/connection/connection-entities/whsapi-entities";
import {WarehouseApiConnector} from "../../../system/connection/whsapiconnector";
import {ApiConnectorComponent} from "../../../system/connection/apiconnector.component";

@Component({
  selector:"whs-rloc",
  template:`

    <div class="whs-loc-rem">
      <div class="container-fluid">
          
        <form class="center-text">
          <label> You sure to remove this location? </label>
          <button class="btn btn-primary" type="submit" (click)="clickRemove($event)"> Remove </button>
          <button class="btn btn-primary" (click)="clickCancel($event)"> Cancel </button>
        </form>
        
      </div>
    </div>
    
  `,
  styles:[
    `
      .whs-loc-rem {
        color: #333333;
        width: 100%;
        display: inline-block;
        vertical-align: top;
      }
    `
  ]
})

export class WarehouseRemoveLocationComponent implements ComponentImplementingModalFramePass {

  @Input() name: string = "";

	loadCallback:any;

  dataObject:any;

  closingMethod:any;

  successCallback: any;

  failureCallback: any;

  public clickCancel(e) {
    e.preventDefault();
    this.closingMethod();
  }

	/** ------------------------------------------
   *
   *
   *  ------------------------------------------
   *
   * @param e
   */
  public clickRemove(e) {

    e.preventDefault();

    var wconn: WarehouseApiConnector = ApiConnectorComponent.getWarehouseApiConnector();

    if(this.dataObject.hasOwnProperty("warehouseId") && this.dataObject.hasOwnProperty("id")) {

      var data:WarehouseLocationEntity = <WarehouseLocationEntity>this.dataObject;

      console.log(wconn);

      wconn.trashLocation(data.id).subscribe((entity) => {

        console.log(entity);
        this.successCallback(data.id);
        this.closingMethod();

      },(err) => {

        var error: WindowCallbackError = {errorCode: 500, errorMessage: "Something happen", data: err};
        console.log(error);
        this.failureCallback(error);
        this.closingMethod();

      });

    }

  }

}
