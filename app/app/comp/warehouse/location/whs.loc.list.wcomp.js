"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var window_component_1 = require("../../../system/window/window.component");
var apiconnector_component_1 = require("../../../system/connection/apiconnector.component");
var whsloc_entity_wcomp_1 = require("./whsloc.entity.wcomp");
var app_1 = require("../../../system/app");
var WarehouseLocationListComponent = (function (_super) {
    __extends(WarehouseLocationListComponent, _super);
    /** ------------------------------------------------------------------------------
     *
     *  ------------------------------------------------------------------------------ */
    function WarehouseLocationListComponent() {
        this.listForVariationId = null;
        this.locations = [];
        this.listeners = [];
        this.whsConn = apiconnector_component_1.ApiConnectorComponent.getWarehouseApiConnector();
    }
    /** ---------------------------------------------------------------------------------
   *  Loading of form data
   *  --------------------------------------------------------------------------------- */
    WarehouseLocationListComponent.prototype.onLoad = function () {
        this.listeners.push(app_1.App.inputScannerObserver.addCommandListener("close", this.onCommandClose.bind(this)));
        if (this.dataObject.hasOwnProperty("variationId")) {
            this.listForVariationId = this.dataObject.variationId;
            this.selectLocationsBasedOnVariationId();
        }
    };
    WarehouseLocationListComponent.prototype.onClose = function () {
        app_1.App.inputScannerObserver.removeListenerWithGUID(this.listeners);
    };
    /** ---------------------------------------------------------------------------------
   *  Executing App request for all locations which has item of specified variation
   *  --------------------------------------------------------------------------------- */
    WarehouseLocationListComponent.prototype.selectLocationsBasedOnVariationId = function () {
        var _this = this;
        this.whsConn.getLocationsByVariationId(this.listForVariationId).subscribe(function (data) {
            //this.locations = this.reduceVariationArray(data);
            _this.locations = _this.reduceLocationArray(data);
        }, function (err) {
            console.log("Can't select locations: " + err.status);
        });
    };
    /** ---------------------------------------------------------------------------------
   *  Reduce array of item warehouse variation entities
   *  ---------------------------------------------------------------------------------
   * @param d
   * @returns {WarehouseLocationEntity[]}
   */
    WarehouseLocationListComponent.prototype.reduceVariationArray = function (d) {
        var p = new Uint32Array(d.length);
        var l = [];
        var i = 0;
        d.forEach(function (e) {
            if (p.indexOf(e.locationId) == -1) {
                l.push({
                    id: e["locationId"],
                    warehouseId: null,
                    name: ""
                });
                p[i++] = e["locationId"];
            }
        });
        return l;
    };
    WarehouseLocationListComponent.prototype.reduceLocationArray = function (d) {
        var p = new Uint32Array(d.length);
        var l = [];
        var i = 0;
        d.forEach(function (e) {
            // Check if inserted
            var loc = p.indexOf(e.id);
            // If inserted ++ stack amount
            if (loc != -1)
                l[loc].amountOnLocation++;
            else {
                e.amountOnLocation = 1;
                l.push(e);
                p[i++] = e.id;
            }
        });
        return l;
    };
    WarehouseLocationListComponent.prototype.onCommandClose = function () {
        this.closingMethod();
    };
    WarehouseLocationListComponent = __decorate([
        core_1.Component({
            selector: "whsloclist",
            host: {},
            template: "\n    <div>\n      <whslocentity *ngFor=\"let whs of locations\" [entity]=\"whs\" [listeners]=\"listeners\" [variation]=\"listForVariationId\"> </whslocentity>\n    </div>\n  ",
            styles: ["\n  \n  "],
            directives: [whsloc_entity_wcomp_1.WarehouseLocationEntityComponent]
        }), 
        __metadata('design:paramtypes', [])
    ], WarehouseLocationListComponent);
    return WarehouseLocationListComponent;
}(window_component_1.WindowComponent));
exports.WarehouseLocationListComponent = WarehouseLocationListComponent;
//# sourceMappingURL=whs.loc.list.wcomp.js.map