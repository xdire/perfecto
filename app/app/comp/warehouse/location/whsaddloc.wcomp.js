"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var apiconnector_component_1 = require("../../../system/connection/apiconnector.component");
var WarehouseAddLocationComponent = (function () {
    function WarehouseAddLocationComponent() {
        this.name = "";
    }
    /** ------------------------------------------------------------------------------------------------
   *
   *
   *
   *  ------------------------------------------------------------------------------------------------
   *
   * @param e
   */
    WarehouseAddLocationComponent.prototype.clickCancel = function (e) {
        e.preventDefault();
        this.failureCallback();
        this.closingMethod();
    };
    /** ------------------------------------------------------------------------------------------------
   *
   *
   *
   *  ------------------------------------------------------------------------------------------------
   *
   * @param e
   */
    WarehouseAddLocationComponent.prototype.clickSave = function (e) {
        var _this = this;
        e.preventDefault();
        var aapiconn = apiconnector_component_1.ApiConnectorComponent.getWarehouseApiConnector();
        if (this.dataObject.hasOwnProperty("warehouseId") && this.dataObject.hasOwnProperty("parentId")) {
            var data = this.dataObject;
            var loc = {
                parentId: data.id,
                warehouseId: data.warehouseId,
                name: this.name,
                isAnchor: false
            };
            aapiconn.createNewLocation(loc).subscribe(function (entity) {
                console.log(entity);
                loc.id = entity.id;
                _this.successCallback(loc);
                _this.closingMethod();
            }, function (err) {
                var error = { errorCode: 500, errorMessage: "Something happen", data: err };
                _this.failureCallback(error);
                _this.closingMethod();
            });
        }
    };
    __decorate([
        core_1.Input(), 
        __metadata('design:type', String)
    ], WarehouseAddLocationComponent.prototype, "name", void 0);
    WarehouseAddLocationComponent = __decorate([
        core_1.Component({
            selector: "whs-aloc",
            template: "\n\n    <div class=\"whs-loc-add\">\n      <div class=\"container-fluid\">\n          \n        <form class=\"center-text\">\n          <label> Location name </label>\n          <input type=\"text\" [(ngModel)]=\"name\">\n          <button class=\"btn btn-primary\" type=\"submit\" (click)=\"clickSave($event)\"> Save </button>\n          <button class=\"btn btn-primary\" (click)=\"clickCancel($event)\"> Cancel </button>\n        </form>\n        \n      </div>\n    </div>\n    \n  ",
            styles: [
                "\n      .whs-loc-add {\n        width: 100%;\n        display: inline-block;\n        vertical-align: top;\n      }\n    "
            ]
        }), 
        __metadata('design:paramtypes', [])
    ], WarehouseAddLocationComponent);
    return WarehouseAddLocationComponent;
}());
exports.WarehouseAddLocationComponent = WarehouseAddLocationComponent;
//# sourceMappingURL=whsaddloc.wcomp.js.map