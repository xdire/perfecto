"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var apiconnector_component_1 = require("../../../system/connection/apiconnector.component");
var window_component_1 = require("../../../system/window/window.component");
var WarehouseEditLocationComponent = (function (_super) {
    __extends(WarehouseEditLocationComponent, _super);
    function WarehouseEditLocationComponent() {
        _super.apply(this, arguments);
        this.entity = null;
    }
    WarehouseEditLocationComponent.prototype.onLoad = function () {
        this.entity = this.dataObject;
        this.refreshView();
    };
    WarehouseEditLocationComponent.prototype.onClose = function () {
    };
    /** ------------------------------------------------------------------------------------
   *
   *
   *  ------------------------------------------------------------------------------------
   * @param e
   */
    WarehouseEditLocationComponent.prototype.clickSave = function (e) {
        var _this = this;
        e.preventDefault();
        var wconn = apiconnector_component_1.ApiConnectorComponent.getWarehouseApiConnector();
        var ent = {
            id: this.entity.id,
            parentId: this.entity.parentId,
            warehouseId: this.entity.warehouseId,
            name: this.entity.name,
            description: this.entity.description,
            isAnchor: this.entity.isAnchor
        };
        wconn.updateLocation(ent).subscribe(function (data) {
            _this.successCallback(_this.entity);
            _this.closingMethod();
        }, function (err) {
            _this.failureCallback();
            _this.closingMethod();
        });
    };
    /** ------------------------------------------------------------------------------------
     *
     *
     *  ------------------------------------------------------------------------------------
     *
     * @param e
     */
    WarehouseEditLocationComponent.prototype.clickCancel = function (e) {
        e.preventDefault();
        this.closingMethod();
    };
    WarehouseEditLocationComponent = __decorate([
        core_1.Component({
            selector: "whs-rloc",
            template: "\n\n    <div class=\"whs-loc-rem\">\n      <div class=\"container-fluid\">\n          \n        <form class=\"center-text\">\n          \n          <div class=\"row offset-top-xs\">\n            <div class=\"col-lg-6 text-right\">\n              <label> Set location name </label>\n            </div>\n            <div class=\"col-lg-6 text-left\">\n              <input class=\"input-default\" [(ngModel)]=\"entity.name\">\n            </div>\n          </div>\n          \n          <div class=\"row offset-top-xs\">\n            <div class=\"col-lg-6 text-right\">\n              <label> Set location description </label>\n            </div>\n            <div class=\"col-lg-6 text-left\">\n              <input class=\"input-default\" [(ngModel)]=\"entity.description\">\n            </div>\n          </div>\n          \n          <div class=\"row\">\n            <div class=\"col-lg-12\">\n              <div style=\"margin-top: 10px;\">\n                <button class=\"btn btn-primary\" type=\"submit\" (click)=\"clickSave($event)\"> Update </button>\n                <button class=\"btn btn-primary\" (click)=\"clickCancel($event)\"> Cancel </button>\n              </div>\n            </div>\n          </div>\n          \n        </form>\n        \n      </div>\n    </div>\n    \n  ",
            styles: [
                "\n      .whs-loc-rem {\n        color: #333333;\n        width: 100%;\n        display: inline-block;\n        vertical-align: top;\n      }\n    "
            ]
        }), 
        __metadata('design:paramtypes', [])
    ], WarehouseEditLocationComponent);
    return WarehouseEditLocationComponent;
}(window_component_1.WindowComponent));
exports.WarehouseEditLocationComponent = WarehouseEditLocationComponent;
//# sourceMappingURL=whseditloc.wcomp.js.map