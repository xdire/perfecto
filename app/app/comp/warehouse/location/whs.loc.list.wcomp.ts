import {Component} from "@angular/core";
import {WindowComponent} from "../../../system/window/window.component";
import {ComponentImplementingModalFramePass} from "../../../system/entities/system.entities";
import {
  WarehouseLocationEntity,
  WarehouseItemEntity
} from "../../../system/connection/connection-entities/whsapi-entities";
import {WarehouseApiConnector} from "../../../system/connection/whsapiconnector";
import {ApiConnectorComponent} from "../../../system/connection/apiconnector.component";
import {WarehouseLocationEntityComponent} from "./whsloc.entity.wcomp";
import {App} from "../../../system/app";

@Component({
  selector: "whsloclist",
  host: {

  },
  template: `
    <div>
      <whslocentity *ngFor="let whs of locations" [entity]="whs" [listeners]="listeners" [variation]="listForVariationId"> </whslocentity>
    </div>
  `,
  styles:[`
  
  `],
  directives:[WarehouseLocationEntityComponent]
})

export class WarehouseLocationListComponent extends WindowComponent implements ComponentImplementingModalFramePass {

	dataObject:any;
  closingMethod:any;
  successCallback:any;
  failureCallback:any;

  private listForVariationId: number = null;

  private locations: WarehouseLocationEntity[] = [];

  public listeners: Array<string> = [];

  private whsConn: WarehouseApiConnector;
  /** ------------------------------------------------------------------------------
   *
   *  ------------------------------------------------------------------------------ */
  constructor() {
    this.whsConn = ApiConnectorComponent.getWarehouseApiConnector();
  }

	/** ---------------------------------------------------------------------------------
   *  Loading of form data
   *  --------------------------------------------------------------------------------- */
  onLoad() {

    this.listeners.push(App.inputScannerObserver.addCommandListener("close",this.onCommandClose.bind(this)));

    if(this.dataObject.hasOwnProperty("variationId")) {

      this.listForVariationId = this.dataObject.variationId;

      this.selectLocationsBasedOnVariationId();

    }

  }

  onClose() {
    App.inputScannerObserver.removeListenerWithGUID(this.listeners);
  }

	/** ---------------------------------------------------------------------------------
   *  Executing App request for all locations which has item of specified variation
   *  --------------------------------------------------------------------------------- */
  private selectLocationsBasedOnVariationId() {

    this.whsConn.getLocationsByVariationId(this.listForVariationId).subscribe((data) => {

      //this.locations = this.reduceVariationArray(data);

      this.locations = this.reduceLocationArray(data);

    }, (err) => {

      console.log("Can't select locations: "+err.status);

    });

  }

	/** ---------------------------------------------------------------------------------
   *  Reduce array of item warehouse variation entities
   *  ---------------------------------------------------------------------------------
   * @param d
   * @returns {WarehouseLocationEntity[]}
   */
  private reduceVariationArray(d: Array<WarehouseItemEntity>) : WarehouseLocationEntity[] {

    var p: Uint32Array = new Uint32Array(d.length);
    var l: WarehouseLocationEntity[] = [];
    var i = 0;

    d.forEach((e) => {

      if(p.indexOf(e.locationId) == -1) {

        l.push({
          id: e["locationId"],
          warehouseId: null,
          name: ""
        });
        p[i++] = e["locationId"];

      }

    });

    return l;

  }

  private reduceLocationArray(d: Array<WarehouseLocationEntity>) : WarehouseLocationEntity[] {

    var p: Uint32Array = new Uint32Array(d.length);
    var l: WarehouseLocationEntity[] = [];
    var i = 0;

    d.forEach((e) => {

      // Check if inserted
      var loc: number = p.indexOf(e.id);
      // If inserted ++ stack amount
      if(loc != -1)
        l[loc].amountOnLocation++;
      // If not — insert and index
      else {
        e.amountOnLocation = 1;
        l.push(e);
        p[i++] = e.id;
      }

    });

    return l;

  }

  public onCommandClose(){
    this.closingMethod();
  }

}
