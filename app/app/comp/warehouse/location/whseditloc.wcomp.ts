import {Component} from "@angular/core";
import {ComponentImplementingModalFramePass, WindowCallbackError} from "../../../system/entities/system.entities";
import {WarehouseLocationEntity} from "../../../system/connection/connection-entities/whsapi-entities";
import {WarehouseApiConnector} from "../../../system/connection/whsapiconnector";
import {ApiConnectorComponent} from "../../../system/connection/apiconnector.component";
import {WindowComponent} from "../../../system/window/window.component";

@Component({
  selector:"whs-rloc",
  template:`

    <div class="whs-loc-rem">
      <div class="container-fluid">
          
        <form class="center-text">
          
          <div class="row offset-top-xs">
            <div class="col-lg-6 text-right">
              <label> Set location name </label>
            </div>
            <div class="col-lg-6 text-left">
              <input class="input-default" [(ngModel)]="entity.name">
            </div>
          </div>
          
          <div class="row offset-top-xs">
            <div class="col-lg-6 text-right">
              <label> Set location description </label>
            </div>
            <div class="col-lg-6 text-left">
              <input class="input-default" [(ngModel)]="entity.description">
            </div>
          </div>
          
          <div class="row">
            <div class="col-lg-12">
              <div style="margin-top: 10px;">
                <button class="btn btn-primary" type="submit" (click)="clickSave($event)"> Update </button>
                <button class="btn btn-primary" (click)="clickCancel($event)"> Cancel </button>
              </div>
            </div>
          </div>
          
        </form>
        
      </div>
    </div>
    
  `,
  styles:[
    `
      .whs-loc-rem {
        color: #333333;
        width: 100%;
        display: inline-block;
        vertical-align: top;
      }
    `
  ]
})

export class WarehouseEditLocationComponent extends WindowComponent implements ComponentImplementingModalFramePass {

  dataObject:any;
  closingMethod:any;
  successCallback: any;
  failureCallback: any;

  public entity: WarehouseLocationEntity = null;

  onLoad() {
    this.entity = this.dataObject;
    this.refreshView();
  }

  onClose() {

  }

	/** ------------------------------------------------------------------------------------
   *
   *
   *  ------------------------------------------------------------------------------------
   * @param e
   */
  public clickSave(e) {

    e.preventDefault();
    var wconn: WarehouseApiConnector = ApiConnectorComponent.getWarehouseApiConnector();

    var ent = {
      id: this.entity.id,
      parentId: this.entity.parentId,
      warehouseId: this.entity.warehouseId,
      name: this.entity.name,
      description: this.entity.description,
      isAnchor: this.entity.isAnchor
    };

    wconn.updateLocation(ent).subscribe((data) => {

      this.successCallback(this.entity);
      this.closingMethod();

    },(err) => {

      this.failureCallback();
      this.closingMethod();

    });

  }

  /** ------------------------------------------------------------------------------------
   *
   *
   *  ------------------------------------------------------------------------------------
   *
   * @param e
   */
  public clickCancel(e) {

    e.preventDefault();
    this.closingMethod();

  }

}

