"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var app_1 = require("../../../system/app");
var whslocshow_wcomp_1 = require("../locitems/whslocshow.wcomp");
var WarehouseLocationEntityComponent = (function () {
    /** ------------------------------------------------------------------------------
     *
     *  ------------------------------------------------------------------------------ */
    function WarehouseLocationEntityComponent() {
        this.entity = null;
        this.listeners = [];
        this.variation = null;
    }
    /** ------------------------------------------------------------------------------
     *
     *  ------------------------------------------------------------------------------ */
    WarehouseLocationEntityComponent.prototype.openBox = function () {
        var _this = this;
        app_1.App.window.loadComponentIntoModalFrame(whslocshow_wcomp_1.WarehouseLocationShowItemsComponent, {
            title: "List of items in the scanned location id: " + this.entity.id,
            data: { id: this.entity.id },
            onSuccess: function (data) {
            },
            onFailure: function (err) {
            },
            onClose: function (data) {
                app_1.App.inputScannerObserver.resumeListenerWithGIUD(_this.listeners);
            },
            getReference: function (ref) {
                app_1.App.inputScannerObserver.pauseListenerWithGIUD(_this.listeners);
                console.log(ref);
                if (typeof ref.highlightVariationId === 'function') {
                    ref.highlightVariationId(_this.variation);
                }
            }
        });
    };
    __decorate([
        core_1.Input(), 
        __metadata('design:type', Object)
    ], WarehouseLocationEntityComponent.prototype, "entity", void 0);
    __decorate([
        core_1.Input(), 
        __metadata('design:type', Array)
    ], WarehouseLocationEntityComponent.prototype, "listeners", void 0);
    __decorate([
        core_1.Input(), 
        __metadata('design:type', Number)
    ], WarehouseLocationEntityComponent.prototype, "variation", void 0);
    WarehouseLocationEntityComponent = __decorate([
        core_1.Component({
            selector: "whslocentity",
            host: {},
            template: "\n    <div class=\"whs-loc-entity\">\n    \n      <div class=\"whs-loc-entity-l\">\n        \n        <div>\n          Stacks at location {{(entity?.amountOnLocation > 0) ? entity?.amountOnLocation : 0}}\n        </div>\n        \n        <div>\n          <i class=\"fa fa-barcode\"></i> {{entity?.id}}\n        </div>\n        \n        <div>\n          <i class=\"fa fa-bookmark\"></i> \n          <span \n            class=\"whs-iteme-extraspan\" \n            style=\"background-color: rgb(52,159,137); color:#FFFFFF\">\n            {{entity?.name}}\n          </span>\n        </div>\n        \n        <div class=\"offset-top-sm\">\n          {{entity?.description}}    \n        </div>\n        \n      </div>\n      \n      <div class=\"whs-loc-entity-r\">\n        <button class=\"btn btn-primary\" style=\"height:64px;\" (click)=\"openBox()\"> Open Box </button>\n      </div>\n      \n      <div style=\"height: 1px; width: 100%; clear: both\"></div>\n      \n    </div>\n  ",
            styles: ["\n  \n  "]
        }), 
        __metadata('design:paramtypes', [])
    ], WarehouseLocationEntityComponent);
    return WarehouseLocationEntityComponent;
}());
exports.WarehouseLocationEntityComponent = WarehouseLocationEntityComponent;
//# sourceMappingURL=whsloc.entity.wcomp.js.map