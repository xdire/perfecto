"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var app_1 = require("../../../system/app");
var whsaddloc_wcomp_1 = require("./whsaddloc.wcomp");
var whsremloc_wcomp_1 = require("./whsremloc.wcomp");
var whsloc_sys_1 = require("./whsloc.sys");
var whseditloc_wcomp_1 = require("./whseditloc.wcomp");
var whslocshow_wcomp_1 = require("../locitems/whslocshow.wcomp");
var WarehouseLocationComponent = (function () {
    function WarehouseLocationComponent() {
        this.isVisible = true;
        this.isExpanded = false;
        this.isChildsExpanded = false;
        this.name = "";
        this.childLocations = [];
    }
    /** --------------------------------------------------------------------------------------
   *
   *
   *  --------------------------------------------------------------------------------------
   *
   *  @param parentId
   *  @param warehouseId
   */
    WarehouseLocationComponent.prototype.addNewLocation = function (parentId, warehouseId) {
        var _this = this;
        app_1.App.window.loadComponentIntoModalFrame(whsaddloc_wcomp_1.WarehouseAddLocationComponent, {
            title: "Add new location",
            data: { id: this.id, parentId: this.parentId, warehouseId: this.warehouseId },
            onLoad: function () {
            },
            onSuccess: function (data) {
                _this.listObject.appendToElementWithId(_this.id, data);
            },
            onClose: function (data) {
            },
            onFailure: function (error) {
                alert("Some error happened, operation can't be done");
            },
            getReference: function (ref) {
            }
        });
    };
    WarehouseLocationComponent.prototype.expandInnerContents = function () {
        this.isChildsExpanded = !this.isChildsExpanded;
    };
    /** --------------------------------------------------------------------------------------
   *
   *
   *  --------------------------------------------------------------------------------------
   *
   *
   */
    WarehouseLocationComponent.prototype.editLocation = function () {
        var _this = this;
        app_1.App.window.loadComponentIntoModalFrame(whseditloc_wcomp_1.WarehouseEditLocationComponent, {
            title: "Edit location :" + this.location.id,
            data: this.location,
            onLoad: function () {
            },
            onSuccess: function (data) {
                _this.location = data;
            },
            onClose: function (data) {
            },
            onFailure: function (error) {
                alert("Some error happened, operation can't be done");
            },
            getReference: function (ref) {
            }
        });
    };
    /** --------------------------------------------------------------------------------------
   *
   *
   *  --------------------------------------------------------------------------------------
   */
    WarehouseLocationComponent.prototype.printLabelForLocation = function () {
        var locLabel = app_1.App.codes.convertIntToLocationId(this.id);
        app_1.App.print.printBarCode128Z(locLabel);
    };
    /** --------------------------------------------------------------------------------------
   *
   *
   *  --------------------------------------------------------------------------------------
   */
    WarehouseLocationComponent.prototype.editThisLocation = function () {
    };
    /** --------------------------------------------------------------------------------------
   *
   *
   *  --------------------------------------------------------------------------------------
   */
    WarehouseLocationComponent.prototype.removeThisLocation = function () {
        var _this = this;
        app_1.App.window.loadComponentIntoModalFrame(whsremloc_wcomp_1.WarehouseRemoveLocationComponent, {
            title: "Some removing sex with locations?",
            data: { id: this.id, parentId: this.parentId, warehouseId: this.warehouseId },
            onLoad: function () {
            },
            onSuccess: function (data) {
                console.log("It's success fucking fuck");
                console.log(data);
                _this.listObject.hideElementWithId(data.id);
                _this.isVisible = false;
            },
            onClose: function (data) {
            },
            onFailure: function (error) {
                console.log("It's bad sucking shit fucking fuck");
                alert("Some error happened, operation can't be done");
            },
            getReference: function (ref) {
            }
        });
    };
    /** ------------------------------------------------------------------------------
     *
     *  ------------------------------------------------------------------------------ */
    WarehouseLocationComponent.prototype.openBox = function () {
        app_1.App.window.loadComponentIntoModalFrame(whslocshow_wcomp_1.WarehouseLocationShowItemsComponent, {
            title: "List of items in the location id: " + this.id + " " + this.name,
            data: { id: this.id },
            onSuccess: function (data) {
            },
            onFailure: function (err) {
            },
            onClose: function (data) {
                //App.inputScannerObserver.resumeListenerWithGIUD(this.listeners);
            },
            getReference: function (ref) {
                //App.inputScannerObserver.pauseListenerWithGIUD(this.listeners);
                // To make dependant modal window acting as linked window - View need to be refreshed
                // this.refreshView();
            }
        });
    };
    __decorate([
        core_1.Input(), 
        __metadata('design:type', whsloc_sys_1.WarehouseLocationsListSystemTable)
    ], WarehouseLocationComponent.prototype, "listObject", void 0);
    __decorate([
        core_1.Input(), 
        __metadata('design:type', Boolean)
    ], WarehouseLocationComponent.prototype, "isVisible", void 0);
    __decorate([
        core_1.Input(), 
        __metadata('design:type', Boolean)
    ], WarehouseLocationComponent.prototype, "isExpanded", void 0);
    __decorate([
        core_1.Input(), 
        __metadata('design:type', Boolean)
    ], WarehouseLocationComponent.prototype, "isChildsExpanded", void 0);
    __decorate([
        core_1.Input(), 
        __metadata('design:type', String)
    ], WarehouseLocationComponent.prototype, "name", void 0);
    __decorate([
        core_1.Input(), 
        __metadata('design:type', Number)
    ], WarehouseLocationComponent.prototype, "id", void 0);
    __decorate([
        core_1.Input(), 
        __metadata('design:type', Number)
    ], WarehouseLocationComponent.prototype, "parentId", void 0);
    __decorate([
        core_1.Input(), 
        __metadata('design:type', Number)
    ], WarehouseLocationComponent.prototype, "warehouseId", void 0);
    __decorate([
        core_1.Input(), 
        __metadata('design:type', Object)
    ], WarehouseLocationComponent.prototype, "location", void 0);
    __decorate([
        core_1.Input(), 
        __metadata('design:type', Array)
    ], WarehouseLocationComponent.prototype, "childLocations", void 0);
    WarehouseLocationComponent = __decorate([
        core_1.Component({
            selector: "whs-loc",
            host: {
                '[class.hiddenFrame]': '!isVisible || !isExpanded'
            },
            template: "\n\n    <div class=\"whs-loc\">\n    \n      <div class=\"location-view container-fluid\">\n         \n         <div class=\"row\">\n          \n          <div class=\"col-xs-1\" style=\"color:#FFFFFF; height: 40px;\" (click)=\"expandInnerContents()\">\n              <div class=\"list-item\"> {{location.id}} </div>\n    \n          </div>\n          \n          <div class=\"col-xs-5\">\n              <div class=\"list-item\"> {{location.name}} </div>\n              <div class=\"list-item\"> {{location.description}} </div>\n          </div>\n          \n          <div class=\"col-xs-6 text-right\">\n              \n              <button class=\"btn btn-primary\" (click)=\"addNewLocation(id,warehouseId)\">\n                <i class=\"fa fa-plus\"></i>\n              </button>\n              <button class=\"btn btn-primary\" (click)=\"editLocation()\">\n                <i class=\"fa fa-pencil\"></i>\n              </button>\n              <button class=\"btn btn-primary\" (click)=\"printLabelForLocation()\">\n                <i class=\"fa fa-print\"></i>\n              </button>\n              <button class=\"btn btn-primary\" (click)=\"removeThisLocation()\">\n                <i class=\"fa fa-times\"></i>\n              </button>\n              <button class=\"btn btn-primary\">\n                <i class=\"fa fa-folder-open\" (click)=\"openBox()\"></i>\n              </button>\n          \n          </div>\n          \n         </div>\n\n      </div>\n      \n      <div>\n          <ul class=\"wtable\">\n          \n            <li *ngFor=\"let loc of childLocations\">\n                <whs-loc\n                  [location]=loc\n                  [name]=\"loc.name\" \n                  [id]=\"loc.id\" \n                  [warehouseId]=\"loc.warehouseId\" \n                  [parentId]=\"loc.parentId\" \n                  [childLocations]=\"loc.childs\" \n                  [isVisible]=\"!loc.isDeleted\"\n                  [isExpanded]=\"isChildsExpanded\"\n                  [listObject]=\"listObject\">\n                </whs-loc>\n            </li>\n            \n          </ul>\n      </div>\n         \n    </div>\n    \n  ",
            styles: [
                "    \n      \n    "
            ],
            directives: [WarehouseLocationComponent]
        }), 
        __metadata('design:paramtypes', [])
    ], WarehouseLocationComponent);
    return WarehouseLocationComponent;
}());
exports.WarehouseLocationComponent = WarehouseLocationComponent;
//# sourceMappingURL=whslocation.wcomp.js.map