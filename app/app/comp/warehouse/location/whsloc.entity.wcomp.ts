import {Component, Input} from "@angular/core";
import {App} from "../../../system/app";
import {WarehouseLocationEntity} from "../../../system/connection/connection-entities/whsapi-entities";
import {WarehouseLocationShowItemsComponent} from "../locitems/whslocshow.wcomp";

@Component({
  selector: "whslocentity",
  host: {

  },
  template: `
    <div class="whs-loc-entity">
    
      <div class="whs-loc-entity-l">
        
        <div>
          Stacks at location {{(entity?.amountOnLocation > 0) ? entity?.amountOnLocation : 0}}
        </div>
        
        <div>
          <i class="fa fa-barcode"></i> {{entity?.id}}
        </div>
        
        <div>
          <i class="fa fa-bookmark"></i> 
          <span 
            class="whs-iteme-extraspan" 
            style="background-color: rgb(52,159,137); color:#FFFFFF">
            {{entity?.name}}
          </span>
        </div>
        
        <div class="offset-top-sm">
          {{entity?.description}}    
        </div>
        
      </div>
      
      <div class="whs-loc-entity-r">
        <button class="btn btn-primary" style="height:64px;" (click)="openBox()"> Open Box </button>
      </div>
      
      <div style="height: 1px; width: 100%; clear: both"></div>
      
    </div>
  `,
  styles:[`
  
  `]
})

export class WarehouseLocationEntityComponent {

  @Input() public entity: WarehouseLocationEntity = null;

  @Input() public listeners: Array<string> = [];

  @Input() public variation: number = null;

  /** ------------------------------------------------------------------------------
   *
   *  ------------------------------------------------------------------------------ */
  constructor() {

  }
  /** ------------------------------------------------------------------------------
   *
   *  ------------------------------------------------------------------------------ */
  public openBox() {

    App.window.loadComponentIntoModalFrame(WarehouseLocationShowItemsComponent, {

      title: "List of items in the scanned location id: "+this.entity.id,
      data: {id:this.entity.id},
      onSuccess: (data) => {

      },
      onFailure: (err) => {

      },
      onClose: (data) => {
        App.inputScannerObserver.resumeListenerWithGIUD(this.listeners);
      },
      getReference: (ref) => {

        App.inputScannerObserver.pauseListenerWithGIUD(this.listeners);
        console.log(ref);
        if(typeof ref.highlightVariationId === 'function') {
          ref.highlightVariationId(this.variation);
        }

      }

    });

  }

}

