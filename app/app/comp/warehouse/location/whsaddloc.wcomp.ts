import {Component, Input} from "@angular/core";
import {ComponentImplementingModalFramePass, WindowCallbackError} from "../../../system/entities/system.entities";
import {WarehouseLocationEntity} from "../../../system/connection/connection-entities/whsapi-entities";
import {WarehouseApiConnector} from "../../../system/connection/whsapiconnector";
import {ApiConnectorComponent} from "../../../system/connection/apiconnector.component";

@Component({
  selector:"whs-aloc",
  template:`

    <div class="whs-loc-add">
      <div class="container-fluid">
          
        <form class="center-text">
          <label> Location name </label>
          <input type="text" [(ngModel)]="name">
          <button class="btn btn-primary" type="submit" (click)="clickSave($event)"> Save </button>
          <button class="btn btn-primary" (click)="clickCancel($event)"> Cancel </button>
        </form>
        
      </div>
    </div>
    
  `,
  styles:[
    `
      .whs-loc-add {
        width: 100%;
        display: inline-block;
        vertical-align: top;
      }
    `
  ]
})

export class WarehouseAddLocationComponent implements ComponentImplementingModalFramePass {

  @Input() name: string = "";

	loadCallback:any;

  dataObject:any;

  closingMethod:any;

  successCallback: any;

  failureCallback: any;

	/** ------------------------------------------------------------------------------------------------
   *
   *
   *
   *  ------------------------------------------------------------------------------------------------
   *
   * @param e
   */
  public clickCancel(e) {
    e.preventDefault();
    this.failureCallback();
    this.closingMethod();
  }

	/** ------------------------------------------------------------------------------------------------
   *
   *
   *
   *  ------------------------------------------------------------------------------------------------
   *
   * @param e
   */
  public clickSave(e) {

    e.preventDefault();

    var aapiconn: WarehouseApiConnector = ApiConnectorComponent.getWarehouseApiConnector();

    if(this.dataObject.hasOwnProperty("warehouseId") && this.dataObject.hasOwnProperty("parentId")) {

      var data:WarehouseLocationEntity = <WarehouseLocationEntity>this.dataObject;

      var loc:WarehouseLocationEntity = {
        parentId: data.id,
        warehouseId: data.warehouseId,
        name: this.name,
        isAnchor: false
      };

      aapiconn.createNewLocation(loc).subscribe((entity) => {

        console.log(entity);
        loc.id = entity.id;
        this.successCallback(loc);
        this.closingMethod();

      },(err) => {

        var error: WindowCallbackError = {errorCode: 500, errorMessage: "Something happen", data: err};
        this.failureCallback(error);
        this.closingMethod();

      });

    }

  }

}
