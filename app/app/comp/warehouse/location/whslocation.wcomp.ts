import {Component, Input} from "@angular/core";
import {WarehouseLocationEntity} from "../../../system/connection/connection-entities/whsapi-entities";
import {App} from "../../../system/app";
import {WarehouseAddLocationComponent} from "./whsaddloc.wcomp";
import {WarehouseRemoveLocationComponent} from "./whsremloc.wcomp";
import {WarehouseLocationsListSystemTable} from "./whsloc.sys";
import {WarehouseEditLocationComponent} from "./whseditloc.wcomp";
import {WarehouseLocationShowItemsComponent} from "../locitems/whslocshow.wcomp";

@Component({
  selector:"whs-loc",
  host: {
    '[class.hiddenFrame]':'!isVisible || !isExpanded'
  },
  template:`

    <div class="whs-loc">
    
      <div class="location-view container-fluid">
         
         <div class="row">
          
          <div class="col-xs-1" style="color:#FFFFFF; height: 40px;" (click)="expandInnerContents()">
              <div class="list-item"> {{location.id}} </div>
    
          </div>
          
          <div class="col-xs-5">
              <div class="list-item"> {{location.name}} </div>
              <div class="list-item"> {{location.description}} </div>
          </div>
          
          <div class="col-xs-6 text-right">
              
              <button class="btn btn-primary" (click)="addNewLocation(id,warehouseId)">
                <i class="fa fa-plus"></i>
              </button>
              <button class="btn btn-primary" (click)="editLocation()">
                <i class="fa fa-pencil"></i>
              </button>
              <button class="btn btn-primary" (click)="printLabelForLocation()">
                <i class="fa fa-print"></i>
              </button>
              <button class="btn btn-primary" (click)="removeThisLocation()">
                <i class="fa fa-times"></i>
              </button>
              <button class="btn btn-primary">
                <i class="fa fa-folder-open" (click)="openBox()"></i>
              </button>
          
          </div>
          
         </div>

      </div>
      
      <div>
          <ul class="wtable">
          
            <li *ngFor="let loc of childLocations">
                <whs-loc
                  [location]=loc
                  [name]="loc.name" 
                  [id]="loc.id" 
                  [warehouseId]="loc.warehouseId" 
                  [parentId]="loc.parentId" 
                  [childLocations]="loc.childs" 
                  [isVisible]="!loc.isDeleted"
                  [isExpanded]="isChildsExpanded"
                  [listObject]="listObject">
                </whs-loc>
            </li>
            
          </ul>
      </div>
         
    </div>
    
  `,
  styles:[
    `    
      
    `
  ],
  directives:[WarehouseLocationComponent]
})


export class WarehouseLocationComponent {

  @Input() public listObject: WarehouseLocationsListSystemTable;

  @Input() public isVisible: boolean = true;

  @Input() public isExpanded: boolean = false;

  @Input() public isChildsExpanded: boolean = false;

  @Input() public name: String = "";

  @Input() public id: Number;

  @Input() public parentId: Number;

  @Input() public warehouseId: Number;

  @Input() public location: WarehouseLocationEntity;

  @Input() public childLocations: WarehouseLocationEntity[] = [];

	/** --------------------------------------------------------------------------------------
   *
   *
   *  --------------------------------------------------------------------------------------
   *
   *  @param parentId
   *  @param warehouseId
   */
  public addNewLocation(parentId: number, warehouseId: number) {

    App.window.loadComponentIntoModalFrame(WarehouseAddLocationComponent, {

      title:"Add new location",
      data: {id: this.id, parentId: this.parentId, warehouseId: this.warehouseId},
      onLoad: () => {

      },
      onSuccess: (data) => {
        this.listObject.appendToElementWithId(this.id,data);
      },
      onClose: (data) => {

      },
      onFailure: (error) => {
        alert("Some error happened, operation can't be done");
      },
      getReference: (ref) => {

      }

    });

  }

  public expandInnerContents() {
    this.isChildsExpanded = !this.isChildsExpanded;
  }
	/** --------------------------------------------------------------------------------------
   *
   *
   *  --------------------------------------------------------------------------------------
   *
   *
   */
  public editLocation() {

    App.window.loadComponentIntoModalFrame(WarehouseEditLocationComponent, {
      title:"Edit location :"+this.location.id,
      data: this.location,
      onLoad: () => {

      },
      onSuccess: (data) => {
        this.location = data;
      },
      onClose: (data) => {

      },
      onFailure: (error) => {
        alert("Some error happened, operation can't be done");
      },
      getReference: (ref) => {

      }
    });

  }

	/** --------------------------------------------------------------------------------------
   *
   *
   *  --------------------------------------------------------------------------------------
   */
  public printLabelForLocation() {

    var locLabel = App.codes.convertIntToLocationId(this.id);
    App.print.printBarCode128Z(locLabel);

  }

	/** --------------------------------------------------------------------------------------
   *
   *
   *  --------------------------------------------------------------------------------------
   */
  public editThisLocation() {

  }

	/** --------------------------------------------------------------------------------------
   *
   *
   *  --------------------------------------------------------------------------------------
   */
  public removeThisLocation() {

    App.window.loadComponentIntoModalFrame(WarehouseRemoveLocationComponent, {

      title:"Some removing sex with locations?",
      data: {id: this.id, parentId: this.parentId, warehouseId: this.warehouseId},
      onLoad: () => {

      },
      onSuccess: (data) => {
        console.log("It's success fucking fuck");
        console.log(data);
        this.listObject.hideElementWithId(data.id);
        this.isVisible = false;
      },
      onClose: (data) => {

      },
      onFailure: (error) => {
        console.log("It's bad sucking shit fucking fuck");
        alert("Some error happened, operation can't be done");
      },
      getReference: (ref) => {

      }

    });
  }

  /** ------------------------------------------------------------------------------
   *
   *  ------------------------------------------------------------------------------ */
  public openBox() {

    App.window.loadComponentIntoModalFrame(WarehouseLocationShowItemsComponent, {

      title: "List of items in the location id: "+this.id+" "+this.name,
      data: {id:this.id},
      onSuccess: (data) => {

      },
      onFailure: (err) => {

      },
      onClose: (data) => {
        //App.inputScannerObserver.resumeListenerWithGIUD(this.listeners);
      },
      getReference: (ref) => {
        //App.inputScannerObserver.pauseListenerWithGIUD(this.listeners);
        // To make dependant modal window acting as linked window - View need to be refreshed
        // this.refreshView();
      }

    });

  }

}
