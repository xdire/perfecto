"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var apiconnector_component_1 = require("../../../system/connection/apiconnector.component");
var WarehouseRemoveLocationComponent = (function () {
    function WarehouseRemoveLocationComponent() {
        this.name = "";
    }
    WarehouseRemoveLocationComponent.prototype.clickCancel = function (e) {
        e.preventDefault();
        this.closingMethod();
    };
    /** ------------------------------------------
   *
   *
   *  ------------------------------------------
   *
   * @param e
   */
    WarehouseRemoveLocationComponent.prototype.clickRemove = function (e) {
        var _this = this;
        e.preventDefault();
        var wconn = apiconnector_component_1.ApiConnectorComponent.getWarehouseApiConnector();
        if (this.dataObject.hasOwnProperty("warehouseId") && this.dataObject.hasOwnProperty("id")) {
            var data = this.dataObject;
            console.log(wconn);
            wconn.trashLocation(data.id).subscribe(function (entity) {
                console.log(entity);
                _this.successCallback(data.id);
                _this.closingMethod();
            }, function (err) {
                var error = { errorCode: 500, errorMessage: "Something happen", data: err };
                console.log(error);
                _this.failureCallback(error);
                _this.closingMethod();
            });
        }
    };
    __decorate([
        core_1.Input(), 
        __metadata('design:type', String)
    ], WarehouseRemoveLocationComponent.prototype, "name", void 0);
    WarehouseRemoveLocationComponent = __decorate([
        core_1.Component({
            selector: "whs-rloc",
            template: "\n\n    <div class=\"whs-loc-rem\">\n      <div class=\"container-fluid\">\n          \n        <form class=\"center-text\">\n          <label> You sure to remove this location? </label>\n          <button class=\"btn btn-primary\" type=\"submit\" (click)=\"clickRemove($event)\"> Remove </button>\n          <button class=\"btn btn-primary\" (click)=\"clickCancel($event)\"> Cancel </button>\n        </form>\n        \n      </div>\n    </div>\n    \n  ",
            styles: [
                "\n      .whs-loc-rem {\n        color: #333333;\n        width: 100%;\n        display: inline-block;\n        vertical-align: top;\n      }\n    "
            ]
        }), 
        __metadata('design:paramtypes', [])
    ], WarehouseRemoveLocationComponent);
    return WarehouseRemoveLocationComponent;
}());
exports.WarehouseRemoveLocationComponent = WarehouseRemoveLocationComponent;
//# sourceMappingURL=whsremloc.wcomp.js.map