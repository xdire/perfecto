"use strict";
/** -------------------------------------------------------------------------------------------------
 *
 *
 *                      Class creates and maintains structure of warehouse locations
 *
 *
 *  -------------------------------------------------------------------------------------------------
 *
 */
var WarehouseLocationsListSystemTable = (function () {
    function WarehouseLocationsListSystemTable() {
        /**
       *  List of locations represented by array structure
       *  @type {Array}
       */
        this.list = [];
        /**
       *  Index list of elements, holding refernces to main array structure
       *  @type {{}}
       */
        this.index = {};
    }
    /** -------------------------------------------------------------------------------------------------
   *
   *              Parse the array structure from an API to create initial array of data
   *
   *
   *  -------------------------------------------------------------------------------------------------
   *
   * @param data
   */
    WarehouseLocationsListSystemTable.prototype.createFromArray = function (data) {
        for (var i = 0, k = 0; i < data.length; i++) {
            data[i].childs = [];
            if (this.index.hasOwnProperty(data[i].parentId)) {
                if (data[i].parentId !== null) {
                    // Lookup in index for parent three reference
                    var parent = this.index[data[i].parentId];
                    // Take length of parent childs tree
                    var len = parent.childs.length;
                    // Set depth of the child according to parent depth
                    data[i].depth = parent.depth + 1;
                    // Append as last element in array
                    parent.childs[len] = data[i];
                    // Put in index at child ID - child reference from parent
                    this.index[data[i].id] = parent.childs[len++];
                    continue;
                }
            }
            // Set root level to 1 at the first level
            data[i].depth = 1;
            // Add to main list
            this.list[k] = data[i];
            // Add to index
            this.index[data[i].id] = this.list[k++];
        }
    };
    /** -------------------------------------------------------------------------------------------------
   *
   *                  Append new Location entity to some element in the structure
   *
   *      Method demands proper data structure of WarehouseLocationEntity interface, if method
   *      found that passed id of desired parent element is null then new element will be applied
   *      to the root of the warehouse
   *
   *  -------------------------------------------------------------------------------------------------
   *
   * @param id
   */
    WarehouseLocationsListSystemTable.prototype.appendToElementWithId = function (id, data) {
        if (id !== null) {
            if (this.index.hasOwnProperty(id)) {
                data.depth = this.index[id].depth + 1;
                this.index[id].childs.push(data);
            }
        }
        else {
            data["childs"] = [];
            data.depth = 1;
            this.list.push(data);
            this.index[data.id] = this.list[this.list.length - 1];
        }
    };
    /** -------------------------------------------------------------------------------------------------
   *
   *                                Hide (Soft remove / Archive) location
   *
   *
   *  -------------------------------------------------------------------------------------------------
   *
   * @param id
   */
    WarehouseLocationsListSystemTable.prototype.hideElementWithId = function (id) {
        if (this.index.hasOwnProperty(id)) {
            this.index[id].isDeleted = true;
        }
    };
    /** -------------------------------------------------------------------------------------------------
   *
   *                               Remove location completely from the list
   *
   *
   *  -------------------------------------------------------------------------------------------------
   *
   * @param whichElement
   * @param fromParent
   */
    WarehouseLocationsListSystemTable.prototype.deleteElementWithIdFromElementWithId = function (whichElement, fromParent) {
        if (this.index.hasOwnProperty(whichElement)) {
            this.index[whichElement].isDeleted = true;
        }
        if (this.index.hasOwnProperty(fromParent)) {
            var childs = this.index[fromParent].childs;
            var newChilds = [];
            for (var i = 0, k = 0; i < childs.length; i++) {
                if (childs[i].id != whichElement) {
                    newChilds[k++] = childs[i];
                }
            }
            this.index[fromParent].childs = newChilds;
        }
    };
    return WarehouseLocationsListSystemTable;
}());
exports.WarehouseLocationsListSystemTable = WarehouseLocationsListSystemTable;
//# sourceMappingURL=whsloc.sys.js.map