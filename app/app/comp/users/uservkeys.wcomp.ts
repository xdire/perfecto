import {Component, Input} from "@angular/core";
import {UserApiVendorKeysEntity} from "../../system/entities/system.entities";
import {WindowComponent} from "../../system/window/window.component";
import {App} from "../../system/app";

@Component({
  selector: "uservkeys",
  host: {
    '[class.hiddenFrame]': '!_isVisible'
  },
  template: `
    <div class="keys-container" *ngFor="#key of keys; #i = index;">
      <ul class="list-container" style="list-style-type: none;">
        <li> <span class="list-title">user #</span> {{key?.id}}</li>
        <li> <span class="list-title">vendor #</span> {{key?.vendor}}</li>
        <li> <span class="list-title">client ID:</span> {{key?.authId}}</li>
        <li> <span class="list-title">client Key:</span> {{key?.authKey}}</li>
        <li> <span class="list-title">client Secret:</span> {{key?.authSecret}}</li>
      </ul>
      <div class="list-button-container">
        Use this key to all further operations in application.<br>
        <button class="btn btn-primary list-button" (click)="onClickSetVendorKey(i)">Use this key</button>
      </div>
    </div>
  `,
  styles:[`
    .list-title {width:120px; display:inline-block; font-weight:600;}
    .list-container {
      margin-top:10px;
      padding-bottom:10px;
      width: 100%;
      display:inline-block;
      vertical-align:top;
      text-align: left;}
    .keys-container {padding-bottom:10px; text-align:center; border:dashed 1px #dedede; border-radius: 4px; background-color:#f8f8f8;}
    .list-button-container {display:inline-block; border-top:1px solid #cacaca; padding-top: 8px; width:100%; text-align:center; position:relative; height:inherit; }
    .list-button {position:relative; margin-top:15px;}
  `]
})

export class UserVendorKeysComponent extends WindowComponent {

  @Input() keys: Array<UserApiVendorKeysEntity>;

  private _isVisible: boolean = false;

  ngOnInit(){
    super.constructor();
  }

  constructor(){
    super.constructor();
  }

  public show() {
    this._isVisible = true;
  }

  public hide() {
    this._isVisible = false;
  }

  get isVisible():boolean {
    return this._isVisible;
  }

  @Input() set keysArray(keys: Array) {


    if(keys.length > 0) {

      this.keys = keys;
      this.show();

    } else {

      this.keys = keys;
      this.hide();

    }

  }

  public onClickSetVendorKey(index) {

    var key = this.keys[index];

    App.state.__setCurrentUserVendorKey(key);

  }

}


