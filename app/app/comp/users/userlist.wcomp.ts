import {Component, Input} from "@angular/core";
import {UserApiUserEntity} from "../../system/entities/system.entities";
import {WindowComponent} from "../../system/window/window.component";
import {App} from "../../system/app";

@Component({
  selector: "userlist",
  host: {
    '[class.hiddenFrame]': '!_isVisible'
  },
  template: `
    <table class="table table-striped">
    <tr class="keys-container" *ngFor="#usr of users; #i = index;">
   
        <td>  {{usr?.id}}</td>
        <td>  {{usr?.login}}</td>
        <td>  {{usr?.email}}</td>
        <td>  {{usr?.firstName}}</td>
        <td>  {{usr?.lastName}}</td>
        <td>  {{usr?.active}}</td>
        <td>  <button class="btn btn-primary" (click)="onEditUser(i)"> Edit </button> </td>
   
    </tr>
    </table>
  `,
  styles:[`
    .list-title { width: 120px; display: inline-block; font-weight: 600;}
    .list-container {margin-top:10px; border-bottom: dashed 1px #66b8da; padding-bottom: 10px; max-width: 580px; display: inline-block; vertical-align: top; }
    .keys-container:last-child { border-bottom: dashed 1px #CCC;}
    .keys-container td:first-child {border-left: dashed 1px #CCC;}
    .keys-container td {padding: 4px; text-align: center; padding-right: 10px; border-right: dashed 1px #CCC; vertical-align: middle}
    .list-button-container {display: inline-block; margin-left: 10px; }
    .list-button {margin-top: 33%;}
  `]
})

export class UserListComponent extends WindowComponent {

  @Input() users: Array<UserApiUserEntity>;

  private _isVisible: boolean = false;

  ngOnInit(){
    super.constructor();
  }

  constructor(){
    super.constructor();
  }

  public show() {
    this._isVisible = true;
  }

  public hide() {
    this._isVisible = false;
  }

  get isVisible():boolean {
    return this._isVisible;
  }

  @Input() set usersArray(keys: Array) {


    if(keys.length > 0) {

      this.users = keys;
      this.show();

    } else {

      this.users = keys;
      this.hide();

    }

  }

  onEditUser(id) {

    console.log("Edit user: ");
    console.log(this.users[id]);

  }


}



