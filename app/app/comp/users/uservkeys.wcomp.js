"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var window_component_1 = require("../../system/window/window.component");
var app_1 = require("../../system/app");
var UserVendorKeysComponent = (function (_super) {
    __extends(UserVendorKeysComponent, _super);
    function UserVendorKeysComponent() {
        this._isVisible = false;
        _super.prototype.constructor.call(this);
    }
    UserVendorKeysComponent.prototype.ngOnInit = function () {
        _super.prototype.constructor.call(this);
    };
    UserVendorKeysComponent.prototype.show = function () {
        this._isVisible = true;
    };
    UserVendorKeysComponent.prototype.hide = function () {
        this._isVisible = false;
    };
    Object.defineProperty(UserVendorKeysComponent.prototype, "isVisible", {
        get: function () {
            return this._isVisible;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(UserVendorKeysComponent.prototype, "keysArray", {
        set: function (keys) {
            if (keys.length > 0) {
                this.keys = keys;
                this.show();
            }
            else {
                this.keys = keys;
                this.hide();
            }
        },
        enumerable: true,
        configurable: true
    });
    UserVendorKeysComponent.prototype.onClickSetVendorKey = function (index) {
        var key = this.keys[index];
        app_1.App.state.__setCurrentUserVendorKey(key);
    };
    __decorate([
        core_1.Input(), 
        __metadata('design:type', Array)
    ], UserVendorKeysComponent.prototype, "keys", void 0);
    __decorate([
        core_1.Input(), 
        __metadata('design:type', Array), 
        __metadata('design:paramtypes', [Array])
    ], UserVendorKeysComponent.prototype, "keysArray", null);
    UserVendorKeysComponent = __decorate([
        core_1.Component({
            selector: "uservkeys",
            host: {
                '[class.hiddenFrame]': '!_isVisible'
            },
            template: "\n    <div class=\"keys-container\" *ngFor=\"#key of keys; #i = index;\">\n      <ul class=\"list-container\" style=\"list-style-type: none;\">\n        <li> <span class=\"list-title\">user #</span> {{key?.id}}</li>\n        <li> <span class=\"list-title\">vendor #</span> {{key?.vendor}}</li>\n        <li> <span class=\"list-title\">client ID:</span> {{key?.authId}}</li>\n        <li> <span class=\"list-title\">client Key:</span> {{key?.authKey}}</li>\n        <li> <span class=\"list-title\">client Secret:</span> {{key?.authSecret}}</li>\n      </ul>\n      <div class=\"list-button-container\">\n        Use this key to all further operations in application.<br>\n        <button class=\"btn btn-primary list-button\" (click)=\"onClickSetVendorKey(i)\">Use this key</button>\n      </div>\n    </div>\n  ",
            styles: ["\n    .list-title {width:120px; display:inline-block; font-weight:600;}\n    .list-container {\n      margin-top:10px;\n      padding-bottom:10px;\n      width: 100%;\n      display:inline-block;\n      vertical-align:top;\n      text-align: left;}\n    .keys-container {padding-bottom:10px; text-align:center; border:dashed 1px #dedede; border-radius: 4px; background-color:#f8f8f8;}\n    .list-button-container {display:inline-block; border-top:1px solid #cacaca; padding-top: 8px; width:100%; text-align:center; position:relative; height:inherit; }\n    .list-button {position:relative; margin-top:15px;}\n  "]
        }), 
        __metadata('design:paramtypes', [])
    ], UserVendorKeysComponent);
    return UserVendorKeysComponent;
}(window_component_1.WindowComponent));
exports.UserVendorKeysComponent = UserVendorKeysComponent;
//# sourceMappingURL=uservkeys.wcomp.js.map