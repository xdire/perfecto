import {Component} from "@angular/core";
import {WindowComponent} from "../../system/window/window.component";
import {UserVendorKeysComponent} from "./uservkeys.wcomp";
import {AuthApiConnector} from "../../system/connection/authapiconnector";
import {ApiConnectorComponent} from "../../system/connection/apiconnector.component";
import {UserListComponent} from "./userlist.wcomp";

@Component({
  selector:"users",
  template:`

    <div class="container-fluid">
      <div class="row">
        <div class="col-xs-12">
          <h1> Users control panel </h1>
          
          <div class="panel panel-default">
            <div class="panel-heading">System users list
              <a class="pull-right" name="showusers" 
              (click)="onShowUsersClick($event)">
                {{usersExpandNote?"[show]":"[hide]"}}
              </a>
            </div>
            <div class="panel-body">
            
              <div *ngIf="usersExpandNote"> System users </div>
              
              <userlist [usersArray]="usersList">
                
              </userlist>
              
            </div>
          </div>
          
          <div class="panel panel-default">
            <div class="panel-heading">User vendor keys 
              <a class="pull-right" name="showkeys" 
              (click)="onShowKeysClick($event)">
                {{vendorKeysNote?"[show]":"[hide]"}}
              </a>
            </div>
            <div class="panel-body">
            
              <div *ngIf="vendorKeysNote"> Press show to view existing keys </div>
              
              <uservkeys [keysArray]="vendorKeys">
          
              </uservkeys>
              
            </div>
          </div>
        </div>
      </div>  
    </div>
    
  `,
  directives:[UserVendorKeysComponent, UserListComponent]
})

export class UsersComponent extends WindowComponent {

  private usersExpandNote: boolean = true;

  private vendorKeysNote: boolean = true;

  public vendorKeys: Array = [];

  public usersList: Array = [];

  constructor() {
    super.constructor();
    this.isRoot = true;
    this.isWithMenu = true;
    this.publicName = "user";
  }

  public onCreate() {
    console.log("User screen on create fired ");
  }

	/** ------------------------------------------------------------------
   *
   *                    Show System Users in a tab
   *
   *  ------------------------------------------------------------------
   *
   * @param $event
   */
  public onShowUsersClick($event) {

    this.usersExpandNote = !this.usersExpandNote;

    if(!this.usersExpandNote) {

      var aapiconn:AuthApiConnector = ApiConnectorComponent.getAuthApiConnector();

      aapiconn.getUserList().subscribe((usersInfo) => {

        this.usersList = usersInfo;

      }, (err) => {


      });

    } else {

      this.usersList = [];

    }

  }

	/** ------------------------------------------------------------------
   *
   *                    Show User Keys Information in a tab
   *
   *  ------------------------------------------------------------------
   *
   * @param $event
   */
  public onShowKeysClick($event) {

    this.vendorKeysNote = !this.vendorKeysNote;

    if(!this.vendorKeysNote) {

      var aapiconn:AuthApiConnector = ApiConnectorComponent.getAuthApiConnector();

      aapiconn.getUserVendorKeys().subscribe((keysInfo) => {

        this.vendorKeys = keysInfo;

      }, (err) => {


      });

    } else {

      this.vendorKeys = [];

    }

  }

}
