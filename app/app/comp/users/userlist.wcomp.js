"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var window_component_1 = require("../../system/window/window.component");
var UserListComponent = (function (_super) {
    __extends(UserListComponent, _super);
    function UserListComponent() {
        this._isVisible = false;
        _super.prototype.constructor.call(this);
    }
    UserListComponent.prototype.ngOnInit = function () {
        _super.prototype.constructor.call(this);
    };
    UserListComponent.prototype.show = function () {
        this._isVisible = true;
    };
    UserListComponent.prototype.hide = function () {
        this._isVisible = false;
    };
    Object.defineProperty(UserListComponent.prototype, "isVisible", {
        get: function () {
            return this._isVisible;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(UserListComponent.prototype, "usersArray", {
        set: function (keys) {
            if (keys.length > 0) {
                this.users = keys;
                this.show();
            }
            else {
                this.users = keys;
                this.hide();
            }
        },
        enumerable: true,
        configurable: true
    });
    UserListComponent.prototype.onEditUser = function (id) {
        console.log("Edit user: ");
        console.log(this.users[id]);
    };
    __decorate([
        core_1.Input(), 
        __metadata('design:type', Array)
    ], UserListComponent.prototype, "users", void 0);
    __decorate([
        core_1.Input(), 
        __metadata('design:type', Array), 
        __metadata('design:paramtypes', [Array])
    ], UserListComponent.prototype, "usersArray", null);
    UserListComponent = __decorate([
        core_1.Component({
            selector: "userlist",
            host: {
                '[class.hiddenFrame]': '!_isVisible'
            },
            template: "\n    <table class=\"table table-striped\">\n    <tr class=\"keys-container\" *ngFor=\"#usr of users; #i = index;\">\n   \n        <td>  {{usr?.id}}</td>\n        <td>  {{usr?.login}}</td>\n        <td>  {{usr?.email}}</td>\n        <td>  {{usr?.firstName}}</td>\n        <td>  {{usr?.lastName}}</td>\n        <td>  {{usr?.active}}</td>\n        <td>  <button class=\"btn btn-primary\" (click)=\"onEditUser(i)\"> Edit </button> </td>\n   \n    </tr>\n    </table>\n  ",
            styles: ["\n    .list-title { width: 120px; display: inline-block; font-weight: 600;}\n    .list-container {margin-top:10px; border-bottom: dashed 1px #66b8da; padding-bottom: 10px; max-width: 580px; display: inline-block; vertical-align: top; }\n    .keys-container:last-child { border-bottom: dashed 1px #CCC;}\n    .keys-container td:first-child {border-left: dashed 1px #CCC;}\n    .keys-container td {padding: 4px; text-align: center; padding-right: 10px; border-right: dashed 1px #CCC; vertical-align: middle}\n    .list-button-container {display: inline-block; margin-left: 10px; }\n    .list-button {margin-top: 33%;}\n  "]
        }), 
        __metadata('design:paramtypes', [])
    ], UserListComponent);
    return UserListComponent;
}(window_component_1.WindowComponent));
exports.UserListComponent = UserListComponent;
//# sourceMappingURL=userlist.wcomp.js.map