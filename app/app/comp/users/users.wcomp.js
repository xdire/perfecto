"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var window_component_1 = require("../../system/window/window.component");
var uservkeys_wcomp_1 = require("./uservkeys.wcomp");
var apiconnector_component_1 = require("../../system/connection/apiconnector.component");
var userlist_wcomp_1 = require("./userlist.wcomp");
var UsersComponent = (function (_super) {
    __extends(UsersComponent, _super);
    function UsersComponent() {
        this.usersExpandNote = true;
        this.vendorKeysNote = true;
        this.vendorKeys = [];
        this.usersList = [];
        _super.prototype.constructor.call(this);
        this.isRoot = true;
        this.isWithMenu = true;
        this.publicName = "user";
    }
    UsersComponent.prototype.onCreate = function () {
        console.log("User screen on create fired ");
    };
    /** ------------------------------------------------------------------
   *
   *                    Show System Users in a tab
   *
   *  ------------------------------------------------------------------
   *
   * @param $event
   */
    UsersComponent.prototype.onShowUsersClick = function ($event) {
        var _this = this;
        this.usersExpandNote = !this.usersExpandNote;
        if (!this.usersExpandNote) {
            var aapiconn = apiconnector_component_1.ApiConnectorComponent.getAuthApiConnector();
            aapiconn.getUserList().subscribe(function (usersInfo) {
                _this.usersList = usersInfo;
            }, function (err) {
            });
        }
        else {
            this.usersList = [];
        }
    };
    /** ------------------------------------------------------------------
   *
   *                    Show User Keys Information in a tab
   *
   *  ------------------------------------------------------------------
   *
   * @param $event
   */
    UsersComponent.prototype.onShowKeysClick = function ($event) {
        var _this = this;
        this.vendorKeysNote = !this.vendorKeysNote;
        if (!this.vendorKeysNote) {
            var aapiconn = apiconnector_component_1.ApiConnectorComponent.getAuthApiConnector();
            aapiconn.getUserVendorKeys().subscribe(function (keysInfo) {
                _this.vendorKeys = keysInfo;
            }, function (err) {
            });
        }
        else {
            this.vendorKeys = [];
        }
    };
    UsersComponent = __decorate([
        core_1.Component({
            selector: "users",
            template: "\n\n    <div class=\"container-fluid\">\n      <div class=\"row\">\n        <div class=\"col-xs-12\">\n          <h1> Users control panel </h1>\n          \n          <div class=\"panel panel-default\">\n            <div class=\"panel-heading\">System users list\n              <a class=\"pull-right\" name=\"showusers\" \n              (click)=\"onShowUsersClick($event)\">\n                {{usersExpandNote?\"[show]\":\"[hide]\"}}\n              </a>\n            </div>\n            <div class=\"panel-body\">\n            \n              <div *ngIf=\"usersExpandNote\"> System users </div>\n              \n              <userlist [usersArray]=\"usersList\">\n                \n              </userlist>\n              \n            </div>\n          </div>\n          \n          <div class=\"panel panel-default\">\n            <div class=\"panel-heading\">User vendor keys \n              <a class=\"pull-right\" name=\"showkeys\" \n              (click)=\"onShowKeysClick($event)\">\n                {{vendorKeysNote?\"[show]\":\"[hide]\"}}\n              </a>\n            </div>\n            <div class=\"panel-body\">\n            \n              <div *ngIf=\"vendorKeysNote\"> Press show to view existing keys </div>\n              \n              <uservkeys [keysArray]=\"vendorKeys\">\n          \n              </uservkeys>\n              \n            </div>\n          </div>\n        </div>\n      </div>  \n    </div>\n    \n  ",
            directives: [uservkeys_wcomp_1.UserVendorKeysComponent, userlist_wcomp_1.UserListComponent]
        }), 
        __metadata('design:paramtypes', [])
    ], UsersComponent);
    return UsersComponent;
}(window_component_1.WindowComponent));
exports.UsersComponent = UsersComponent;
//# sourceMappingURL=users.wcomp.js.map