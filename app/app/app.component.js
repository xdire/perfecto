"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var apiconnector_component_1 = require("./system/connection/apiconnector.component");
var app_1 = require("./system/app");
var AppComponent = (function () {
    function AppComponent() {
        // LAUNCH TESTING FUNCTION ON START
        //this.mainTest();
    }
    /**
     *  MAIN TESTING FUNCTION
     */
    AppComponent.prototype.mainTest = function () {
        var _this = this;
        var aapiconn = apiconnector_component_1.ApiConnectorComponent.getAuthApiConnector();
        try {
            console.log(aapiconn.authorizeUser({ email: "dire.one@gmail.com", login: "", pass: "Wrfof1983!", agent: "dtp" })
                .subscribe(function (posts) {
                //console.log(posts);
                var user = app_1.App.state.user;
                user.__clearCache();
                user.__keyInfoToCache(posts);
                app_1.App.state.__setAuthorized();
                //console.log(user);
                if (user.login == null || user.email == null)
                    _this.userViewTest(posts.user, posts.key);
            }, function (error) {
                console.log(error);
            }));
        }
        catch (e) {
            console.log(e);
        }
    };
    /**
   *
   */
    AppComponent.prototype.userViewTest = function (user, key) {
        try {
            var aapiconn = apiconnector_component_1.ApiConnectorComponent.getAuthApiConnector();
            aapiconn.getUserInfo({ user: user, key: key }).subscribe(function (creds) {
                app_1.App.state.user.__fromUserEntity(creds);
                app_1.App.state.user.timestamp = Date.now();
                app_1.App.state.user.__toCache();
            }, function (error) {
                console.log(error);
            });
        }
        catch (e) {
            console.log(e);
        }
    };
    AppComponent.prototype.saveViewTest = function () {
    };
    AppComponent = __decorate([
        core_1.Component({
            selector: 'app',
            template: '<window></window>'
        }), 
        __metadata('design:paramtypes', [])
    ], AppComponent);
    return AppComponent;
}());
exports.AppComponent = AppComponent;
//# sourceMappingURL=app.component.js.map