"use strict";
var DataAccumulator = (function () {
    /** --------------------------------------------------------------
   *                  Create capacitor
   *  --------------------------------------------------------------
   *
   * @param capacity
   */
    function DataAccumulator(capacity) {
        if (capacity === void 0) { capacity = 1024; }
        this.collection = {};
        this.capacity = 1024;
        this.blocks = 8;
        this.currentBlock = 0;
        this.maxBlockCapacity = 1;
        this.cleaning = false;
        this.capacity = capacity;
        this.maxBlockCapacity = Math.floor(this.capacity / this.blocks);
    }
    /** --------------------------------------------------------------
   *
   *  --------------------------------------------------------------
   *
   * @param value
   * @param limit
   * @returns {any}
   */
    DataAccumulator.prototype.getValue = function (value, limit) {
        if (limit === void 0) { limit = 10; }
        if (this.collection.hasOwnProperty(value)) {
            return [value];
        }
        else {
            var res = this.getValuesCloseToValue(value);
            if (res.length > limit) {
                res = res.slice(0, limit);
            }
            return res;
        }
    };
    /** --------------------------------------------------------------
   *    Get list of values which can be close by match to value
   *  --------------------------------------------------------------
   *
   * @param value
   */
    DataAccumulator.prototype.getValuesCloseToValue = function (value) {
        var keys = Object.keys(this.collection);
        var operable = true;
        var ops = 0;
        var compare = value;
        while (operable) {
            var s = this.searchAndCompare(keys, compare);
            if (s.length > 0)
                return s;
            compare = value.slice(0, -1);
            if (ops++ > 3)
                break;
        }
        return [];
    };
    /** --------------------------------------------------------------
   *                 Get all data which is matching
   *  --------------------------------------------------------------
   *
   * @param array
   * @param value
   * @returns {Array}
   */
    DataAccumulator.prototype.searchAndCompare = function (array, value) {
        var b = array.filter(function (val) {
            return val.indexOf(value) == 0;
        });
        console.log("FILTERING RESULTS FOR -> " + value);
        console.log(b);
        return b;
    };
    /** --------------------------------------------------------------
   *                  Fill data from array
   *  --------------------------------------------------------------
   *
   * @param data
   */
    DataAccumulator.prototype.fromArray = function (data) {
        if (++this.currentBlock > 8) {
            this.currentBlock = 1;
            this.cleaning = true;
        }
        if (this.cleaning)
            this.cleanData();
        for (var i = 0; i < data.length; i++) {
            this.collection[data[i]] = this.currentBlock;
            if (i > this.maxBlockCapacity)
                break;
        }
    };
    /** --------------------------------------------------------------
   *              Replenish free slots in collection
   *  --------------------------------------------------------------
   */
    DataAccumulator.prototype.cleanData = function () {
        for (var key in this.collection) {
            if (this.collection[key] == this.currentBlock) {
                delete this.collection[key];
            }
        }
    };
    return DataAccumulator;
}());
exports.DataAccumulator = DataAccumulator;
//# sourceMappingURL=dataaccu.sys.js.map