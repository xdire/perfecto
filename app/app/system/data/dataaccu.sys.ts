export class DataAccumulator {

  private collection: {[name:string]:number} = {};

  private capacity: number = 1024;

  private blocks: number = 8;

  private currentBlock: number = 0;

  private maxBlockCapacity: number = 1;

  private cleaning: boolean = false;

	/** --------------------------------------------------------------
   *                  Create capacitor
   *  --------------------------------------------------------------
   *
   * @param capacity
   */
  constructor(capacity: number = 1024) {
    this.capacity = capacity;
    this.maxBlockCapacity = Math.floor(this.capacity / this.blocks);
  }

	/** --------------------------------------------------------------
   * 
   *  --------------------------------------------------------------
   *  
   * @param value
   * @param limit
   * @returns {any}
   */
  public getValue(value: string, limit: number = 10) : Array {

    if(this.collection.hasOwnProperty(value)) {
      return [value];
    } else {
      var res = this.getValuesCloseToValue(value);
      if(res.length > limit) {
        res = res.slice(0,limit);
      }
      return res;
    }

  }

	/** --------------------------------------------------------------
   *    Get list of values which can be close by match to value
   *  --------------------------------------------------------------
   *
   * @param value
   */
  public getValuesCloseToValue(value: string) : Array {

    var keys: Array = Object.keys(this.collection);

    var operable = true;
    var ops = 0;

    var compare = value;

    while (operable) {

      var s: Array = this.searchAndCompare(keys, compare);
      if(s.length > 0)
        return s;

      compare = value.slice(0,-1);
      if(ops++ > 3) break;

    }

    return [];

  }

	/** --------------------------------------------------------------
   *                 Get all data which is matching
   *  --------------------------------------------------------------
   *
   * @param array
   * @param value
   * @returns {Array}
   */
  private searchAndCompare(array: Array, value: string) : Array {

    var b: Array = array.filter((val) => {
      return val.indexOf(value) == 0;
    });

    console.log("FILTERING RESULTS FOR -> "+value);
    console.log(b);

    return b;

  }

	/** --------------------------------------------------------------
   *                  Fill data from array
   *  --------------------------------------------------------------
   *
   * @param data
   */
  public fromArray(data: Array<any>) {

    if(++this.currentBlock > 8) {
      this.currentBlock = 1;
      this.cleaning = true;
    }

    if(this.cleaning)
      this.cleanData();

    for (var i = 0; i < data.length; i++) {
      this.collection[data[i]] = this.currentBlock;
      if(i > this.maxBlockCapacity)
        break;
    }

  }

	/** --------------------------------------------------------------
   *              Replenish free slots in collection
   *  --------------------------------------------------------------
   */
  private cleanData() {

    for(var key in this.collection) {

      if(this.collection[key] == this.currentBlock){
        delete this.collection[key];
      }

    }

  }

}
