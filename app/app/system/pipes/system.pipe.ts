import {Pipe, PipeTransform} from "@angular/core";
/** ------
 * 
 */
@Pipe({
  name:'arrayReverse',
  pure: false
})
export class ArrayReversePipe implements PipeTransform {
	transform(value:Array):any {
    var a = value.slice();
    return a.reverse();
  }
}
