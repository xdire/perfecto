import {Pipe, PipeTransform} from "@angular/core";
/** ------
 *
 */
@Pipe({
  name:'colorizeWHIObject',
  pure: false
})
export class WHIObjectToColorizedString implements PipeTransform {

  transform(object:Object):string {

    var str = "";

    var c: any = {}
    c.r = 77;
    c.g = 191;
    c.b = 162;

    c.t = '#FFFFFF';
    //var keys = Object.keys(object);

    if(object.hasOwnProperty("style")) {
      str += '<span class="whs-iteme-extraspan" '
        +'style="background-color: rgb(' + c.r + ',' + c.g + ',' + c.b + '); color:'+c.t+'">'
        + object['style']
        + '</span>';
      this.colorChange(c);
    }

    if(object.hasOwnProperty("size")) {
      str += '<span class="whs-iteme-extraspan" '
        +'style="background-color: rgb(' + c.r + ',' + c.g + ',' + c.b + '); color:'+c.t+'">'
        + object['size']
        + '</span>';
      this.colorChange(c);
    }

    if(object.hasOwnProperty("color")) {
      str += '<span class="whs-iteme-extraspan" '
        +'style="background-color: rgb(' + c.r + ',' + c.g + ',' + c.b + '); color:'+c.t+'">'
        + object['color']
        + '</span>';
      this.colorChange(c);
    }

    for(var key in object) {

      if(key != "image" && key != "style" && key != "size" && key != "color") {

        str += '<span class="whs-iteme-extraspan" '
          +'style="background-color: rgb(' + c.r + ',' + c.g + ',' + c.b + '); color:'+c.t+'">'
          + object[key]
          + '</span>';

        this.colorChange(c);

      }

    }

    return str;

  }

  colorChange(c) {

    if(c.r > 25)
      c.r -= 25;
    if(c.g > 32)
      c.g -= 32;
    if(c.b > 25)
      c.b -= 25;

    if(((c.r+c.g+c.b)/3) < 170)
      c.t = '#FFFFFF';

  }

}
