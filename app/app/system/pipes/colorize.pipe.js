"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
/** ------
 *
 */
var WHIObjectToColorizedString = (function () {
    function WHIObjectToColorizedString() {
    }
    WHIObjectToColorizedString.prototype.transform = function (object) {
        var str = "";
        var c = {};
        c.r = 77;
        c.g = 191;
        c.b = 162;
        c.t = '#FFFFFF';
        //var keys = Object.keys(object);
        if (object.hasOwnProperty("style")) {
            str += '<span class="whs-iteme-extraspan" '
                + 'style="background-color: rgb(' + c.r + ',' + c.g + ',' + c.b + '); color:' + c.t + '">'
                + object['style']
                + '</span>';
            this.colorChange(c);
        }
        if (object.hasOwnProperty("size")) {
            str += '<span class="whs-iteme-extraspan" '
                + 'style="background-color: rgb(' + c.r + ',' + c.g + ',' + c.b + '); color:' + c.t + '">'
                + object['size']
                + '</span>';
            this.colorChange(c);
        }
        if (object.hasOwnProperty("color")) {
            str += '<span class="whs-iteme-extraspan" '
                + 'style="background-color: rgb(' + c.r + ',' + c.g + ',' + c.b + '); color:' + c.t + '">'
                + object['color']
                + '</span>';
            this.colorChange(c);
        }
        for (var key in object) {
            if (key != "image" && key != "style" && key != "size" && key != "color") {
                str += '<span class="whs-iteme-extraspan" '
                    + 'style="background-color: rgb(' + c.r + ',' + c.g + ',' + c.b + '); color:' + c.t + '">'
                    + object[key]
                    + '</span>';
                this.colorChange(c);
            }
        }
        return str;
    };
    WHIObjectToColorizedString.prototype.colorChange = function (c) {
        if (c.r > 25)
            c.r -= 25;
        if (c.g > 32)
            c.g -= 32;
        if (c.b > 25)
            c.b -= 25;
        if (((c.r + c.g + c.b) / 3) < 170)
            c.t = '#FFFFFF';
    };
    WHIObjectToColorizedString = __decorate([
        core_1.Pipe({
            name: 'colorizeWHIObject',
            pure: false
        }), 
        __metadata('design:paramtypes', [])
    ], WHIObjectToColorizedString);
    return WHIObjectToColorizedString;
}());
exports.WHIObjectToColorizedString = WHIObjectToColorizedString;
//# sourceMappingURL=colorize.pipe.js.map