export interface ApiRelationLinkInterface {
  host: string;
  port: number;
}

export interface SystemConfig {
  systemApiList: {[apiName: string] : ApiRelationLinkInterface};
  systemApiKeyType: string;
}

export class ApiRelationLink implements ApiRelationLinkInterface {

  host:string;
  port:number;

  constructor(host:string, port:number) {
    this.host = host;
    this.port = port;
  }

}
