"use strict";
var WindowComponentSet = (function () {
    function WindowComponentSet() {
        this.set = {};
    }
    /**
   * Set window component for key
   *
   * @param key
   * @param component
   */
    WindowComponentSet.prototype.add = function (key, component) {
        this.set[key] = component;
    };
    /**
   * Get window component by key
   *
   * @param key
   * @returns {any}
   */
    WindowComponentSet.prototype.get = function (key) {
        if (this.set.hasOwnProperty(key)) {
            if (this.set[key] != null)
                return this.set[key];
        }
        return null;
    };
    WindowComponentSet.prototype.remove = function (key) {
        if (this.set.hasOwnProperty(key)) {
            this.set[key] = null;
        }
        return null;
    };
    return WindowComponentSet;
}());
exports.WindowComponentSet = WindowComponentSet;
//# sourceMappingURL=window.components.set.js.map