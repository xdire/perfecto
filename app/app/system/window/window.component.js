"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var core_1 = require("@angular/core");
var WindowComponent = (function (_super) {
    __extends(WindowComponent, _super);
    function WindowComponent() {
        this.componentRef = null;
        this.componentZone = null;
        this.hostComponent = null;
        this.isLoaded = false;
        this.isRoot = true;
        this.isCacheable = true;
        this.isSideModal = false;
        this.isOverlayModal = false;
        this.isWithMenu = true;
        this.isWithBar = true;
        this.publicName = "componentName";
    }
    WindowComponent.prototype.ngOnInit = function () { this.componentZone = new core_1.NgZone(true); this.isLoaded = true; };
    WindowComponent.prototype.onLoad = function () { };
    ;
    WindowComponent.prototype.onCreate = function () { };
    ;
    WindowComponent.prototype.onFocus = function () { };
    ;
    WindowComponent.prototype.onExit = function () { };
    ;
    WindowComponent.prototype.refreshView = function () {
        if (this.componentRef !== null && typeof this.componentRef !== 'undefined')
            this.componentRef.changeDetectorRef.detectChanges();
        if (this.componentZone !== null && typeof this.componentZone !== 'undefined')
            this.componentZone.run(function () { });
    };
    return WindowComponent;
}(core_1.Type));
exports.WindowComponent = WindowComponent;
//# sourceMappingURL=window.component.js.map