"use strict";
var WindowStack = (function () {
    function WindowStack() {
        this.size = 0;
        this.stack = new Array(32);
    }
    /** ----------------------------------------
   *
   *         Enqueue Component to Stack
   *
   *  ----------------------------------------
   * @param component
   */
    WindowStack.prototype.enqueue = function (component) {
        this.stack[this.size++] = component;
    };
    /** ----------------------------------------
   *
   *          Dequeue Component from Stack
   *
   *  ----------------------------------------
   *
   * @returns {any}
   */
    WindowStack.prototype.dequeue = function () {
        if (this.size > 0) {
            var v = this.stack[--this.size];
            this.stack[this.size] = null;
            return v;
        }
        return null;
    };
    /** ----------------------------------------
   *
   *          Get Current Active Component
   *
   *  ----------------------------------------
   *
   * @returns {any}
   */
    WindowStack.prototype.current = function () {
        if (this.size > 0) {
            return this.stack[(this.size - 1)];
        }
        return null;
    };
    /**
   *
   * @returns {Array<ComponentRef>}
   */
    WindowStack.prototype.getAll = function () {
        return this.stack;
    };
    return WindowStack;
}());
exports.WindowStack = WindowStack;
//# sourceMappingURL=window.stack.js.map