import {WindowComponent} from "./window.component";
export class WindowComponentSet {

  private set: {[name: string]: WindowComponent};

  constructor() {
      this.set = {};
  }

	/**
   * Set window component for key
   *
   * @param key
   * @param component
   */
  public add(key: string, component: WindowComponent) {
      this.set[key] = component;
  }

	/**
   * Get window component by key
   *
   * @param key
   * @returns {any}
   */
  public get(key: string) : WindowComponent {

    if(this.set.hasOwnProperty(key)) {
      if(this.set[key] != null)
      return this.set[key];
    }
    return null;

  }

  public remove(key: string) {

    if(this.set.hasOwnProperty(key)){
      this.set[key] = null;
    }
    return null;

  }

}
