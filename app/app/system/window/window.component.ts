import {Type, NgZone, ElementRef, ComponentRef} from "@angular/core";

export class WindowComponent extends Type implements WindowComponentInterface {

  public componentRef: ComponentRef = null;

  public componentZone: NgZone = null;

  public hostComponent: ElementRef = null;

  ngOnInit() {this.componentZone = new NgZone(true); this.isLoaded = true;}

  constructor() {

  }

  isLoaded: boolean = false;

  isRoot: boolean = true;

  isCacheable: boolean = true;

  isSideModal: boolean = false;

  isOverlayModal: boolean = false;

  isWithMenu: boolean = true;

  isWithBar: boolean = true;

  publicName: string = "componentName";

  public onLoad(){};

  public onCreate(){};

  public onFocus(){};

  public onExit(){};

  public refreshView() {
    if(this.componentRef !== null && typeof this.componentRef !== 'undefined')
      this.componentRef.changeDetectorRef.detectChanges();
    if(this.componentZone !== null && typeof this.componentZone !== 'undefined')
      this.componentZone.run(()=>{});
  }

}

export interface WindowComponentInterface {

  isRoot: boolean;

  isSideModal: boolean;

  isWithMenu: boolean;

  publicName: string;

  onCreate();

  onFocus();

  onExit();

}
