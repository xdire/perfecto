import {ComponentRef} from "@angular/core";
export class WindowStack {

  private stack: Array<ComponentRef>;

  private size: number = 0;

  constructor() {
    this.stack = new Array(32);
  }

	/** ----------------------------------------
   *
   *         Enqueue Component to Stack
   *
   *  ----------------------------------------
   * @param component
   */
  public enqueue(component: ComponentRef) {

    this.stack[this.size++] = component;

  }

	/** ----------------------------------------
   *
   *          Dequeue Component from Stack
   *
   *  ----------------------------------------
   *
   * @returns {any}
   */
  public dequeue() : ComponentRef {

    if(this.size > 0) {

      var v = this.stack[--this.size];
      this.stack[this.size] = null;
      return v;

    }

    return null;

  }

	/** ----------------------------------------
   *
   *          Get Current Active Component
   *
   *  ----------------------------------------
   *
   * @returns {any}
   */
  public current() : ComponentRef {

    if(this.size > 0) {

      return this.stack[(this.size-1)];

    }

    return null;

  }

	/**
   * 
   * @returns {Array<ComponentRef>}
   */
  public getAll() : Array<ComponentRef> {
    return this.stack;
  }

}
