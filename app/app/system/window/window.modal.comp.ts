
import {Component, Input, ComponentRef,ViewContainerRef, ViewChild} from "@angular/core";
import {WindowComponent} from "./window.component";
import {App} from "../app";
import {WindowModalFloatParameters, ComponentImplementingModalFramePass} from "../entities/system.entities";

@Component({
  selector: 'window-float',
  template: `
     <div class="title-bar"> 
       <div class="title-bar-bar">
          {{title}} 
          <div class="close-btn">
          <i class="fa fa-close" (click)="close()"></i>
          </div>
          <div style="clear: both; width: 100%; height: 1px;"></div>
       </div>
     </div>
     <div class="content-space">
      <div #wflc></div>
     </div>
  `,
  host: {
    'class':'windowFloat',
    '[style.left]':'left',
    '[style.top]':'top',
    '[style.width]':'width',
    '[style.height]':'height'
  },
  styles: [
    `
      .title-bar {width: 100%; height: 34px; font-size: 20px; padding-left: 10px; margin-bottom: 4px;}
      .title-bar-bar {
        position: absolute;
        width: calc(100% + 4px);
        font-size: 0.9em;
        min-height: 44px;
        margin: -15px;
        padding: 15px 5px 0 15px;
        color: #333333;
        font-weight: 100;
        /*background: #16222A;
        background: -webkit-linear-gradient(to left, #16222A , #3A6073);
        background: linear-gradient(to left, #16222A , #3A6073);*/
        background: #eeeeee;
        background: -moz-linear-gradient(top, #eeeeee 0%, #cccccc 100%);
        background: -webkit-linear-gradient(top, #eeeeee 0%,#cccccc 100%);
        background: linear-gradient(to bottom, #eeeeee 0%,#cccccc 100%);
        filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#eeeeee', endColorstr='#cccccc',GradientType=0 );
      }
      .content-space {position: relative; width: 100%; height: calc(100% - 36px); overflow-y: scroll; overflow-x: hidden;}
      .close-btn {float: right; font-size: 22px; padding-right: 4px; cursor: pointer;}
    `
  ]
})

export class WindowFloatModalFrame extends WindowComponent {

  @ViewChild("wflc",{read: ViewContainerRef}) private contentContainer: ViewContainerRef;

  @Input() title = "";

  @Input() width = 100;

  @Input() height = 100;

  @Input() top = 50;

  @Input() left = 50;

  public proportionOfMainWidth = 90;

  public proportionOfMainHeight = 90;

  public self: ComponentRef;

  private loadedComponent: ComponentRef;

  private closeCB: any;

  constructor() {
    super.constructor();
    this.isRoot = false;
    this.isSideModal = false;
    this.isOverlayModal = true;
  }

  onFocus () {

    if(typeof this.loadedComponent !== "undefined"){
      if(typeof this.loadedComponent.instance.onFocus === "function"){
        this.loadedComponent.instance.onFocus();
      }
    }

  }
  /** ----------------------------------------------------------------------
   *
   *
   *
   *  ----------------------------------------------------------------------
   *
   *
   *  @param loadComponent any | ComponentImplementingModalFramePass :
   *  Load component parameter need to be component implementing
   *  ComponentImplementingModalFramePass interface for properly set
   *  callback methods which gives ability for parent to interact with
   *  the modal frame loaded component
   *
   *  @param parameters WindowModalFloatParameters
   */
  public load(loadComponent: any, parameters: WindowModalFloatParameters, callback) {

    this.setup(parameters);

    if(typeof this.contentContainer === 'undefined' || this.contentContainer === null) {
      this.self.changeDetectorRef.detectChanges();
    }

    this.loadNext(loadComponent, parameters, callback);

  }

	/** ----------------------------------------------------------------------
   *
   *
   *
   *  ----------------------------------------------------------------------
   *
   *  @param loadComponent
   *  @param parameters
   */
  public loadNext(loadComponent: any, parameters: WindowModalFloatParameters, callback) {

    App.window.loadComponentIntoLocation(loadComponent, this.contentContainer, (ref) => {

      this.loadedComponent = ref;

      var host = ref.instance;

      var h = <ComponentImplementingModalFramePass>host;

      h.dataObject = parameters.data;

      h.closingMethod = (data?) => {this.close(data)};

      h.successCallback = parameters.onSuccess;

      h.failureCallback = parameters.onFailure;

      this.closeCB = parameters.onClose;

      var hw = <WindowComponent>h;

      if(typeof hw.componentRef !== "undefined"){
        hw.componentRef = ref;
      }

      parameters.getReference(ref.instance);

      if(typeof callback === "function") callback();

      if(typeof hw.onLoad !== "undefined" && typeof hw.onLoad === "function"){
        hw.onLoad();
      }

    });

  }

	/** ----------------------------------------------------------------------
   *
   *
   *
   *  ----------------------------------------------------------------------
   *
   * @param parameters
   */
  public setup(parameters: WindowModalFloatParameters){
    this.title = parameters.title;
  }

	/** ----------------------------------------------------------------------
   *
   *                      Close window lifecycle action
   *
   *  ----------------------------------------------------------------------
   *
   */
  public close(data?) {

    if(typeof data !== "undefined") {
      if (data.hasOwnProperty("reset")) {
        if (data.reset.hasOwnProperty("listeners")) {
          App.inputScannerObserver.removeListenerWithGUID(data.reset.listeners);
        }
      }
    }

    var component = this.loadedComponent.instance;

    if(typeof component.onClose === "function") {
      component.onClose();
    }

    this.loadedComponent.destroy();
    this.closeCB(data);
    this.refreshView();
    App.window.closeCurrentWindow();

  }

	/** ----------------------------------------------------------------------
   *
   *
   *
   *  ----------------------------------------------------------------------
   *
   */
  public onExit() {

    this.loadedComponent.destroy();

  }

	/** ----------------------------------------------------------------------
   *
   *
   *
   *  ----------------------------------------------------------------------
   *
   *  @param width
   *  @param height
   */
  public adjustSize(width: number, height: number) {

    var nWidth: number = width/100*this.proportionOfMainWidth;

    var nHeight: number = height/100*this.proportionOfMainHeight;

    var marginTop: number = (height - nHeight) / 2;

    var marginLeft: number = (width - nWidth) / 2;

    this.top = marginTop;

    this.left = marginLeft;

    this.height = nHeight;

    this.width = nWidth;

    this.refreshView();

  }

}
