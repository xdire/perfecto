import {
  Component, ElementRef, DynamicComponentLoader, ComponentRef, ViewContainerRef,
  ComponentResolver, ViewChild
} from '@angular/core';

import {WindowComponentSet} from "./window.components.set";
import {WindowStack} from "./window.stack";
import {WindowComponent} from "./window.component";
import {MenuComponent} from "../menu/menu.component";
import {App} from "../app";
import {BarComponent} from "../menu/bar.component";
import {WindowFloatModalFrame} from "./window.modal.comp";
import {WindowModalFloatParameters} from "../entities/system.entities";

@Component({
  selector: 'window',
  template: `
     <div #windowframe></div>
  `,
})

export class WindowDispatcher {

  @ViewChild("windowframe",{ read: ViewContainerRef }) contentContainer: ViewContainerRef;

  private width: number;

  private height: number;

  private self: ElementRef;

  private selfStyle: any;

  private currentComponent: ComponentRef;

  private windowComponents: WindowComponentSet;

  private windowStack: WindowStack;

  private barComponent: BarComponent;

  private barFrame: ComponentRef;

  private barStyle: any;

  private _menuComponent: MenuComponent;

  private menuFrame: ComponentRef;

  private menuStyle: any;

  private dispatcherModalLock: boolean = false;

	/** -----------------------------------------------------------
   *
   *                    Initialize with class name
   *
   *  -----------------------------------------------------------
   */
  ngOnInit() {

    // Window set size
    this.width = window.innerWidth;
    this.height = window.innerHeight;

    // Set resize events
    window.onresize = () => {
      this.width = window.innerWidth;
      this.height = window.innerHeight;
      this.updateWindowSize();
      this.updateChildSize();
    };

    // Set defaults for window Component
    this.self.nativeElement.className = "windowDispatcher bgDarkBlue";
    this.selfStyle = this.self.nativeElement.style;

    this.loadComponent(BarComponent, (ref) => {
      this.barComponent = ref.instance;
      this.barFrame = ref;
      this.barStyle = ref.location.nativeElement.style;
      this.barComponent.height = parseInt(window.getComputedStyle(ref.location.nativeElement,null).getPropertyValue("height"));
    });

    this.loadComponent(MenuComponent, (ref) => {
      this._menuComponent = ref.instance;
      this.menuFrame = ref;
      this.menuStyle = ref.location.nativeElement.style;
      this._menuComponent.width = parseInt(window.getComputedStyle(ref.location.nativeElement,null).getPropertyValue("width"));
    });

  }

  public loadComponent(component: any, callback) : void {
    this.resolver.resolveComponent(component).then(cf => {
      let ref = this.contentContainer.createComponent(cf);
      callback(ref);
    })
  }

  updateWindowSize() {

    this.selfStyle.width = this.width;
    this.selfStyle.height = this.height;
    this.menuStyle.height = this.height;
    this._menuComponent.setSize({width:this._menuComponent.width, height: this.height});

  }

  updateChildSize() {

    var totalHeight: number = this.height;

    if(this.barComponent.isVisible) {
      totalHeight = totalHeight - this.barComponent.height;
    }

    var recWidth = 0;
    var recHeight = 0;

    if(this._menuComponent.isVisible) {

      recWidth = this.width - (this._menuComponent.width);
      recHeight = totalHeight;
      this.menuStyle.height = totalHeight;

    } else {

      recWidth = this.width;
      recHeight = totalHeight;

    }

    this.windowStack.getAll().map((elem) => {

      if(elem != null) {

        if (elem.instance.isRoot) {

          var s = elem.location.nativeElement.style;
          s.width = recWidth;
          s.height = recHeight;

        } else if (elem.instance.isOverlayModal) {

          elem.instance.adjustSize(this.width, this.height);

        }
        
      }

    });

  }

  /** -----------------------------------------------------------
   *
   *    Can be constructed only through Component Bootstrap
   *
   *  -----------------------------------------------------------
   *
   * @param loader
   * @param self
   * @param view
   * @param resolver
	 */
  constructor(private loader: DynamicComponentLoader,
              private self: ElementRef,
              private view: ViewContainerRef,
              private resolver: ComponentResolver) {
    this.windowComponents = new WindowComponentSet;
    this.windowStack = new WindowStack;

  }

	/** -----------------------------------------------------------
   *
   *                    Register multiple components
   *
   *  -----------------------------------------------------------
   *
   * @param components
   */
  public registerComponents(components:{[name:string]: WindowComponent}) {
    for(var k in components){
      this.windowComponents.add(k,components[k]);
    }
  }

  /** -----------------------------------------------------------
   *
   *                      Register window component
   *
   *  -----------------------------------------------------------
   *
   * @param name
   * @param component
   */
  public registerComponent(name: string, component: WindowComponent) {
      this.windowComponents.add(name,component);
  }

	/** -----------------------------------------------------------
   *
   *                Drop window component if one stored
   *
   *  -----------------------------------------------------------
   *
   * @param name
   */
  public unregisterComponent(name: string) {
      this.windowComponents.remove(name);
  }

	/** -----------------------------------------------------------
	 *
   *                  Load component from set
   *
   *  -----------------------------------------------------------
   *
   * @param component
   */
  public loadWindowComponent(component: string) {

    var comp: WindowComponent = null;
    var isRoot: boolean = false;

    if(comp = this.windowComponents.get(component)) {

      this.resolver.resolveComponent(comp).then(cf => {

        let ref = this.contentContainer.createComponent(cf);

        var host: WindowComponent = ref.instance;
        var elem: any = ref.location.nativeElement;

        // If it's an element declraed as Root Component element
        if(host.isRoot === true) {

          elem.className = "windowFrame";

          // Execute disposing of current opened elements
          this.closeOpenWindowStack();

          // Enqueue new window to window stack
          this.windowStack.enqueue(ref);

          // Set current component
          this.currentComponent = ref;

          // ----------------------------------------
          // Set up dimensions
          // ----------------------------------------
          var totalHeight: number = this.height;
          var windowWidth: number = this.width;

          // ----------------------------------------
          // Set up bar dimensions
          // ----------------------------------------
          if(host.isWithBar) {

            totalHeight = totalHeight-64;
            this.barComponent.show();

          } else {

            this.barComponent.hide();
            this.menuStyle.height = this.height;

          }

          // ----------------------------------------
          // Set up menu dimensions
          // ----------------------------------------
          if(host.isWithMenu) {

            App.state.__setCurrentUserCredentials();

            this.menuStyle.width = 250;
            windowWidth = windowWidth - 250;

            this._menuComponent.setSize({width: 250, height: totalHeight});
            this._menuComponent.show();
            this._menuComponent.selectMenyItemWithWindowComponent(component);

          }
          // If Component declared with isWithMenu = false
          else {

            this._menuComponent.hide();

          }

          elem.style.height = totalHeight;
          elem.style.width = windowWidth;

          isRoot = true;

          host.hostComponent = ref.location;

        }
        else if(host.isOverlayModal){

          elem.className = "windowFrameModal";  // Add this to styles

        }
        // If it's an element declared as Modal Component element
        else {

          this.currentComponent = ref;

        }

        // Do finishing actions if component was Root Element
        if(isRoot) {
          // Set last loaded component to cache
          if(host.isCacheable)
            App.state.__setLastLoadedComponent(component);
          history.pushState({}, null, host.publicName);
        }

        // Execute Methods on start
        host.onCreate();
        host.onFocus();

      });

    }

  }

  /** -----------------------------------------------------------
   *
   *              Load Component Into Some Position
   *
   *  -----------------------------------------------------------
   *
   * @param component any | WindowComponent
   * @param container ViewContainerRef
   * @param callback(ComponentRef)
   */
  public loadComponentIntoLocation(component: any, container: ViewContainerRef, callback) {

    this.resolver.resolveComponent(component).then((cf) => {
      var ref = container.createComponent(cf);
      callback(ref);
    });

  }

  /** -----------------------------------------------------------
   *
   *              Load component Into Floating Modal
   *
   *  -----------------------------------------------------------
   *
   * @param component
   * @param parameters
   */
  public loadComponentIntoModalFrame(component: any, parameters: WindowModalFloatParameters) {

    if(this.dispatcherModalLock) return;

    this.dispatcherModalLock = true;

    this.resolver.resolveComponent(WindowFloatModalFrame).then((cf) => {

      var ref = this.contentContainer.createComponent(cf);
      var host: WindowFloatModalFrame = ref.instance;
      host.self = ref;

      // Enqueue new window to window stack
      this.windowStack.enqueue(ref);

      host.adjustSize(this.width, this.height);
      // Execute Methods on start
      host.load(component, parameters, () => {this.dispatcherModalLock = false;});
      host.onFocus();

      this.currentComponent = ref;

    });

  }

  /** -----------------------------------------------------------
   *
   *                      Destroy opened window
   *              and call onFocus on prev window on
   *                          Window Stack
   *
   *  -----------------------------------------------------------
   */
  public closeCurrentWindow() {

    var w:ComponentRef;

    if(w = this.windowStack.dequeue()){

      w.instance.onExit();
      w.destroy();

      var c:ComponentRef;

      if(c = this.windowStack.current()){

        console.log("CURRENT");
        console.log(c);
        c.instance.onFocus();
        this.currentComponent = c;

      }

    }

  }

	/** -----------------------------------------------------------
   *
   *                  Destroy opened window stack
   *              and define new empty currentComponent
   *
   *  -----------------------------------------------------------
   */
  private closeOpenWindowStack() {

    var w:ComponentRef;

    while (w = this.windowStack.dequeue()) {

      w.instance.onExit();
      w.destroy();

    }

    this.currentComponent = null;

  }

	/** -----------------------------------------------------------
   *
   *                Get Menu Component for
   *            access to inner setters and options
   *
   *  -----------------------------------------------------------
   *
   * @returns {MenuComponent}
   */
	get menuComponent():MenuComponent {
    return this._menuComponent;
  }
}

export interface WindowSizeEntity {
  height: number;
  width: number;
}

export enum WindowSizeType {
  Attached,
  Fixed
}
