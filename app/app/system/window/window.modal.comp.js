"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var window_component_1 = require("./window.component");
var app_1 = require("../app");
var WindowFloatModalFrame = (function (_super) {
    __extends(WindowFloatModalFrame, _super);
    function WindowFloatModalFrame() {
        this.title = "";
        this.width = 100;
        this.height = 100;
        this.top = 50;
        this.left = 50;
        this.proportionOfMainWidth = 90;
        this.proportionOfMainHeight = 90;
        _super.prototype.constructor.call(this);
        this.isRoot = false;
        this.isSideModal = false;
        this.isOverlayModal = true;
    }
    WindowFloatModalFrame.prototype.onFocus = function () {
        if (typeof this.loadedComponent !== "undefined") {
            if (typeof this.loadedComponent.instance.onFocus === "function") {
                this.loadedComponent.instance.onFocus();
            }
        }
    };
    /** ----------------------------------------------------------------------
     *
     *
     *
     *  ----------------------------------------------------------------------
     *
     *
     *  @param loadComponent any | ComponentImplementingModalFramePass :
     *  Load component parameter need to be component implementing
     *  ComponentImplementingModalFramePass interface for properly set
     *  callback methods which gives ability for parent to interact with
     *  the modal frame loaded component
     *
     *  @param parameters WindowModalFloatParameters
     */
    WindowFloatModalFrame.prototype.load = function (loadComponent, parameters, callback) {
        this.setup(parameters);
        if (typeof this.contentContainer === 'undefined' || this.contentContainer === null) {
            this.self.changeDetectorRef.detectChanges();
        }
        this.loadNext(loadComponent, parameters, callback);
    };
    /** ----------------------------------------------------------------------
   *
   *
   *
   *  ----------------------------------------------------------------------
   *
   *  @param loadComponent
   *  @param parameters
   */
    WindowFloatModalFrame.prototype.loadNext = function (loadComponent, parameters, callback) {
        var _this = this;
        app_1.App.window.loadComponentIntoLocation(loadComponent, this.contentContainer, function (ref) {
            _this.loadedComponent = ref;
            var host = ref.instance;
            var h = host;
            h.dataObject = parameters.data;
            h.closingMethod = function (data) { _this.close(data); };
            h.successCallback = parameters.onSuccess;
            h.failureCallback = parameters.onFailure;
            _this.closeCB = parameters.onClose;
            var hw = h;
            if (typeof hw.componentRef !== "undefined") {
                hw.componentRef = ref;
            }
            parameters.getReference(ref.instance);
            if (typeof callback === "function")
                callback();
            if (typeof hw.onLoad !== "undefined" && typeof hw.onLoad === "function") {
                hw.onLoad();
            }
        });
    };
    /** ----------------------------------------------------------------------
   *
   *
   *
   *  ----------------------------------------------------------------------
   *
   * @param parameters
   */
    WindowFloatModalFrame.prototype.setup = function (parameters) {
        this.title = parameters.title;
    };
    /** ----------------------------------------------------------------------
   *
   *                      Close window lifecycle action
   *
   *  ----------------------------------------------------------------------
   *
   */
    WindowFloatModalFrame.prototype.close = function (data) {
        if (typeof data !== "undefined") {
            if (data.hasOwnProperty("reset")) {
                if (data.reset.hasOwnProperty("listeners")) {
                    app_1.App.inputScannerObserver.removeListenerWithGUID(data.reset.listeners);
                }
            }
        }
        var component = this.loadedComponent.instance;
        if (typeof component.onClose === "function") {
            component.onClose();
        }
        this.loadedComponent.destroy();
        this.closeCB(data);
        this.refreshView();
        app_1.App.window.closeCurrentWindow();
    };
    /** ----------------------------------------------------------------------
   *
   *
   *
   *  ----------------------------------------------------------------------
   *
   */
    WindowFloatModalFrame.prototype.onExit = function () {
        this.loadedComponent.destroy();
    };
    /** ----------------------------------------------------------------------
   *
   *
   *
   *  ----------------------------------------------------------------------
   *
   *  @param width
   *  @param height
   */
    WindowFloatModalFrame.prototype.adjustSize = function (width, height) {
        var nWidth = width / 100 * this.proportionOfMainWidth;
        var nHeight = height / 100 * this.proportionOfMainHeight;
        var marginTop = (height - nHeight) / 2;
        var marginLeft = (width - nWidth) / 2;
        this.top = marginTop;
        this.left = marginLeft;
        this.height = nHeight;
        this.width = nWidth;
        this.refreshView();
    };
    __decorate([
        core_1.ViewChild("wflc", { read: core_1.ViewContainerRef }), 
        __metadata('design:type', core_1.ViewContainerRef)
    ], WindowFloatModalFrame.prototype, "contentContainer", void 0);
    __decorate([
        core_1.Input(), 
        __metadata('design:type', Object)
    ], WindowFloatModalFrame.prototype, "title", void 0);
    __decorate([
        core_1.Input(), 
        __metadata('design:type', Object)
    ], WindowFloatModalFrame.prototype, "width", void 0);
    __decorate([
        core_1.Input(), 
        __metadata('design:type', Object)
    ], WindowFloatModalFrame.prototype, "height", void 0);
    __decorate([
        core_1.Input(), 
        __metadata('design:type', Object)
    ], WindowFloatModalFrame.prototype, "top", void 0);
    __decorate([
        core_1.Input(), 
        __metadata('design:type', Object)
    ], WindowFloatModalFrame.prototype, "left", void 0);
    WindowFloatModalFrame = __decorate([
        core_1.Component({
            selector: 'window-float',
            template: "\n     <div class=\"title-bar\"> \n       <div class=\"title-bar-bar\">\n          {{title}} \n          <div class=\"close-btn\">\n          <i class=\"fa fa-close\" (click)=\"close()\"></i>\n          </div>\n          <div style=\"clear: both; width: 100%; height: 1px;\"></div>\n       </div>\n     </div>\n     <div class=\"content-space\">\n      <div #wflc></div>\n     </div>\n  ",
            host: {
                'class': 'windowFloat',
                '[style.left]': 'left',
                '[style.top]': 'top',
                '[style.width]': 'width',
                '[style.height]': 'height'
            },
            styles: [
                "\n      .title-bar {width: 100%; height: 34px; font-size: 20px; padding-left: 10px; margin-bottom: 4px;}\n      .title-bar-bar {\n        position: absolute;\n        width: calc(100% + 4px);\n        font-size: 0.9em;\n        min-height: 44px;\n        margin: -15px;\n        padding: 15px 5px 0 15px;\n        color: #333333;\n        font-weight: 100;\n        /*background: #16222A;\n        background: -webkit-linear-gradient(to left, #16222A , #3A6073);\n        background: linear-gradient(to left, #16222A , #3A6073);*/\n        background: #eeeeee;\n        background: -moz-linear-gradient(top, #eeeeee 0%, #cccccc 100%);\n        background: -webkit-linear-gradient(top, #eeeeee 0%,#cccccc 100%);\n        background: linear-gradient(to bottom, #eeeeee 0%,#cccccc 100%);\n        filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#eeeeee', endColorstr='#cccccc',GradientType=0 );\n      }\n      .content-space {position: relative; width: 100%; height: calc(100% - 36px); overflow-y: scroll; overflow-x: hidden;}\n      .close-btn {float: right; font-size: 22px; padding-right: 4px; cursor: pointer;}\n    "
            ]
        }), 
        __metadata('design:paramtypes', [])
    ], WindowFloatModalFrame);
    return WindowFloatModalFrame;
}(window_component_1.WindowComponent));
exports.WindowFloatModalFrame = WindowFloatModalFrame;
//# sourceMappingURL=window.modal.comp.js.map