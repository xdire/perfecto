"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var window_components_set_1 = require("./window.components.set");
var window_stack_1 = require("./window.stack");
var menu_component_1 = require("../menu/menu.component");
var app_1 = require("../app");
var bar_component_1 = require("../menu/bar.component");
var window_modal_comp_1 = require("./window.modal.comp");
var WindowDispatcher = (function () {
    /** -----------------------------------------------------------
     *
     *    Can be constructed only through Component Bootstrap
     *
     *  -----------------------------------------------------------
     *
     * @param loader
     * @param self
     * @param view
     * @param resolver
       */
    function WindowDispatcher(loader, self, view, resolver) {
        this.loader = loader;
        this.self = self;
        this.view = view;
        this.resolver = resolver;
        this.dispatcherModalLock = false;
        this.windowComponents = new window_components_set_1.WindowComponentSet;
        this.windowStack = new window_stack_1.WindowStack;
    }
    /** -----------------------------------------------------------
   *
   *                    Initialize with class name
   *
   *  -----------------------------------------------------------
   */
    WindowDispatcher.prototype.ngOnInit = function () {
        var _this = this;
        // Window set size
        this.width = window.innerWidth;
        this.height = window.innerHeight;
        // Set resize events
        window.onresize = function () {
            _this.width = window.innerWidth;
            _this.height = window.innerHeight;
            _this.updateWindowSize();
            _this.updateChildSize();
        };
        // Set defaults for window Component
        this.self.nativeElement.className = "windowDispatcher bgDarkBlue";
        this.selfStyle = this.self.nativeElement.style;
        this.loadComponent(bar_component_1.BarComponent, function (ref) {
            _this.barComponent = ref.instance;
            _this.barFrame = ref;
            _this.barStyle = ref.location.nativeElement.style;
            _this.barComponent.height = parseInt(window.getComputedStyle(ref.location.nativeElement, null).getPropertyValue("height"));
        });
        this.loadComponent(menu_component_1.MenuComponent, function (ref) {
            _this._menuComponent = ref.instance;
            _this.menuFrame = ref;
            _this.menuStyle = ref.location.nativeElement.style;
            _this._menuComponent.width = parseInt(window.getComputedStyle(ref.location.nativeElement, null).getPropertyValue("width"));
        });
    };
    WindowDispatcher.prototype.loadComponent = function (component, callback) {
        var _this = this;
        this.resolver.resolveComponent(component).then(function (cf) {
            var ref = _this.contentContainer.createComponent(cf);
            callback(ref);
        });
    };
    WindowDispatcher.prototype.updateWindowSize = function () {
        this.selfStyle.width = this.width;
        this.selfStyle.height = this.height;
        this.menuStyle.height = this.height;
        this._menuComponent.setSize({ width: this._menuComponent.width, height: this.height });
    };
    WindowDispatcher.prototype.updateChildSize = function () {
        var _this = this;
        var totalHeight = this.height;
        if (this.barComponent.isVisible) {
            totalHeight = totalHeight - this.barComponent.height;
        }
        var recWidth = 0;
        var recHeight = 0;
        if (this._menuComponent.isVisible) {
            recWidth = this.width - (this._menuComponent.width);
            recHeight = totalHeight;
            this.menuStyle.height = totalHeight;
        }
        else {
            recWidth = this.width;
            recHeight = totalHeight;
        }
        this.windowStack.getAll().map(function (elem) {
            if (elem != null) {
                if (elem.instance.isRoot) {
                    var s = elem.location.nativeElement.style;
                    s.width = recWidth;
                    s.height = recHeight;
                }
                else if (elem.instance.isOverlayModal) {
                    elem.instance.adjustSize(_this.width, _this.height);
                }
            }
        });
    };
    /** -----------------------------------------------------------
   *
   *                    Register multiple components
   *
   *  -----------------------------------------------------------
   *
   * @param components
   */
    WindowDispatcher.prototype.registerComponents = function (components) {
        for (var k in components) {
            this.windowComponents.add(k, components[k]);
        }
    };
    /** -----------------------------------------------------------
     *
     *                      Register window component
     *
     *  -----------------------------------------------------------
     *
     * @param name
     * @param component
     */
    WindowDispatcher.prototype.registerComponent = function (name, component) {
        this.windowComponents.add(name, component);
    };
    /** -----------------------------------------------------------
   *
   *                Drop window component if one stored
   *
   *  -----------------------------------------------------------
   *
   * @param name
   */
    WindowDispatcher.prototype.unregisterComponent = function (name) {
        this.windowComponents.remove(name);
    };
    /** -----------------------------------------------------------
     *
   *                  Load component from set
   *
   *  -----------------------------------------------------------
   *
   * @param component
   */
    WindowDispatcher.prototype.loadWindowComponent = function (component) {
        var _this = this;
        var comp = null;
        var isRoot = false;
        if (comp = this.windowComponents.get(component)) {
            this.resolver.resolveComponent(comp).then(function (cf) {
                var ref = _this.contentContainer.createComponent(cf);
                var host = ref.instance;
                var elem = ref.location.nativeElement;
                // If it's an element declraed as Root Component element
                if (host.isRoot === true) {
                    elem.className = "windowFrame";
                    // Execute disposing of current opened elements
                    _this.closeOpenWindowStack();
                    // Enqueue new window to window stack
                    _this.windowStack.enqueue(ref);
                    // Set current component
                    _this.currentComponent = ref;
                    // ----------------------------------------
                    // Set up dimensions
                    // ----------------------------------------
                    var totalHeight = _this.height;
                    var windowWidth = _this.width;
                    // ----------------------------------------
                    // Set up bar dimensions
                    // ----------------------------------------
                    if (host.isWithBar) {
                        totalHeight = totalHeight - 64;
                        _this.barComponent.show();
                    }
                    else {
                        _this.barComponent.hide();
                        _this.menuStyle.height = _this.height;
                    }
                    // ----------------------------------------
                    // Set up menu dimensions
                    // ----------------------------------------
                    if (host.isWithMenu) {
                        app_1.App.state.__setCurrentUserCredentials();
                        _this.menuStyle.width = 250;
                        windowWidth = windowWidth - 250;
                        _this._menuComponent.setSize({ width: 250, height: totalHeight });
                        _this._menuComponent.show();
                        _this._menuComponent.selectMenyItemWithWindowComponent(component);
                    }
                    else {
                        _this._menuComponent.hide();
                    }
                    elem.style.height = totalHeight;
                    elem.style.width = windowWidth;
                    isRoot = true;
                    host.hostComponent = ref.location;
                }
                else if (host.isOverlayModal) {
                    elem.className = "windowFrameModal"; // Add this to styles
                }
                else {
                    _this.currentComponent = ref;
                }
                // Do finishing actions if component was Root Element
                if (isRoot) {
                    // Set last loaded component to cache
                    if (host.isCacheable)
                        app_1.App.state.__setLastLoadedComponent(component);
                    history.pushState({}, null, host.publicName);
                }
                // Execute Methods on start
                host.onCreate();
                host.onFocus();
            });
        }
    };
    /** -----------------------------------------------------------
     *
     *              Load Component Into Some Position
     *
     *  -----------------------------------------------------------
     *
     * @param component any | WindowComponent
     * @param container ViewContainerRef
     * @param callback(ComponentRef)
     */
    WindowDispatcher.prototype.loadComponentIntoLocation = function (component, container, callback) {
        this.resolver.resolveComponent(component).then(function (cf) {
            var ref = container.createComponent(cf);
            callback(ref);
        });
    };
    /** -----------------------------------------------------------
     *
     *              Load component Into Floating Modal
     *
     *  -----------------------------------------------------------
     *
     * @param component
     * @param parameters
     */
    WindowDispatcher.prototype.loadComponentIntoModalFrame = function (component, parameters) {
        var _this = this;
        if (this.dispatcherModalLock)
            return;
        this.dispatcherModalLock = true;
        this.resolver.resolveComponent(window_modal_comp_1.WindowFloatModalFrame).then(function (cf) {
            var ref = _this.contentContainer.createComponent(cf);
            var host = ref.instance;
            host.self = ref;
            // Enqueue new window to window stack
            _this.windowStack.enqueue(ref);
            host.adjustSize(_this.width, _this.height);
            // Execute Methods on start
            host.load(component, parameters, function () { _this.dispatcherModalLock = false; });
            host.onFocus();
            _this.currentComponent = ref;
        });
    };
    /** -----------------------------------------------------------
     *
     *                      Destroy opened window
     *              and call onFocus on prev window on
     *                          Window Stack
     *
     *  -----------------------------------------------------------
     */
    WindowDispatcher.prototype.closeCurrentWindow = function () {
        var w;
        if (w = this.windowStack.dequeue()) {
            w.instance.onExit();
            w.destroy();
            var c;
            if (c = this.windowStack.current()) {
                console.log("CURRENT");
                console.log(c);
                c.instance.onFocus();
                this.currentComponent = c;
            }
        }
    };
    /** -----------------------------------------------------------
   *
   *                  Destroy opened window stack
   *              and define new empty currentComponent
   *
   *  -----------------------------------------------------------
   */
    WindowDispatcher.prototype.closeOpenWindowStack = function () {
        var w;
        while (w = this.windowStack.dequeue()) {
            w.instance.onExit();
            w.destroy();
        }
        this.currentComponent = null;
    };
    Object.defineProperty(WindowDispatcher.prototype, "menuComponent", {
        /** -----------------------------------------------------------
       *
       *                Get Menu Component for
       *            access to inner setters and options
       *
       *  -----------------------------------------------------------
       *
       * @returns {MenuComponent}
       */
        get: function () {
            return this._menuComponent;
        },
        enumerable: true,
        configurable: true
    });
    __decorate([
        core_1.ViewChild("windowframe", { read: core_1.ViewContainerRef }), 
        __metadata('design:type', core_1.ViewContainerRef)
    ], WindowDispatcher.prototype, "contentContainer", void 0);
    WindowDispatcher = __decorate([
        core_1.Component({
            selector: 'window',
            template: "\n     <div #windowframe></div>\n  ",
        }), 
        __metadata('design:paramtypes', [core_1.DynamicComponentLoader, core_1.ElementRef, core_1.ViewContainerRef, core_1.ComponentResolver])
    ], WindowDispatcher);
    return WindowDispatcher;
}());
exports.WindowDispatcher = WindowDispatcher;
(function (WindowSizeType) {
    WindowSizeType[WindowSizeType["Attached"] = 0] = "Attached";
    WindowSizeType[WindowSizeType["Fixed"] = 1] = "Fixed";
})(exports.WindowSizeType || (exports.WindowSizeType = {}));
var WindowSizeType = exports.WindowSizeType;
//# sourceMappingURL=window.dispatcher.js.map