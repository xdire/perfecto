/** --------------------------------------
 *    Component which can be swapped in
 *    some component with other component
 *  --------------------------------------
 */
export interface SwitchableComponent {

  _isVisible: boolean;
  onFocus();
  onLostFocus();
  onParentUnload();

}

/** --------------------------------------
 *    Menu item which Menu Component
 *    demand in menu structure
 *  --------------------------------------
 */
export interface MenuItemEntity {

  shortTitleName: string;
  shortImageName?: string;
  shortClassName?: string;
  shortType: MenuItemEntityRenderType;

  titleName: string;
  className?: string;
  imageName?: string;
  normalType: MenuItemEntityRenderType;

  windowToLoad: string;
  selected: boolean;

}

export enum MenuItemEntityRenderType {
  Title,
  Class,
  Image
}

/** --------------------------------------
 *    Key which user can retrieve from API
 *  --------------------------------------
 */
export interface UserApiVendorKeysEntity {
  id?: number;
  authId?: string;
  authKey?: string;
  authSecret?: string;
  user: number;
  vendor: number;
}
/** --------------------------------------
 *
 *  --------------------------------------
 */
export interface UserApiUserEntity {
  user?: number;
  email?: string;
  login?: string;
  pass?: string;
  pass_confirm?: string;
  agent?: string;
  firstName?: string;
  lastName?: string;
  key?: string;
}
/** --------------------------------------
 *
 *  --------------------------------------
 */
export interface AuthApiSecurityEntity {
  r: boolean;
  w: boolean;
  x: boolean;
  gid: number;
  lid: string;
  gid1: number;
  lid1: number;
  gid2: number;
  lid2: number;
}

/** --------------------------------------
 *
 *  --------------------------------------
 */
export interface WindowModalFloatParameters {

  title: string;
  width?: number;
  height?: number;
  top?: number;
  left?: number;
  background?: string;
  data?: any;
  onSuccess(data: any);
  onClose(data:any);
  onFailure(error: WindowCallbackError);
  getReference(reference: any);

}

/** --------------------------------------
 *
 *  --------------------------------------
 */
export interface ComponentImplementingModalFramePass {
  dataObject: any;
  closingMethod: any;
  successCallback: any;
  failureCallback: any;
}

export interface WindowCallbackError {
  errorCode: number;
  errorMessage: string;
  data: any
}
