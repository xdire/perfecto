"use strict";
(function (MenuItemEntityRenderType) {
    MenuItemEntityRenderType[MenuItemEntityRenderType["Title"] = 0] = "Title";
    MenuItemEntityRenderType[MenuItemEntityRenderType["Class"] = 1] = "Class";
    MenuItemEntityRenderType[MenuItemEntityRenderType["Image"] = 2] = "Image";
})(exports.MenuItemEntityRenderType || (exports.MenuItemEntityRenderType = {}));
var MenuItemEntityRenderType = exports.MenuItemEntityRenderType;
//# sourceMappingURL=system.entities.js.map