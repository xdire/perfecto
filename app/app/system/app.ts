import {Config} from "../config";
import {ApiRelationLink} from "interface/config.dec";
import {SystemState} from "./state/system.state";
import {WindowDispatcher} from "./window/window.dispatcher";
import {bootstrap}    from '@angular/platform-browser-dynamic';
import {WindowComponent} from "./window/window.component";
import {InputScannerObserver} from "./input/input.observer";
import {CryptoSystem} from "./crypto/crypto.sys";
import {PrintSystem} from "./print/printing.sys";
import {CodesSystem} from "./codes/codes.sys";
// ------------------------------------------------------------------------------
//                      Application helper class
// ------------------------------------------------------------------------------
export class App {

  private static _systemState: SystemState;

  private static _windowManager: WindowDispatcher;

  private static _inputScannerObserver: InputScannerObserver;

  private static _cryptoModule: CryptoSystem;

  private static _printSystem: PrintSystem;

  private static _codesSystem: CodesSystem;

	/** ---------------------------------------------------------------------------
   *
   *
   *  ---------------------------------------------------------------------------
   *
   *  @param components
   *  @returns {Promise<App>|Promise}
   *
   */
  public static __init(components?: {[name:string]: WindowComponent}) {

    this._systemState = new SystemState();
    this._inputScannerObserver = new InputScannerObserver();
    this._cryptoModule = new CryptoSystem();
    this._printSystem = new PrintSystem();
    this._codesSystem = new CodesSystem();
    this._printSystem.init();
    
    return new Promise<this>((resolve, reject) => {

      bootstrap(WindowDispatcher).then((ref) => {

        this._windowManager = ref.instance;

        if(components !== null) {
          this._windowManager.registerComponents(components);
        }

        resolve(this);

      },(err) => {

        reject(err);

      });

    });
    
  }

  /**
   * --------------------------
   * Return Api Endpoints List
   * --------------------------
   *
   * @returns {{}}
   */
  public static getSysApiCredentialList() : {[name: string] : ApiRelationLink} {
    return Config.systemApiList;
  }

	/** ---------------------------------------------------------------------------
   *
   *  ---------------------------------------------------------------------------
   *
   * @returns {WindowDispatcher}
   */
  public static get window(): WindowDispatcher {
    return this._windowManager;
  }

	/** ---------------------------------------------------------------------------
   *
   *  ---------------------------------------------------------------------------
   *
   * @returns {SystemState}
   */
  public static get state(): SystemState {
    return this._systemState;
  }

	/** ---------------------------------------------------------------------------
   *
   *  ---------------------------------------------------------------------------
   *
   * @returns {InputScannerObserver}
   */
  static get inputScannerObserver():InputScannerObserver {
    return this._inputScannerObserver;
  }

	/** ---------------------------------------------------------------------------
   *
   *  ---------------------------------------------------------------------------
   *
   * @returns {CryptoSystem}
   */
	static get crypto():CryptoSystem {
	    return this._cryptoModule;
  }

	/** ---------------------------------------------------------------------------
   *
   *  ---------------------------------------------------------------------------
   *
   * @returns {PrintSystem}
   */
  static get print():PrintSystem{
	    return this._printSystem;
  }

	/** ---------------------------------------------------------------------------
   *
   *  ---------------------------------------------------------------------------
   *
   * @returns {CodesSystem}
   */
	static get codes():CodesSystem{
	    return this._codesSystem;
  }

  /** --------------------------------------------------------------------------
   *
   *                          RFC 4122 v4 GUID Generator
   *
   *  --------------------------------------------------------------------------
   * @returns {string}
   */
  public static newGUID() {

    var ga: Array = [];

    // Generate FF Hex array
    for(var i = 0; i < 256; i++) {
      ga[i] = (i < 16 ? '0':'') + (i).toString(16);
    }

    // Generate 4 random numbers for 4 sections of GUID
    var rSec1 = Math.random() * 0xFFFFFFFF|0;
    var rSec2 = Math.random() * 0xFFFFFFFF|0;
    var rSec3 = Math.random() * 0xFFFFFFFF|0;
    var rSec4 = Math.random() * 0xFFFFFFFF|0;

    // Fill array with each 8 bit sets of 32 bit value
    // Note that section 2 & 3 having some control bits
    return ga[rSec1 & 0xFF] + ga[rSec2 >> 8 & 0xFF] + ga[rSec3 >> 16 & 0xFF] + ga[rSec4 >> 24 & 0xFF]+
      '-'+ga[rSec2 & 0xFF] + ga[rSec2 >> 8 & 0xFF]+'-'+ga[rSec2 >> 16 & 0x0F|0x40] + ga[rSec2 >> 24 & 0xFF]+
      '-'+ga[rSec3 & 0x3F|0x80] + ga[rSec3 >> 8 & 0xFF]+'-'+ga[rSec3 >> 16 & 0xFF] + ga[rSec3 >> 24 & 0xFF]+
      '-'+ga[rSec4 & 0xFF] + ga[rSec4 >> 8 & 0xFF]+ ga[rSec4 >> 16 & 0xFF] + ga[rSec4 >> 24 & 0xFF];

  }

  /** -------------------------------------------------
   *  Clone object
   *  -------------------------------------------------
   *
   * @param someObject
   * @returns {{}}
   */
  public static cloneObjectWithAttributes(someObject: Object): any {

    var cloneObj = {};

    for (var attr in someObject) {

      if (typeof someObject[attr] === "object") {
        cloneObj[attr] = this.cloneObjectWithAttributes(someObject[attr]);
      } else {
        cloneObj[attr] = someObject[attr];
      }
    }
    return cloneObj;

  }



}

