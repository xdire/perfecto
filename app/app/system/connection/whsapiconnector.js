"use strict";
var api_sys_1 = require("./api.sys");
var http_1 = require("@angular/http");
/** ------------------------------------------------------------ \
 *                WAREHOUSE API CONNECTOR CLASS
 *  ----------------------------------------------------------- */
var WarehouseApiConnector = (function () {
    function WarehouseApiConnector() {
        /* ------------------------------------------------------------ \
                       GET ITEMS & VARIATION ROUTES
        \  ------------------------------------------------------------ */
        this.getItemByIdRT = "/api/v1/item";
        this.getAllVariationsFromAllLocations = "/api/v1/item/variation";
        this.getAllItemsFromLocation = "/api/v1/item/location";
        /* ------------------------------------------------------------ \
                    CREATE AND DELETE WAREHOUSE LOCATIONS
        \  ------------------------------------------------------------ */
        this.createLocation = "/api/v1/location/create";
        this.deleteLocationSoft = "/api/v1/location/delete";
        this.deleteLocationPermament = "/api/v1/location/delete-hard";
        this.updateLocationRT = "/api/v1/location/update";
        /* ------------------------------------------------------------ \
                       GET LOCATION ROUTES
        \  ------------------------------------------------------------ */
        // /api/v1/location/[id]
        this.getLocationById = "/api/v1/location";
        // /api/v1/location/children/[id]
        this.getLocationChildsById = "/api/v1/location/children";
        // /api/v1/location/all/[id]"
        this.getLocationByWarehouseId = "/api/v1/location/all";
        //
        this.getLocationsByVariationIdRT = "/api/v1/location/variation";
        /* ------------------------------------------------------------ \
                       CREATE AND MOVE ITEMS
        \  ------------------------------------------------------------ */
        this.createItemRT = "/api/v1/item/create";
        this.updateItemRT = "/api/v1/item/update";
        this.moveItemRT = "/api/v1/item/move";
        this.archiveItemRT = "/api/v1/item/delete";
        this.deleteItemRT = "/api/v1/item/delete-hard";
    }
    /** -------------------------------------------------
   *
   *  -------------------------------------------------
   *  @param item
   *  @returns {Observable<R>}
   */
    WarehouseApiConnector.prototype.createNewItem = function (item) {
        var conn = new api_sys_1.ApiConnectorBackend({
            url: this.connInfo.host + this.createItemRT,
            method: http_1.RequestMethod.Post,
            headers: null,
            body: item
        });
        conn.addVendorAuth();
        var http = new http_1.Http(conn, new http_1.RequestOptions());
        return http.request(this.connInfo.host).map(function (response) {
            return response.json();
        });
    };
    /** -------------------------------------------------
     *  Update Item Entity
     *  -------------------------------------------------
     *  @param item
     *  @returns {Observable<R>}
     */
    WarehouseApiConnector.prototype.updateItem = function (item) {
        console.log(item);
        var newItem = this.clone(item);
        var id = item.id;
        delete newItem.id;
        var conn = new api_sys_1.ApiConnectorBackend({
            url: this.connInfo.host + this.updateItemRT + '/' + id,
            method: http_1.RequestMethod.Post,
            headers: null,
            body: newItem
        });
        conn.addVendorAuth();
        var http = new http_1.Http(conn, new http_1.RequestOptions());
        return http.request(this.connInfo.host).map(function (response) {
            return response.json();
        });
    };
    /** -------------------------------------------------
     *  Move Item Entity
     *  -------------------------------------------------
     *  @param item
     *  @param locationId
     *  @returns {Observable<Object>}
     */
    WarehouseApiConnector.prototype.moveItem = function (item, locationId) {
        var conn = new api_sys_1.ApiConnectorBackend({
            url: this.connInfo.host + this.moveItemRT + '/' + item.id,
            method: http_1.RequestMethod.Post,
            headers: null,
            body: { locationId: locationId }
        });
        conn.addVendorAuth();
        var http = new http_1.Http(conn, new http_1.RequestOptions());
        return http.request(this.connInfo.host).map(function (response) {
            return response.json();
        });
    };
    /** -------------------------------------------------
     *  Delete Item Entity
     *  -------------------------------------------------
     *  @param item
     *  @returns {Observable<Object>}
     */
    WarehouseApiConnector.prototype.deleteItem = function (item) {
        var conn = new api_sys_1.ApiConnectorBackend({
            url: this.connInfo.host + this.deleteItemRT + '/' + item.id,
            method: http_1.RequestMethod.Post,
            headers: null,
            body: null
        });
        conn.addVendorAuth();
        var http = new http_1.Http(conn, new http_1.RequestOptions());
        return http.request(this.connInfo.host).map(function (response) {
            return response.json();
        });
    };
    /** -------------------------------------------------
     *  Archive Item Entity
     *  -------------------------------------------------
     *
     *  @param item
     *  @returns {Observable<Object>}
     */
    WarehouseApiConnector.prototype.archiveItem = function (item) {
        var conn = new api_sys_1.ApiConnectorBackend({
            url: this.connInfo.host + this.archiveItemRT + '/' + item.id,
            method: http_1.RequestMethod.Post,
            headers: null,
            body: {}
        });
        conn.addVendorAuth();
        var http = new http_1.Http(conn, new http_1.RequestOptions());
        return http.request(this.connInfo.host).map(function (response) {
            return response.json();
        });
    };
    /** -------------------------------------------------
   *  Clone object
   *  -------------------------------------------------
   *
   * @param someObject
   * @returns {{}}
   */
    WarehouseApiConnector.prototype.clone = function (someObject) {
        var cloneObj = {};
        for (var attr in someObject) {
            if (typeof someObject[attr] === "object") {
                cloneObj[attr] = this.clone(someObject[attr]);
            }
            else {
                cloneObj[attr] = someObject[attr];
            }
        }
        return cloneObj;
    };
    /** -------------------------------------------------
     *  Get Item By ID
     *  -------------------------------------------------
     *  @param itemId
     *  @returns {Observable<WarehouseLocationEntity>}
     */
    WarehouseApiConnector.prototype.getItemById = function (itemId) {
        var conn = new api_sys_1.ApiConnectorBackend({
            url: this.connInfo.host + this.getItemByIdRT + '/' + itemId,
            method: http_1.RequestMethod.Get,
            headers: null,
            body: null
        });
        conn.addVendorAuth();
        var http = new http_1.Http(conn, new http_1.RequestOptions());
        return http.request(this.connInfo.host).map(function (response) {
            return response.json();
        });
    };
    /** -------------------------------------------------
     *  Create location
     *  -------------------------------------------------
     *  @param location
     *  @returns {Observable<WarehouseLocationEntity>}
     */
    WarehouseApiConnector.prototype.createNewLocation = function (location) {
        var conn = new api_sys_1.ApiConnectorBackend({
            url: this.connInfo.host + this.createLocation,
            method: http_1.RequestMethod.Post,
            headers: null,
            body: location
        });
        conn.addVendorAuth();
        var http = new http_1.Http(conn, new http_1.RequestOptions());
        return http.request(this.connInfo.host).map(function (response) {
            return response.json();
        });
    };
    /** -------------------------------------------------
   *  Get Item From Location
   *  -------------------------------------------------
   *  @param locationId
   *  @returns {Observable<R>}
   */
    WarehouseApiConnector.prototype.getItemsFromLocation = function (locationId) {
        var conn = new api_sys_1.ApiConnectorBackend({
            url: this.connInfo.host + this.getAllItemsFromLocation + '/' + locationId,
            method: http_1.RequestMethod.Get,
            headers: null,
            body: null
        });
        conn.addVendorAuth();
        var http = new http_1.Http(conn, new http_1.RequestOptions());
        return http.request(this.connInfo.host).map(function (response) {
            return response.json();
        });
    };
    /** -------------------------------------------------
     *  Get locations by Item Variation id
     *  -------------------------------------------------
     *  @param variationId
     *  @returns {Observable<WarehouseLocationEntity[]>}
     */
    WarehouseApiConnector.prototype.getLocationsByVariationId = function (variationId) {
        // TODO : Warehouse ID need to be fixed
        var conn = new api_sys_1.ApiConnectorBackend({
            url: this.connInfo.host + this.getLocationsByVariationIdRT + "/1/" + variationId,
            method: http_1.RequestMethod.Get,
            headers: null,
            body: null
        });
        conn.addVendorAuth();
        var http = new http_1.Http(conn, new http_1.RequestOptions());
        return http.request(this.connInfo.host).map(function (response) {
            return response.json();
        });
    };
    /** -------------------------------------------------
     *  Delete location soft
     *  -------------------------------------------------
     *  @param locationId
     *  @returns {Observable<WarehouseLocationEntity>}
     */
    WarehouseApiConnector.prototype.trashLocation = function (locationId) {
        var conn = new api_sys_1.ApiConnectorBackend({
            url: this.connInfo.host + this.deleteLocationSoft + "/" + locationId,
            method: http_1.RequestMethod.Post,
            headers: null,
            body: {}
        });
        conn.addVendorAuth();
        var http = new http_1.Http(conn, new http_1.RequestOptions());
        return http.request(this.connInfo.host).map(function (response) {
            return response.json();
        });
    };
    /** -------------------------------------------------
     *  Delete location soft
     *  -------------------------------------------------
     *  @param location WarehouseLocationEntity
     *  @returns {Observable<WarehouseLocationEntity>}
     */
    WarehouseApiConnector.prototype.updateLocation = function (location) {
        if (location.hasOwnProperty("id")) {
            var id = location.id;
            delete location.id;
        }
        else
            return;
        var conn = new api_sys_1.ApiConnectorBackend({
            url: this.connInfo.host + this.updateLocationRT + "/" + id,
            method: http_1.RequestMethod.Post,
            headers: null,
            body: location
        });
        conn.addVendorAuth();
        var http = new http_1.Http(conn, new http_1.RequestOptions());
        return http.request(this.connInfo.host).map(function (response) {
            return response.json();
        });
    };
    /** --------------------------------------------------------------------------------------------------
     *  Delete location hard
     *  -------------------------------------------------
     *  @param locationId
     *  @returns {Observable<WarehouseLocationEntity>}
     */
    WarehouseApiConnector.prototype.removeLocation = function (locationId) {
        var conn = new api_sys_1.ApiConnectorBackend({
            url: this.connInfo.host + this.deleteLocationPermament + "/" + locationId,
            method: http_1.RequestMethod.Post,
            headers: null,
            body: null
        });
        conn.addVendorAuth();
        var http = new http_1.Http(conn, new http_1.RequestOptions());
        return http.request(this.connInfo.host).map(function (response) {
            return response.json();
        });
    };
    /** -------------------------------------------------
     *  Retrieve list of locations
     *  -------------------------------------------------
     *  @param warehouseId
     *  @returns {Observable<WarehouseLocationEntity[]>}
     */
    WarehouseApiConnector.prototype.getAllLocationsForWarehouseId = function (warehouseId) {
        var conn = new api_sys_1.ApiConnectorBackend({
            url: this.connInfo.host + this.getLocationByWarehouseId + "/" + warehouseId,
            method: http_1.RequestMethod.Get,
            headers: null,
            body: null
        });
        conn.addVendorAuth();
        var http = new http_1.Http(conn, new http_1.RequestOptions());
        return http.request(this.connInfo.host).map(function (response) {
            return response.json();
        });
    };
    /** -------------------------------------------------
     *  Get location by location ID
     *  -------------------------------------------------
     *  @param locationId
     *  @returns {Observable<WarehouseLocationEntity>}
     */
    WarehouseApiConnector.prototype.getLocationById = function (locationId) {
        var conn = new api_sys_1.ApiConnectorBackend({
            url: this.connInfo.host + this.getLocationById,
            method: http_1.RequestMethod.Get,
            headers: null,
            body: null
        });
        conn.addVendorAuth();
        var http = new http_1.Http(conn, new http_1.RequestOptions());
        return http.request(this.connInfo.host).map(function (response) {
            return response.json();
        });
    };
    /** -------------------------------------------------
     *  Reserve an Item
     *  -------------------------------------------------
     *
     *  @param item
     *  @returns {Observable<R>}
     */
    WarehouseApiConnector.prototype.reserveItem = function (item) {
        var conn = new api_sys_1.ApiConnectorBackend({
            url: this.connInfo.host + "/api/v1/reserve/create",
            method: http_1.RequestMethod.Post,
            headers: null,
            body: item
        });
        conn.addVendorAuth();
        var http = new http_1.Http(conn, new http_1.RequestOptions());
        return http.request(this.connInfo.host).map(function (response) {
            return response.json();
        });
    };
    /** -------------------------------------------------
     *  Release reserve for Item
     *  -------------------------------------------------
     *
     *  @param item
     *  @returns {Observable<R>}
     */
    WarehouseApiConnector.prototype.releaseReserveForItem = function (item) {
        var conn = new api_sys_1.ApiConnectorBackend({
            url: this.connInfo.host + "api/v1/reserve/delete/" + item.itemId,
            method: http_1.RequestMethod.Post,
            headers: null,
            body: item
        });
        conn.addVendorAuth();
        var http = new http_1.Http(conn, new http_1.RequestOptions());
        return http.request(this.connInfo.host).map(function (response) {
            return response.json();
        });
    };
    /** -------------------------------------------------
     *  Ship Item From warehouse
     *  -------------------------------------------------
     */
    WarehouseApiConnector.prototype.shipItemFromWarehouse = function (itemId, quantity) {
        var conn = new api_sys_1.ApiConnectorBackend({
            url: this.connInfo.host + "api/v1/item/ship/" + itemId,
            method: http_1.RequestMethod.Post,
            headers: null,
            body: { quantity: quantity }
        });
        conn.addVendorAuth();
        var http = new http_1.Http(conn, new http_1.RequestOptions());
        return http.request(this.connInfo.host).map(function (response) {
            return response.json();
        });
    };
    /** -------------------------------------------------
     *  Wipe off reserved for Item from Warehouse
     *  -------------------------------------------------
     */
    WarehouseApiConnector.prototype.shipReservedItemFromWarehouse = function (ritem) {
        var conn = new api_sys_1.ApiConnectorBackend({
            url: this.connInfo.host + "api/v1/reserve/ship/" + ritem.itemId,
            method: http_1.RequestMethod.Post,
            headers: null,
            body: null
        });
        conn.addVendorAuth();
        var http = new http_1.Http(conn, new http_1.RequestOptions());
        return http.request(this.connInfo.host).map(function (response) {
            return response.json();
        });
    };
    /** -------------------------------------------------
     *  Initializer
     *  -------------------------------------------------
     *  @param relComponent
     *  @param relKeyType
     *
     *  @returns {AuthApiConnector}
     *  @private
     */
    WarehouseApiConnector.prototype.__initWithRelComponent = function (relComponent, relKeyType) {
        this.connInfo = relComponent;
        this.connKeyType = relKeyType;
        return this;
    };
    WarehouseApiConnector.prototype.getVariations = function () {
    };
    WarehouseApiConnector.prototype.getVariationsFromLocation = function () {
    };
    return WarehouseApiConnector;
}());
exports.WarehouseApiConnector = WarehouseApiConnector;
//# sourceMappingURL=whsapiconnector.js.map