import {ApiRelationLink} from "../interface/config.dec";
import {Observable} from "rxjs/Observable";
import {ApiConnectorBackend} from "./api.sys";
import {RequestMethod, RequestOptions, Http} from "@angular/http";
import {
  WarehouseLocationEntity, WarehouseItemEntity,
  WarehouseReservationObject, WarehouseItemShippingObject
} from "./connection-entities/whsapi-entities";
/** ------------------------------------------------------------ \
 *                WAREHOUSE API CONNECTOR CLASS
 *  ----------------------------------------------------------- */
export class WarehouseApiConnector {

  private connInfo:ApiRelationLink;
  private connKeyType:string;

  /* ------------------------------------------------------------ \
                 GET ITEMS & VARIATION ROUTES
  \  ------------------------------------------------------------ */
  private getItemByIdRT:string = "/api/v1/item";
  private getAllVariationsFromAllLocations:string = "/api/v1/item/variation";
  private getAllItemsFromLocation:string = "/api/v1/item/location";

  /* ------------------------------------------------------------ \
              CREATE AND DELETE WAREHOUSE LOCATIONS
  \  ------------------------------------------------------------ */
  private createLocation:string = "/api/v1/location/create";
  private deleteLocationSoft:string = "/api/v1/location/delete";
  private deleteLocationPermament:string = "/api/v1/location/delete-hard";
  private updateLocationRT:string = "/api/v1/location/update";

  /* ------------------------------------------------------------ \
                 GET LOCATION ROUTES
  \  ------------------------------------------------------------ */
  // /api/v1/location/[id]
  private getLocationById:string = "/api/v1/location";
  // /api/v1/location/children/[id]
  private getLocationChildsById:string = "/api/v1/location/children";
  // /api/v1/location/all/[id]"
  private getLocationByWarehouseId:string = "/api/v1/location/all";
  //
  private getLocationsByVariationIdRT: string = "/api/v1/location/variation";

  /* ------------------------------------------------------------ \
                 CREATE AND MOVE ITEMS
  \  ------------------------------------------------------------ */
  private createItemRT: string = "/api/v1/item/create";
  private updateItemRT: string = "/api/v1/item/update";

  private moveItemRT: string = "/api/v1/item/move";
  private archiveItemRT: string = "/api/v1/item/delete";
  private deleteItemRT: string = "/api/v1/item/delete-hard";

	/** -------------------------------------------------
   *
   *  -------------------------------------------------
   *  @param item
   *  @returns {Observable<R>}
   */
  public createNewItem(item: WarehouseItemEntity) : Observable<WarehouseItemEntity> {

    var conn: ApiConnectorBackend = new ApiConnectorBackend({
      url:this.connInfo.host+this.createItemRT,
      method: RequestMethod.Post,
      headers: null,
      body: item
    });

    conn.addVendorAuth();

    var http = new Http(conn, new RequestOptions());

    return http.request(this.connInfo.host).map(function (response) {
      return response.json();
    });

  }

  /** -------------------------------------------------
   *  Update Item Entity
   *  -------------------------------------------------
   *  @param item
   *  @returns {Observable<R>}
   */
  public updateItem(item: WarehouseItemEntity) : Observable<WarehouseItemEntity> {

    console.log(item);
    var newItem = this.clone(item);
    var id = item.id;
    delete newItem.id;

    var conn: ApiConnectorBackend = new ApiConnectorBackend({
      url:this.connInfo.host+this.updateItemRT+'/'+id,
      method: RequestMethod.Post,
      headers: null,
      body: newItem
    });

    conn.addVendorAuth();

    var http = new Http(conn, new RequestOptions());

    return http.request(this.connInfo.host).map(function (response) {
      return response.json();
    });

  }

  /** -------------------------------------------------
   *  Move Item Entity
   *  -------------------------------------------------
   *  @param item
   *  @param locationId
   *  @returns {Observable<Object>}
   */
  public moveItem(item: WarehouseItemEntity, locationId: number) : Observable<Object> {

    var conn: ApiConnectorBackend = new ApiConnectorBackend({
      url:this.connInfo.host+this.moveItemRT+'/'+item.id,
      method: RequestMethod.Post,
      headers: null,
      body: {locationId:locationId}
    });

    conn.addVendorAuth();

    var http = new Http(conn, new RequestOptions());

    return http.request(this.connInfo.host).map(function (response) {
      return response.json();
    });

  }

  /** -------------------------------------------------
   *  Delete Item Entity
   *  -------------------------------------------------
   *  @param item
   *  @returns {Observable<Object>}
   */
  public deleteItem(item: WarehouseItemEntity) : Observable<Object> {

    var conn: ApiConnectorBackend = new ApiConnectorBackend({
      url:this.connInfo.host+this.deleteItemRT+'/'+item.id,
      method: RequestMethod.Post,
      headers: null,
      body: null
    });

    conn.addVendorAuth();

    var http = new Http(conn, new RequestOptions());

    return http.request(this.connInfo.host).map(function (response) {
      return response.json();
    });

  }

  /** -------------------------------------------------
   *  Archive Item Entity
   *  -------------------------------------------------
   *
   *  @param item
   *  @returns {Observable<Object>}
   */
  public archiveItem(item: WarehouseItemEntity) : Observable<Object> {

    var conn: ApiConnectorBackend = new ApiConnectorBackend({
      url:this.connInfo.host+this.archiveItemRT+'/'+item.id,
      method: RequestMethod.Post,
      headers: null,
      body: {}
    });

    conn.addVendorAuth();

    var http = new Http(conn, new RequestOptions());

    return http.request(this.connInfo.host).map(function (response) {
      return response.json();
    });

  }

	/** -------------------------------------------------
   *  Clone object
   *  -------------------------------------------------
   *
   * @param someObject
   * @returns {{}}
   */
  public clone(someObject: Object): any {

    var cloneObj = {};

    for (var attr in someObject) {

      if (typeof someObject[attr] === "object") {
        cloneObj[attr] = this.clone(someObject[attr]);
      } else {
        cloneObj[attr] = someObject[attr];
      }
    }
    return cloneObj;

  }

  /** -------------------------------------------------
   *  Get Item By ID
   *  -------------------------------------------------
   *  @param itemId
   *  @returns {Observable<WarehouseLocationEntity>}
   */
  public getItemById(itemId: number) : Observable<WarehouseItemEntity> {

    var conn: ApiConnectorBackend = new ApiConnectorBackend({
      url:this.connInfo.host+this.getItemByIdRT+'/'+itemId,
      method: RequestMethod.Get,
      headers: null,
      body: null
    });

    conn.addVendorAuth();

    var http = new Http(conn, new RequestOptions());

    return http.request(this.connInfo.host).map(function (response) {
      return response.json();
    });

  }

  /** -------------------------------------------------
   *  Create location
   *  -------------------------------------------------
   *  @param location
   *  @returns {Observable<WarehouseLocationEntity>}
   */
  public createNewLocation(location: WarehouseLocationEntity) : Observable<WarehouseLocationEntity> {

    var conn: ApiConnectorBackend = new ApiConnectorBackend({
      url:this.connInfo.host+this.createLocation,
      method: RequestMethod.Post,
      headers: null,
      body: location
    });

    conn.addVendorAuth();

    var http = new Http(conn, new RequestOptions());

    return http.request(this.connInfo.host).map(function (response) {
      return response.json();
    });

  }

	/** -------------------------------------------------
   *  Get Item From Location
   *  -------------------------------------------------
   *  @param locationId
   *  @returns {Observable<R>}
   */
  public getItemsFromLocation(locationId: number) : Observable<WarehouseItemEntity[]> {

    var conn: ApiConnectorBackend = new ApiConnectorBackend({
      url:this.connInfo.host+this.getAllItemsFromLocation+'/'+locationId,
      method: RequestMethod.Get,
      headers: null,
      body: null
    });

    conn.addVendorAuth();

    var http = new Http(conn, new RequestOptions());

    return http.request(this.connInfo.host).map(function (response) {
      return response.json();
    });

  }

  /** -------------------------------------------------
   *  Get locations by Item Variation id
   *  -------------------------------------------------
   *  @param variationId
   *  @returns {Observable<WarehouseLocationEntity[]>}
   */
  public getLocationsByVariationId(variationId: number) : Observable<WarehouseLocationEntity[]> {

    // TODO : Warehouse ID need to be fixed

    var conn: ApiConnectorBackend = new ApiConnectorBackend({
      url: this.connInfo.host+this.getLocationsByVariationIdRT+"/1/"+variationId,
      method: RequestMethod.Get,
      headers: null,
      body: null
    });

    conn.addVendorAuth();

    var http = new Http(conn, new RequestOptions());

    return http.request(this.connInfo.host).map(function (response) {
      return response.json();
    });

  }

  /** -------------------------------------------------
   *  Delete location soft
   *  -------------------------------------------------
   *  @param locationId
   *  @returns {Observable<WarehouseLocationEntity>}
   */
  public trashLocation(locationId: number) : Observable<WarehouseLocationEntity> {

    var conn: ApiConnectorBackend = new ApiConnectorBackend({
      url: this.connInfo.host+this.deleteLocationSoft+"/"+locationId,
      method: RequestMethod.Post,
      headers: null,
      body: {}
    });

    conn.addVendorAuth();

    var http = new Http(conn, new RequestOptions());

    return http.request(this.connInfo.host).map(function (response) {
      return response.json();
    });

  }

  /** -------------------------------------------------
   *  Delete location soft
   *  -------------------------------------------------
   *  @param location WarehouseLocationEntity
   *  @returns {Observable<WarehouseLocationEntity>}
   */
  public updateLocation(location: WarehouseLocationEntity) : Observable<WarehouseLocationEntity> {


    if(location.hasOwnProperty("id")) {
      var id = location.id;
      delete location.id;
    } else return;

    var conn: ApiConnectorBackend = new ApiConnectorBackend({
      url: this.connInfo.host+this.updateLocationRT+"/"+id,
      method: RequestMethod.Post,
      headers: null,
      body: location
    });

    conn.addVendorAuth();

    var http = new Http(conn, new RequestOptions());

    return http.request(this.connInfo.host).map(function (response) {
      return response.json();
    });

  }

  /** --------------------------------------------------------------------------------------------------
   *  Delete location hard
   *  -------------------------------------------------
   *  @param locationId
   *  @returns {Observable<WarehouseLocationEntity>}
   */
  public removeLocation(locationId: number) : Observable<WarehouseLocationEntity> {

    var conn: ApiConnectorBackend = new ApiConnectorBackend({
      url: this.connInfo.host+this.deleteLocationPermament+"/"+locationId,
      method: RequestMethod.Post,
      headers: null,
      body: null
    });

    conn.addVendorAuth();

    var http = new Http(conn, new RequestOptions());

    return http.request(this.connInfo.host).map(function (response) {
      return response.json();
    });

  }

  /** -------------------------------------------------
   *  Retrieve list of locations
   *  -------------------------------------------------
   *  @param warehouseId
   *  @returns {Observable<WarehouseLocationEntity[]>}
   */
  public getAllLocationsForWarehouseId(warehouseId: Number) : Observable<WarehouseLocationEntity[]> {

    var conn: ApiConnectorBackend = new ApiConnectorBackend({
      url:this.connInfo.host+this.getLocationByWarehouseId+"/"+warehouseId,
      method: RequestMethod.Get,
      headers: null,
      body: null
    });

    conn.addVendorAuth();

    var http = new Http(conn, new RequestOptions());

    return http.request(this.connInfo.host).map(function (response) {
      return response.json();
    });

  }

  /** -------------------------------------------------
   *  Get location by location ID
   *  -------------------------------------------------
   *  @param locationId
   *  @returns {Observable<WarehouseLocationEntity>}
   */
  public getLocationById(locationId: Number) : Observable<WarehouseLocationEntity> {

    var conn: ApiConnectorBackend = new ApiConnectorBackend({
      url:this.connInfo.host+this.getLocationById,
      method: RequestMethod.Get,
      headers: null,
      body: null
    });

    conn.addVendorAuth();

    var http = new Http(conn, new RequestOptions());

    return http.request(this.connInfo.host).map(function (response) {
      return response.json();
    });

  }

  /** -------------------------------------------------
   *  Reserve an Item
   *  -------------------------------------------------
   *
   *  @param item
   *  @returns {Observable<R>}
   */
  public reserveItem(item: WarehouseReservationObject) : Observable<WarehouseReservationObject> {

    var conn: ApiConnectorBackend = new ApiConnectorBackend({
      url:this.connInfo.host + "/api/v1/reserve/create",
      method: RequestMethod.Post,
      headers: null,
      body: item
    });

    conn.addVendorAuth();

    var http = new Http(conn, new RequestOptions());

    return http.request(this.connInfo.host).map(function (response) {
      return response.json();
    });

  }

  /** -------------------------------------------------
   *  Release reserve for Item
   *  -------------------------------------------------
   *
   *  @param item
   *  @returns {Observable<R>}
   */
  public releaseReserveForItem(item: WarehouseReservationObject) : Observable<WarehouseReservationObject> {

    var conn: ApiConnectorBackend = new ApiConnectorBackend({
      url:this.connInfo.host + "api/v1/reserve/delete/"+item.itemId,
      method: RequestMethod.Post,
      headers: null,
      body: item
    });

    conn.addVendorAuth();

    var http = new Http(conn, new RequestOptions());

    return http.request(this.connInfo.host).map(function (response) {
      return response.json();
    });

  }

  /** -------------------------------------------------
   *  Ship Item From warehouse
   *  -------------------------------------------------
   */
  public shipItemFromWarehouse(itemId: number, quantity: number) : Observable<WarehouseItemEntity> {

    var conn: ApiConnectorBackend = new ApiConnectorBackend({
      url:this.connInfo.host + "api/v1/item/ship/"+itemId,
      method: RequestMethod.Post,
      headers: null,
      body: {quantity:quantity}
    });

    conn.addVendorAuth();

    var http = new Http(conn, new RequestOptions());

    return http.request(this.connInfo.host).map(function (response) {
      return response.json();
    });

  }

  /** -------------------------------------------------
   *  Wipe off reserved for Item from Warehouse
   *  -------------------------------------------------
   */
  public shipReservedItemFromWarehouse(ritem: WarehouseReservationObject) : Observable<WarehouseReservationObject> {

    var conn: ApiConnectorBackend = new ApiConnectorBackend({
      url:this.connInfo.host + "api/v1/reserve/ship/"+ritem.itemId,
      method: RequestMethod.Post,
      headers: null,
      body: null
    });

    conn.addVendorAuth();

    var http = new Http(conn, new RequestOptions());

    return http.request(this.connInfo.host).map(function (response) {
      return response.json();
    });

  }

  /** -------------------------------------------------
   *  Initializer
   *  -------------------------------------------------
   *  @param relComponent
   *  @param relKeyType
   *
   *  @returns {AuthApiConnector}
   *  @private
   */
  public __initWithRelComponent(relComponent:ApiRelationLink, relKeyType:string):WarehouseApiConnector {
    this.connInfo = relComponent;
    this.connKeyType = relKeyType;
    return this;
  }

  public getVariations() {

  }

  public getVariationsFromLocation() {

  }

}
