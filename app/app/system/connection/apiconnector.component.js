"use strict";
require('rxjs/add/operator/map');
require('rxjs/add/operator/toPromise');
var authapiconnector_1 = require("./authapiconnector");
var config_1 = require("../../config");
var whsapiconnector_1 = require("./whsapiconnector");
var itemapiconnector_1 = require("./itemapiconnector");
var ApiConnectorComponent = (function () {
    function ApiConnectorComponent() {
    }
    /**
     *  Authorization API Connector Methods
     *
     * @returns {AuthApiConnector}
     */
    ApiConnectorComponent.getAuthApiConnector = function () {
        if (this.config == null) {
            this.config = config_1.Config;
        }
        var connector = new authapiconnector_1.AuthApiConnector();
        return connector.__initWithRelComponent(this.config.systemApiList['authApi'], this.config.systemApiKeyType);
    };
    /**
   *  Item API Connector Methods
   */
    ApiConnectorComponent.getItemApiConnector = function () {
        if (this.config == null) {
            this.config = config_1.Config;
        }
        var connector = new itemapiconnector_1.ItemApiConnector();
        return connector.__initWithRelComponent(this.config.systemApiList['itemApi'], this.config.systemApiKeyType);
    };
    /**
     *
     * @returns {WarehouseApiConnector}
     */
    ApiConnectorComponent.getWarehouseApiConnector = function () {
        if (this.config == null) {
            this.config = config_1.Config;
        }
        var connector = new whsapiconnector_1.WarehouseApiConnector();
        return connector.__initWithRelComponent(this.config.systemApiList['wrhsApi'], this.config.systemApiKeyType);
    };
    return ApiConnectorComponent;
}());
exports.ApiConnectorComponent = ApiConnectorComponent;
//# sourceMappingURL=apiconnector.component.js.map