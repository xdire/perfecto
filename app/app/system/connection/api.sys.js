"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var http_1 = require("@angular/http");
var app_1 = require("../app");
/** --------------------------------------------------- \
 *                  REQUEST HELPER CLASS
 *  --------------------------------------------------- */
var ApiConnectorBackend = (function (_super) {
    __extends(ApiConnectorBackend, _super);
    function ApiConnectorBackend(request) {
        this.request = request;
    }
    ApiConnectorBackend.prototype.createConnection = function () {
        var hdr = new http_1.Headers(this.request.headers);
        var opt = new http_1.RequestOptions({ url: this.request.url, method: this.request.method, headers: hdr, body: JSON.stringify(this.request.body) });
        var req = new http_1.Request(opt);
        return new http_1.XHRConnection(req, new http_1.BrowserXhr());
    };
    ApiConnectorBackend.prototype.addVendorAuth = function () {
        if (this.request.headers === null) {
            this.request.headers = {};
        }
        var vkobject = app_1.App.state.getCurrentUserVendorKey();
        this.request.headers["AuthUser"] = vkobject.authId;
        this.request.headers["AuthKey"] = app_1.App.crypto.hmacSHA1(vkobject.authKey, vkobject.authSecret);
    };
    ApiConnectorBackend.prototype.addUserAuth = function () {
    };
    return ApiConnectorBackend;
}(http_1.ConnectionBackend));
exports.ApiConnectorBackend = ApiConnectorBackend;
//# sourceMappingURL=api.sys.js.map