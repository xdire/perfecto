"use strict";
var http_1 = require("@angular/http");
var app_1 = require("../app");
var api_sys_1 = require("./api.sys");
/** ------------------------------------------------------------ \
 *                AUTH API CONNECTOR CLASS
 *  ----------------------------------------------------------- */
var AuthApiConnector = (function () {
    function AuthApiConnector() {
        this.signUserWithPasswordRoute = "/api/v1/user/add";
        this.createVendorKeyForUserRoute = "/api/v1/vendor/add";
        this.authUserWithPasswordRoute = "/api/v1/user/auth";
        this.authAndViewUserProfileRoute = "/api/v1/user/view";
        this.authAndViewUserListRoute = "/api/v1/users/view";
        this.authAndViewUserVendorKeysRoute = "/api/v1/user/vendorkeys";
        this.authUserWithUserKeyRoute = "/api/v1/user/auth/key";
    }
    /**
   * -------------------------------------------------
   * Initializer
   * -------------------------------------------------
   * @param relComponent
   * @param relKeyType
   *
   * @returns {AuthApiConnector}
   * @private
   */
    AuthApiConnector.prototype.__initWithRelComponent = function (relComponent, relKeyType) {
        this.connInfo = relComponent;
        this.connKeyType = relKeyType;
        return this;
    };
    /**
   * -------------------------------------------------
   * Signup process
   * -------------------------------------------------
   * @param user
   */
    AuthApiConnector.prototype.signupUser = function (user) {
    };
    /**
     * -------------------------------------------------
     * Authorize User by Passing User Object to Request
     * -------------------------------------------------
     * @param user
     * @returns {Observable<R>}
     */
    AuthApiConnector.prototype.authorizeUser = function (user) {
        var conn = new api_sys_1.ApiConnectorBackend({
            url: this.connInfo.host + this.authUserWithPasswordRoute,
            method: http_1.RequestMethod.Post,
            headers: null,
            body: user
        });
        var http = new http_1.Http(conn, new http_1.RequestOptions());
        return http.request(this.connInfo.host).map(function (response) {
            return response.json();
        });
    };
    /**
   * -------------------------------------------------
   * Check user key against API for security creds
   * -------------------------------------------------
   *
   * @returns {Observable<R>}
   */
    AuthApiConnector.prototype.checkUserAuthorization = function () {
        var userObject = app_1.App.state.user;
        var conn = new api_sys_1.ApiConnectorBackend({
            url: this.connInfo.host + this.authUserWithUserKeyRoute,
            method: http_1.RequestMethod.Get,
            headers: { "AuthUser": userObject.id, "AuthKey": userObject.key, "AuthAgent": this.connKeyType }
        });
        var http = new http_1.Http(conn, new http_1.RequestOptions());
        return http.request(this.connInfo.host).map(function (response) {
            return response.json();
        });
    };
    /**
   *  -------------------------------------------------
   *  Logout User
   *  -------------------------------------------------
   *
   */
    AuthApiConnector.prototype.logoutUser = function () {
        app_1.App.state.__setUnauthorized();
    };
    /**
   *  -------------------------------------------------
   *  Get User Info
   *  -------------------------------------------------
   */
    AuthApiConnector.prototype.getUserInfo = function (userKey) {
        var conn = new api_sys_1.ApiConnectorBackend({
            url: this.connInfo.host + this.authAndViewUserProfileRoute,
            method: http_1.RequestMethod.Get,
            headers: { "AuthUser": userKey.user, "AuthKey": userKey.key, "AuthAgent": this.connKeyType }
        });
        var http = new http_1.Http(conn, new http_1.RequestOptions());
        return http.request(this.connInfo.host).map(function (response) {
            return response.json();
        });
    };
    /**
   * -------------------------------------------------
   * Get List of Vendor Keys Belongs to User
   * -------------------------------------------------
   *
   * @returns {Observable<R>}
   */
    AuthApiConnector.prototype.getUserVendorKeys = function () {
        var userObject = app_1.App.state.user;
        var conn = new api_sys_1.ApiConnectorBackend({
            url: this.connInfo.host + this.authAndViewUserVendorKeysRoute,
            method: http_1.RequestMethod.Get,
            headers: { "AuthUser": userObject.id, "AuthKey": userObject.key, "AuthAgent": this.connKeyType }
        });
        var http = new http_1.Http(conn, new http_1.RequestOptions());
        return http.request(this.connInfo.host).map(function (response) {
            return response.json();
        });
    };
    /**
     * -------------------------------------------------
     * Get List of Vendor Keys Belongs to User
     * -------------------------------------------------
     *
     * @returns {Observable<R>}
     */
    AuthApiConnector.prototype.getUserList = function () {
        var userObject = app_1.App.state.user;
        var conn = new api_sys_1.ApiConnectorBackend({
            url: this.connInfo.host + this.authAndViewUserListRoute,
            method: http_1.RequestMethod.Get,
            headers: { "AuthUser": userObject.id, "AuthKey": userObject.key, "AuthAgent": this.connKeyType }
        });
        var http = new http_1.Http(conn, new http_1.RequestOptions());
        return http.request(this.connInfo.host).map(function (response) {
            return response.json();
        });
    };
    return AuthApiConnector;
}());
exports.AuthApiConnector = AuthApiConnector;
//# sourceMappingURL=authapiconnector.js.map