export interface ApiConnectionEntitiy {

  url: string;
  method: number;
  headers: {[name: string] : string};
  body?: {[name: string] : any};

}
