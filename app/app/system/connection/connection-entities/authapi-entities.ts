export interface AuthApiUserEntity {

  user?: number;
  email?: string;
  login?: string;
  pass?: string;
  pass_confirm?: string;
  agent?: string;
  firstName?: string;
  lastName?: string;
  key?: string;

}

export interface AuthApiUserKeyEntity {

  user?: number;
  key?: string;

}

export interface AuthApiUserSecEntity {

  user: number;
  security: AuthApiSecurityEntity

}

export interface AuthApiSecurityEntity {

  r: boolean;
  w: boolean;
  x: boolean;
  gid: number;
  lid: string;
  gid1: number;
  lid1: number;
  gid2: number;
  lid2: number;

}

