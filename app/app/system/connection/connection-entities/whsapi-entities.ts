/** -------------------------------------------
 *
 *    Interface of Warehouse Location Entity
 *
 *  -------------------------------------------
 */
export interface WarehouseLocationEntity {

  id?: number;
  parentId?: number;
  warehouseId: number;
  name: string;
  description?: string;
  isAnchor?: boolean;
  created?: number;
  updated?: number;
  isDeleted?: boolean;
  childs?: WarehouseLocationEntity[];
  amountOnLocation?: number;
  depth?:number;

}

/** -------------------------------------------
 *
 *    Interface of Warehouse Item Entity
 *
 *  -------------------------------------------
 */
export interface WarehouseItemEntity {

  id?: number;
  locationId?: number;
  productVariationId?: number;
  quantity?: number;
  condition?: string;
  name?: string;
  status?: number;
  extra?: {};
  created?: number;
  updated?: number;
  image?:string;

}

/** -------------------------------------------
 *
 *    Interface of Warehouse Reservation
 *
 *  -------------------------------------------
 */
export interface WarehouseReservationObject {

  itemId: number,
  expires: number,
  reason: number,
  orderId: number,
  marketplaceOrderId: String,
  userId: number

}

/** -------------------------------------------
 *
 *    Construct Warehouse Reservation Entity
 *
 *  -------------------------------------------
 */
export class WarehouseReservationEntity implements WarehouseReservationObject {

  public itemId: number = null;
  public expires: number = null;
  public reason: number = null;
  public orderId: number = null;
  public marketplaceOrderId: String = "";
  public userId: number = null;

  constructor(itemId: number, expires: number = null) {

    this.itemId = itemId;
    if (expires == null){
      this.expires = (Date.now()/1000) + 86400;
    }

  }

}

export interface WarehouseItemShippingObject {
  quantity: number;
}


