"use strict";
/** -------------------------------------------
 *
 *    Construct Warehouse Reservation Entity
 *
 *  -------------------------------------------
 */
var WarehouseReservationEntity = (function () {
    function WarehouseReservationEntity(itemId, expires) {
        if (expires === void 0) { expires = null; }
        this.itemId = null;
        this.expires = null;
        this.reason = null;
        this.orderId = null;
        this.marketplaceOrderId = "";
        this.userId = null;
        this.itemId = itemId;
        if (expires == null) {
            this.expires = (Date.now() / 1000) + 86400;
        }
    }
    return WarehouseReservationEntity;
}());
exports.WarehouseReservationEntity = WarehouseReservationEntity;
//# sourceMappingURL=whsapi-entities.js.map