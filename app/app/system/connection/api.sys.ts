import {
  ConnectionBackend, Connection, Headers, Request, RequestOptions, BrowserXhr, XHRConnection
} from "@angular/http";
import {ApiConnectionEntitiy} from "./connection-entities/api-entities";
import {UserApiVendorKeysEntity} from "../entities/system.entities";
import {App} from "../app";

/** --------------------------------------------------- \
 *                  REQUEST HELPER CLASS
 *  --------------------------------------------------- */
export class ApiConnectorBackend extends ConnectionBackend {

  constructor (private request: ApiConnectionEntitiy){

  }

  createConnection():Connection {

    var hdr: Headers = new Headers(this.request.headers);

    var opt: RequestOptions = new RequestOptions(
      {url:this.request.url, method:this.request.method, headers:hdr, body:JSON.stringify(this.request.body)});

    var req: Request = new Request(opt);

    return new XHRConnection(req, new BrowserXhr());

  }

  public addVendorAuth(){
      if(this.request.headers === null){
        this.request.headers = {};
      }
      var vkobject: UserApiVendorKeysEntity = App.state.getCurrentUserVendorKey();
      this.request.headers["AuthUser"] = vkobject.authId;
      this.request.headers["AuthKey"] = App.crypto.hmacSHA1(vkobject.authKey, vkobject.authSecret);
  }

  public addUserAuth(){

  }

}
