import {ApiRelationLink} from "../interface/config.dec";
import {AuthApiUserEntity, AuthApiUserKeyEntity, AuthApiUserSecEntity} from "./connection-entities/authapi-entities";
import {RequestOptions,RequestMethod,Http} from "@angular/http";
import {Observable} from "rxjs/Observable";
import {App} from "../app";
import {UserApiVendorKeysEntity, UserApiUserEntity} from "../entities/system.entities";
import {ApiConnectorBackend} from "./api.sys";

/** ------------------------------------------------------------ \
 *                AUTH API CONNECTOR CLASS
 *  ----------------------------------------------------------- */
export class AuthApiConnector {

  private connInfo: ApiRelationLink;
  private connKeyType: string;

  private signUserWithPasswordRoute: string = "/api/v1/user/add";
  private createVendorKeyForUserRoute: string = "/api/v1/vendor/add";
  private authUserWithPasswordRoute: string = "/api/v1/user/auth";
  private authAndViewUserProfileRoute: string = "/api/v1/user/view";
  private authAndViewUserListRoute: string = "/api/v1/users/view";
  private authAndViewUserVendorKeysRoute: string = "/api/v1/user/vendorkeys";
  private authUserWithUserKeyRoute: string = "/api/v1/user/auth/key";

	/**
   * -------------------------------------------------
   * Initializer
   * -------------------------------------------------
   * @param relComponent
   * @param relKeyType
   *
   * @returns {AuthApiConnector}
   * @private
   */
  public __initWithRelComponent(relComponent: ApiRelationLink, relKeyType: string) : AuthApiConnector {
    this.connInfo = relComponent;
    this.connKeyType = relKeyType;
    return this;
  }

	/**
   * -------------------------------------------------
   * Signup process
   * -------------------------------------------------
   * @param user
   */
  public signupUser(user: AuthApiUserEntity) {



  }

  /**
   * -------------------------------------------------
   * Authorize User by Passing User Object to Request
   * -------------------------------------------------
   * @param user
   * @returns {Observable<R>}
   */
  public authorizeUser(user: AuthApiUserEntity) : Observable<AuthApiUserKeyEntity> {

      var conn: ApiConnectorBackend = new ApiConnectorBackend({
          url:this.connInfo.host+this.authUserWithPasswordRoute,
          method: RequestMethod.Post,
          headers: null,
          body: user
      });

      var http = new Http(conn, new RequestOptions());

      return http.request(this.connInfo.host).map(function (response) {
        return response.json();
      });

  }

	/**
   * -------------------------------------------------
   * Check user key against API for security creds
   * -------------------------------------------------
   *
   * @returns {Observable<R>}
   */
  public checkUserAuthorization() : Observable<AuthApiUserSecEntity> {

    var userObject = App.state.user;

    var conn: ApiConnectorBackend = new ApiConnectorBackend({
      url:this.connInfo.host+this.authUserWithUserKeyRoute,
      method: RequestMethod.Get,
      headers: {"AuthUser":userObject.id,"AuthKey":userObject.key,"AuthAgent":this.connKeyType}
    });

    var http = new Http(conn, new RequestOptions());

    return http.request(this.connInfo.host).map(function (response) {
      return response.json();
    });

  }

	/**
   *  -------------------------------------------------
   *  Logout User
   *  -------------------------------------------------
   *
   */
  public logoutUser() {
    App.state.__setUnauthorized();
  }

	/**
   *  -------------------------------------------------
   *  Get User Info
   *  -------------------------------------------------
   */
  public getUserInfo(userKey: AuthApiUserKeyEntity) : Observable<AuthApiUserEntity> {

    var conn: ApiConnectorBackend = new ApiConnectorBackend({
      url:this.connInfo.host+this.authAndViewUserProfileRoute,
      method: RequestMethod.Get,
      headers: {"AuthUser":userKey.user,"AuthKey":userKey.key,"AuthAgent":this.connKeyType}
    });

    var http = new Http(conn, new RequestOptions());

    return http.request(this.connInfo.host).map(function (response) {
      return response.json();
    });

  }

	/**
   * -------------------------------------------------
   * Get List of Vendor Keys Belongs to User
   * -------------------------------------------------
   *
   * @returns {Observable<R>}
   */
  public getUserVendorKeys() : Observable<UserApiVendorKeysEntity[]> {

    var userObject = App.state.user;

    var conn: ApiConnectorBackend = new ApiConnectorBackend({
      url:this.connInfo.host+this.authAndViewUserVendorKeysRoute,
      method: RequestMethod.Get,
      headers: {"AuthUser":userObject.id,"AuthKey":userObject.key,"AuthAgent":this.connKeyType}
    });

    var http = new Http(conn, new RequestOptions());

    return http.request(this.connInfo.host).map(function (response) {
      return response.json();
    });

  }

  /**
   * -------------------------------------------------
   * Get List of Vendor Keys Belongs to User
   * -------------------------------------------------
   *
   * @returns {Observable<R>}
   */
  public getUserList() : Observable<UserApiUserEntity[]> {

    var userObject = App.state.user;

    var conn: ApiConnectorBackend = new ApiConnectorBackend({
      url:this.connInfo.host+this.authAndViewUserListRoute,
      method: RequestMethod.Get,
      headers: {"AuthUser":userObject.id,"AuthKey":userObject.key,"AuthAgent":this.connKeyType}
    });

    var http = new Http(conn, new RequestOptions());

    return http.request(this.connInfo.host).map(function (response) {
      return response.json();
    });

  }

}
