import {ApiRelationLink} from "../interface/config.dec";
import {Observable} from "rxjs/Observable";
import {ApiConnectorBackend} from "./api.sys";
import {RequestMethod, RequestOptions, Http} from "@angular/http";

/** ------------------------------------------------------------ \
 *                ITEM API CONNECTOR CLASS
 *  ----------------------------------------------------------- */
export class ItemApiConnector {

  private connInfo:ApiRelationLink;
  private connKeyType:string;

  /* ------------------------------------------------------------ \
   GET ITEM ROUTES
  \  ------------------------------------------------------------ */
  //// -> /api/v1/item/varstyle/31233
  private getItemByStyleId:string = "/api/v1/item/style";
  private getItemByItemId:string = "/api/v1/item/id";
  private getItemByItemVariationCodeRT:string = "/api/v1/item/varcode";
  private getItemSingleByItemVariationRT:string = "/api/v1/item/short";

  //// -> /api/v1/style/ascode/code3213
  private getItemStylesByStyleCode = "/api/v1/style/list";
  private getItemStylesByItemIdRt = "/api/v1/style/item";

  ////
  private getVariationByIdRt:string = "/api/v1/variation/id";
  private getVariationByCodeRt:string = "/api/v1/variation/code";

  /** -------------------------------------------------
   *
   *  -------------------------------------------------
   *  @param id
   *  @returns {Observable<R>}
   */
  public getVariationById(id: number) : Observable<ItemVariationEntity>  {
    var conn: ApiConnectorBackend = new ApiConnectorBackend({
      url: this.connInfo.host + this.getVariationByIdRt + "/" + id,
      method: RequestMethod.Get,
      headers: null,
      body: null
    });

    conn.addVendorAuth();

    var http = new Http(conn, new RequestOptions());

    return http.request(this.connInfo.host).map(function (response) {
      return response.json();
    });
  }

  /** -------------------------------------------------
   *
   *  -------------------------------------------------
   *  @param code
   *  @returns {Observable<R>}
   */
  public getVariationByCode(code: string) : Observable<ItemVariationEntity[]> {
    var conn: ApiConnectorBackend = new ApiConnectorBackend({
      url: this.connInfo.host + this.getVariationByCodeRt + "/" + code,
      method: RequestMethod.Get,
      headers: null,
      body: null
    });

    conn.addVendorAuth();

    var http = new Http(conn, new RequestOptions());

    return http.request(this.connInfo.host).map(function (response) {
      return response.json();
    });
  }

  /** -------------------------------------------------
   *  GET ALL STYLES BY STYLE PATTERN
   *  -------------------------------------------------
   *
   *  @param substring
   *  @returns {Observable<StyleEntity[]>}
   */
  public getStylesByStyleSubstring(substring: string) : Observable<StyleEntity[]>  {

    var conn: ApiConnectorBackend = new ApiConnectorBackend({
      url: this.connInfo.host + this.getItemStylesByStyleCode + "/" + substring,
      method: RequestMethod.Get,
      headers: null,
      body: null
    });

    conn.addVendorAuth();

    var http = new Http(conn, new RequestOptions());

    return http.request(this.connInfo.host).map(function (response) {
      return response.json();
    });

  }

  /** -------------------------------------------------
   *  GET ALL STYLES BY ITEM ID
   *  -------------------------------------------------
   *
   *  @param id
   *  @returns {Observable<StyleEntity[]>}
   */
  public getStylesByItemId(id: number) : Observable<StyleEntity[]>  {

    var conn: ApiConnectorBackend = new ApiConnectorBackend({
      url: this.connInfo.host + this.getItemStylesByStyleCode + "/" + id,
      method: RequestMethod.Get,
      headers: null,
      body: null
    });

    conn.addVendorAuth();

    var http = new Http(conn, new RequestOptions());

    return http.request(this.connInfo.host).map(function (response) {
      return response.json();
    });

  }

  /**
   * -------------------------------------------------
   * GET ALL ITEMS BY STYLE PATTERN
   * -------------------------------------------------
   * @param styleId
   * @returns {Observable<ItemEntity[]>}
   */
  public getItemsByStyleId(styleId: string) : Observable<ItemEntity[]> {

    var conn: ApiConnectorBackend = new ApiConnectorBackend({
      url: this.connInfo.host + this.getItemByStyleId + "/" + encodeURIComponent(styleId),
      method: RequestMethod.Get,
      headers: null,
      body: null
    });

    conn.addVendorAuth();

    var http = new Http(conn, new RequestOptions());

    return http.request(this.connInfo.host).map(function (response) {
      return response.json();
    });

  }

  /**
   * -------------------------------------------------
   * GET ITEMS BY VARIATION CODE
   * -------------------------------------------------
   * @param code
   * @returns {Observable<ItemEntity[]>}
   */
  public getItemsByVariationCode(code: string) : Observable<ItemEntity[]> {

    var conn: ApiConnectorBackend = new ApiConnectorBackend({
      url: this.connInfo.host + this.getItemByItemVariationCodeRT+ "/" + code,
      method: RequestMethod.Get,
      headers: null,
      body: null
    });

    conn.addVendorAuth();

    var http = new Http(conn, new RequestOptions());

    return http.request(this.connInfo.host).map(function (response) {
      return response.json();
    });

  }

  /**
   * -------------------------------------------------
   * Initializer
   * -------------------------------------------------
   * @param relComponent
   * @param relKeyType
   *
   * @returns {AuthApiConnector}
   * @private
   */
  public __initWithRelComponent(relComponent:ApiRelationLink, relKeyType:string):ItemApiConnector {
    this.connInfo = relComponent;
    this.connKeyType = relKeyType;
    return this;
  }

}

export interface ItemEntity {
  id?:number;
  title?: string;
  brand?: string;
  brandId?: number;
  category?: ItemCategoryEntity[];
  customId?: string;
  description?: ItemDescriptionEntity;
  info?: ItemInfoEntity;
  style?: StyleEntity[];
  variations?: ItemVariationEntity[];
  definedStyle?: string;
}

export interface ItemVariationEntity {
  id?: number,
  parentId?: number,
  customId?: number,
  size?:string;
  color?:string;
  info?: ItemInfoEntity;
}

export interface StyleEntity {
  name: string;
  vendor?: string;
}

export interface ItemCategoryEntity {

}

export interface ItemDescriptionEntity {
  full?: string;
  short?: string;
}

export interface ItemInfoEntity {
  bullets?: string[];
  images: ItemInfoImageContainer;
}

export interface ItemInfoImageContainer {
  imageSet: ItemInfoImageEntity[];
  titleImage: ItemInfoImageEntity;
}

export interface ItemInfoImageEntity {
  desc: string;
  id?: number;
  src: string;
  ext?: string;
}

