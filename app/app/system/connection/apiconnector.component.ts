import {SystemConfig} from "../interface/config.dec";
import 'rxjs/add/operator/map'
import 'rxjs/add/operator/toPromise'
import {AuthApiConnector} from "./authapiconnector";
import {Config} from "../../config";
import {WarehouseApiConnector} from "./whsapiconnector";
import {ItemApiConnector} from "./itemapiconnector";

export class ApiConnectorComponent {


  private static config: SystemConfig;

  /**
   *  Authorization API Connector Methods
   *
   * @returns {AuthApiConnector}
   */
  public static getAuthApiConnector() : AuthApiConnector {

    if(this.config == null){
        this.config = Config;
    }

    var connector = new AuthApiConnector();
    return connector.__initWithRelComponent(this.config.systemApiList['authApi'], this.config.systemApiKeyType);

  }

	/**
   *  Item API Connector Methods
   */
  public static getItemApiConnector() : ItemApiConnector{

    if(this.config == null){
      this.config = Config;
    }

    var connector = new ItemApiConnector();
    return connector.__initWithRelComponent(this.config.systemApiList['itemApi'], this.config.systemApiKeyType);

  }

  /**
   *
   * @returns {WarehouseApiConnector}
   */
  public static getWarehouseApiConnector() : WarehouseApiConnector {

    if(this.config == null){
      this.config = Config;
    }

    var connector = new WarehouseApiConnector();
    return connector.__initWithRelComponent(this.config.systemApiList['wrhsApi'], this.config.systemApiKeyType);

  }

}



