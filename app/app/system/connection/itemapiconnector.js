"use strict";
var api_sys_1 = require("./api.sys");
var http_1 = require("@angular/http");
/** ------------------------------------------------------------ \
 *                ITEM API CONNECTOR CLASS
 *  ----------------------------------------------------------- */
var ItemApiConnector = (function () {
    function ItemApiConnector() {
        /* ------------------------------------------------------------ \
         GET ITEM ROUTES
        \  ------------------------------------------------------------ */
        //// -> /api/v1/item/varstyle/31233
        this.getItemByStyleId = "/api/v1/item/style";
        this.getItemByItemId = "/api/v1/item/id";
        this.getItemByItemVariationCodeRT = "/api/v1/item/varcode";
        this.getItemSingleByItemVariationRT = "/api/v1/item/short";
        //// -> /api/v1/style/ascode/code3213
        this.getItemStylesByStyleCode = "/api/v1/style/list";
        this.getItemStylesByItemIdRt = "/api/v1/style/item";
        ////
        this.getVariationByIdRt = "/api/v1/variation/id";
        this.getVariationByCodeRt = "/api/v1/variation/code";
    }
    /** -------------------------------------------------
     *
     *  -------------------------------------------------
     *  @param id
     *  @returns {Observable<R>}
     */
    ItemApiConnector.prototype.getVariationById = function (id) {
        var conn = new api_sys_1.ApiConnectorBackend({
            url: this.connInfo.host + this.getVariationByIdRt + "/" + id,
            method: http_1.RequestMethod.Get,
            headers: null,
            body: null
        });
        conn.addVendorAuth();
        var http = new http_1.Http(conn, new http_1.RequestOptions());
        return http.request(this.connInfo.host).map(function (response) {
            return response.json();
        });
    };
    /** -------------------------------------------------
     *
     *  -------------------------------------------------
     *  @param code
     *  @returns {Observable<R>}
     */
    ItemApiConnector.prototype.getVariationByCode = function (code) {
        var conn = new api_sys_1.ApiConnectorBackend({
            url: this.connInfo.host + this.getVariationByCodeRt + "/" + code,
            method: http_1.RequestMethod.Get,
            headers: null,
            body: null
        });
        conn.addVendorAuth();
        var http = new http_1.Http(conn, new http_1.RequestOptions());
        return http.request(this.connInfo.host).map(function (response) {
            return response.json();
        });
    };
    /** -------------------------------------------------
     *  GET ALL STYLES BY STYLE PATTERN
     *  -------------------------------------------------
     *
     *  @param substring
     *  @returns {Observable<StyleEntity[]>}
     */
    ItemApiConnector.prototype.getStylesByStyleSubstring = function (substring) {
        var conn = new api_sys_1.ApiConnectorBackend({
            url: this.connInfo.host + this.getItemStylesByStyleCode + "/" + substring,
            method: http_1.RequestMethod.Get,
            headers: null,
            body: null
        });
        conn.addVendorAuth();
        var http = new http_1.Http(conn, new http_1.RequestOptions());
        return http.request(this.connInfo.host).map(function (response) {
            return response.json();
        });
    };
    /** -------------------------------------------------
     *  GET ALL STYLES BY ITEM ID
     *  -------------------------------------------------
     *
     *  @param id
     *  @returns {Observable<StyleEntity[]>}
     */
    ItemApiConnector.prototype.getStylesByItemId = function (id) {
        var conn = new api_sys_1.ApiConnectorBackend({
            url: this.connInfo.host + this.getItemStylesByStyleCode + "/" + id,
            method: http_1.RequestMethod.Get,
            headers: null,
            body: null
        });
        conn.addVendorAuth();
        var http = new http_1.Http(conn, new http_1.RequestOptions());
        return http.request(this.connInfo.host).map(function (response) {
            return response.json();
        });
    };
    /**
     * -------------------------------------------------
     * GET ALL ITEMS BY STYLE PATTERN
     * -------------------------------------------------
     * @param styleId
     * @returns {Observable<ItemEntity[]>}
     */
    ItemApiConnector.prototype.getItemsByStyleId = function (styleId) {
        var conn = new api_sys_1.ApiConnectorBackend({
            url: this.connInfo.host + this.getItemByStyleId + "/" + encodeURIComponent(styleId),
            method: http_1.RequestMethod.Get,
            headers: null,
            body: null
        });
        conn.addVendorAuth();
        var http = new http_1.Http(conn, new http_1.RequestOptions());
        return http.request(this.connInfo.host).map(function (response) {
            return response.json();
        });
    };
    /**
     * -------------------------------------------------
     * GET ITEMS BY VARIATION CODE
     * -------------------------------------------------
     * @param code
     * @returns {Observable<ItemEntity[]>}
     */
    ItemApiConnector.prototype.getItemsByVariationCode = function (code) {
        var conn = new api_sys_1.ApiConnectorBackend({
            url: this.connInfo.host + this.getItemByItemVariationCodeRT + "/" + code,
            method: http_1.RequestMethod.Get,
            headers: null,
            body: null
        });
        conn.addVendorAuth();
        var http = new http_1.Http(conn, new http_1.RequestOptions());
        return http.request(this.connInfo.host).map(function (response) {
            return response.json();
        });
    };
    /**
     * -------------------------------------------------
     * Initializer
     * -------------------------------------------------
     * @param relComponent
     * @param relKeyType
     *
     * @returns {AuthApiConnector}
     * @private
     */
    ItemApiConnector.prototype.__initWithRelComponent = function (relComponent, relKeyType) {
        this.connInfo = relComponent;
        this.connKeyType = relKeyType;
        return this;
    };
    return ItemApiConnector;
}());
exports.ItemApiConnector = ItemApiConnector;
//# sourceMappingURL=itemapiconnector.js.map