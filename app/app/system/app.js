"use strict";
var config_1 = require("../config");
var system_state_1 = require("./state/system.state");
var window_dispatcher_1 = require("./window/window.dispatcher");
var platform_browser_dynamic_1 = require('@angular/platform-browser-dynamic');
var input_observer_1 = require("./input/input.observer");
var crypto_sys_1 = require("./crypto/crypto.sys");
var printing_sys_1 = require("./print/printing.sys");
var codes_sys_1 = require("./codes/codes.sys");
// ------------------------------------------------------------------------------
//                      Application helper class
// ------------------------------------------------------------------------------
var App = (function () {
    function App() {
    }
    /** ---------------------------------------------------------------------------
   *
   *
   *  ---------------------------------------------------------------------------
   *
   *  @param components
   *  @returns {Promise<App>|Promise}
   *
   */
    App.__init = function (components) {
        var _this = this;
        this._systemState = new system_state_1.SystemState();
        this._inputScannerObserver = new input_observer_1.InputScannerObserver();
        this._cryptoModule = new crypto_sys_1.CryptoSystem();
        this._printSystem = new printing_sys_1.PrintSystem();
        this._codesSystem = new codes_sys_1.CodesSystem();
        this._printSystem.init();
        return new Promise(function (resolve, reject) {
            platform_browser_dynamic_1.bootstrap(window_dispatcher_1.WindowDispatcher).then(function (ref) {
                _this._windowManager = ref.instance;
                if (components !== null) {
                    _this._windowManager.registerComponents(components);
                }
                resolve(_this);
            }, function (err) {
                reject(err);
            });
        });
    };
    /**
     * --------------------------
     * Return Api Endpoints List
     * --------------------------
     *
     * @returns {{}}
     */
    App.getSysApiCredentialList = function () {
        return config_1.Config.systemApiList;
    };
    Object.defineProperty(App, "window", {
        /** ---------------------------------------------------------------------------
       *
       *  ---------------------------------------------------------------------------
       *
       * @returns {WindowDispatcher}
       */
        get: function () {
            return this._windowManager;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(App, "state", {
        /** ---------------------------------------------------------------------------
       *
       *  ---------------------------------------------------------------------------
       *
       * @returns {SystemState}
       */
        get: function () {
            return this._systemState;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(App, "inputScannerObserver", {
        /** ---------------------------------------------------------------------------
       *
       *  ---------------------------------------------------------------------------
       *
       * @returns {InputScannerObserver}
       */
        get: function () {
            return this._inputScannerObserver;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(App, "crypto", {
        /** ---------------------------------------------------------------------------
       *
       *  ---------------------------------------------------------------------------
       *
       * @returns {CryptoSystem}
       */
        get: function () {
            return this._cryptoModule;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(App, "print", {
        /** ---------------------------------------------------------------------------
       *
       *  ---------------------------------------------------------------------------
       *
       * @returns {PrintSystem}
       */
        get: function () {
            return this._printSystem;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(App, "codes", {
        /** ---------------------------------------------------------------------------
       *
       *  ---------------------------------------------------------------------------
       *
       * @returns {CodesSystem}
       */
        get: function () {
            return this._codesSystem;
        },
        enumerable: true,
        configurable: true
    });
    /** --------------------------------------------------------------------------
     *
     *                          RFC 4122 v4 GUID Generator
     *
     *  --------------------------------------------------------------------------
     * @returns {string}
     */
    App.newGUID = function () {
        var ga = [];
        // Generate FF Hex array
        for (var i = 0; i < 256; i++) {
            ga[i] = (i < 16 ? '0' : '') + (i).toString(16);
        }
        // Generate 4 random numbers for 4 sections of GUID
        var rSec1 = Math.random() * 0xFFFFFFFF | 0;
        var rSec2 = Math.random() * 0xFFFFFFFF | 0;
        var rSec3 = Math.random() * 0xFFFFFFFF | 0;
        var rSec4 = Math.random() * 0xFFFFFFFF | 0;
        // Fill array with each 8 bit sets of 32 bit value
        // Note that section 2 & 3 having some control bits
        return ga[rSec1 & 0xFF] + ga[rSec2 >> 8 & 0xFF] + ga[rSec3 >> 16 & 0xFF] + ga[rSec4 >> 24 & 0xFF] +
            '-' + ga[rSec2 & 0xFF] + ga[rSec2 >> 8 & 0xFF] + '-' + ga[rSec2 >> 16 & 0x0F | 0x40] + ga[rSec2 >> 24 & 0xFF] +
            '-' + ga[rSec3 & 0x3F | 0x80] + ga[rSec3 >> 8 & 0xFF] + '-' + ga[rSec3 >> 16 & 0xFF] + ga[rSec3 >> 24 & 0xFF] +
            '-' + ga[rSec4 & 0xFF] + ga[rSec4 >> 8 & 0xFF] + ga[rSec4 >> 16 & 0xFF] + ga[rSec4 >> 24 & 0xFF];
    };
    /** -------------------------------------------------
     *  Clone object
     *  -------------------------------------------------
     *
     * @param someObject
     * @returns {{}}
     */
    App.cloneObjectWithAttributes = function (someObject) {
        var cloneObj = {};
        for (var attr in someObject) {
            if (typeof someObject[attr] === "object") {
                cloneObj[attr] = this.cloneObjectWithAttributes(someObject[attr]);
            }
            else {
                cloneObj[attr] = someObject[attr];
            }
        }
        return cloneObj;
    };
    return App;
}());
exports.App = App;
//# sourceMappingURL=app.js.map