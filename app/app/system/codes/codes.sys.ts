/** ---------------------------------------------------------------------------
 *
 *
 *
 *  ---------------------------------------------------------------------------
 *
 */
export class CodesSystem {

  private maxDigits = 14;

  private locationPrefix: string = "l-";

  private itemPrefix: string = "i-";

	/** ---------------------------------------------------------------------------
   *
   *
   *
   *  ---------------------------------------------------------------------------
   *
   *  @param intCode
   *  @returns {string}
   */
  public generateCodeFromInt(intCode: number) : string {

    var str = intCode.toString();
    var add = this.maxDigits - str.length;

    var mstr = "";
    for(var i=0; i < add; i++){
      mstr += "0";
    }

    return mstr + str;

  }

	/** ---------------------------------------------------------------------------
   *
   *
   *
   *  ---------------------------------------------------------------------------
   *
   *  @param intCode
   *  @returns {string}
   */
  public convertIntToLocationId(intCode: number): string {

    return this.locationPrefix + this.generateCodeFromInt(intCode);

  }

	/** ---------------------------------------------------------------------------
   *
   *
   *
   *  ---------------------------------------------------------------------------
   *
   *  @param intCode
   *  @returns {string}
   */
  public convertIntToItemId(intCode: number): string {

    return this.itemPrefix + this.generateCodeFromInt(intCode);

  }

	/** ---------------------------------------------------------------------------
   *
   *
   *
   *  ---------------------------------------------------------------------------
   *
   *  @param string
   *  @returns {CodesEntity}
   */
  public convertStringToTypedInt(string: string): CodesEntity {

    var stra = this.strToArray(string);
    var chk = this.checkStr(stra);

    var entity: CodesEntity = new CodesEntity();

    entity.type = chk.t;
    entity.id = parseInt(chk.i);

    return entity;

  }

  /** ---------------------------------------------------------------------------
   *
   *
   *
   *  ---------------------------------------------------------------------------
   *
   *  @param string
   *  @returns {CodesEntity}
   */
  public convertStringToTypedString(string: string): CodesEntity {

    var stra = this.strToArray(string);
    var chk = this.checkStr(stra);

    var entity: CodesEntity = new CodesEntity();

    entity.type = chk.t;
    entity.id = chk.i;

    return entity;

  }

	/**
   *
   *  @param string
   *  @returns {{a: (any|string[]), f: (any|string), s: (any|string)}}
   */
  private strToArray(string): Object {

    var str = string.split("");
    return {a:str,f:str[0],s:str[1]};

  }

	/**
   *
   *  @param stringObject
   *  @returns {{t: number, i: string}}
   */
  private checkStr(stringObject: any) {

    var type: number = 0;
    var id: string = "";

    if(stringObject.s == "-") {

      if(stringObject.f == "l" || stringObject.f == "L") {

        type = CodesTypes.LocationType;

      } else if(stringObject.f == "i" || stringObject.f == "I") {

        type = CodesTypes.ItemType;

      } else {

        type = CodesTypes.NoType;

      }

      id = stringObject.a.slice(2).join("");

    } else {

      type = CodesTypes.NoType;
      id = stringObject.a.join("");

    }

    return {t:type,i:id};

  }

}

/** ---------------------------------------------------------------------------
 *
 *
 *
 *  ---------------------------------------------------------------------------
 *
 */
export class CodesEntity {

  private _type: CodesTypes;

  private _id: string|number;

	get type():CodesTypes {
    return this._type;
  }

  set type(value:CodesTypes){
      this._type=value;
	  }

  get id():string|number{
      return this._id;
	  }

  set id(value:string|number){
      this._id=value;
	  }
}

/** ---------------------------------------------------------------------------
 *
 *
 *
 *  ---------------------------------------------------------------------------
 *
 */
export enum CodesTypes {
  NoType,
  LocationType,
  ItemType
}
