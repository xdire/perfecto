"use strict";
/** ---------------------------------------------------------------------------
 *
 *
 *
 *  ---------------------------------------------------------------------------
 *
 */
var CodesSystem = (function () {
    function CodesSystem() {
        this.maxDigits = 14;
        this.locationPrefix = "l-";
        this.itemPrefix = "i-";
    }
    /** ---------------------------------------------------------------------------
   *
   *
   *
   *  ---------------------------------------------------------------------------
   *
   *  @param intCode
   *  @returns {string}
   */
    CodesSystem.prototype.generateCodeFromInt = function (intCode) {
        var str = intCode.toString();
        var add = this.maxDigits - str.length;
        var mstr = "";
        for (var i = 0; i < add; i++) {
            mstr += "0";
        }
        return mstr + str;
    };
    /** ---------------------------------------------------------------------------
   *
   *
   *
   *  ---------------------------------------------------------------------------
   *
   *  @param intCode
   *  @returns {string}
   */
    CodesSystem.prototype.convertIntToLocationId = function (intCode) {
        return this.locationPrefix + this.generateCodeFromInt(intCode);
    };
    /** ---------------------------------------------------------------------------
   *
   *
   *
   *  ---------------------------------------------------------------------------
   *
   *  @param intCode
   *  @returns {string}
   */
    CodesSystem.prototype.convertIntToItemId = function (intCode) {
        return this.itemPrefix + this.generateCodeFromInt(intCode);
    };
    /** ---------------------------------------------------------------------------
   *
   *
   *
   *  ---------------------------------------------------------------------------
   *
   *  @param string
   *  @returns {CodesEntity}
   */
    CodesSystem.prototype.convertStringToTypedInt = function (string) {
        var stra = this.strToArray(string);
        var chk = this.checkStr(stra);
        var entity = new CodesEntity();
        entity.type = chk.t;
        entity.id = parseInt(chk.i);
        return entity;
    };
    /** ---------------------------------------------------------------------------
     *
     *
     *
     *  ---------------------------------------------------------------------------
     *
     *  @param string
     *  @returns {CodesEntity}
     */
    CodesSystem.prototype.convertStringToTypedString = function (string) {
        var stra = this.strToArray(string);
        var chk = this.checkStr(stra);
        var entity = new CodesEntity();
        entity.type = chk.t;
        entity.id = chk.i;
        return entity;
    };
    /**
   *
   *  @param string
   *  @returns {{a: (any|string[]), f: (any|string), s: (any|string)}}
   */
    CodesSystem.prototype.strToArray = function (string) {
        var str = string.split("");
        return { a: str, f: str[0], s: str[1] };
    };
    /**
   *
   *  @param stringObject
   *  @returns {{t: number, i: string}}
   */
    CodesSystem.prototype.checkStr = function (stringObject) {
        var type = 0;
        var id = "";
        if (stringObject.s == "-") {
            if (stringObject.f == "l" || stringObject.f == "L") {
                type = CodesTypes.LocationType;
            }
            else if (stringObject.f == "i" || stringObject.f == "I") {
                type = CodesTypes.ItemType;
            }
            else {
                type = CodesTypes.NoType;
            }
            id = stringObject.a.slice(2).join("");
        }
        else {
            type = CodesTypes.NoType;
            id = stringObject.a.join("");
        }
        return { t: type, i: id };
    };
    return CodesSystem;
}());
exports.CodesSystem = CodesSystem;
/** ---------------------------------------------------------------------------
 *
 *
 *
 *  ---------------------------------------------------------------------------
 *
 */
var CodesEntity = (function () {
    function CodesEntity() {
    }
    Object.defineProperty(CodesEntity.prototype, "type", {
        get: function () {
            return this._type;
        },
        set: function (value) {
            this._type = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(CodesEntity.prototype, "id", {
        get: function () {
            return this._id;
        },
        set: function (value) {
            this._id = value;
        },
        enumerable: true,
        configurable: true
    });
    return CodesEntity;
}());
exports.CodesEntity = CodesEntity;
/** ---------------------------------------------------------------------------
 *
 *
 *
 *  ---------------------------------------------------------------------------
 *
 */
(function (CodesTypes) {
    CodesTypes[CodesTypes["NoType"] = 0] = "NoType";
    CodesTypes[CodesTypes["LocationType"] = 1] = "LocationType";
    CodesTypes[CodesTypes["ItemType"] = 2] = "ItemType";
})(exports.CodesTypes || (exports.CodesTypes = {}));
var CodesTypes = exports.CodesTypes;
//# sourceMappingURL=codes.sys.js.map