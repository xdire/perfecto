"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var system_entities_1 = require("../entities/system.entities");
var app_1 = require("../app");
var window_component_1 = require("../window/window.component");
var MenuComponent = (function (_super) {
    __extends(MenuComponent, _super);
    function MenuComponent(zone) {
        this.zone = zone;
        this._menuTitleText = "Some Title";
        this.bgStyle = true;
        this.visible = true;
        this._width = 0;
        this._height = 0;
        this.selected = null;
        this.menuList = [
            {
                shortTitleName: "D",
                titleName: 'Dashboard',
                className: 'fa fa-fw fa-dashboard',
                normalType: system_entities_1.MenuItemEntityRenderType.Title,
                shortType: system_entities_1.MenuItemEntityRenderType.Title,
                windowToLoad: "dashboardComponent",
                selected: false
            },
            {
                shortTitleName: "U",
                titleName: "Users",
                className: 'fa fa-fw fa-users',
                normalType: system_entities_1.MenuItemEntityRenderType.Title,
                shortType: system_entities_1.MenuItemEntityRenderType.Title,
                windowToLoad: "userComponent",
                selected: false
            },
            {
                shortTitleName: "W",
                titleName: "Warehouse",
                className: 'fa fa-fw fa-cubes',
                normalType: system_entities_1.MenuItemEntityRenderType.Title,
                shortType: system_entities_1.MenuItemEntityRenderType.Title,
                windowToLoad: "warehouseComponent",
                selected: false
            },
            {
                shortTitleName: "I",
                titleName: "Items",
                className: 'fa fa-fw fa-cube',
                normalType: system_entities_1.MenuItemEntityRenderType.Title,
                shortType: system_entities_1.MenuItemEntityRenderType.Title,
                windowToLoad: null,
                selected: false
            },
            {
                shortTitleName: "L",
                titleName: "Logout",
                className: 'fa fa-fw fa-power-off ',
                normalType: system_entities_1.MenuItemEntityRenderType.Title,
                shortType: system_entities_1.MenuItemEntityRenderType.Title,
                windowToLoad: "loginComponent",
                action: "logout",
                selected: false
            },
        ];
    }
    MenuComponent.prototype.setSize = function (size) {
        this._width = size.width;
        this._height = size.height;
    };
    MenuComponent.prototype.hide = function () {
        this.visible = false;
        this.zone.run(function () { });
    };
    MenuComponent.prototype.show = function () {
        this.visible = true;
        this.zone.run(function () { });
    };
    Object.defineProperty(MenuComponent.prototype, "isVisible", {
        get: function () {
            return this.visible;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MenuComponent.prototype, "width", {
        get: function () {
            return this._width;
        },
        set: function (value) {
            this._width = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MenuComponent.prototype, "height", {
        get: function () {
            return this._height;
        },
        set: function (value) {
            this._height = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MenuComponent.prototype, "menuTitleText", {
        get: function () {
            return this._menuTitleText;
        },
        set: function (value) {
            this._menuTitleText = value;
            this.zone.run(function () { });
        },
        enumerable: true,
        configurable: true
    });
    MenuComponent.prototype.selectMenuItemWithIndex = function (index) {
    };
    MenuComponent.prototype.selectMenyItemWithWindowComponent = function (windowComponent) {
        var _this = this;
        this.menuList.map(function (data, index) {
            if (data.windowToLoad == windowComponent) {
                _this.selected = index;
                data.selected = true;
            }
        });
    };
    MenuComponent.prototype.clickMenuItem = function ($event, menuItemIndex) {
        if (this.selected != menuItemIndex) {
            var item = this.menuList[menuItemIndex];
            if (item.hasOwnProperty("action")) {
                var action = item['action'];
                if (action == "logout") {
                    app_1.App.state.__setUnauthorized();
                    console.log(app_1.App.state);
                }
            }
            app_1.App.window.loadWindowComponent(item.windowToLoad);
        }
    };
    __decorate([
        core_1.Input(), 
        __metadata('design:type', String)
    ], MenuComponent.prototype, "_menuTitleText", void 0);
    MenuComponent = __decorate([
        core_1.Component({
            selector: "menuframe",
            host: {
                '[class]': 'visible?"normalFrame":"hiddenFrame"'
            },
            template: "\n    <div class=\"menuContainer\"\n    [class.menuframeBgr]=\"bgStyle\"\n    [style.display]=\"isVisible?'inline-block':'none'\">\n      <div class=\"menuTitleContainer\">\n        <h3> {{menuTitleText}} </h3>\n      </div>\n      <ul>\n         <li *ngFor=\"let m of menuList; let i = index\" (click)=\"clickMenuItem($event,i)\">\n          <a name=\"{{i}}\"> <i [class]=\"m.className\">\n                           </i> {{m.titleName}} </a>\n         </li>\n      </ul>\n    </div>\n  ",
            styles: [
                "\n      .menuContainer {\n        width: 100%;\n        height: 100%;\n        box-shadow: inset -10px 0 32px rgba(0,0,0,0.1);\n        color: #FFFFFF;\n      }\n      .menuTitleContainer {\n        width:100%; \n        height: 96px;\n        padding: 4px;\n      }\n      .menuContainer ul {\n          width: 100%; list-style-type: none; padding: 0px !important;\n      }\n      .menuContainer ul li {\n          width: 100%;\n          margin:0 !important;\n          padding: 10px 4px 10px 8px !important;\n          cursor: pointer;\n      }\n      .menuContainer ul li:hover {\n          background-color:rgba(32,120,200,0.2);\n      }\n      .menuContainer ul li a {\n          color: #FFFFFF;\n          font-size: 1.0em;\n          text-decoration: none;\n      }\n    "
            ]
        }), 
        __metadata('design:paramtypes', [core_1.NgZone])
    ], MenuComponent);
    return MenuComponent;
}(window_component_1.WindowComponent));
exports.MenuComponent = MenuComponent;
//# sourceMappingURL=menu.component.js.map