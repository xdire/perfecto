import {Component, NgZone, Input} from "@angular/core";
import {WindowSizeEntity} from "../window/window.dispatcher";
import {MenuItemEntity, MenuItemEntityRenderType} from "../entities/system.entities";
import {App} from "../app";
import {WindowComponent} from "../window/window.component";

@Component({
  selector: "menuframe",
  host: {
    '[class]': 'visible?"normalFrame":"hiddenFrame"'
  },
  template: `
    <div class="menuContainer"
    [class.menuframeBgr]="bgStyle"
    [style.display]="isVisible?'inline-block':'none'">
      <div class="menuTitleContainer">
        <h3> {{menuTitleText}} </h3>
      </div>
      <ul>
         <li *ngFor="let m of menuList; let i = index" (click)="clickMenuItem($event,i)">
          <a name="{{i}}"> <i [class]="m.className">
                           </i> {{m.titleName}} </a>
         </li>
      </ul>
    </div>
  `,
  styles: [
    `
      .menuContainer {
        width: 100%;
        height: 100%;
        box-shadow: inset -10px 0 32px rgba(0,0,0,0.1);
        color: #FFFFFF;
      }
      .menuTitleContainer {
        width:100%; 
        height: 96px;
        padding: 4px;
      }
      .menuContainer ul {
          width: 100%; list-style-type: none; padding: 0px !important;
      }
      .menuContainer ul li {
          width: 100%;
          margin:0 !important;
          padding: 10px 4px 10px 8px !important;
          cursor: pointer;
      }
      .menuContainer ul li:hover {
          background-color:rgba(32,120,200,0.2);
      }
      .menuContainer ul li a {
          color: #FFFFFF;
          font-size: 1.0em;
          text-decoration: none;
      }
    `
  ]
})

export class MenuComponent extends WindowComponent {

  @Input() private _menuTitleText: string = "Some Title";

  private menuList: MenuItemEntity[];

  private bgStyle:boolean = true;

  private visible: boolean = true;

  private _width: number = 0;

  private _height: number = 0;

  private selected: number = null;

  constructor(private zone: NgZone) {

    this.menuList = [
      {
        shortTitleName: "D",
        titleName: 'Dashboard',
        className: 'fa fa-fw fa-dashboard',
        normalType: MenuItemEntityRenderType.Title,
        shortType: MenuItemEntityRenderType.Title,
        windowToLoad: "dashboardComponent",
        selected: false
      },
      {
        shortTitleName: "U",
        titleName: "Users",
        className: 'fa fa-fw fa-users',
        normalType: MenuItemEntityRenderType.Title,
        shortType: MenuItemEntityRenderType.Title,
        windowToLoad: "userComponent",
        selected: false
      },
      {
        shortTitleName: "W",
        titleName: "Warehouse",
        className: 'fa fa-fw fa-cubes',
        normalType: MenuItemEntityRenderType.Title,
        shortType: MenuItemEntityRenderType.Title,
        windowToLoad: "warehouseComponent",
        selected: false
      },
      {
        shortTitleName: "I",
        titleName: "Items",
        className: 'fa fa-fw fa-cube',
        normalType: MenuItemEntityRenderType.Title,
        shortType: MenuItemEntityRenderType.Title,
        windowToLoad: null,
        selected: false
      },
      {
        shortTitleName: "L",
        titleName: "Logout",
        className: 'fa fa-fw fa-power-off ',
        normalType: MenuItemEntityRenderType.Title,
        shortType: MenuItemEntityRenderType.Title,
        windowToLoad: "loginComponent",
        action: "logout",
        selected: false
      },
    ];

  }

  public setSize(size: WindowSizeEntity) {
    this._width = size.width;
    this._height = size.height;
  }

  public hide() {
    this.visible = false;
    this.zone.run(()=>{});
  }

  public show() {
    this.visible = true;
    this.zone.run(()=>{});
  }

  get isVisible() {
    return this.visible;
  }

	get width():number {
    return this._width;
  }

  get height():number {
      return this._height;
  }

	set width(value:number) {
	    this._width=value;
  }

  set height(value:number) {
      this._height=value;
  }

	set menuTitleText(value:string){
    this._menuTitleText=value;
    this.zone.run(()=>{});
  }

	get menuTitleText():string{
    return this._menuTitleText;
  }

  selectMenuItemWithIndex(index: number) {

  }

  selectMenyItemWithWindowComponent(windowComponent: string) {
    
    this.menuList.map((data,index) => {

      if(data.windowToLoad == windowComponent){
        this.selected = index;
        data.selected = true;
      }

    });
    
  }

  clickMenuItem($event, menuItemIndex: number) {

    if(this.selected != menuItemIndex) {

      var item:MenuItemEntity = this.menuList[menuItemIndex];
      if (item.hasOwnProperty("action")) {
        var action:string = item['action'];
        if (action == "logout") {
          App.state.__setUnauthorized();
          console.log(App.state);
        }
      }

      App.window.loadWindowComponent(item.windowToLoad);

    }

  }

}
