import {Component} from "@angular/core";
import {WindowComponent} from "../window/window.component";

@Component({
  selector: "bar",
  host: {
    'class': '"navbar navbar-default bar-fixed"',
    '[class.bar-fixed-float]': 'isFloat',
    '[class.hiddenFrame]': '!_isVisible'
  },
  template: `
    <div class="container"
    [style.display]="isVisible?'inline-block':'none'"
    >
      
    </div>
  `
})

export class BarComponent extends WindowComponent {

  public height: number = 0;

  public width: number = 0;

  public isFloat: boolean = false;

  private _isVisible: boolean = false;

  public show() {
    this._isVisible = true;
  }

  public hide() {
    this._isVisible = false;
  }

	get isVisible():boolean {
    return this._isVisible;
  }

}
