"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var window_component_1 = require("../window/window.component");
var AttachableMenuComponent = (function () {
    function AttachableMenuComponent() {
        this._isVertical = true;
        this._isVisible = true;
        this._defaultThickness = 64;
    }
    AttachableMenuComponent.prototype.executeAction = function ($event, number) {
        var callable = this.menuItemsList[number].callable;
        callable($event, number);
    };
    Object.defineProperty(AttachableMenuComponent.prototype, "isVertical", {
        /** ---------------------------------- \
       *  Vertical / Horizontal Orientation
       *  ----------------------------------
       * getter
       * setter
       *
       * @returns {boolean}
       \
       /
       */
        get: function () {
            return this._isVertical;
        },
        /** */
        set: function (value) {
            this._isVertical = value;
        },
        enumerable: true,
        configurable: true
    });
    __decorate([
        core_1.Input(), 
        __metadata('design:type', Array)
    ], AttachableMenuComponent.prototype, "menuItemsList", void 0);
    __decorate([
        core_1.Input(), 
        __metadata('design:type', window_component_1.WindowComponent)
    ], AttachableMenuComponent.prototype, "menuBindedComponent", void 0);
    __decorate([
        core_1.Input(), 
        __metadata('design:type', Boolean)
    ], AttachableMenuComponent.prototype, "_isVertical", void 0);
    __decorate([
        core_1.Input(), 
        __metadata('design:type', Boolean)
    ], AttachableMenuComponent.prototype, "_isVisible", void 0);
    AttachableMenuComponent = __decorate([
        core_1.Component({
            selector: "menubar",
            host: {
                '[style.height]': 'isVertical?"100%":_defaultThickness',
                '[style.width]': 'isVertical?_defaultThickness:"100%"',
                '[class.hiddenFrame]': '!_isVisible',
                '[class.topFrame]': '!_isVertical',
                '[class.sideFrame]': '_isVertical',
            },
            template: "\n    <div class=\"menuBarDefault\" [class.mbVo]=\"_isVertical\" [class.mbHo]=\"!_isVertical\">\n      <ul style=\"list-style-type: none; margin: 0; padding: 0;\">\n        <li class=\"mbMi\" *ngFor=\"let menu of menuItemsList; let i = index\" [class.mbMiHo]=\"!_isVertical\" (click)=\"executeAction($event,i)\">\n          <i [class.hiddenFrame]=\"menu.className?.length == 0\" class=\"{{menu.className}}\"> </i>\n          <span [class.hiddenFrame]=\"menu.className?.length > 0\"> {{menu.title}} </span>\n        </li>\n      </ul>\n    </div>\n  ",
            styles: ["\n    .mbVo {\n      overflow-y:scroll; width:100%; height:100%;\n    }\n    .mbHo {\n      overflow-x:scroll; width:100%; height:100%;\n    }\n    .mbMi {\n      width: 64px;\n      height: 64px;\n      padding-top: 16px;\n      font-size: 24px;\n      text-align: center;\n      overflow: hidden;\n      display: block;\n      cursor: pointer;\n    }\n    .mbMiHo {\n      display: inline-block !important; \n    }\n  "]
        }), 
        __metadata('design:paramtypes', [])
    ], AttachableMenuComponent);
    return AttachableMenuComponent;
}());
exports.AttachableMenuComponent = AttachableMenuComponent;
//# sourceMappingURL=menu.att.component.js.map