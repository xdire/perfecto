import {Component, Input} from "@angular/core";
import {WindowComponent} from "../window/window.component";

@Component({
  selector: "menubar",
  host: {
    '[style.height]':'isVertical?"100%":_defaultThickness',
    '[style.width]':'isVertical?_defaultThickness:"100%"',
    '[class.hiddenFrame]':'!_isVisible',
    '[class.topFrame]':'!_isVertical',
    '[class.sideFrame]':'_isVertical',
  },
  template: `
    <div class="menuBarDefault" [class.mbVo]="_isVertical" [class.mbHo]="!_isVertical">
      <ul style="list-style-type: none; margin: 0; padding: 0;">
        <li class="mbMi" *ngFor="let menu of menuItemsList; let i = index" [class.mbMiHo]="!_isVertical" (click)="executeAction($event,i)">
          <i [class.hiddenFrame]="menu.className?.length == 0" class="{{menu.className}}"> </i>
          <span [class.hiddenFrame]="menu.className?.length > 0"> {{menu.title}} </span>
        </li>
      </ul>
    </div>
  `,
  styles:[`
    .mbVo {
      overflow-y:scroll; width:100%; height:100%;
    }
    .mbHo {
      overflow-x:scroll; width:100%; height:100%;
    }
    .mbMi {
      width: 64px;
      height: 64px;
      padding-top: 16px;
      font-size: 24px;
      text-align: center;
      overflow: hidden;
      display: block;
      cursor: pointer;
    }
    .mbMiHo {
      display: inline-block !important; 
    }
  `]
})

export class AttachableMenuComponent {

  @Input() menuItemsList: Array<AttachableMenuItem>;

  @Input() menuBindedComponent: WindowComponent;

  @Input() private _isVertical: boolean = true;

  @Input() private _isVisible: boolean = true;

  private _defaultThickness: number = 64;

  constructor() {

  }

  public executeAction($event,number) {
    var callable = this.menuItemsList[number].callable;
    callable($event, number);
  }

	/** ---------------------------------- \
   *  Vertical / Horizontal Orientation
   *  ----------------------------------
   * getter
   * setter
   *
   * @returns {boolean}
   \
   /
   */
  get isVertical():boolean {
    return this._isVertical;
  }
  /** */
  set isVertical(value:boolean){
    this._isVertical=value;
  }
  /** ---------------------------------- */

}

export interface AttachableMenuItem {

  className?: string;
  title?: string;
  callable(event?: any, cellId?: number);

}
