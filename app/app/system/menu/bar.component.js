"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var window_component_1 = require("../window/window.component");
var BarComponent = (function (_super) {
    __extends(BarComponent, _super);
    function BarComponent() {
        _super.apply(this, arguments);
        this.height = 0;
        this.width = 0;
        this.isFloat = false;
        this._isVisible = false;
    }
    BarComponent.prototype.show = function () {
        this._isVisible = true;
    };
    BarComponent.prototype.hide = function () {
        this._isVisible = false;
    };
    Object.defineProperty(BarComponent.prototype, "isVisible", {
        get: function () {
            return this._isVisible;
        },
        enumerable: true,
        configurable: true
    });
    BarComponent = __decorate([
        core_1.Component({
            selector: "bar",
            host: {
                'class': '"navbar navbar-default bar-fixed"',
                '[class.bar-fixed-float]': 'isFloat',
                '[class.hiddenFrame]': '!_isVisible'
            },
            template: "\n    <div class=\"container\"\n    [style.display]=\"isVisible?'inline-block':'none'\"\n    >\n      \n    </div>\n  "
        }), 
        __metadata('design:paramtypes', [])
    ], BarComponent);
    return BarComponent;
}(window_component_1.WindowComponent));
exports.BarComponent = BarComponent;
//# sourceMappingURL=bar.component.js.map