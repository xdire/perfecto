declare var jsSHA: any;

export class SHA1Encyption {

  public getHmacSHA1(key, salt) {

    //let jsSHA = require("../../../node_modules/jssha/src/sha1.js");
    //return CryptoJS.HmacSHA1(key, salt);
    var shaObj = new jsSHA("SHA-1", 'TEXT');
    shaObj.setHMACKey(salt, "TEXT");
    shaObj.update(key);
    //console.log(shaObj.getHMAC("HEX"));
    return shaObj.getHMAC("HEX")
  }

}

/*/!**
 * Generate HMAC SHA-1
 *!/
 public __getHmacSHA1(key, salt)
 {
 var bkey = this.strToBigEndian(key);
 if(bkey.length > 16) bkey = this.sha1OfBigEndian(bkey, key.length * 8);

 var ipad = Array(16), opad = Array(16);

 for(var i = 0; i < 16; i++)
 {
 ipad[i] = bkey[i] ^ 0x36363636;
 opad[i] = bkey[i] ^ 0x5C5C5C5C;
 }

 var hash = this.sha1OfBigEndian(ipad.concat(this.strToBigEndian(salt)), 512 + salt.length * 8);
 console.log("SHA1 Generation");
 console.log(hash);
 console.log(this.strToHex(this.arrayToLittleEndianStr(this.sha1OfBigEndian(opad.concat(hash), 512 + 160))));
 return this.strToHex(this.arrayToLittleEndianStr(this.sha1OfBigEndian(opad.concat(hash), 512 + 160)));

 }

 private strToHex(string) {
 var hex_tab = "0123456789abcdef";
 var output = "";
 var x;
 for(var i = 0; i < string.length; i++)
 {
 x = string.charCodeAt(i);
 output += hex_tab.charAt((x >>> 4) & 0x0F)
 +  hex_tab.charAt( x        & 0x0F);
 }
 return output;
 }

 private strToBigEndian(string) : Array {
 var output = new Array(string.length >> 2);
 for(var i = 0; i < output.length; i++)
 output[i] = 0;
 for(var i = 0; i < string.length * 8; i += 8)
 output[i>>5] |= (string.charCodeAt(i / 8) & 0xFF) << (24 - i % 32);
 return output;
 }

 private arrayToLittleEndianStr(input)
 {
 var output = "";
 for(var i = 0; i < input.length * 32; i += 8)
 output += String.fromCharCode((input[i>>5] >>> (24 - i % 32)) & 0xFF);
 return output;
 }

 private sha1OfBigEndian(arr: Array, len: Number) {

 /!* append padding *!/
 arr[len >> 5] |= 0x80 << (24 - len % 32);
 arr[((len + 64 >> 9) << 4) + 15] = len;

 var w = Array(80);
 var a =  1732584193;
 var b = -271733879;
 var c = -1732584194;
 var d =  271733878;
 var e = -1009589776;

 for(var i = 0; i < arr.length; i += 16)
 {
 var olda = a;
 var oldb = b;
 var oldc = c;
 var oldd = d;
 var olde = e;

 for(var j = 0; j < 80; j++)
 {
 if(j < 16) w[j] = arr[i + j];
 else w[j] = this.bit_rol(w[j-3] ^ w[j-8] ^ w[j-14] ^ w[j-16], 1);
 var t = this.safe_add(this.safe_add(this.bit_rol(a, 5), this.sha1_ft(j, b, c, d)),
 this.safe_add(this.safe_add(e, w[j]), this.sha1_kt(j)));
 e = d;
 d = c;
 c = this.bit_rol(b, 30);
 b = a;
 a = t;
 }

 a = this.safe_add(a, olda);
 b = this.safe_add(b, oldb);
 c = this.safe_add(c, oldc);
 d = this.safe_add(d, oldd);
 e = this.safe_add(e, olde);
 }
 return Array(a, b, c, d, e);

 }

 /!**
 * Add integers, wrapping at 2^32. This uses 16-bit operations internally
 * to work around bugs in some JS interpreters.
 *!/
 private safe_add(x, y)
 {
 var lsw = (x & 0xFFFF) + (y & 0xFFFF);
 var msw = (x >> 16) + (y >> 16) + (lsw >> 16);
 return (msw << 16) | (lsw & 0xFFFF);
 }

 /!**
 * Bitwise rotate a 32-bit number to the left.
 *!/
 private bit_rol(num, cnt)
 {
 return (num << cnt) | (num >>> (32 - cnt));
 }

 /!**
 * Perform the appropriate triplet combination function for the current
 * iteration
 *!/
 private sha1_ft(t, b, c, d)
 {
 if(t < 20) return (b & c) | ((~b) & d);
 if(t < 40) return b ^ c ^ d;
 if(t < 60) return (b & c) | (b & d) | (c & d);
 return b ^ c ^ d;
 }

 /!**
 * Determine the appropriate additive constant for the current iteration
 *!/
 private sha1_kt(t)
 {
 return (t < 20) ?  1518500249 : (t < 40) ?  1859775393 :
 (t < 60) ? -1894007588 : -899497514;
 }*/
