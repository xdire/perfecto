import {SHA1Encyption} from "./sha1.sys";

export class CryptoSystem {

  public hmacSHA1(string, hash){

    var s1 : SHA1Encyption = new SHA1Encyption();
    return s1.getHmacSHA1(string,hash).toString();

  }

}
