"use strict";
var sha1_sys_1 = require("./sha1.sys");
var CryptoSystem = (function () {
    function CryptoSystem() {
    }
    CryptoSystem.prototype.hmacSHA1 = function (string, hash) {
        var s1 = new sha1_sys_1.SHA1Encyption();
        return s1.getHmacSHA1(string, hash).toString();
    };
    return CryptoSystem;
}());
exports.CryptoSystem = CryptoSystem;
//# sourceMappingURL=crypto.sys.js.map