"use strict";
var InputAccumulator = (function () {
    function InputAccumulator() {
        this.chainLength = 0;
    }
    /** -------------------------------------------------------------------
   *
   *
   *  -------------------------------------------------------------------
   *
   */
    InputAccumulator.prototype.makeEmpty = function () {
        if (this.chainLength > 0) {
            while (this.pop() !== null) {
            }
            this.chainTop = null;
        }
    };
    /** -------------------------------------------------------------------
   *
   *
   *  -------------------------------------------------------------------
   *
   * @returns {any}
   */
    InputAccumulator.prototype.pop = function () {
        if (this.chainLength > 0) {
            var node = this.chainTop;
            this.chainTop = node.prev;
            this.chainLength--;
            return node;
        }
        return null;
    };
    /** -------------------------------------------------------------------
   *
   *
   *  -------------------------------------------------------------------
   *
   * @param value
   */
    InputAccumulator.prototype.push = function (value) {
        if (this.chainLength > 0) {
            var pnode = this.chainTop;
            this.chainTop = new InputAccumulatorNode(value, pnode);
        }
        else {
            this.chainTop = new InputAccumulatorNode(value, null);
        }
        this.chainLength++;
    };
    return InputAccumulator;
}());
exports.InputAccumulator = InputAccumulator;
var InputAccumulatorNode = (function () {
    function InputAccumulatorNode(value, prev) {
        this._nodeValue = value;
        this._prev = prev;
    }
    Object.defineProperty(InputAccumulatorNode.prototype, "nodeValue", {
        get: function () {
            return this._nodeValue;
        },
        set: function (value) {
            this._nodeValue = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(InputAccumulatorNode.prototype, "prev", {
        get: function () {
            return this._prev;
        },
        set: function (value) {
            this._prev = value;
        },
        enumerable: true,
        configurable: true
    });
    return InputAccumulatorNode;
}());
exports.InputAccumulatorNode = InputAccumulatorNode;
//# sourceMappingURL=inpaccu.sys.js.map