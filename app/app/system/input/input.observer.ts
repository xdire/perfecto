import {App} from "../app";
/** -----------------------------------------------------------------------------------------------
 *  Keyboard listener / Command listener
 *  -----------------------------------------------------------------------------------------------
 */
export class InputScannerObserver {

  private commandListeners: {[name:string]: any} = {};
  private inputListeners: {[name:string]: any} = {G1945$3:{}};
  private listenersAttached: {[name:string]: any} = {};

  // Define buffers
  private commandBufferTime: Uint32Array = new Uint32Array(64);
  private commandBuffer: Array<number> = new Array(64);
  private bufferSize: number = 0;

  private isCurrentInputCommand: boolean = false;

  private currentCommand: String;
  private currentCommandValue: String;

  private timeout: number;
  private toHandler: any;

  private listenersAmount: number = 0;

  private observerRunning: boolean = true;

  constructor() {

    window.addEventListener('keypress', (e: KeyboardEvent) => {
      this.listenForInput(e);
    });

  }

	/** --------------------------------------------------------------------------------------------
   *                      Listen for inputs from keyboard emulating devices
   *  --------------------------------------------------------------------------------------------
   * @param e
   */
  private listenForInput(e: KeyboardEvent) {

    if(this.observerRunning && this.listenersAmount > 0) {
      this.parseInput(e.charCode);
    }

  }

	/** --------------------------------------------------------------------------------------------
   *                                   Reset buffers
   *  --------------------------------------------------------------------------------------------
   */
  public reset() {
    this.bufferSize = 0;
    this.commandBuffer = [];
  }

	/** --------------------------------------------------------------------------------------------
   *                                  Reset Listeners
   *  --------------------------------------------------------------------------------------------
   */
  public resetListeners() {
    this.commandListeners = {};
    this.inputListeners = {G1945$3:{}};
    this.listenersAttached = {};
  }

	/** --------------------------------------------------------------------------------------------
   *                             Pause Observer for listening inputs
   *  --------------------------------------------------------------------------------------------
   */
  public pauseObserving() {
    this.observerRunning = false;
  }

	/** --------------------------------------------------------------------------------------------
   *                                  Resume observer work
   *  --------------------------------------------------------------------------------------------
   */
  public resumeObserving() {
    this.observerRunning = true;
  }

	/** --------------------------------------------------------------------------------------------
   *                             Helper function for check String guts
   *  --------------------------------------------------------------------------------------------
   *  @param string
   *  @returns {Array}
   */
  public strToChars(string: String) : Array<number> {

    var a = [];

    for(var i = 0; i < string.length; i++) {
      a.push(string.charCodeAt(i));
    }

    return a;

  }
	/** --------------------------------------------------------------------------------------------
   *
   *                                     Scanner input parser
   *
   *  --------------------------------------------------------------------------------------------
   *
   * @param char
   */
  private parseInput(char: number) {

    clearTimeout(this.toHandler);
    this.commandBuffer[this.bufferSize] = char;
    this.commandBufferTime[this.bufferSize++] = Date.now();

    // Every time set some time offset to check if input is done
    this.toHandler = setTimeout( () => {

      this.isCurrentInputCommand = false;

      var accu = 0;

      // Reduce CommandBuffer Timings to total time of all commands
      this.commandBufferTime.reduce(function (a,b) {
        if(b>0) {
          accu += b - a;
        }
        return b;
      });

      // Count mean time of input for make it recognizable
      // as human input or device input
      var mean: number = accu / this.bufferSize-1;

      // ! Device input is somewhere around 10-25ms
      // ! Human input is around 40+ms
      // -> (mean > 0) for not to catch single button clicks
      if(mean > 0 && mean < 30) {

        this.currentCommand = this.bufferToString(this.commandBuffer);

        // Check Command Listeners for current input
        if(this.isCurrentInputCommand) {

          var ca:String[] = this.currentCommand.split(":");
          var cm:String = ca[0];

          var lc: String = cm.toLowerCase().trim();

          if(this.commandListeners[lc]) {

            var cl = this.commandListeners[lc];

            for(var key in cl) {

              if(cl.hasOwnProperty(key)) {
                cl[key].run(cm);
              }

            }

          }

        }
        // Check for common input listeners for current input
        else {

          // Check listeners for ANY input
          if(this.inputListeners.hasOwnProperty("G1945$3")){

            var il = this.inputListeners["G1945$3"];

            for(var key in il) {

              if(il.hasOwnProperty(key)) {
                il[key].run(this.currentCommand.trim());
              }

            }

          }
          // Check listeners for specific catch phrase input
          else if(this.commandListeners.hasOwnProperty(this.currentCommand)) {

            var il = this.inputListeners[this.currentCommand];

            for(var key in il) {

              if(il.hasOwnProperty(key)) {
                il[key].run(this.currentCommand.trim());
              }

            }

          }

        }

      }

      this.commandBuffer = new Array(64);
      this.bufferSize = 0;
      this.commandBufferTime = new Uint32Array(64);

    }, 200);

  }


	/** --------------------------------------------------------------------------------------------
   *
   *                        Copy current buffer to string and
   *                        detecting is this a command or only
   *                        some random input string
   *
   *  --------------------------------------------------------------------------------------------
   *
   * @param buffer
   * @returns {string}
   */
  private bufferToString(buffer: Array) : String {

    var len: number = buffer.length;
    var arr: Array<String> = [];

    for(var i=0; i < len; i++) {

      if(buffer[i] > 0) {

        arr.push(String.fromCharCode(buffer[i]));

        if (i == 3) {

          if (buffer[0] == 67 && buffer[1] == 77 && buffer[2] == 68 && buffer[3] == 58) {
            this.isCurrentInputCommand = true;
          } else if (buffer[0] == 99 && buffer[1] == 109 && buffer[2] == 100 && buffer[3] == 58) {
            this.isCurrentInputCommand = true;
          }

        }

      }

    }

    if(this.isCurrentInputCommand){
      arr = arr.slice(4);
    }

    return arr.join("");

  }

  /** -----------------------------------------------------------------
   *
   *        Add Callback for specific type of command happen
   *
   *  -----------------------------------------------------------------
   *
   * @param command
   * @param callableFunction
   * @returns {String}
	 */
  public addCommandListener(command: String, callableFunction: any) : String {

    var guid: String = App.newGUID();

    if(typeof callableFunction === 'function') {

      if (!this.commandListeners.hasOwnProperty(command))
        this.commandListeners[command] = {};

      //this.commandListeners[command][guid] = callableFunction;
      this.commandListeners[command][guid] = new InputObserverEntity(callableFunction);
      this.listenersAttached[guid] = this.commandListeners[command];

      this.listenersAmount++;

      return guid;

    }
    else
      throw new Error("Listener need to be callable type");

  }

  /** -----------------------------------------------------------------
   *
   *        Add Callback for specific type of input happen
   *
   *  -----------------------------------------------------------------
   *
   *  Input parameter can be as any string as the unified pattern
   *
   *  [*any] - will add callback function to any scanning input happen
   *
   * @param input
   * @param callableFunction
   * @returns {String}
	 */
  public addInputListener(input: String, callableFunction: any) : String {

    var guid: String = App.newGUID();

    if(typeof callableFunction === 'function') {

      if(input == "*any"){
        input = "G1945$3";
      }
      else if (!this.inputListeners.hasOwnProperty(input))
        this.inputListeners[input] = {};

      //this.inputListeners[input][guid] = callableFunction;
      this.inputListeners[input][guid] = new InputObserverEntity(callableFunction);
      this.listenersAttached[guid] = this.inputListeners[input];

      this.listenersAmount++;
      return guid;

    }
    else
      throw new Error("Listener need to be callable type");

  }

	/** -----------------------------------------------------------------
   *
   *
   *  -----------------------------------------------------------------
   *
   * @param guid
   */
  public removeListenerWithGUID(guid: String[]) {

    if(typeof guid === "undefined") return;
    
    for(var i=0; i < guid.length; i++) {

      if (this.listenersAttached.hasOwnProperty(guid[i])) {

        var a: any = this.listenersAttached[guid[i]];
        delete a[guid[i]];
        delete this.listenersAttached[guid[i]];

      }

    }

  }

  /** -----------------------------------------------------------------
   *
   *
   *  -----------------------------------------------------------------
   *
   * @param guid
   */
  public pauseListenerWithGIUD(guid: String[]) {

    if(typeof guid === "undefined") return;

    for(var i=0; i < guid.length; i++) {

      if (this.listenersAttached.hasOwnProperty(guid[i])) {

        var a: any = this.listenersAttached[guid[i]];
        a[guid[i]].pause();

      }

    }

  }

  /** -----------------------------------------------------------------
   *
   *
   *  -----------------------------------------------------------------
   *
   * @param guid
   */
  public resumeListenerWithGIUD(guid: String[]) {

    if(typeof guid === "undefined") return;
    
    for(var i=0; i < guid.length; i++) {

      if (this.listenersAttached.hasOwnProperty(guid[i])) {

        var a: any = this.listenersAttached[guid[i]];
        a[guid[i]].resume();

      }

    }

  }

	/** -----------------------------------------------------------------
   *
   *
   *  -----------------------------------------------------------------
   */
  public pauseCurrentListeners() {

    var listeneres = Object.keys(this.listenersAttached);

    for(var i=0; i < listeneres.length; i++) {
      this.listenersAttached[listeneres[i]][listeneres[i]].pause();
    }

  }

	/** -----------------------------------------------------------------
   *
   *
   *  -----------------------------------------------------------------
   */
  public resumeAllListeners() {

    var listeneres = Object.keys(this.listenersAttached);

    for(var i=0; i < listeneres.length; i++) {
      this.listenersAttached[listeneres[i]][listeneres[i]].resume();
    }

  }

}

export class InputObserverEntity {

  public outdated: boolean = false;
  public status: boolean = true;
  public action: any = null;

	/**
   * Construct from callabale
   *
   * @param callableAction
   */
  constructor(callableAction: any) {
    this.action = callableAction;
  }

  /**
   * Run observer function or catch error on execution and mark it outdated
   *
   * @param input
   */
  public run(input: any = null) {

    if(this.status && !this.outdated) {

      try {

        this.action(input);

      } catch (e) {

        this.outdated = true;
        console.warn("Input observer extension found outdated module");
        console.warn(e);

      }
    }

  }

	/**
   * Pause current event listener
   */
  public pause() {
    this.status = false;
  }

	/**
   * Resume current event listener
   */
  public resume() {
    this.status = true;
  }

}
