"use strict";
var app_1 = require("../app");
/** -----------------------------------------------------------------------------------------------
 *  Keyboard listener / Command listener
 *  -----------------------------------------------------------------------------------------------
 */
var InputScannerObserver = (function () {
    function InputScannerObserver() {
        var _this = this;
        this.commandListeners = {};
        this.inputListeners = { G1945$3: {} };
        this.listenersAttached = {};
        // Define buffers
        this.commandBufferTime = new Uint32Array(64);
        this.commandBuffer = new Array(64);
        this.bufferSize = 0;
        this.isCurrentInputCommand = false;
        this.listenersAmount = 0;
        this.observerRunning = true;
        window.addEventListener('keypress', function (e) {
            _this.listenForInput(e);
        });
    }
    /** --------------------------------------------------------------------------------------------
   *                      Listen for inputs from keyboard emulating devices
   *  --------------------------------------------------------------------------------------------
   * @param e
   */
    InputScannerObserver.prototype.listenForInput = function (e) {
        if (this.observerRunning && this.listenersAmount > 0) {
            this.parseInput(e.charCode);
        }
    };
    /** --------------------------------------------------------------------------------------------
   *                                   Reset buffers
   *  --------------------------------------------------------------------------------------------
   */
    InputScannerObserver.prototype.reset = function () {
        this.bufferSize = 0;
        this.commandBuffer = [];
    };
    /** --------------------------------------------------------------------------------------------
   *                                  Reset Listeners
   *  --------------------------------------------------------------------------------------------
   */
    InputScannerObserver.prototype.resetListeners = function () {
        this.commandListeners = {};
        this.inputListeners = { G1945$3: {} };
        this.listenersAttached = {};
    };
    /** --------------------------------------------------------------------------------------------
   *                             Pause Observer for listening inputs
   *  --------------------------------------------------------------------------------------------
   */
    InputScannerObserver.prototype.pauseObserving = function () {
        this.observerRunning = false;
    };
    /** --------------------------------------------------------------------------------------------
   *                                  Resume observer work
   *  --------------------------------------------------------------------------------------------
   */
    InputScannerObserver.prototype.resumeObserving = function () {
        this.observerRunning = true;
    };
    /** --------------------------------------------------------------------------------------------
   *                             Helper function for check String guts
   *  --------------------------------------------------------------------------------------------
   *  @param string
   *  @returns {Array}
   */
    InputScannerObserver.prototype.strToChars = function (string) {
        var a = [];
        for (var i = 0; i < string.length; i++) {
            a.push(string.charCodeAt(i));
        }
        return a;
    };
    /** --------------------------------------------------------------------------------------------
   *
   *                                     Scanner input parser
   *
   *  --------------------------------------------------------------------------------------------
   *
   * @param char
   */
    InputScannerObserver.prototype.parseInput = function (char) {
        var _this = this;
        clearTimeout(this.toHandler);
        this.commandBuffer[this.bufferSize] = char;
        this.commandBufferTime[this.bufferSize++] = Date.now();
        // Every time set some time offset to check if input is done
        this.toHandler = setTimeout(function () {
            _this.isCurrentInputCommand = false;
            var accu = 0;
            // Reduce CommandBuffer Timings to total time of all commands
            _this.commandBufferTime.reduce(function (a, b) {
                if (b > 0) {
                    accu += b - a;
                }
                return b;
            });
            // Count mean time of input for make it recognizable
            // as human input or device input
            var mean = accu / _this.bufferSize - 1;
            // ! Device input is somewhere around 10-25ms
            // ! Human input is around 40+ms
            // -> (mean > 0) for not to catch single button clicks
            if (mean > 0 && mean < 30) {
                _this.currentCommand = _this.bufferToString(_this.commandBuffer);
                // Check Command Listeners for current input
                if (_this.isCurrentInputCommand) {
                    var ca = _this.currentCommand.split(":");
                    var cm = ca[0];
                    var lc = cm.toLowerCase().trim();
                    if (_this.commandListeners[lc]) {
                        var cl = _this.commandListeners[lc];
                        for (var key in cl) {
                            if (cl.hasOwnProperty(key)) {
                                cl[key].run(cm);
                            }
                        }
                    }
                }
                else {
                    // Check listeners for ANY input
                    if (_this.inputListeners.hasOwnProperty("G1945$3")) {
                        var il = _this.inputListeners["G1945$3"];
                        for (var key in il) {
                            if (il.hasOwnProperty(key)) {
                                il[key].run(_this.currentCommand.trim());
                            }
                        }
                    }
                    else if (_this.commandListeners.hasOwnProperty(_this.currentCommand)) {
                        var il = _this.inputListeners[_this.currentCommand];
                        for (var key in il) {
                            if (il.hasOwnProperty(key)) {
                                il[key].run(_this.currentCommand.trim());
                            }
                        }
                    }
                }
            }
            _this.commandBuffer = new Array(64);
            _this.bufferSize = 0;
            _this.commandBufferTime = new Uint32Array(64);
        }, 200);
    };
    /** --------------------------------------------------------------------------------------------
   *
   *                        Copy current buffer to string and
   *                        detecting is this a command or only
   *                        some random input string
   *
   *  --------------------------------------------------------------------------------------------
   *
   * @param buffer
   * @returns {string}
   */
    InputScannerObserver.prototype.bufferToString = function (buffer) {
        var len = buffer.length;
        var arr = [];
        for (var i = 0; i < len; i++) {
            if (buffer[i] > 0) {
                arr.push(String.fromCharCode(buffer[i]));
                if (i == 3) {
                    if (buffer[0] == 67 && buffer[1] == 77 && buffer[2] == 68 && buffer[3] == 58) {
                        this.isCurrentInputCommand = true;
                    }
                    else if (buffer[0] == 99 && buffer[1] == 109 && buffer[2] == 100 && buffer[3] == 58) {
                        this.isCurrentInputCommand = true;
                    }
                }
            }
        }
        if (this.isCurrentInputCommand) {
            arr = arr.slice(4);
        }
        return arr.join("");
    };
    /** -----------------------------------------------------------------
     *
     *        Add Callback for specific type of command happen
     *
     *  -----------------------------------------------------------------
     *
     * @param command
     * @param callableFunction
     * @returns {String}
       */
    InputScannerObserver.prototype.addCommandListener = function (command, callableFunction) {
        var guid = app_1.App.newGUID();
        if (typeof callableFunction === 'function') {
            if (!this.commandListeners.hasOwnProperty(command))
                this.commandListeners[command] = {};
            //this.commandListeners[command][guid] = callableFunction;
            this.commandListeners[command][guid] = new InputObserverEntity(callableFunction);
            this.listenersAttached[guid] = this.commandListeners[command];
            this.listenersAmount++;
            return guid;
        }
        else
            throw new Error("Listener need to be callable type");
    };
    /** -----------------------------------------------------------------
     *
     *        Add Callback for specific type of input happen
     *
     *  -----------------------------------------------------------------
     *
     *  Input parameter can be as any string as the unified pattern
     *
     *  [*any] - will add callback function to any scanning input happen
     *
     * @param input
     * @param callableFunction
     * @returns {String}
       */
    InputScannerObserver.prototype.addInputListener = function (input, callableFunction) {
        var guid = app_1.App.newGUID();
        if (typeof callableFunction === 'function') {
            if (input == "*any") {
                input = "G1945$3";
            }
            else if (!this.inputListeners.hasOwnProperty(input))
                this.inputListeners[input] = {};
            //this.inputListeners[input][guid] = callableFunction;
            this.inputListeners[input][guid] = new InputObserverEntity(callableFunction);
            this.listenersAttached[guid] = this.inputListeners[input];
            this.listenersAmount++;
            return guid;
        }
        else
            throw new Error("Listener need to be callable type");
    };
    /** -----------------------------------------------------------------
   *
   *
   *  -----------------------------------------------------------------
   *
   * @param guid
   */
    InputScannerObserver.prototype.removeListenerWithGUID = function (guid) {
        if (typeof guid === "undefined")
            return;
        for (var i = 0; i < guid.length; i++) {
            if (this.listenersAttached.hasOwnProperty(guid[i])) {
                var a = this.listenersAttached[guid[i]];
                delete a[guid[i]];
                delete this.listenersAttached[guid[i]];
            }
        }
    };
    /** -----------------------------------------------------------------
     *
     *
     *  -----------------------------------------------------------------
     *
     * @param guid
     */
    InputScannerObserver.prototype.pauseListenerWithGIUD = function (guid) {
        if (typeof guid === "undefined")
            return;
        for (var i = 0; i < guid.length; i++) {
            if (this.listenersAttached.hasOwnProperty(guid[i])) {
                var a = this.listenersAttached[guid[i]];
                a[guid[i]].pause();
            }
        }
    };
    /** -----------------------------------------------------------------
     *
     *
     *  -----------------------------------------------------------------
     *
     * @param guid
     */
    InputScannerObserver.prototype.resumeListenerWithGIUD = function (guid) {
        if (typeof guid === "undefined")
            return;
        for (var i = 0; i < guid.length; i++) {
            if (this.listenersAttached.hasOwnProperty(guid[i])) {
                var a = this.listenersAttached[guid[i]];
                a[guid[i]].resume();
            }
        }
    };
    /** -----------------------------------------------------------------
   *
   *
   *  -----------------------------------------------------------------
   */
    InputScannerObserver.prototype.pauseCurrentListeners = function () {
        var listeneres = Object.keys(this.listenersAttached);
        for (var i = 0; i < listeneres.length; i++) {
            this.listenersAttached[listeneres[i]][listeneres[i]].pause();
        }
    };
    /** -----------------------------------------------------------------
   *
   *
   *  -----------------------------------------------------------------
   */
    InputScannerObserver.prototype.resumeAllListeners = function () {
        var listeneres = Object.keys(this.listenersAttached);
        for (var i = 0; i < listeneres.length; i++) {
            this.listenersAttached[listeneres[i]][listeneres[i]].resume();
        }
    };
    return InputScannerObserver;
}());
exports.InputScannerObserver = InputScannerObserver;
var InputObserverEntity = (function () {
    /**
   * Construct from callabale
   *
   * @param callableAction
   */
    function InputObserverEntity(callableAction) {
        this.outdated = false;
        this.status = true;
        this.action = null;
        this.action = callableAction;
    }
    /**
     * Run observer function or catch error on execution and mark it outdated
     *
     * @param input
     */
    InputObserverEntity.prototype.run = function (input) {
        if (input === void 0) { input = null; }
        if (this.status && !this.outdated) {
            try {
                this.action(input);
            }
            catch (e) {
                this.outdated = true;
                console.warn("Input observer extension found outdated module");
                console.warn(e);
            }
        }
    };
    /**
   * Pause current event listener
   */
    InputObserverEntity.prototype.pause = function () {
        this.status = false;
    };
    /**
   * Resume current event listener
   */
    InputObserverEntity.prototype.resume = function () {
        this.status = true;
    };
    return InputObserverEntity;
}());
exports.InputObserverEntity = InputObserverEntity;
//# sourceMappingURL=input.observer.js.map