export class InputAccumulator {

  private chainTop: InputAccumulatorNode;

  private chainLength: number = 0;

	/** -------------------------------------------------------------------
   *
   *
   *  -------------------------------------------------------------------
   *
   */
  public makeEmpty() {

    if(this.chainLength > 0) {

      while(this.pop() !== null) {

      }

      this.chainTop = null;

    }

  }

	/** -------------------------------------------------------------------
   *
   *
   *  -------------------------------------------------------------------
   *
   * @returns {any}
   */
  public pop(): InputAccumulatorNode {

    if(this.chainLength > 0) {

      var node = this.chainTop;
      this.chainTop = node.prev;
      this.chainLength--;
      return node;

    }

    return null;

  }

	/** -------------------------------------------------------------------
   *
   *
   *  -------------------------------------------------------------------
   *
   * @param value
   */
  public push(value: string | number) {

    if(this.chainLength > 0) {

      var pnode = this.chainTop;
      this.chainTop = new InputAccumulatorNode(value,pnode);

    } else {

      this.chainTop = new InputAccumulatorNode(value,null);

    }

    this.chainLength++;

  }

}

export class InputAccumulatorNode {

  private _nodeValue: string | number;

  private _prev: InputAccumulatorNode;

  constructor(value, prev) {
    this._nodeValue = value;
    this._prev = prev;
  }

	get nodeValue():string|number {
    return this._nodeValue;
  }

  set nodeValue(value:string|number){
      this._nodeValue=value;
  }

  get prev():InputAccumulatorNode{
      return this._prev;
  }

  set prev(value:InputAccumulatorNode){
      this._prev=value;
  }

}
