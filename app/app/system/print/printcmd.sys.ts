
export class PrintSystemCommand {

  private _command: Object;

  private _type: number;

	/** ------------------------------------------------------------------
   *
   *
   *  ------------------------------------------------------------------
   *
   * @param type
   * @param data
   */
  constructor(type: number, data?: PrintSystemCommandDescription) {

    if(type == PrintSystemCommandType.Print) {
      this.createPrintCommand(data);
    }

    if(type == PrintSystemCommandType.PrinterList) {
      this.createListCommand()
    }

  }

	/** ------------------------------------------------------------------
   *
   *
   *  ------------------------------------------------------------------
   *
   * @param data
   */
  private createPrintCommand(data: PrintSystemCommandDescription) {

    if(data.hasOwnProperty('mode')
      && data.hasOwnProperty('type')
      && data.hasOwnProperty('format')
      && data.hasOwnProperty('value')) {

      if(data.id === null){
        delete data.id;
      }
      
      data["command"] = "print";
      this._command = data;
    }

  }

  public createPrintCommandExact() {

  }

	/** ------------------------------------------------------------------
   *
   *
   *  ------------------------------------------------------------------
   *
   */
  private createListCommand() {

  }

	/** ------------------------------------------------------------------
   *
   *
   *  ------------------------------------------------------------------
   *
   * @returns {string}
   */
  get command(): string {
    console.log(JSON.stringify(this._command));
    return JSON.stringify(this._command);
  }

	/** ------------------------------------------------------------------
   *
   *
   *  ------------------------------------------------------------------
   *
   * @returns {number}
   */
  get cmdtype():number{
      return this._type;
  }

}

export enum PrintSystemCommandType {
  Print,
  PrinterList
}

export interface PrintSystemCommandDescription {
  id?: number;
  mode: string;
  type: string;
  format: number;
  value: string;
  addon?: {};
}
