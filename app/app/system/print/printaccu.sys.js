"use strict";
var PrintingMessageAccumulator = (function () {
    function PrintingMessageAccumulator(timeoutLimit) {
        if (timeoutLimit === void 0) { timeoutLimit = 30000; }
        this.accumulator = {};
        this.watcherTime = 30000;
        this.watcherTime = timeoutLimit;
        this.startMaintenanceWatcher();
    }
    PrintingMessageAccumulator.prototype.setWaitingExecutor = function (waitingId, waitingExec) {
        var id = Number(waitingId);
        if (typeof waitingExec === "function")
            this.accumulator[id] = { f: waitingExec, t: (Date.now() / 1000) };
    };
    PrintingMessageAccumulator.prototype.getExecutor = function (id) {
        id = parseInt(id);
        if (this.accumulator.hasOwnProperty(id)) {
            this.accumulator[id].f();
            this.eraseRecord(id);
        }
    };
    PrintingMessageAccumulator.prototype.eraseRecord = function (id) {
        if (this.accumulator.hasOwnProperty(id)) {
            delete this.accumulator[id];
        }
    };
    PrintingMessageAccumulator.prototype.cleanPool = function () {
        var d = (Date.now() / 1000);
        for (var k in this.accumulator) {
            var e = this.accumulator[k];
            if ((d - e.t) > 30) {
                this.eraseRecord(k);
            }
        }
    };
    PrintingMessageAccumulator.prototype.stopMaintenanceWatcher = function () {
        if (this.cleanIV !== null) {
            clearInterval(this.cleanIV);
        }
    };
    PrintingMessageAccumulator.prototype.startMaintenanceWatcher = function () {
        var _this = this;
        this.cleanIV = setInterval(function () {
            _this.cleanPool();
        }, this.watcherTime);
    };
    return PrintingMessageAccumulator;
}());
exports.PrintingMessageAccumulator = PrintingMessageAccumulator;
//# sourceMappingURL=printaccu.sys.js.map