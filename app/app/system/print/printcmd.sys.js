"use strict";
var PrintSystemCommand = (function () {
    /** ------------------------------------------------------------------
   *
   *
   *  ------------------------------------------------------------------
   *
   * @param type
   * @param data
   */
    function PrintSystemCommand(type, data) {
        if (type == PrintSystemCommandType.Print) {
            this.createPrintCommand(data);
        }
        if (type == PrintSystemCommandType.PrinterList) {
            this.createListCommand();
        }
    }
    /** ------------------------------------------------------------------
   *
   *
   *  ------------------------------------------------------------------
   *
   * @param data
   */
    PrintSystemCommand.prototype.createPrintCommand = function (data) {
        if (data.hasOwnProperty('mode')
            && data.hasOwnProperty('type')
            && data.hasOwnProperty('format')
            && data.hasOwnProperty('value')) {
            if (data.id === null) {
                delete data.id;
            }
            data["command"] = "print";
            this._command = data;
        }
    };
    PrintSystemCommand.prototype.createPrintCommandExact = function () {
    };
    /** ------------------------------------------------------------------
   *
   *
   *  ------------------------------------------------------------------
   *
   */
    PrintSystemCommand.prototype.createListCommand = function () {
    };
    Object.defineProperty(PrintSystemCommand.prototype, "command", {
        /** ------------------------------------------------------------------
       *
       *
       *  ------------------------------------------------------------------
       *
       * @returns {string}
       */
        get: function () {
            console.log(JSON.stringify(this._command));
            return JSON.stringify(this._command);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PrintSystemCommand.prototype, "cmdtype", {
        /** ------------------------------------------------------------------
       *
       *
       *  ------------------------------------------------------------------
       *
       * @returns {number}
       */
        get: function () {
            return this._type;
        },
        enumerable: true,
        configurable: true
    });
    return PrintSystemCommand;
}());
exports.PrintSystemCommand = PrintSystemCommand;
(function (PrintSystemCommandType) {
    PrintSystemCommandType[PrintSystemCommandType["Print"] = 0] = "Print";
    PrintSystemCommandType[PrintSystemCommandType["PrinterList"] = 1] = "PrinterList";
})(exports.PrintSystemCommandType || (exports.PrintSystemCommandType = {}));
var PrintSystemCommandType = exports.PrintSystemCommandType;
//# sourceMappingURL=printcmd.sys.js.map