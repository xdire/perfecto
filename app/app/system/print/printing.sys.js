"use strict";
var printcmd_sys_1 = require("./printcmd.sys");
var printaccu_sys_1 = require("./printaccu.sys");
var PrintSystem = (function () {
    function PrintSystem() {
        this.driverPort = 9007;
        this.driverHost = "localhost";
        this.connected = false;
        this.connectionTryouts = 0;
        this.printingMA = new printaccu_sys_1.PrintingMessageAccumulator();
    }
    /** --------------------------------------------------------------------------
   *
   *  -------------------------------------------------------------------------- */
    PrintSystem.prototype.init = function () {
        this.driverEstablishConnection();
    };
    /** --------------------------------------------------------------------------
   *
   *
   *
   *  -------------------------------------------------------------------------- */
    PrintSystem.prototype.driverEstablishConnection = function () {
        var _this = this;
        if (!this.connected) {
            this.driverConnection = new WebSocket("ws://" + this.driverHost + ":" + this.driverPort);
            this.driverConnection.onopen = function (ev) {
                _this.connected = true;
                console.log("%c Printer connected ", 'background-color:#008080; color:#FFFFFF;');
            };
            this.driverConnection.onmessage = function (ev) {
                var answer = new PrintingServerAnswer(ev.data);
                if (answer.status === true) {
                    if (answer.id !== null) {
                        _this.printingMA.getExecutor(answer.id);
                    }
                }
            };
            this.driverConnection.onclose = function (ev) {
                _this.connected = false;
            };
        }
    };
    /** --------------------------------------------------------------------------
     *
     *
     *
     *  --------------------------------------------------------------------------
     *
     * @param code
     * @param opid
     * @param callback
     */
    PrintSystem.prototype.printBarCode128Z = function (code, opid, callback) {
        var _this = this;
        if (opid === void 0) { opid = null; }
        if (callback === void 0) { callback = null; }
        if (this.connected) {
            var cmd = new printcmd_sys_1.PrintSystemCommand(printcmd_sys_1.PrintSystemCommandType.Print, {
                mode: "zpl",
                type: "single",
                format: 128,
                value: code
            });
            try {
                this.driverConnection.send(cmd.command);
            }
            catch (e) {
                console.log("Error: trying to send message before connection to printer");
            }
            // -----------------------------------------------------
            if (opid !== null) {
                cmd["id"] = opid;
                if (typeof callback === "function") {
                    this.printingMA.setWaitingExecutor(opid, callback);
                }
            }
        }
        else {
            this.connectionTOIV = setInterval(function () {
                _this.connectionTryouts++;
                _this.driverEstablishConnection();
                if (_this.connected) {
                    var cmd = new printcmd_sys_1.PrintSystemCommand(printcmd_sys_1.PrintSystemCommandType.Print, {
                        mode: "zpl",
                        type: "single",
                        format: 128,
                        value: code
                    });
                    if (opid !== null) {
                        cmd["id"] = opid;
                        if (typeof callback === "function") {
                            _this.printingMA.setWaitingExecutor(opid, callback);
                        }
                    }
                    try {
                        _this.driverConnection.send(cmd.command);
                    }
                    catch (e) {
                        console.log("Error: trying to send message before connection to printer");
                    }
                    clearInterval(_this.connectionTOIV);
                }
                if (_this.connectionTryouts > 5) {
                    clearInterval(_this.connectionTOIV);
                }
            }, 2000);
        }
    };
    /** --------------------------------------------------------------------------
     *
     *
     *
     *  --------------------------------------------------------------------------
     *
     * @param code
     * @param addon
     * @param opid
     * @param callback
       */
    PrintSystem.prototype.printBarCode128ZAddon = function (code, addon, opid, callback) {
        var _this = this;
        if (opid === void 0) { opid = null; }
        if (callback === void 0) { callback = null; }
        if (this.connected) {
            var cmd = new printcmd_sys_1.PrintSystemCommand(printcmd_sys_1.PrintSystemCommandType.Print, {
                id: opid,
                mode: "zpl",
                type: "addon",
                format: 128,
                value: code,
                addon: addon
            });
            if (typeof callback === "function") {
                this.printingMA.setWaitingExecutor(opid, callback);
            }
            try {
                this.driverConnection.send(cmd.command);
            }
            catch (e) {
                console.log("Error: trying to send message before conenction to printer");
            }
        }
        else {
            this.connectionTOIV = setInterval(function () {
                _this.connectionTryouts++;
                _this.driverEstablishConnection();
                if (_this.connected) {
                    var cmd = new printcmd_sys_1.PrintSystemCommand(printcmd_sys_1.PrintSystemCommandType.Print, {
                        id: opid,
                        mode: "zpl",
                        type: "addon",
                        format: 128,
                        value: code,
                        addon: addon
                    });
                    // ---------------------------------------------------
                    if (typeof callback === "function") {
                        _this.printingMA.setWaitingExecutor(opid, callback);
                    }
                    try {
                        _this.driverConnection.send(cmd.command);
                    }
                    catch (e) {
                        console.log("Error: trying to send message before conenction to printer");
                    }
                    clearInterval(_this.connectionTOIV);
                }
                if (_this.connectionTryouts > 5) {
                    clearInterval(_this.connectionTOIV);
                }
            }, 2000);
        }
    };
    return PrintSystem;
}());
exports.PrintSystem = PrintSystem;
var PrintingServerAnswer = (function () {
    function PrintingServerAnswer(msg) {
        this.id = null;
        this.content = "";
        this.label = null;
        this.status = false;
        this.addon = {};
        this.assemble(msg);
    }
    PrintingServerAnswer.prototype.assemble = function (msg) {
        console.log(msg);
        try {
            var msg = JSON.parse(msg);
            if (msg.hasOwnProperty("id")) {
                this.id = parseInt(msg.id);
            }
            if (msg.hasOwnProperty("content")) {
                this.content = msg.content;
            }
            if (msg.hasOwnProperty("label")) {
                this.label = msg.label;
            }
            if (msg.hasOwnProperty("status")) {
                this.status = msg.status;
            }
            if (msg.hasOwnProperty("addon")) {
                for (var k in msg.addon) {
                    this.addon[k] = msg.addon[k];
                }
            }
        }
        catch (e) {
            console.log("Printing system can't dechipher message from driver");
            console.log(e);
        }
    };
    return PrintingServerAnswer;
}());
exports.PrintingServerAnswer = PrintingServerAnswer;
//# sourceMappingURL=printing.sys.js.map