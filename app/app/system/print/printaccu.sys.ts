export class PrintingMessageAccumulator {

  private accumulator: {[index: number]: {f:any,t:number}} = {};

  private cleanIV: any;

  private watcherTime: number = 30000;

  constructor(timeoutLimit = 30000) {
    this.watcherTime = timeoutLimit;
    this.startMaintenanceWatcher();
  }

  public setWaitingExecutor(waitingId: number, waitingExec: any) {

    var id = Number(waitingId);
    if(typeof waitingExec === "function")
      this.accumulator[id] = {f:waitingExec,t:(Date.now() / 1000)};

  }

  public getExecutor(id: any) {

    id = parseInt(id);

    if(this.accumulator.hasOwnProperty(id)) {
      this.accumulator[id].f();
      this.eraseRecord(id);
    }

  }

  public eraseRecord(id) {
    if(this.accumulator.hasOwnProperty(id)) {
      delete this.accumulator[id];
    }
  }

  public cleanPool() {

    var d: number = (Date.now() / 1000);

    for(var k in this.accumulator) {

      var e = this.accumulator[k];

      if((d - e.t) > 30){
        this.eraseRecord(k);
      }

    }

  }

  public stopMaintenanceWatcher() {
    if(this.cleanIV !== null){
      clearInterval(this.cleanIV);
    }
  }

  public startMaintenanceWatcher() {
    this.cleanIV = setInterval(() => {
      this.cleanPool();
    },this.watcherTime);
  }

}
