import {PrintSystemCommand, PrintSystemCommandType} from "./printcmd.sys";
import {PrintingMessageAccumulator} from "./printaccu.sys";

export class PrintSystem {

  public driverPort: number = 9007;

  private driverHost: string = "localhost";

  public printerList: Array;

  public currentPrinter: any;

  public printingMA: PrintingMessageAccumulator;

  private driverConnection: any;

  private connected: boolean = false;

  private connectionTryouts = 0;

  private connectionTOIV: any;

  constructor() {

    this.printingMA = new PrintingMessageAccumulator();

  }

	/** --------------------------------------------------------------------------
   *
   *  -------------------------------------------------------------------------- */
  public init() {

    this.driverEstablishConnection();

  }

	/** --------------------------------------------------------------------------
   *
   *
   *
   *  -------------------------------------------------------------------------- */
  private driverEstablishConnection() {

    if(!this.connected) {

      this.driverConnection = new WebSocket("ws://" + this.driverHost + ":" + this.driverPort);

      this.driverConnection.onopen = (ev) => {
        this.connected = true;
        console.log("%c Printer connected ",'background-color:#008080; color:#FFFFFF;');
      };

      this.driverConnection.onmessage = (ev) => {

        var answer = new PrintingServerAnswer(ev.data);
        if(answer.status === true){
          if(answer.id !== null){
            this.printingMA.getExecutor(answer.id);
          }
        }

      };

      this.driverConnection.onclose = (ev) => {
        this.connected = false;
      };

    }

  }

  /** --------------------------------------------------------------------------
   *
   *
   *
   *  --------------------------------------------------------------------------
   *
   * @param code
   * @param opid
   * @param callback
   */
  public printBarCode128Z(code: string, opid = null, callback = null) {

    if(this.connected) {

      var cmd = new PrintSystemCommand(PrintSystemCommandType.Print, {
        mode: "zpl",
        type: "single",
        format: 128,
        value: code
      });

      try {
        this.driverConnection.send(cmd.command);
      } catch (e) {
        console.log("Error: trying to send message before connection to printer");
      }
      // -----------------------------------------------------

      if(opid !== null) {

        cmd["id"] = opid;

        if(typeof callback === "function"){
          this.printingMA.setWaitingExecutor(opid, callback);
        }

      }

    } else {

      this.connectionTOIV = setInterval(() => {

        this.connectionTryouts++;

        this.driverEstablishConnection();

        if(this.connected) {

          var cmd = new PrintSystemCommand(PrintSystemCommandType.Print, {
            mode: "zpl",
            type: "single",
            format: 128,
            value: code
          });

          if(opid !== null) {

            cmd["id"] = opid;

            if(typeof callback === "function"){
              this.printingMA.setWaitingExecutor(opid, callback);
            }

          }

          try {
            this.driverConnection.send(cmd.command);
          } catch (e) {
            console.log("Error: trying to send message before connection to printer");
          }

          clearInterval(this.connectionTOIV);

        }

        if(this.connectionTryouts > 5) {
          clearInterval(this.connectionTOIV);
        }

      },2000);

    }

  }

  /** --------------------------------------------------------------------------
   *
   *
   *
   *  --------------------------------------------------------------------------
   *
   * @param code
   * @param addon
   * @param opid
   * @param callback
	 */
  public printBarCode128ZAddon(code: string, addon: Object, opid = null, callback = null) {

    if(this.connected) {

      var cmd = new PrintSystemCommand(PrintSystemCommandType.Print, {
        id: opid,
        mode: "zpl",
        type: "addon",
        format: 128,
        value: code,
        addon: addon
      });

      if(typeof callback === "function"){
        this.printingMA.setWaitingExecutor(opid, callback);
      }

      try {
        this.driverConnection.send(cmd.command);
      } catch (e) {
        console.log("Error: trying to send message before conenction to printer");
      }


    } else {

      this.connectionTOIV = setInterval(() => {

        this.connectionTryouts++;

        this.driverEstablishConnection();

        if(this.connected) {

          var cmd = new PrintSystemCommand(PrintSystemCommandType.Print, {
            id: opid,
            mode: "zpl",
            type: "addon",
            format: 128,
            value: code,
            addon: addon
          });

          // ---------------------------------------------------

          if(typeof callback === "function"){
            this.printingMA.setWaitingExecutor(opid, callback);
          }

          try {
            this.driverConnection.send(cmd.command);
          } catch (e) {
            console.log("Error: trying to send message before conenction to printer");
          }

          clearInterval(this.connectionTOIV);

        }

        if(this.connectionTryouts > 5) {
          clearInterval(this.connectionTOIV);
        }

      },2000);

    }

  }

}

export class PrintingServerAnswer {

  id: number = null;
  content: string = "";
  label: string = null;
  status: boolean = false;
  addon: Object = {};

  constructor(msg: string) {
    this.assemble(msg)
  }

  public assemble(msg: string) {

    console.log(msg);

    try {

      var msg = JSON.parse(msg);

      if(msg.hasOwnProperty("id")){
        this.id = parseInt(msg.id);
      }

      if(msg.hasOwnProperty("content")) {
        this.content = msg.content;
      }

      if(msg.hasOwnProperty("label")) {
        this.label = msg.label;
      }

      if(msg.hasOwnProperty("status")) {
        this.status = msg.status;
      }

      if(msg.hasOwnProperty("addon")) {

        for (var k in msg.addon) {
          this.addon[k] = msg.addon[k];
        }

      }

    }
    catch(e) {
      console.log("Printing system can't dechipher message from driver");
      console.log(e);
    }

  }
}
