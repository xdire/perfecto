"use strict";
var CacheService = (function () {
    function CacheService() {
        this.envCacheEnabled = false;
        this.errMessage = null;
        this._checkStorageAvailable();
    }
    /**
   * @returns Storage | null
   */
    CacheService.prototype.accessStorage = function () {
        return this.envCacheEnabled ? this.storage : null;
    };
    /**
   * Set parameter to storage
   *
   * @param key
   * @param value
   */
    CacheService.prototype.storeValue = function (key, value) {
        this.storage.setItem(key, value);
    };
    /**
   * Get parameter from storage
   *
   * @param key
   */
    CacheService.prototype.getValue = function (key) {
        return this.storage.getItem(key);
    };
    /**
   * Drop value in storage
   *
   * @param key
   */
    CacheService.prototype.dropValue = function (key) {
        this.storage.removeItem(key);
    };
    /**
   * Initialization function
   *
   * Checks availability of storage
   *
   * @private
   */
    CacheService.prototype._checkStorageAvailable = function () {
        // Check if local storage is exists on the machine
        try {
            if (typeof window.localStorage !== 'undefined') {
                this.storage = window.localStorage;
                this.storage.setItem("__testValue12345", "testValue");
                this.storage.getItem("__testValue12345");
                this.storage.removeItem("__testValue12345");
                this.envCacheEnabled = true;
            }
        }
        // Do something on storage error
        catch (err) {
            this.errMessage = err;
        }
    };
    return CacheService;
}());
exports.CacheService = CacheService;
//# sourceMappingURL=cache.service.js.map