export class CacheService {

  private envCacheEnabled: boolean = false;

  private errMessage: string = null;

  private storage;

  constructor() {

    this._checkStorageAvailable();

  }

	/**
   * @returns Storage | null
   */
  public accessStorage() {
      return this.envCacheEnabled ? this.storage:null;
  }

	/**
   * Set parameter to storage
   *
   * @param key
   * @param value
   */
  public storeValue(key: string, value: string | number) {
      this.storage.setItem(key,value);
  }

	/**
   * Get parameter from storage
   *
   * @param key
   */
  public getValue(key: string) {
      return this.storage.getItem(key);
  }

	/**
   * Drop value in storage
   *
   * @param key
   */
  public dropValue(key: string) {
      this.storage.removeItem(key);
  }

	/**
   * Initialization function
   *
   * Checks availability of storage
   *
   * @private
   */
  private _checkStorageAvailable() {

    // Check if local storage is exists on the machine
    try {

      if (typeof window.localStorage !== 'undefined') {

          this.storage = window.localStorage;

          this.storage.setItem("__testValue12345","testValue");
          this.storage.getItem("__testValue12345");
          this.storage.removeItem("__testValue12345");

          this.envCacheEnabled = true;

      }

    }
    // Do something on storage error
    catch (err) {

      this.errMessage = err;

    }

  }

}
