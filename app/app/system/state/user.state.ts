import {AuthApiUserEntity, AuthApiUserKeyEntity} from "../connection/connection-entities/authapi-entities";
import {CacheService} from "../cache/cache.service";

/**
 *  User state class
 *
 *  can be instantiated only with
 *  CacheService class instantiated
 *  and passed to constructor
 */
export class UserState {

  private storage: CacheService = null;

  private _timestamp: number = null;

  private _id: number = null;

  private _login: string = null;

  private _email: string = null;

  private _key: string = null;

  private _fisrtName: string = null;

  private _lastName: string = null;

  private _groupId: number = null;

  private _groupName: string = null;

  private _group2Id: number = null;

  private _group2Name: string = null;

  private _group3Id: number = null;

  private _group3Name: string = null;

  private _elderGroupId: number = 255;

	/**
   * Construct Object with cache service
   *
   * @param s
   */
  constructor(s: CacheService) {
      this.storage = s;
  }

	/**
   * Set object from local cache
   *
   * @private
   */
  public __fromCache() {

      this._id = parseInt(this.storage.getValue("_afapi_userid"));
      this._login = this.storage.getValue("_afapi_userlogin");
      this._email = this.storage.getValue("_afapi_useremail");
      this._key = this.storage.getValue("_afapi_userkey");
      this._fisrtName = this.storage.getValue("_afapi_userfirstn");
      this._lastName = this.storage.getValue("_afapi_userlastn");
      this._timestamp = parseInt(this.storage.getValue("_afapi_usertstmp"));

  }

	/**
   * Set object to loacl cache
   *
   * @private
   */
  public __toCache() {

    this.storage.storeValue("_afapi_userid",""+this._id);
    this.storage.storeValue("_afapi_userlogin",this._login == null?"":this._login);
    this.storage.storeValue("_afapi_useremail",this._email == null?"":this.email);
    this.storage.storeValue("_afapi_userkey",this._key);
    this.storage.storeValue("_afapi_userfirstn",this._fisrtName);
    this.storage.storeValue("_afapi_userlastn",this._lastName);
    this.storage.storeValue("_afapi_usertstmp",""+this._timestamp);

  }

	/**
   * Sets key information to UserState
   *
   * @param e
   * @private
   */
  public __keyInfoToCache(e: AuthApiUserKeyEntity) {

    if(this.id != e.user) {
      this._clearInformation();
    }

    this._key = e.key;
    this._id = e.user;

    this.storage.storeValue("_afapi_userkey",e.key);
    this.storage.storeValue("_afapi_userid",e.user);

  }

	/**
   * Set object from User Entity Object
   *
   * @param e
   * @private
   */
  public __fromUserEntity(e: AuthApiUserEntity) {

    if(e.hasOwnProperty("user"))
      this._id = e.user;
    if(e.hasOwnProperty("login"))
      this._login = e.login;
    if(e.hasOwnProperty("email"))
      this._email = e.email;
    if(e.hasOwnProperty("key"))
      this._key = e.key;
    if(e.hasOwnProperty("firstName"))
      this._fisrtName = e.firstName;
    if(e.hasOwnProperty("lastName"))
      this._lastName = e.lastName;
    
  }

	/**
   * Drop every value of object in the cache
   *
   * @private
   */
  public __clearCache() {

    this._clearInformation();
    this.storage.dropValue("_afapi_userid");
    this.storage.dropValue("_afapi_userlogin");
    this.storage.dropValue("_afapi_useremail");
    this.storage.dropValue("_afapi_userkey");
    this.storage.dropValue("_afapi_userfirstn");
    this.storage.dropValue("_afapi_userlastn");

  }

	/**
   * Clears all information about user in entity
   *
   * @private
   */
  private _clearInformation() {

    this._id = null;
    this._key = null;
    this._login = null;
    this._email = null;
    this._fisrtName = null;
    this._lastName = null;
    this._groupId = null;
    this._groupName = null;
    this._group2Id = null;
    this._group2Name = null;
    this._group3Id = null;
    this._group3Name = null;
    this._elderGroupId = 255;

  }

	get id():number {
    return this._id;
  }

  set id(value:number){
      this._id=value;
  }

  get login():string{
      return this._login;
  }

  set login(value:string){
      this._login=value;
  }

  get email():string{
      return this._email;
  }

  set email(value:string){
      this._email=value;
  }

  get key():string{
      return this._key;
  }

  set key(value:string){
      this._key=value;
  }

  get fisrtName():string{
      return this._fisrtName;
  }

  set fisrtName(value:string){
      this._fisrtName=value;
	  }

  get lastName():string{
      return this._lastName;
  }

  set lastName(value:string){
      this._lastName=value;
  }


	get groupId():number {
    return this._groupId;
  }

  set groupId(value:number){
      this._groupId=value;
      if(value < this._elderGroupId)
        this._elderGroupId = value;
	  }

  get groupName():string{
      return this._groupName;
	  }

  set groupName(value:string){
      this._groupName=value;
	  }

  get group2Id():number{
      return this._group2Id;
	  }

  set group2Id(value:number){
      this._group2Id=value;
      if(value < this._elderGroupId)
        this._elderGroupId = value;
	  }

  get group2Name():string{
      return this._group2Name;
	  }

  set group2Name(value:string){
      this._group2Name=value;
	  }

  get group3Id():number{
      return this._group3Id;
	  }

  set group3Id(value:number){
      this._group3Id=value;
      if(value < this._elderGroupId)
        this._elderGroupId = value;
	  }

  get group3Name():string{
      return this._group3Name;
	  }

  set group3Name(value:string){
      this._group3Name=value;
	  }

  get elderGroupId(){
    return this._elderGroupId;
  }

	get timestamp():number {
    return this._timestamp;
  }

  set timestamp(value:number){
      this._timestamp=value;
  }

}
