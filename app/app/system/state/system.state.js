"use strict";
var cache_service_1 = require("../cache/cache.service");
var user_state_1 = require("./user.state");
var app_1 = require("../app");
/** ----------------------------------
 *
 *  System State Manager
 *
 *  ----------------------------------
 *
 */
var SystemState = (function () {
    /** ----------------------------------
   *
   *    Construct System State service
   *
   *  ----------------------------------
   */
    function SystemState() {
        this._storageSrvc = null;
        this._userSrvc = null;
        this.notificator = null;
        this.debugMode = true;
        this.authorized = false;
        var storage = new cache_service_1.CacheService();
        if (storage.accessStorage() !== null) {
            // Init storage
            this._storageSrvc = storage;
            // Init User state service
            this._userSrvc = new user_state_1.UserState(storage);
            // Pull data from cache
            this._userSrvc.__fromCache();
        }
    }
    Object.defineProperty(SystemState.prototype, "storage", {
        /** ----------------------------------
       *
       *    Access to storage functions
       *
       *  ----------------------------------
       *
       *
       */
        /*public storage() {
            return this._storageSrvc;
        }*/
        get: function () {
            return this._storageSrvc;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(SystemState.prototype, "user", {
        /** ----------------------------------
       *
       *    Access to user functions
       *
       *  ----------------------------------
       *
       *
       */
        /*public user() {
            return this._userSrvc;
        }*/
        get: function () {
            return this._userSrvc;
        },
        enumerable: true,
        configurable: true
    });
    /** ----------------------------------
     *
     *    Show authorized flag
     *
     *  ----------------------------------
     *
     * @returns {boolean}
     */
    SystemState.prototype.isAuthorized = function () {
        return this.authorized;
    };
    /** ----------------------------------
   *
   *  System method
   *  set auth marker
   *
   *  ----------------------------------
   *
   * @private
   */
    SystemState.prototype.__setAuthorized = function () {
        this.authorized = true;
    };
    /** ----------------------------------
     *
     *  System method
     *  unset auth marker and clear cache
     *
     *  ----------------------------------
     *
     * @private
     */
    SystemState.prototype.__setUnauthorized = function () {
        this.authorized = false;
        this._userSrvc.__clearCache();
        this.clearCurrentUserVendorKey();
    };
    /** ----------------------------------
   *
   *  Set last loaded
   *  component name to cache
   *
   *  ----------------------------------
   *
   * @param componentName
   * @private
   */
    SystemState.prototype.__setLastLoadedComponent = function (componentName) {
        this._storageSrvc.storeValue("_afapi_lastcomp", componentName);
    };
    /** ----------------------------------
   *
   *  Get last loaded component
   *  from cache
   *
   *  ----------------------------------
   *
   * @private
   */
    SystemState.prototype.getLastLoadedComponent = function () {
        return this._storageSrvc.getValue("_afapi_lastcomp");
    };
    /** ----------------------------------
   *
   *  Set current user credentials to
   *  every place where it's need to
   *  be visible for all app
   *  components
   *
   *  ----------------------------------
   *
   * @private
   */
    SystemState.prototype.__setCurrentUserCredentials = function () {
        var _this = this;
        var menuComp = app_1.App.window.menuComponent;
        if (this._userSrvc.fisrtName == null) {
            var iv = setInterval(function () {
                if (_this._userSrvc.fisrtName != null) {
                    menuComp.menuTitleText = _this._userSrvc.fisrtName + " " + _this._userSrvc.lastName;
                    clearInterval(iv);
                }
            }, 2000);
        }
        else {
            menuComp.menuTitleText = this._userSrvc.fisrtName + " " + this._userSrvc.lastName;
        }
    };
    /** ----------------------------------
   *
   *  Set Vendor Key as Current Key
   *  which system methods can retrieve
   *  with getCurrentUserVendorKey()
   *
   *  ----------------------------------
   *
   * @param key
   * @private
   */
    SystemState.prototype.__setCurrentUserVendorKey = function (key) {
        this._storageSrvc.storeValue("_afapi_cvckey_id", key.authId);
        this._storageSrvc.storeValue("_afapi_cvckey_key", key.authKey);
        this._storageSrvc.storeValue("_afapi_cvckey_sec", key.authSecret);
    };
    /** ----------------------------------
   *
   *  Get Current Vendor Key which User
   *  wants to use for system calls
   *
   *  ----------------------------------
   *
   * @returns {UserApiVendorKeysEntity}
   */
    SystemState.prototype.getCurrentUserVendorKey = function () {
        var key = {
            authId: this._storageSrvc.getValue("_afapi_cvckey_id"),
            authKey: this._storageSrvc.getValue("_afapi_cvckey_key"),
            authSecret: this._storageSrvc.getValue("_afapi_cvckey_sec"),
            user: null,
            vendor: null
        };
        return key;
    };
    /** ----------------------------------
     *
     *  Clear value of vendor keys
     *
     *  ----------------------------------
     */
    SystemState.prototype.clearCurrentUserVendorKey = function () {
        this._storageSrvc.dropValue("_afapi_cvckey_id");
        this._storageSrvc.dropValue("_afapi_cvckey_key");
        this._storageSrvc.dropValue("_afapi_cvckey_sec");
    };
    return SystemState;
}());
exports.SystemState = SystemState;
//# sourceMappingURL=system.state.js.map