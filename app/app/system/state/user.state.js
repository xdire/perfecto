"use strict";
/**
 *  User state class
 *
 *  can be instantiated only with
 *  CacheService class instantiated
 *  and passed to constructor
 */
var UserState = (function () {
    /**
   * Construct Object with cache service
   *
   * @param s
   */
    function UserState(s) {
        this.storage = null;
        this._timestamp = null;
        this._id = null;
        this._login = null;
        this._email = null;
        this._key = null;
        this._fisrtName = null;
        this._lastName = null;
        this._groupId = null;
        this._groupName = null;
        this._group2Id = null;
        this._group2Name = null;
        this._group3Id = null;
        this._group3Name = null;
        this._elderGroupId = 255;
        this.storage = s;
    }
    /**
   * Set object from local cache
   *
   * @private
   */
    UserState.prototype.__fromCache = function () {
        this._id = parseInt(this.storage.getValue("_afapi_userid"));
        this._login = this.storage.getValue("_afapi_userlogin");
        this._email = this.storage.getValue("_afapi_useremail");
        this._key = this.storage.getValue("_afapi_userkey");
        this._fisrtName = this.storage.getValue("_afapi_userfirstn");
        this._lastName = this.storage.getValue("_afapi_userlastn");
        this._timestamp = parseInt(this.storage.getValue("_afapi_usertstmp"));
    };
    /**
   * Set object to loacl cache
   *
   * @private
   */
    UserState.prototype.__toCache = function () {
        this.storage.storeValue("_afapi_userid", "" + this._id);
        this.storage.storeValue("_afapi_userlogin", this._login == null ? "" : this._login);
        this.storage.storeValue("_afapi_useremail", this._email == null ? "" : this.email);
        this.storage.storeValue("_afapi_userkey", this._key);
        this.storage.storeValue("_afapi_userfirstn", this._fisrtName);
        this.storage.storeValue("_afapi_userlastn", this._lastName);
        this.storage.storeValue("_afapi_usertstmp", "" + this._timestamp);
    };
    /**
   * Sets key information to UserState
   *
   * @param e
   * @private
   */
    UserState.prototype.__keyInfoToCache = function (e) {
        if (this.id != e.user) {
            this._clearInformation();
        }
        this._key = e.key;
        this._id = e.user;
        this.storage.storeValue("_afapi_userkey", e.key);
        this.storage.storeValue("_afapi_userid", e.user);
    };
    /**
   * Set object from User Entity Object
   *
   * @param e
   * @private
   */
    UserState.prototype.__fromUserEntity = function (e) {
        if (e.hasOwnProperty("user"))
            this._id = e.user;
        if (e.hasOwnProperty("login"))
            this._login = e.login;
        if (e.hasOwnProperty("email"))
            this._email = e.email;
        if (e.hasOwnProperty("key"))
            this._key = e.key;
        if (e.hasOwnProperty("firstName"))
            this._fisrtName = e.firstName;
        if (e.hasOwnProperty("lastName"))
            this._lastName = e.lastName;
    };
    /**
   * Drop every value of object in the cache
   *
   * @private
   */
    UserState.prototype.__clearCache = function () {
        this._clearInformation();
        this.storage.dropValue("_afapi_userid");
        this.storage.dropValue("_afapi_userlogin");
        this.storage.dropValue("_afapi_useremail");
        this.storage.dropValue("_afapi_userkey");
        this.storage.dropValue("_afapi_userfirstn");
        this.storage.dropValue("_afapi_userlastn");
    };
    /**
   * Clears all information about user in entity
   *
   * @private
   */
    UserState.prototype._clearInformation = function () {
        this._id = null;
        this._key = null;
        this._login = null;
        this._email = null;
        this._fisrtName = null;
        this._lastName = null;
        this._groupId = null;
        this._groupName = null;
        this._group2Id = null;
        this._group2Name = null;
        this._group3Id = null;
        this._group3Name = null;
        this._elderGroupId = 255;
    };
    Object.defineProperty(UserState.prototype, "id", {
        get: function () {
            return this._id;
        },
        set: function (value) {
            this._id = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(UserState.prototype, "login", {
        get: function () {
            return this._login;
        },
        set: function (value) {
            this._login = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(UserState.prototype, "email", {
        get: function () {
            return this._email;
        },
        set: function (value) {
            this._email = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(UserState.prototype, "key", {
        get: function () {
            return this._key;
        },
        set: function (value) {
            this._key = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(UserState.prototype, "fisrtName", {
        get: function () {
            return this._fisrtName;
        },
        set: function (value) {
            this._fisrtName = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(UserState.prototype, "lastName", {
        get: function () {
            return this._lastName;
        },
        set: function (value) {
            this._lastName = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(UserState.prototype, "groupId", {
        get: function () {
            return this._groupId;
        },
        set: function (value) {
            this._groupId = value;
            if (value < this._elderGroupId)
                this._elderGroupId = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(UserState.prototype, "groupName", {
        get: function () {
            return this._groupName;
        },
        set: function (value) {
            this._groupName = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(UserState.prototype, "group2Id", {
        get: function () {
            return this._group2Id;
        },
        set: function (value) {
            this._group2Id = value;
            if (value < this._elderGroupId)
                this._elderGroupId = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(UserState.prototype, "group2Name", {
        get: function () {
            return this._group2Name;
        },
        set: function (value) {
            this._group2Name = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(UserState.prototype, "group3Id", {
        get: function () {
            return this._group3Id;
        },
        set: function (value) {
            this._group3Id = value;
            if (value < this._elderGroupId)
                this._elderGroupId = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(UserState.prototype, "group3Name", {
        get: function () {
            return this._group3Name;
        },
        set: function (value) {
            this._group3Name = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(UserState.prototype, "elderGroupId", {
        get: function () {
            return this._elderGroupId;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(UserState.prototype, "timestamp", {
        get: function () {
            return this._timestamp;
        },
        set: function (value) {
            this._timestamp = value;
        },
        enumerable: true,
        configurable: true
    });
    return UserState;
}());
exports.UserState = UserState;
//# sourceMappingURL=user.state.js.map