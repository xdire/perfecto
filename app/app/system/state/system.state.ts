import {CacheService} from "../cache/cache.service";
import {UserState} from "./user.state";
import {App} from "../app";
import {MenuComponent} from "../menu/menu.component";
import {UserApiVendorKeysEntity} from "../entities/system.entities";

/** ----------------------------------
 *
 *  System State Manager
 *
 *  ----------------------------------
 *
 */
export class SystemState {

  private _storageSrvc: CacheService = null;

  private _userSrvc: UserState = null;

  private notificator: any = null;

  private debugMode: boolean = true;

  private authorized: boolean = false;

	/** ----------------------------------
   *
   *    Construct System State service
   *
   *  ----------------------------------
   */
  constructor() {

    var storage: CacheService = new CacheService();

    if(storage.accessStorage() !== null) {
      // Init storage
      this._storageSrvc = storage;
      // Init User state service
      this._userSrvc = new UserState(storage);
      // Pull data from cache
      this._userSrvc.__fromCache();
    }

  }

	/** ----------------------------------
   *
   *    Access to storage functions
   *
   *  ----------------------------------
   *
   *
   */
  /*public storage() {
      return this._storageSrvc;
  }*/
  get storage():CacheService{
    return this._storageSrvc;
  }

	/** ----------------------------------
   *
   *    Access to user functions
   *
   *  ----------------------------------
   *
   *
   */
  /*public user() {
      return this._userSrvc;
  }*/
  get user():UserState{
      return this._userSrvc;
  }

  /** ----------------------------------
   *
   *    Show authorized flag
   *
   *  ----------------------------------
   *
   * @returns {boolean}
   */
  public isAuthorized() {
      return this.authorized;
  }

	/** ----------------------------------
   *
   *  System method
   *  set auth marker
   *
   *  ----------------------------------
   *
   * @private
   */
  public __setAuthorized() {
      this.authorized = true;
  }

  /** ----------------------------------
   *
   *  System method
   *  unset auth marker and clear cache
   *
   *  ----------------------------------
   *
   * @private
   */
  public __setUnauthorized() {
    this.authorized = false;
    this._userSrvc.__clearCache();
    this.clearCurrentUserVendorKey();
  }

	/** ----------------------------------
   *
   *  Set last loaded
   *  component name to cache
   *
   *  ----------------------------------
   *
   * @param componentName
   * @private
   */
  public __setLastLoadedComponent(componentName: string) {
    this._storageSrvc.storeValue("_afapi_lastcomp",componentName);
  }

	/** ----------------------------------
   *
   *  Get last loaded component
   *  from cache
   *
   *  ----------------------------------
   *
   * @private
   */
  public getLastLoadedComponent() {
    return this._storageSrvc.getValue("_afapi_lastcomp");
  }

	/** ----------------------------------
   *
   *  Set current user credentials to
   *  every place where it's need to
   *  be visible for all app
   *  components
   *
   *  ----------------------------------
   *
   * @private
   */
  public __setCurrentUserCredentials() {

    var menuComp:MenuComponent = App.window.menuComponent;

    if(this._userSrvc.fisrtName == null) {

      var iv = <number>setInterval(()=>{

        if(this._userSrvc.fisrtName != null) {

          menuComp.menuTitleText = this._userSrvc.fisrtName + " " + this._userSrvc.lastName;
          clearInterval(iv);
        }

      },2000);

    } else {
      menuComp.menuTitleText = this._userSrvc.fisrtName + " " + this._userSrvc.lastName;
    }

  }

	/** ----------------------------------
   *
   *  Set Vendor Key as Current Key
   *  which system methods can retrieve
   *  with getCurrentUserVendorKey()
   *
   *  ----------------------------------
   *
   * @param key
   * @private
   */
  public __setCurrentUserVendorKey(key: UserApiVendorKeysEntity) {
    this._storageSrvc.storeValue("_afapi_cvckey_id",key.authId);
    this._storageSrvc.storeValue("_afapi_cvckey_key",key.authKey);
    this._storageSrvc.storeValue("_afapi_cvckey_sec",key.authSecret);
  }

	/** ----------------------------------
   *
   *  Get Current Vendor Key which User
   *  wants to use for system calls
   *
   *  ----------------------------------
   *
   * @returns {UserApiVendorKeysEntity}
   */
  public getCurrentUserVendorKey() : UserApiVendorKeysEntity {

    var key: UserApiVendorKeysEntity = {
      authId: this._storageSrvc.getValue("_afapi_cvckey_id"),
      authKey: this._storageSrvc.getValue("_afapi_cvckey_key"),
      authSecret: this._storageSrvc.getValue("_afapi_cvckey_sec"),
      user: null,
      vendor: null
    };
    return key;

  }

  /** ----------------------------------
   *
   *  Clear value of vendor keys
   *
   *  ----------------------------------
   */
  public clearCurrentUserVendorKey() {
    this._storageSrvc.dropValue("_afapi_cvckey_id");
    this._storageSrvc.dropValue("_afapi_cvckey_key");
    this._storageSrvc.dropValue("_afapi_cvckey_sec");
  }

}
