import {Component} from '@angular/core';
import {ApiConnectorComponent} from "./system/connection/apiconnector.component";
import {AuthApiConnector} from "./system/connection/authapiconnector";
import {App} from "./system/app";

@Component({
    selector: 'app',
    template: '<window></window>'
})
export class AppComponent {

  constructor() {

    // LAUNCH TESTING FUNCTION ON START
    //this.mainTest();
    
  }

  /**
   *  MAIN TESTING FUNCTION
   */
  public mainTest() {

    var aapiconn: AuthApiConnector = ApiConnectorComponent.getAuthApiConnector();

    try {

      console.log(aapiconn.authorizeUser({email:"dire.one@gmail.com", login:"", pass: "Wrfof1983!", agent:"dtp"})
        .subscribe((posts) => {

          //console.log(posts);

          var user = App.state.user;
          user.__clearCache();
          user.__keyInfoToCache(posts);
          App.state.__setAuthorized();

          //console.log(user);

          if(user.login == null || user.email == null)
            this.userViewTest(posts.user, posts.key);

        }, function (error) {
          console.log(error);
        }));

    } catch (e) {
      console.log(e);
    }

  }

	/**
   *
   */

  public userViewTest(user: number, key: string) {

    try {

      var aapiconn: AuthApiConnector = ApiConnectorComponent.getAuthApiConnector();

      aapiconn.getUserInfo({user:user, key:key}).subscribe((creds) => {

        App.state.user.__fromUserEntity(creds);
        App.state.user.timestamp = Date.now();
        App.state.user.__toCache();

      }, function (error) {

        console.log(error);

      });

    } catch (e) {
      console.log(e);
    }

  }

  public saveViewTest() {

  }

}
