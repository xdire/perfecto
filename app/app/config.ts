import {SystemConfig,ApiRelationLink} from './system/interface/config.dec';
export var Config: SystemConfig = {
  systemApiList: {
    authApi: new ApiRelationLink('http://auth.afapi.net',80),
    //authApi: new ApiRelationLink("http://user", 80),
    itemApi: new ApiRelationLink('http://159.203.99.247', 80),
    //itemApi: new ApiRelationLink('http://itemapi', 80),
    wrhsApi: new ApiRelationLink('http://whs1.afapi.net', 80)
  },
  systemApiKeyType: "dtp"
};
