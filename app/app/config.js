"use strict";
var config_dec_1 = require('./system/interface/config.dec');
exports.Config = {
    systemApiList: {
        authApi: new config_dec_1.ApiRelationLink('http://auth.afapi.net', 80),
        //authApi: new ApiRelationLink("http://user", 80),
        itemApi: new config_dec_1.ApiRelationLink('http://159.203.99.247', 80),
        //itemApi: new ApiRelationLink('http://itemapi', 80),
        wrhsApi: new config_dec_1.ApiRelationLink('http://whs1.afapi.net', 80)
    },
    systemApiKeyType: "dtp"
};
//# sourceMappingURL=config.js.map