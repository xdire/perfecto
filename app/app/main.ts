import {bootstrap}    from '@angular/platform-browser-dynamic';
import {AppComponent} from './app.component';
import {App} from "./system/app";
import {LoginWindowComponent} from "./comp/login.wcomp";
import {DashboardComponent} from "./comp/dashboard/dashboard.wcomp";
import {UsersComponent} from "./comp/users/users.wcomp";
import {WarehouseComponent} from "./comp/warehouse/warehouse.wcomp";
bootstrap(AppComponent);

App.__init({
  "loginComponent": LoginWindowComponent,
  "dashboardComponent": DashboardComponent,
  "userComponent": UsersComponent,
  "warehouseComponent": WarehouseComponent
}).then((app) => {
  console.log("Application started, components start to loading");
  App.window.loadWindowComponent('loginComponent');
});
