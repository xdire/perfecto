"use strict";
var platform_browser_dynamic_1 = require('@angular/platform-browser-dynamic');
var app_component_1 = require('./app.component');
var app_1 = require("./system/app");
var login_wcomp_1 = require("./comp/login.wcomp");
var dashboard_wcomp_1 = require("./comp/dashboard/dashboard.wcomp");
var users_wcomp_1 = require("./comp/users/users.wcomp");
var warehouse_wcomp_1 = require("./comp/warehouse/warehouse.wcomp");
platform_browser_dynamic_1.bootstrap(app_component_1.AppComponent);
app_1.App.__init({
    "loginComponent": login_wcomp_1.LoginWindowComponent,
    "dashboardComponent": dashboard_wcomp_1.DashboardComponent,
    "userComponent": users_wcomp_1.UsersComponent,
    "warehouseComponent": warehouse_wcomp_1.WarehouseComponent
}).then(function (app) {
    console.log("Application started, components start to loading");
    app_1.App.window.loadWindowComponent('loginComponent');
});
//# sourceMappingURL=main.js.map